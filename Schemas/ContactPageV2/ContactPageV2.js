define("ContactPageV2", ["RightUtilities", "ProcessModuleUtilities"], function(RightUtilities, ProcessModuleUtilities) {
	return {
		entitySchemaName: "Contact",
		attributes: {
			"DeactivateUserActionVisible": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				value: false
			},
			"IsUserActive": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				value: false
			},
		},
		messages: {
			"DeactivateUserVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"CrocNotificationEmployee1Detail": {
				"schemaName": "CrocNotificationEmployee1Detail",
				"entitySchemaName": "CrocNotificationEmployee",
				"filter": {
					"detailColumn": "CrocContact",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocRole": {
				"9324e004-6072-41b4-a298-1e6c8f6708a9": {
					"uId": "9324e004-6072-41b4-a298-1e6c8f6708a9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "Account",
								"attributePath": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "57412fad-53e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocEmployeeRole": {
				"ca42bec6-9e20-4ef6-906c-23e88ab7cc6f": {
					"uId": "ca42bec6-9e20-4ef6-906c-23e88ab7cc6f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Account",
								"attributePath": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "57412fad-53e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {	
			onEntityInitialized: function() {
				this.callParent(arguments);
				this.isDeactivateUserActionVisible();
			},

			isDeactivateUserActionVisible: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanDeactivateUser"}, function(result) {
						this.checkIsUserActive(function(isActive) {
							this.$IsUserActive = isActive;
							this.$DeactivateUserActionVisible = this.$DeactivateUserActionVisible && this.$IsUserActive;
							this.sandbox.publish("DeactivateUserVisibleChanged", this.$DeactivateUserActionVisible, [this.sandbox.id]);
						}, this);
					this.$DeactivateUserActionVisible = result && !this.isNewMode();
				}, this);
			},
			
			checkIsUserActive: function(callback, scope) {
				var contactId = this.get("Id");
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "SysAdminUnit"});
				esq.addColumn("Active");
				esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Contact.Id", contactId));
				esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Active", true));
				esq.getEntityCollection(function(response) {
						if (response && response.success) {
							var result = response.collection;
							var isActive = (result.collection.length !== 0);
							callback.call(scope, isActive);
						}
					}, this);
			},
			
			callProcessDeactivateUser: function() {
				var contactId = this.get("Id");		
				var args = {
					sysProcessName: "CrocProcessUserDeactivate",
					parameters: {
						ContactId: contactId
					}
				};
				ProcessModuleUtilities.executeProcess(args);
			},			
			
			getActions: function() {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					Type: "Terrasoft.MenuSeparator",
                    Caption: ""	
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": { bindTo: "Resources.Strings.DeactivateUserActionCaption" },
					"Tag": "callProcessDeactivateUser",
					"Visible": {bindTo: "DeactivateUserActionVisible"}
				}));
				return actionMenuItems;
			},
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "Gender",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Language",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "insert",
				"name": "LOOKUP9196eeac-dd45-4d9c-8950-67d2a056c0df",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "ContactGeneralInfoBlock"
					},
					"bindTo": "CrocEmployeeRole",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ContactGeneralInfoBlock",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocNotificationEmployee1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "JobTabContainer",
				"values": {
					"order": 2
				}
			},
			{
				"operation": "merge",
				"name": "HistoryTab",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "merge",
				"name": "NotesAndFilesTab",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 7
				}
			},
			{
				"operation": "merge",
				"name": "EngagementTab",
				"values": {
					"order": 5
				}
			},
			{
				"operation": "remove",
				"name": "Owner"
			},
			{
				"operation": "remove",
				"name": "Tabb9250806TabLabel"
			},
			{
				"operation": "remove",
				"name": "Schema1Detail670f2222"
			},
			{
				"operation": "remove",
				"name": "Schema1Detaildc16a102"
			},
			{
				"operation": "remove",
				"name": "Files"
			},
			{
				"operation": "move",
				"name": "Age",
				"parentName": "ContactGeneralInfoBlock",
				"propertyName": "items",
				"index": 4
			}
		]/**SCHEMA_DIFF*/
	};
});
