define("CrocProgressReport1Page", [], function() {
	return {
		entitySchemaName: "CrocProgressReport",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"FileDetailV21ca29de1": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocProgressReportFile",
				"filter": {
					"detailColumn": "CrocProgressReport",
					"masterColumn": "Id"
				}
			},
			"CrocProgressReportRejectReasonDetail1":{
				"schemaName": "CrocProgressReportRejectReason1Detail",
				"entitySchemaName": "CrocProgressReportRejectReason",
				"filter": {
					"detailColumn": "CrocProgressReport",
					"masterColumn": "Id"
				}
			},
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			onEntityInitialized: function(){
				this.callParent(arguments);
				if (this.isNewMode()) {
					this.set("CrocName", "Отчет о проделанной работе " + Ext.Date.format(new Date(),'d.m.Y H:i'));
				}
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocReportDatee4babb22-6d07-472d-9fdb-adcfd81f12f4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocReportDate",
					"enabled": false
				},
				
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocReportStatus9576a14d-7e2d-4734-be69-8b98b835dcb6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocReportStatus",
					"enabled": false
				},
				
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab42416520TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab42416520TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FileDetailV21ca29de1",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab42416520TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocProgressReportRejectReasonDetail1",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab42416520TabLabel",
				"propertyName": "items",
				"index": 1
			}
		]/**SCHEMA_DIFF*/
	};
});
