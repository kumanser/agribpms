define("CrocQuoteRequest1Detail", ["ProcessModuleUtilities"], function(ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocQuoteRequest",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "CreateQuoteRequestButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onCreateQuoteRequestButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": {"bindTo": "Resources.Strings.CreateQuoteRequestButtonCaption"},
					"enabled": true,
					"visible": true
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
			getCopyRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,
			
			onCreateQuoteRequestButtonClick: function() {
				var errandId = this.get("MasterRecordId");
				var config = {
						sysProcessName: "CrocProcessQuoteRequest",
						parameters: {
							IdErrand: errandId,
							isDC: false
						}
					};
					ProcessModuleUtilities.executeProcess(config);
			}
		}
	};
});
