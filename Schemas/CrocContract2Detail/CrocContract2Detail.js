define("CrocContract2Detail", [], function() {
	return {
		entitySchemaName: "CrocContract",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: function() {
                return false;
            },
			  getAddTypedRecordButtonVisible: function() {
                return false;
            }
		}
	};
});
