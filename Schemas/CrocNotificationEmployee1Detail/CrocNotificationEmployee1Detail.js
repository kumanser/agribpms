define("CrocNotificationEmployee1Detail", [], function() {
	return {
		entitySchemaName: "CrocNotificationEmployee",
		attributes: {
			NotificationId: {
				"dataValueType": Terrasoft.DataValueType.GUID,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			filterName: {
				dataValueType: Terrasoft.DataValueType.TEXT,
				value: "NotificationIDFilter"
			}
		},
		messages: {
			"SelectNotificationSettindChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.subscribeNotificationSettings();
			},

			subscribeNotificationSettings: function () {
				var masterRecId = this.get("MasterRecordId");
				if (masterRecId) {
					this.sandbox.subscribe("SelectNotificationSettindChanged", this.onSelectNotificationSettindChangedSubscribe, this, [masterRecId]);
				}
			},

			onSelectNotificationSettindChangedSubscribe: function(val) {
				var masterRecId = this.get("MasterRecordId");
				if(val && masterRecId === val.masterRecId) {
					this.$NotificationId = val.settingId;
					this.reloadGridData();
				}
			},

			getFilters: function() {
				var filterName = this.$filterName;
				const filters = this.callParent(arguments);
				if(filters.contains(filterName)) {
					filters.removeByKey(filterName);
				}
				if(this.$NotificationId) {
					var filter = this.Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,"CrocEvent", this.$NotificationId, Terrasoft.DataValueType.GUID);
					filter.key = filterName
					filters.addItem(filter);
				}
				return filters;
			},
		}
	};
});
