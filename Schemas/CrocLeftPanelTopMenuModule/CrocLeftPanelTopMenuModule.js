 define("CrocLeftPanelTopMenuModule", ["LeftPanelTopMenuModule"],
    function() {
        Ext.define("Terrasoft.configuration.CrocLeftPanelTopMenuModuleViewModel", {
            alternateClassName: "Terrasoft.CrocLeftPanelTopMenuModuleViewModel",
            override: "Terrasoft.LeftPanelTopMenuModuleViewModel",

            getTopMenuConfig: function() {
                var esq = this.callParent(arguments);
                var index = esq.map(function(e) { return e.id; }).indexOf("menu-startprocess-button");
                if (index > -1) {
                    esq.splice(index, 1);
                }
                return esq;
            }
        });
    }
);