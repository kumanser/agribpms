define("CrocQuoteRequestGridButtonUtility", ["CrocDeliverySchedule", "CrocQuoteRequestGridButtonUtilityResources"],
		function(DeliveryScheduleSchema, resources) {

	Ext.define("Terrasoft.configuration.CrocQuoteRequestGridButtonUtility", {
		extend: "Terrasoft.BaseObject",
		alternateClassName: "Terrasoft.CrocQuoteRequestGridButtonUtility",

		Ext: null,
		Terrasoft: null,

		getQuoteColumnConfig: function() {
			var quotesColumn = DeliveryScheduleSchema.getColumnByName("CrocQuoteRequests");
			var buttonName = quotesColumn.name + "Button";
			var quoteButtonClickMethodName = "onCrocQuoteRequestsButtonClick";
			var editQuotesButtonConfig = this.getButtonConfig({
				buttonName: buttonName,
				columnName: quotesColumn.name,
				columnCaption: quotesColumn.caption,
				clickMethodName: quoteButtonClickMethodName
			});
			var quoteColumnConfig = {
				prepareResponseRow: function(row) {
					var quoteCount = row.get(this.detailColumn.name) || 0;
					var columnValueFormat = "";
					var quoteCountModulo = quoteCount % 10;
					var isNotTenthNumber = (((quoteCount % 100) - (quoteCount % 10)) / 10) !== 1;
					if (quoteCount === 0) {
						columnValueFormat = resources.localizableStrings.GridButtonAdd;
					} else if (quoteCountModulo === 1 && isNotTenthNumber) {
						columnValueFormat = resources.localizableStrings.GridButtonOneQuote;
					} else if (quoteCountModulo < 5 && isNotTenthNumber) {
						columnValueFormat = resources.localizableStrings.GridButtonLessThanFiveQuotes;
					} else {
						columnValueFormat = resources.localizableStrings.GridButtonFiveAndMoreQuotes;
					}
					row.set(quotesColumn.name, Ext.String.format(columnValueFormat, quoteCount));
				},
				detailColumn: {
					name: "QuotesCount",
					modifyQuery: function(esq) {
						esq.addAggregationSchemaColumn("[CrocVWQuoteRequst:CrocDeliverySchedule].Id",
								Terrasoft.AggregationType.COUNT, this.name);
					}
				},
				clickMethodNames: [quoteButtonClickMethodName],
				controlConfig: {
					"name": "CrocQuoteRequestsButtonsContainer",
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"wrapClass": ["quote-request-buttons-container"],
					"items": [editQuotesButtonConfig]
				}
			};
			return {
				name: quotesColumn.name,
				config: quoteColumnConfig
			};
		},

		getDisplayValue: function(value) {
			return value && (value.displayValue || value);
		},

		getButtonConfig: function(config) {
			var captionConfig = {
				bindTo: config.columnName
			};
			if (config.captionConverter) {
				captionConfig.bindConfig = {
					"converter": config.captionConverter
				};
			}
			return {
				"name": config.buttonName + "Container",
				"itemType": Terrasoft.ViewItemType.CONTAINER,
				"wrapClass": ["editable-grid-button"],
				"items": [
					{
						"name": config.buttonName,
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"caption": captionConfig,
						"click": {"bindTo": config.clickMethodName},
						"visible": {
							"bindTo": config.columnName,
							"bindConfig": {
								"converter": function(value) {
									return Boolean(value);
								}
							}
						},
						"markerValue": this.Ext.String.format("{0} {1}", config.columnName, config.columnCaption),
						"tag": config.columnName,
						"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT
					}
				]
			};
		},

		getCustomLinkColumns: function() {
			if (this.customLinkColumns) {
				return this.customLinkColumns;
			}
			var config = this.customLinkColumns = Ext.create("Terrasoft.Collection");
			var quoteColumnConfig = this.getQuoteColumnConfig();
			config.add(quoteColumnConfig.name, quoteColumnConfig.config);
			return config;
		},

		addGridDataColumns: function(esq) {
			var customColumns = this.getCustomLinkColumns();
			if (customColumns) {
				customColumns.each(function(customColumn) {
					if (customColumn.detailColumn && this.Ext.isFunction(customColumn.detailColumn.modifyQuery)) {
						customColumn.detailColumn.modifyQuery(esq);
					}
				}, this);
			}
		},

		prepareResponseCollectionItem: function(item, detailViewModel) {
			var customColumnButtons = this.getCustomLinkColumns();
			if (customColumnButtons) {
				customColumnButtons.each(function(customColumnButton) {
					if (customColumnButton.clickMethodNames) {
						this.Terrasoft.each(customColumnButton.clickMethodNames, function(clickMethodName) {
							var detailHandler = detailViewModel[clickMethodName];
							if (this.Ext.isFunction(detailHandler)) {
								item[clickMethodName] = function() {
									detailHandler.call(detailViewModel, item);
								};
							}
						}, this);
					}
					if (this.Ext.isFunction(customColumnButton.prepareResponseRow)) {
						customColumnButton.prepareResponseRow(item, detailViewModel);
					}
					item.parentDetailViewModel = detailViewModel;
					this.decorateItemLoadEntity(item, detailViewModel);
				}, this);
			}
		},

		decorateItemLoadEntity: function(item, detailViewModel) {
			if (item.get("IsLoadEntityMethodDecorated")) {
				return;
			}
			var util = this;
			var itemLoadEntity = item.loadEntity;
			item.loadEntity = function(primaryColumnValue, callback, scope) {
				itemLoadEntity.call(item, primaryColumnValue, function(loadedItem) {
					util.prepareResponseCollectionItem(loadedItem, detailViewModel);
					if (callback) { callback.call(scope || this); }
				}, this);
			};
			item.set("IsLoadEntityMethodDecorated", true);
		},

		getCellControlsConfig: function(entitySchemaColumn) {
			var columnName = entitySchemaColumn.name;
			var customColumnButtons = this.getCustomLinkColumns();
			var customColumnConfig = customColumnButtons.find(columnName);
			return (customColumnConfig && customColumnConfig.controlConfig) || null;
		},
	});

	var privateClassName = "Terrasoft.CrocQuoteRequestGridButtonUtility";
	var instance = null;
	var initialized = false;
	var instanceConfig = {};
	var util = {
		init: function(config, className) {
			if (initialized) {
				return;
			}
			if (config) {
				instanceConfig = config;
			}
			if (className) {
				privateClassName = className;
			}
		}
	};
	Object.defineProperty(util, "instance", {
		get: function() {
			if (initialized === false) {
				instance = Ext.create(privateClassName, instanceConfig);
				initialized = true;
			}
			return instance;
		}
	});
	return util;
});
