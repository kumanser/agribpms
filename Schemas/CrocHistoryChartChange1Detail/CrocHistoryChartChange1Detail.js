define("CrocHistoryChartChange1Detail", [], function() {
	return {
		entitySchemaName: "CrocHistoryChartChange",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
			getCopyRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,			
		}
	};
});
