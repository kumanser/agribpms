define("CrocNews1Page", ["RightUtilities", "ProcessModuleUtilities"], function(RightUtilities, ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocNews",
		messages: {
			"PublishNewsButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"UnPublishNewsButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"ReloadCrocNewsPage": {
				"mode": Terrasoft.MessageMode.BROADCAST,
				"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"RunPublishNewsProcess": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
		},
		attributes: {
			"CrocPublishRightsRoleSysSetting": {
				dataValueType: this.Terrasoft.DataValueType.GUID,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"CrocIsPublishRights": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"PublishButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"UnPublishButtonVisible": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"CrocPublicatedOn": {
				dependencies: [
					{
						columns: ["CrocPublicatedOn"],
						methodName: "onPublicatedOnChanged"
					}
				]
			},
			"IsModelItemsEnabled":{
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"value": true,
			}
		},
		methods: {

			init: function(callback, scope) {
				this.callParent(arguments);
				this.sandbox.subscribe("ReloadCrocNewsPage", this.reloadCrocNewsPage, this);
				this.sandbox.subscribe("RunPublishNewsProcess",  function(processName){
					this.runPublishProcess(processName);
				}, this, ["SectionModuleV2_InvoiceSectionV2"]);
			},

			canSeeNewsRecepient: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanSeeNewsRecepient"},
					function(result) {
						var recepients = this.$TabsCollection.get("RecepientsTabLabel");
						recepients.set("Visible", result);
					}, this);
			},

			canPublishNews: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanPublishNews"},
					function(result) {
						this.$CrocIsPublishRights = result;
						this.setPublishButtonsVisible();
					}, this);
			},

			reloadCrocNewsPage: function (args) {
				if(args && args.id === this.$Id) {
					this.reloadEntity();
				}
			},

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.canSeeNewsRecepient();
				this.canPublishNews();
			},

			setPublishButtonsVisible: function () {
				if (this.$CrocIsPublishRights && !this.isNewMode()) {
					this.$PublishButtonVisible = !this.$CrocPublicatedOn;
					this.$UnPublishButtonVisible = !this.$PublishButtonVisible && !!this.$CrocPublicatedOn;
				} else {
					this.$PublishButtonVisible = this.$UnPublishButtonVisible = false;
				}
				this.$IsModelItemsEnabled = (this.isNewMode() || !!(this.$PublishButtonVisible));
				this.sandbox.publish("UnPublishNewsButtonVisibleChanged", this.$UnPublishButtonVisible, ["SectionModuleV2_InvoiceSectionV2"]);
				this.sandbox.publish("PublishNewsButtonVisibleChanged", this.$PublishButtonVisible, ["SectionModuleV2_InvoiceSectionV2"]);
			},

			onPublicatedOnChanged: function() {
				this.setPublishButtonsVisible();
			},

			onPublishButtonClick: function() {
				this.runPublishProcess("CrocProcessPublishNews");
			},

			onUnPublishButtonClick: function() {
				this.runPublishProcess("CrocProcessUnPublishNews");
			},

			runPublishProcess: function(processName) {
				if (this.isEditMode()) {
					this.save({isSilent:true});
					this.reloadEntity();
				}
				var args = {
					sysProcessName: processName,
					parameters: {
						Id: this.$Id
					},
					scope: this
				};
				ProcessModuleUtilities.executeProcess(args);
			}
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "CardContentWrapper",
				"values": {
					"generator": "DisableControlsGenerator.generatePartial"
				}
			},
			{
				"operation": "insert",
				"name": "PublishButton",
				"values": {
					"tag": "onPublishButtonClick",
					"visible": {
						"bindTo": "PublishButtonVisible"
					},
					"itemType": 5,
					"style": "green",
					"caption": {
						"bindTo": "Resources.Strings.PublishButtonCaption"
					},
					"click": {
						"bindTo": "onPublishButtonClick"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "UnPublishButton",
				"values": {
					"tag": "onUnPublishButtonClick",
					"visible": {
						"bindTo": "UnPublishButtonVisible"
					},
					"itemType": 5,
					"style": "red",
					"caption": {
						"bindTo": "Resources.Strings.UnPublishButtonCaption"
					},
					"click": {
						"bindTo": "onUnPublishButtonClick"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocName28af9d3f-fab5-46ab-aba0-ecda5b9b0f2c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocName"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CreatedBy3e3b3047-ea20-4cf8-af9c-f624ebf0140a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CreatedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocPublicatedOn203ee54f-a1e5-485d-adfb-8f3208bf5e62",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocPublicatedOn",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "RecepientsTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.RecepientsTabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocNewsRecipient1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "RecepientsTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/,
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocNewsFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocNews"
				}
			},
			"CrocNewsRecipient1Detail": {
				"schemaName": "CrocNewsRecipient1Detail",
				"entitySchemaName": "CrocNewsRecipient",
				"filter": {
					"detailColumn": "CrocNews",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
	};
});
