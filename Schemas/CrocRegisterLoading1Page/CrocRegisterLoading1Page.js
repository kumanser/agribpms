define("CrocRegisterLoading1Page", [], function() {
	return {
		entitySchemaName: "CrocRegisterLoading",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocNumber60e34db4-3ff8-45d8-9c0e-fcc74629b3c1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocNumber",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocShipperacc19f39-9b3f-4305-a29f-628a52d410e1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocShipper",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocGU12724e4f92-8313-4bd8-b099-54fbefa5c028",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocGU12",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocCarrier29fe0cdf-3452-4c0e-bfde-7b3f3d2bd00a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocCarrier",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocTTN8289faf9-efc6-4d78-be10-62879cfb23d3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocTTN",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocWeightContainerTTNTon0320bc8e-cc0e-4c03-b587-b5a8f1d3b2e4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocWeightContainerTTNTon",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocShippingDatebb9a762c-9390-4b53-b34e-8c6421efd03e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocShippingDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocGrossWeightTon0b1eb262-fdb9-4003-90e3-c35af942565c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocGrossWeightTon",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocShipmentTime3403be4b-e0c3-4fc7-b1ba-d056c1d8993c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocShipmentTime",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "CrocGrossWeightTTNTona06fe2c1-b501-4f5f-9a81-5cc59c5a0795",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocGrossWeightTTNTon",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "CrocProduct65213d0b-b3e2-47b4-83de-0cf6ec5498d2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "CrocProduct",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "CrocGrainsDifferentColor2a9d49a4-4d38-40ee-bd70-e4570455b8c2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "CrocGrainsDifferentColor",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "CrocProductClass856b0221-762b-4670-9328-bf4c2aff8f99",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "CrocProductClass",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "CrocGlutendd5f32b3-8d84-4494-9bb9-b66532d5f742",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "CrocGluten",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "LOOKUPeff067cb-d008-4ef5-8db4-46644c380224",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Header"
					},
					"bindTo": "CrocPointLoading",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "insert",
				"name": "CrocNaturc3fb0785-6fe9-4b2e-aeb1-3ebd3fa2d63e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 7,
						"layoutName": "Header"
					},
					"bindTo": "CrocNatur",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 15
			},
			{
				"operation": "insert",
				"name": "CrocUnloadingPointec28331b-f986-4670-a05d-badcdb37d96f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "Header"
					},
					"bindTo": "CrocUnloadingPoint",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 16
			},
			{
				"operation": "insert",
				"name": "CrocDamaged49943fa6-bfce-49ba-89fc-a30839ce2e28",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 8,
						"layoutName": "Header"
					},
					"bindTo": "CrocDamaged",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 17
			},
			{
				"operation": "insert",
				"name": "CrocDepartureStation5526629c-d2dd-4fca-8b58-44375f05bae9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 9,
						"layoutName": "Header"
					},
					"bindTo": "CrocDepartureStation",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 18
			},
			{
				"operation": "insert",
				"name": "CrocProteine74d29cd-f0c9-462b-be53-cabacb595b47",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 9,
						"layoutName": "Header"
					},
					"bindTo": "CrocProtein",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 19
			},
			{
				"operation": "insert",
				"name": "CrocTransportType9d73df95-fc81-41d2-b301-8b63dcb601ca",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 10,
						"layoutName": "Header"
					},
					"bindTo": "CrocTransportType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 20
			},
			{
				"operation": "insert",
				"name": "CrocWeedImpurity24be4349-dcc5-43db-8961-37b3b7dc686f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 10,
						"layoutName": "Header"
					},
					"bindTo": "CrocWeedImpurity",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 21
			},
			{
				"operation": "insert",
				"name": "CrocTransportNumberb0592177-ed5d-4801-a968-d886d3245c31",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 11,
						"layoutName": "Header"
					},
					"bindTo": "CrocTransportNumber",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 22
			},
			{
				"operation": "insert",
				"name": "CrocChP22acc33e-f657-4214-8cc8-bb7a06b934a0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 11,
						"layoutName": "Header"
					},
					"bindTo": "CrocChP",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 23
			},
			{
				"operation": "insert",
				"name": "CrocTrailerNumberb078b402-dba1-427e-b9b7-990e2818f636",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 12,
						"layoutName": "Header"
					},
					"bindTo": "CrocTrailerNumber",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 24
			},
			{
				"operation": "insert",
				"name": "CrocIDK0728bb2a-ed2c-4f7b-b1dd-97b305b684a1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 12,
						"layoutName": "Header"
					},
					"bindTo": "CrocIDK",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 25
			},
			{
				"operation": "insert",
				"name": "CrocTransportOwner503c2c83-0d4c-459a-9fd9-6ec97f49bb4c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 13,
						"layoutName": "Header"
					},
					"bindTo": "CrocTransportOwner",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 26
			},
			{
				"operation": "insert",
				"name": "CrocHumidity4ab801af-3ce3-47ff-8e2d-28c1a324e501",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 13,
						"layoutName": "Header"
					},
					"bindTo": "CrocHumidity",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 27
			},
			{
				"operation": "insert",
				"name": "CrocDriverafaced3c-1b2b-47f4-9dd6-896ab8c592c3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 14,
						"layoutName": "Header"
					},
					"bindTo": "CrocDriver",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 28
			},
			{
				"operation": "insert",
				"name": "CrocBroken8a1430f3-eaa3-4537-bf49-c0b03356edf2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 14,
						"layoutName": "Header"
					},
					"bindTo": "CrocBroken",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 29
			},
			{
				"operation": "insert",
				"name": "CrocQuarantineCertificate6696086d-5f92-4a51-accf-5703070fddb1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 15,
						"layoutName": "Header"
					},
					"bindTo": "CrocQuarantineCertificate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 30
			},
			{
				"operation": "insert",
				"name": "CrocBugEatedabc8411b-a7b7-4159-869a-013b2779bcbb",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 15,
						"layoutName": "Header"
					},
					"bindTo": "CrocBugEated",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 31
			},
			{
				"operation": "insert",
				"name": "CrocForeignGrain660581e7-5a60-49cf-9a1d-fe996b7a9a37",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 16,
						"layoutName": "Header"
					},
					"bindTo": "CrocForeignGrain"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 32
			},
			{
				"operation": "insert",
				"name": "LOOKUP38778869-fe2b-4824-a924-e703ebcc64ad",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 16,
						"layoutName": "Header"
					},
					"bindTo": "CrocAssignment",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 33
			}
		]/**SCHEMA_DIFF*/
	};
});
