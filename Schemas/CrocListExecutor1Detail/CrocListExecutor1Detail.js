define("CrocListExecutor1Detail", ["LookupMultiAddMixin"], function() {
    return {
        mixins: {
            // Подключение миксина к схеме.
            LookupMultiAddMixin: "Terrasoft.LookupMultiAddMixin"
        },
        methods: {
            // Переопределение базового метода инициализации схемы.
            init: function() {
                this.callParent(arguments);
                //Инициализация миксина.
                this.mixins.LookupMultiAddMixin.init.call(this);
            },
            // Переопределение базового метода отображения кнопки добавления.
            getAddRecordButtonVisible: function() {
                //Отображать кнопку добавления если деталь развернута, даже если для детали не реализована страница редактирования.
                return this.getToolsVisible();
            },
            // Переопределение базового метода.
            // Обработчик события сохранения страницы редактирования детали.
            onCardSaved: function() {
                // Открывает справочное окно с множественным выбором записей.
                this.openLookupWithMultiSelect();
            },
            // Переопределение базового метода добавления записи на деталь.
            addRecord: function() {
                // Открывает справочное окно с множественным выбором записей.
                this.openLookupWithMultiSelect(true);
            },
            // Метод, возвращающий конфигурационный объект для справочного окна.
            getMultiSelectLookupConfig: function() {
                return {
                    // Корневая схема — [Заявки].
                    rootEntitySchemaName: "CrocRequest",
                    // Колонка корневой схемы.
                    rootColumnName: "CrocRequest",
                    // Связанная схема — [Контакты].
                    relatedEntitySchemaName: "Contact",
                    // Колонка связанной схемы.
                    relatedColumnName: "CrocContact"
                };
            }
        }
    };
});
