define("CrocRequest1Detail", [], function() {
	return {
		entitySchemaName: "CrocRequest",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: function() {
                return false;
            }
		}
	};
});
