define("AccountSectionV2", ["ProcessModuleUtilities"], function(ProcessModuleUtilities) {
	return {
		entitySchemaName: "Account",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		attributes: {
			"AllSelectedRowsCount": {
				"dataValueType": Terrasoft.DataValueType.INTEGER,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": 0
			},
			"CrocChangeAssignedRightsRole": {
				dataValueType: this.Terrasoft.DataValueType.GUID,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"CrocIsChangeAssignedRights": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"ChangeAssignedActionVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"AccreditationRequestActionVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"value": false
			},
			"AddNewUserRequestActionVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"value": false
			},
		},
		messages: {
			"AccreditationRequestVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"AddNewUserRequestVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		methods: {
			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("AccreditationRequestVisibleChanged", function(isVisible) {
					this.$AccreditationRequestActionVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
				this.sandbox.subscribe("AddNewUserRequestVisibleChanged", function(isVisible) {
					this.$AddNewUserRequestActionVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
			},
			
			isChangeAssignedActionEnabled: function() {
				var activeRowId = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				return activeRowId ? true : selectedRows ? (selectedRows.length > 0) : false;
			},
			
			initSysSettingsValue: function(settingCode, callback, scope) {
				this.Terrasoft.SysSettings.querySysSettingsItem(settingCode,
					function(value) {
						this.set(settingCode, value);
						if (this.Ext.isFunction(callback)) {
							callback.call(scope);
						}
					}, this);
			},
			
			init: function() {
				this.callParent(arguments);
				this.Terrasoft.chain(
					function(next) {
						this.initSysSettingsValue("CrocChangeAssignedRightsRole", function() {
							next();
						}, this);
					},
					function() {
						this.getChangeRights(function(isChangeRights) {
							this.$CrocIsChangeAssignedRights = isChangeRights;
							this.$ChangeAssignedActionVisible = this.$CrocIsChangeAssignedRights;
						}, this);
					}, this);
			},
			
			getChangeRights: function(callback, scope) {
				var currentUser = Terrasoft.SysValue.CURRENT_USER.value;
				var exporter = this.$CrocChangeAssignedRightsRole.value;
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "SysUserInRole"});
				esq.addColumn("SysRole");
				esq.addColumn("SysUser");
				esq.filters.add("SysUser", Terrasoft.createColumnFilterWithParameter(
					Terrasoft.ComparisonType.EQUAL, "SysUser", currentUser));
				esq.filters.add("SysRole", Terrasoft.createColumnFilterWithParameter(
					Terrasoft.ComparisonType.EQUAL, "SysRole", exporter));
				esq.getEntityCollection(function(response) {
					if (response && response.success) {
						var result = response.collection;
						var isExporter = (result.collection.length !== 0);
						callback.call(scope, isExporter);
					}
				}, this);
			},
			
			callProcessChangeAssignedEmployee: function() {		
				var activeRowId = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				var accountId = "";
				accountId = (selectedRows.length) ? selectedRows.join(';') : (activeRowId + ";");	
					var args = {
					sysProcessName: "CrocProcessChangeAssignedEmployee",
					parameters: {
						StringAccountId: accountId
					}
				};
				ProcessModuleUtilities.executeProcess(args);
				this.unSetMultiSelect();
			},

			createSelectAllButton: function() {
				return this.getButtonMenuItem({
					"Caption": {"bindTo": "Resources.Strings.SelectAllButtonCaption"},
					"Click": {"bindTo": "selectAllItems"},
					"Visible": {"bindTo": "isSelectAllModeVisible"},
					"IsEnabledForSelectedAll": true
				});
			},

			onSelectedRowsChange: function() {
				if (this.get("MultiSelect")) {
					if (this.get("SelectAll")) {
						var selectedRows = this.get("SelectedRows");
						if (this.$AllSelectedRowsCount < selectedRows.length) this.$AllSelectedRowsCount = selectedRows.length
						else this.set("SelectAll", false);
					}
				} else this.set("SelectAll", false);
			},

			unSelectRecords: function() {
				this.callParent(arguments);
				this.set("SelectAll", false);
			},

			selectAllItems: function () {
				this.set("IsPageable", false);
				this.set("RowCount", -1);
				this.set("SelectAll", true);
				this.getGridData().clear();
				this.reloadGridData();
			},

			onGridDataLoaded: function(response) {
				this.callParent(arguments);
				if (this.get("SelectAll")) {
					if (!this.get("MultiSelect")) {
						this.setMultiSelect();
					}
					this.set('SelectedRows', response.collection.collection.keys);
				}
			},

			unSetMultiSelect: function() {
				this.callParent(arguments);
				this.set("MultiSelect", false);
			},

			getSectionActions: function () {
				var actionMenuItems = this.callParent(arguments);
				
				actionMenuItems.addItem(this.getButtonMenuItem({
                    Type: "Terrasoft.MenuSeparator",
                    Caption: ""
                }));
				actionMenuItems.addItem(this.getButtonMenuItem({
                    "Enabled": {bindTo: "isChangeAssignedActionEnabled"},
					"Visible": {bindTo: "ChangeAssignedActionVisible"},
					"Caption" : {bindTo: "Resources.Strings.ChangeAssignedEmployeeActionCaption"},
					"Click": {bindTo: "callProcessChangeAssignedEmployee"},
					"IsEnabledForSelectedAll": true
                }));
				return actionMenuItems;
			}
			
		}
	};
});
