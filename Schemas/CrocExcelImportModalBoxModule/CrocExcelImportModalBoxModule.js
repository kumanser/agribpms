 define("CrocExcelImportModalBoxModule", ["ModalBox", "BaseSchemaModuleV2"],
    function(ModalBox) {
        Ext.define("Terrasoft.configuration.CrocExcelImportModalBoxModule", {
            extend: "Terrasoft.BaseSchemaModule",
            alternateClassName: "Terrasoft.CrocExcelImportModalBoxModule",
            generateViewContainerId: false,
            /**
             * @inheritDoc Terrasoft.BaseSchemaModule#initSchemaName
             * @overridden
             */
            initSchemaName: function() {
                this.schemaName = "CrocExcelImportModalBoxPage";
            },
            createViewModel: function(viewModelClass) {
                var viewModel = this.callParent(arguments);
                var param = this.parameters;
                if(param)
                {
                    viewModel.set("modalWorkParameter", param.workParam);
                }
                return viewModel;
            },

            /**
             * @inheritDoc Terrasoft.BaseSchemaModule#initHistoryState
             * @overridden
             */
            initHistoryState: Terrasoft.emptyFn
        });
        return Terrasoft.CrocExcelImportModalBoxModule;
    });