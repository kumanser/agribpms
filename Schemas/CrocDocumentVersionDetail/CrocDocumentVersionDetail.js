define("CrocDocumentVersionDetail", ["FileDownloader"], function() {
	return {
		entitySchemaName: "CrocDocumentVersion",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "remove",
				"name": "AddRecordButton"
			},
		]/**SCHEMA_DIFF*/,
		methods: {

			sortColumn: this.Terrasoft.emptyFn,

			getGridDataColumns: function() {
				var gridDataColumns = this.callParent(arguments);
				gridDataColumns["CrocVersion"] = gridDataColumns["CrocVersion"] || {path: "CrocVersion"};
				return gridDataColumns;
			},

			initQuerySorting: function(esq) {
				const sortedColumn = esq.columns.find("CrocVersion");
				if (!sortedColumn) {
					return;
				}
				sortedColumn.orderPosition = 0;
				sortedColumn.orderDirection = Terrasoft.OrderDirection.DESC;
			},

			addColumnLink: function(item, column) {
				const columnPath = column.columnPath;
				if (columnPath === item.primaryDisplayColumnName) {
					const linkConfig = this.getColumnLinkConfig(item);
					item["on" + columnPath + "LinkClick"] = function() {
						const value = this.get(columnPath);
						return {
							caption: value,
							target: linkConfig.target,
							title: value,
							url: linkConfig.link
						};
					};
				} else {
					this.callParent(arguments);
				}
			},

			linkClicked: Terrasoft.emptyFn,

			setGridRowValueFromEntity: function(activeRow, entity, columnName) {
				this.callParent(arguments);
				this.addColumnLink(activeRow, activeRow.columns[columnName]);
			},

			getColumnLinkConfig: function(item) {
				this.isFile = true;
				const id = item.get("Id");
				const name = item.get("Name");
				let target = "_self";
				const click = null;
				let link;
				link = Terrasoft.FileDownloader.getFileLink(this.entitySchema.uId, id);
				var config =  {
					target: target,
					link: link,
					click: click
				};
				config.link = this.getS3FileLink(this.entitySchema.name, id);
				return config;
			},

			getS3FileLink: function(entitySchema, fileId) {
				var urlPattern = "{0}/rest/FilesS3Service/GetS3File/{1}/{2}/{3}/{4}";
				var workspaceBaseUrl = Terrasoft.utils.uri.getConfigurationWebServiceBaseUrl();
				var url = Ext.String.format(urlPattern, workspaceBaseUrl, entitySchema, fileId, "CrocData", "CrocS3Link");
				return url;
			}

		}
	};
});
