define("CrocVwContractSection", ["ConfigurationEnums"],
	function(ConfigurationEnums) {
		return {
			entitySchemaName: "CrocVwContract",
			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "merge",
					"name": "DataGrid",
					"values": {
						"hierarchical": true,
						"hierarchicalColumnName": "CrocParent"
					}
				},
				{
					"operation": "merge",
					"name": "SeparateModeAddRecordButton",
					"values": {
						"enabled": false
					}
				},
				{
					"operation": "merge",
					"name": "SeparateModeActionsButton",
					"values": {
						"enabled": false
					}
				},
				{
					"operation": "remove",
					"name": "DataGridActiveRowOpenAction"
				},
				{
					"operation": "remove",
					"name": "DataGridActiveRowCopyAction"
				},
				{
					"operation": "remove",
					"name": "DataGridActiveRowDeleteAction"
				},
				{
					"operation": "remove",
					"name": "ProcessEntryPointGridRowButton"
				},
				{
					"operation": "remove",
					"name": "DataGridActiveRowPrintAction"
				},
				{
					"operation": "remove",
					"name": "DataGridRunProcessAction"
				}
			]/**SCHEMA_DIFF*/,
			messages: {},
			attributes: {},
			methods: {

				editRecord : Terrasoft.emptyFn,

				getGridDataColumns: function() {
					var baseGridDataColumns = this.callParent(arguments);
					var gridDataColumns = {
						"CrocParent": {path: "CrocParent"},
						"CrocAccount": {path: "CrocAccount"},
						"CrocContract": {path: "CrocContract"},
						"CrocSubContract": {path: "CrocSubContract"}
					};
					return Ext.apply(baseGridDataColumns, gridDataColumns);
				},

				getColumnLinkConfig: function(item) {
					const id = item.get("Id");
					let accountId = (item.$CrocAccount) ? item.$CrocAccount.value : null;
					let contractId = (item.$CrocContract) ? item.$CrocContract.value : null;
					let subContractId = (item.$CrocSubContract) ? item.$CrocSubContract.value : null;
					let schemaName = "";
					if (subContractId) schemaName = "CrocContract2Page";
					else if (contractId) schemaName = "CrocContract1Page";
					else if (accountId) schemaName = "AccountPageV2";
					let target = "_self";
					const click = null;
					let link = Terrasoft.getFormattedString("{0}/Nui/ViewModule.aspx#CardModuleV2/{1}/edit/{2}", this.Terrasoft.workspaceBaseUrl, schemaName, id);
					var config =  {
						target: target,
						link: link,
						click: click
					};
					return config;
				},

				addColumnLink: function(item, column) {
					const columnPath = column.columnPath;
					if (columnPath === item.primaryDisplayColumnName) {
						const linkConfig = this.getColumnLinkConfig(item);
						item["on" + columnPath + "LinkClick"] = function() {
							const value = this.get(columnPath);
							return {
								caption: value,
								target: linkConfig.target,
								title: value,
								url: linkConfig.link
							};
						};
					} else {
						this.callParent(arguments);
					}
				},

				linkClicked: function(href, columnName) {
					if (columnName !== this.primaryDisplayColumnName) this.callParent(arguments);
				}

			}
		};
	});
