define("ContactSectionV2", [], function() {
	return {
		entitySchemaName: "Contact",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		attributes: {
			"DeactivateUserActionVisible": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				value: false
			}
		},
		messages: {
			"DeactivateUserVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("DeactivateUserVisibleChanged", function(isVisible) {
					this.$DeactivateUserActionVisible = isVisible;
					console.log(isVisible);
				}, this, [this.sandbox.id + "_CardModuleV2"]);
			},
		}
	};
});
