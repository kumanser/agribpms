define("CrocAcceptanceStop1Page", [], function() {
	return {
		entitySchemaName: "CrocAcceptanceStop",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocPort": {
				"37185dcc-a325-4914-96c7-8e4e5f0daf9e": {
					"uId": "37185dcc-a325-4914-96c7-8e4e5f0daf9e",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "CrocTerminal",
							"attributePath": "CrocAccount"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTerminal"
							}
						}
					]
				}
			},
			"NotesAndFilesTab": {
				"bc199cde-9b8d-453c-951d-83cd50e5f4e9": {
					"uId": "bc199cde-9b8d-453c-951d-83cd50e5f4e9",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 0,
								"value": 1,
								"dataValueType": 4
							},
							"rightExpression": {
								"type": 0,
								"value": 2,
								"dataValueType": 4
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTabGroup7ea9ed1a",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabGroup7ea9ed1aGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTabGridLayout08ac80de",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "NotesAndFilesTabGroup7ea9ed1a",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocPort7b2ad76c-828b-471e-94f2-563f9b40d43a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "NotesAndFilesTabGridLayout08ac80de"
					},
					"bindTo": "CrocPort",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "NotesAndFilesTabGridLayout08ac80de",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocTerminal57fbdf12-6395-4e24-8eea-1b8dc50438e8",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "NotesAndFilesTabGridLayout08ac80de"
					},
					"bindTo": "CrocTerminal",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "NotesAndFilesTabGridLayout08ac80de",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocReason886c3c25-f214-4861-a1fc-aaf6307925e9",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "NotesAndFilesTabGridLayout08ac80de"
					},
					"bindTo": "CrocReason",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "NotesAndFilesTabGridLayout08ac80de",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocStopDate56723418-2ff5-4e4f-b7ec-1ad640e8b91d",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "NotesAndFilesTabGridLayout08ac80de"
					},
					"bindTo": "CrocStopDate",
					"enabled": false
				},
				"parentName": "NotesAndFilesTabGridLayout08ac80de",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocRenewalDatedc2bda60-cbc3-49ed-9b39-5f17b3526a7f",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 3,
						"layoutName": "NotesAndFilesTabGridLayout08ac80de"
					},
					"bindTo": "CrocRenewalDate"
				},
				"parentName": "NotesAndFilesTabGridLayout08ac80de",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocDowntime6cb29877-e94c-4e47-a0c7-efc4b8a59cde",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 16,
						"row": 3,
						"layoutName": "NotesAndFilesTabGridLayout08ac80de"
					},
					"bindTo": "CrocDowntime",
					"enabled": false
				},
				"parentName": "NotesAndFilesTabGridLayout08ac80de",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "remove",
				"name": "ESNTab"
			},
			{
				"operation": "remove",
				"name": "ESNFeedContainer"
			},
			{
				"operation": "remove",
				"name": "ESNFeed"
			}
		]/**SCHEMA_DIFF*/
	};
});
