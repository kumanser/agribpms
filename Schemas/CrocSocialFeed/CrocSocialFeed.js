 define("CrocSocialFeed", ["CrocSocialFeedResources", "ESNConstants", "RightUtilities", "ESNFeedConfig",
		"ConfigurationEnums", "performancecountermanager", "CrocConstantsJS", "ConfigurationConstants","NetworkUtilities",
		 "ESNHtmlEditModule", "SocialMessageViewModel", "SocialFeedUtilities",
		"SocialMentionUtilities", "MultilineLabel", "css!HtmlEditModule", "css!MultilineLabel"],
	function(resources, ESNConstants, RightUtilities, ESNFeedConfig, ConfigurationEnums,
			performanceManager, constantsJS, confConst, NetworkUtilities) {
		return {
			mixins: {
				SocialFeedUtilities: "Terrasoft.SocialFeedUtilities",
				SocialMentionUtilities: "Terrasoft.SocialMentionUtilities"
			},
			messages: {
				/**
				 * @message ChannelSaved
				 * Notify module about channel save.
				 */
				"ChannelSaved": {
					mode: Terrasoft.MessageMode.BROADCAST,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},

				/**
				 * @message RemoveChannel
				 * Notify module about channel remove.
				 */
				"RemoveChannel": {
					mode: Terrasoft.MessageMode.BROADCAST,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},

				/**
				 * @message EntityInitialized
				 * ########### ##### ############# ####### # ########### ########### # ########## #############
				 * ########. # ######## ######### ######### ########## ########## # #######.
				 */
				"EntityInitialized": {
					mode: Terrasoft.MessageMode.BROADCAST,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				}
			},
			attributes: {

				/**
				 *
				 */
				"maskVisible": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
					"value": false
				},

				/**
				* #########.
				*/
				posts: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				/**
				 * True, #### ####### ########## ### ########### ######### ######## # ########### ######.
				 */
				"IsContainerListAsync": {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},

				/**
				 * ######## ####### ##########.
				 */
				sortColumnName: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": "CreatedOn"
				},

				/**
				 * ######## ####### ## ####### ########## ########## ### ############ ######.
				 */
				sortColumnLastValue: {
					"dataValueType": Terrasoft.DataValueType.DATE_TIME,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				/**
				 * ######## ############ #########.
				 */
				"SocialMessageText": {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},

				/**
				 * ###### ######### ### ##########.
				 */
				channels: {
					"dataValueType": Terrasoft.DataValueType.COLLECTION,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				/**
				 * ###### ######### ### ##########.
				 */
				entitiesList: {
					"dataValueType": Terrasoft.DataValueType.COLLECTION,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				channel: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},

				/**
				* True, #### ######## ########## #########.
				*/
				isPosting: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},

				/**
				* True, #### ###### ########### ##### ######### ######.
				*/
				showNewSocialMessagesVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},

				/**
				* ########## ##### #########.
				* @type {Number}
				*/
				newSocialMessagesCount: {
					"dataValueType": Terrasoft.DataValueType.INTEGER,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": 0
				},

				/**
				 * True, #### ####### ########## ###### ###### #######.
				 * @type {Boolean}
				 */
				ChannelEditEnabled: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},

				/**
				 * True, #### ####### ########## ###### ###### #####.
				 * @type {Boolean}
				 */
				ChannelEditVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},

				/**
				 * True, #### ####### ########## ###### ###### #######.
				 * @type {Boolean}
				 */
				ChannelEditFocused: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},

				/**
				* ############# ##### ########.
				*/
				entitySchemaUId: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ESNConstants.ESN.SocialChannelSchemaUId
				},

				/**
				* ######## ##### ########.
				* @type {String}
				*/
				entitySchemaName: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": "SocialChannel"
				},

				/**
				 *
				 */
				postPublishActionsVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},

				/**
				* True, #### ######### ######### ##### ######### # ###### ###### #####.
				* @type {Boolean}
				*/
				ESNFeedHeaderVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},

				/**
				* ############# ###### ############ # #######.
				* @type {String}
				*/
				ESNSectionSandboxId: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": "SectionModuleV2_ESNFeedSectionV2_ESNFeed"
				},

				/**
				* ############# ###### ############ # ################ ######.
				* @type {String}
				*/
				ESNRightPanelSandboxId: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": "ViewModule_RightSideBarModule_ESNFeedModule"
				},

				/**
				* ########## #########, ########### #####.
				* @private
				*/
				InitMessageCount: {
					"dataValueType": Terrasoft.DataValueType.INTEGER,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": 15
				},

				/**
				* ########## #########, ########### ### #########.
				* @private
				*/
				NextMessageCount: {
					"dataValueType": Terrasoft.DataValueType.INTEGER,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": 15
				},

				/**
				* ###### ####### # #########
				* "###### ############ # ###### ## ######### ##### ########### #########", ######### ############
				* ### ##########.
				* @type {Object}
				*/
				ColumnList: {
					"dataValueType": Terrasoft.DataValueType.CUSTOM_OBJECT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": null
				},

				/**
				* ###### ####### # ######### "### ############ ##### ########### #########",
				* ######### ############ ### ##########.
				* @type {Object}
				*/
				SharedColumnList: {
					"dataValueType": Terrasoft.DataValueType.CUSTOM_OBJECT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": null
				},

				/**
				* ###### ############### ####### # ######### "### ############ ##### ########### #########",
				* ######### ############ ### ##########.
				* @type {Array}
				*/
				SharedColumnIdList: {
					"dataValueType": Terrasoft.DataValueType.CUSTOM_OBJECT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": null
				},

				/**
				* ###### ############### # ######### "###### ############ # ###### ## ######### ##### ###########
				* #########", ######### ############ ### ##########.
				* @type {Array}
				*/
				ColumnIdList: {
					"dataValueType": Terrasoft.DataValueType.CUSTOM_OBJECT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": null
				},

				/**
				* ############# ######### (########) #########.
				* @type {String}
				*/
				ActiveSocialMessageId: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},

				/**
				* ######## ########## #########.
				* @type {Array}
				* @private
				*/
				SocialMessageSortColumns: {
					"dataValueType": Terrasoft.DataValueType.CUSTOM_OBJECT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": [
						"CreatedOn",
						"LastActionOn"
					]
				},

				/**
				* Master record identifier.
				* @type {String}
				*/
				EntityId: {
					"dataValueType": Terrasoft.DataValueType.GUID,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				/**
				 * Signs of possible load another page of data.
				 * @type {Boolean}
				 */
				"CanLoadMoreData": {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				EmployeeRole: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					onChange: "onEmployeeRoleChange"
				},
				EmployeeRoleList: {
					"dataValueType": Terrasoft.DataValueType.COLLECTION,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				isOnBasePublish: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				EmployeeList: {
					"dataValueType": Terrasoft.DataValueType.COLLECTION,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				parentFieldName: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				}
			},

			methods: {
				//region Methods: Private

				initCollections: function() {
					this.callParent(arguments);
					this.initCollection("EmployeeRoleList");
					this.initCollection("EmployeeList");
				},
/*
				initCollection: function(collectionName) {
					var collection = this.get(collectionName);
					if (!collection) {
						collection = this.Ext.create("Terrasoft.Collection");
						this.set(collectionName, collection);
					} else {
						try {
							collection.clear();
						}
						catch
						{
							this.set(collectionName, this.Ext.create("Terrasoft.Collection"));
						}
					}
				},
*/
				getEmployeeRoles: function() {
					var elements = this.Ext.create("Terrasoft.Collection");
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocExporterEmployeeRole"});
					esq.addColumn("Id");
					esq.addColumn("Name");
					esq.getEntityCollection(function (result) {
						if(result && result.success) {
							var items = result.collection.collection.items;
							var i = 0;
							Terrasoft.each(items, function(item) {
								elements.add(item.get("Id"),{
									displayValue: item.get("Name"),
									value: item.get("Id")
								});
								i++;
							});
							this.set("EmployeeRoleList", elements);
						}
					}, this);
				},

				setEmployeeRoleList: function(filter, list) {
					if (list === null) { return; }
					list.clear();
					list.loadAll(this.$EmployeeRoleList);
				},

				setDefaultValues: function(recordInfo) {
					this.callParent(arguments);
					this.getEmployeeRoles();
					this.set("EmployeeRole", constantsJS.CrocEmployeeRole.PurchasingManager);
					this.setPublishType();
				},

				setPublishType: function() {
					if(this.sandbox.id.indexOf("Account") !== -1) { this.$parentFieldName = "CrocAccount"; }
					if(this.sandbox.id.indexOf("CrocContract") !== -1) { this.$parentFieldName = "CrocSA"; }
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Contact"});
					esq.addColumn("Account");
					esq.addColumn("Account.Type", "AccountType");
					esq.getEntity(Terrasoft.SysValue.CURRENT_USER_CONTACT, function (result) {
						if(result && result.success) {
							var item = result.entity;
							var type = item.get("AccountType");
							var ourCompany = type && type.value === confConst.AccountType.OurCompany.toLowerCase();
							this.$isOnBasePublish = ourCompany;
						}
					}, this);
				},

				publishSocialMessage: function() {
					if (!this.validateNewSocialMessageData()) {
						return;
					}
					var socialMessageData = this.getNewSocialMessageData();
					var isBase = this.$isOnBasePublish;
					if(isBase === undefined || isBase) {
						this.insertSocialMessage(socialMessageData, this.socialMessageInserted, this);
						this.updateSubscribeAction();
						this.saveCurrentChannelToProfile();
					}
					else {
						var employees = this.$EmployeeList;
						//if(!Terrasoft.isEmpty(employees)) {
						if(employees && employees.collection.length > 0) {
							Terrasoft.each(employees, function(item) {
								socialMessageData.message = socialMessageData.message + " " + this.getEmployeeLinkFormat(item);
							}, this);
						};
						this.insertSocialMessage(socialMessageData, this.socialMessageInserted, this);
						this.updateSubscribeAction();
						this.saveCurrentChannelToProfile();
					}
				},

				getEmployeeLinkFormat: function(item) {
					var value = item.value;
					var href = item.link;
					var title = item.displayValue || href;
					var linkHtmlTemplate = "<a data-link=\"mention\" data-value=\"{2}\" href=\"{0}\" target=\"_self\" title=\"{1}\" >{1}</a>";
					return Ext.String.format(linkHtmlTemplate, href, title, value);
				},

				onEmployeeRoleChange: function(arg) {
					var empolyee = arg.get("EmployeeRole");
					var elements = this.Ext.create("Terrasoft.Collection");
					var rootEmployeeSchemaName = "CrocExporterEmployee";
					var parentRecId = this.$EntityId;
					var parentFieldName = this.$parentFieldName;
					if(!parentFieldName) {
						if(this.sandbox.id.indexOf("Account") !== -1) {
							parentFieldName = "CrocAccount";
							rootEmployeeSchemaName = "CrocExporterEmployee";
						}
						if(this.sandbox.id.indexOf("CrocContract") !== -1) {
							parentFieldName = "CrocSA";
							rootEmployeeSchemaName = "CrocContractExporterEmployee";
						}
					}
					else {
						if(parentFieldName === "CrocAccount") {
							rootEmployeeSchemaName = "CrocExporterEmployee";
						}
						if(parentFieldName === "CrocSA") {
							rootEmployeeSchemaName = "CrocContractExporterEmployee";
						}
					}
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: rootEmployeeSchemaName, isDistinct: true});
					esq.addColumn("CrocContact");
					esq.filters.add("RoleFilter",
						esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "CrocRole", empolyee.value, Terrasoft.DataValueType.GUID))
					if(parentFieldName && parentRecId) {
						esq.filters.add("ParentFilter",
							esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, parentFieldName, parentRecId, Terrasoft.DataValueType.GUID))
					}
					esq.getEntityCollection(function (result) {
						if(result && result.success) {
							var items = result.collection.collection.items;
							var i = 0;
							Terrasoft.each(items, function(item) {
								var contact = item.get("CrocContact");
								elements.add(contact.value,{
									displayValue: contact.displayValue,
									value: contact.value,
									link: Terrasoft.workspaceBaseUrl + "/" + NetworkUtilities.getNavigationServicePageUrl("Contact", contact.value)
								});
								i++;
							});
							this.set("EmployeeList", elements);
						}
					}, this);
				},

				//endregion

				//region Methods: Public

				init: function(callback, scope) {
					performanceManager.start(this.sandbox.id + "_ESNInit");
					this.callParent([function() {
						this.flushCanLoadMoreData();
						this.mixins.SocialFeedUtilities.init.call(this);
						this.mixins.SocialMentionUtilities.init.call(this);
						this.initCollections();
						this.initChannelsList();
						this.subscribeSandboxEvents();
						this.Terrasoft.ServerChannel.on(this.Terrasoft.EventName.ON_MESSAGE,
							this.onSocialMessageReceived, this);
						performanceManager.stop(this.sandbox.id + "_ESNInit");
						this.initCanDeleteAllMessageComment(callback, scope);
					}, this]);
				},

				getRecordInfo: function() {
					return this.sandbox.publish("GetRecordInfo", null, [this.sandbox.id]);
				},

				initModuleViewModel: function(config) {
					this.activeSocialMessageId = config.activeSocialMessageId;
					var recordInfo = this.getRecordInfo();
					this.setDefaultValues(recordInfo);
					this.initCurrentChannel(recordInfo);
					this.init(this.loadInitialSocialMessages, this);
				},

				subscribeSandboxEvents: function() {
					this.sandbox.subscribe("InitModuleViewModel", this.initModuleViewModel, this, [this.sandbox.id]);
					this.sandbox.subscribe("CardSaved", this.onCardSaved, this, [this.sandbox.id]);
					this.sandbox.subscribe("EntityInitialized", this.onChannelInitialized, this, [this.sandbox.id]);
					this.sandbox.subscribe("RemoveChannel", this.onRemoveChannel, this);
					this.sandbox.subscribe("ChannelSaved", this.onChannelSaved, this);
				},

				onRender: function() {
					const recordInfo = this.getRecordInfo();
					this.setDefaultValues(recordInfo);
					this.initCurrentChannel(recordInfo);
					this.initPostMessageFocusedState();
					const socialMessages = this.get("SocialMessages");
					if (!socialMessages.getCount()) {
						this.initMessagesNumberEverySearchIteration(this.loadInitialSocialMessages, this);
					}
				},

				initMessagesNumberEverySearchIteration: function(callback, scope) {
					this.Terrasoft.SysSettings.querySysSettingsItem("MessagesNumberEverySearchIteration",
						function(value) {
							this.set("MessagesNumberEverySearchIteration", value);
							callback.call(scope);
						}, this);
				},

				addChannelToChannelList: function(channelListObject, channel) {
					channelListObject[channel.get("value")] = {
						value: channel.get("value"),
						displayValue: channel.get("displayValue"),
						primaryImageValue: channel.get("primaryImageValue")
					};
				},

				onSocialMessageReceived: function(scope, response) {
					if (response && response.Header.Sender !== "UpdateSocialMessage") {
						return;
					}
					this.flushCanLoadMoreData();
					var receivedMessage = this.Ext.decode(response.Body);
					var loadSocialMessagesConfig = {
						id: response.Id,
						sandbox: this.sandbox,
						canDeleteAllMessageComment: this.get("canDeleteAllMessageComment"),
						onDeleteRecordCallback: this.onDeleteRecordCallback
					};
					var config = {
						response: response,
						loadSocialMessagesConfig: loadSocialMessagesConfig,
						loadSocialMessagesCallback: this.loadSocialMessagesCallback
					};
					switch (receivedMessage.operation) {
						case "insert":
							config.receivedMessage = receivedMessage;
							this.onInsertSocialMessageReceived(config);
							break;
						case "update":
							this.onUpdateSocialMessageReceived(config);
							break;
						case "delete":
							var posts = this.get("SocialMessages");
							posts.removeByKey(response.Id);
							break;
					}
				},

				prepareChannels: function() {
					var list = this.get("channels");
					var resultChannelColumnList = this.get("resultChannelColumnList");
					list.clear();
					list.loadAll(resultChannelColumnList);
				},

				onKeyDown: Terrasoft.emptyFn,

				onEnterKeyPressed: function onKeyDown(e) {
					if (e.ctrlKey) {
						this.onPostPublishClick();
					}
				},

				getNextMessages: function(config, callback, scope) {
					var viewModel = this;
					var canDeleteAllMessageComment = this.get("canDeleteAllMessageComment");
					var sortColumnName = this.get("sortColumnName");
					this.loadSocialMessages({
						schemaUId: this.$entitySchemaUId,
						entityId: this.$EntityId,
						sortColumnName: sortColumnName,
						sandbox: this.sandbox,
						esqConfig: {
							rowCount: config.rowCount,
							filter: config.filter
						},
						canDeleteAllMessageComment: canDeleteAllMessageComment,
						onDeleteRecordCallback: function() {
							var posts = viewModel.get("SocialMessages");
							var postToDeleteId = this.get("Id");
							posts.removeByKey(postToDeleteId);
						}
					}, function(messages) {
						this.setCanLoadMoreData(messages.getCount(), config.rowCount);
						callback.call(scope || this, messages);
					}, this);
				},

				getSortButtonImageConfig: function() {
					return this.getResourceImageConfig("Resources.Images.Sort");
				},

				getShowNewMessagesButtonImageConfig: function() {
					return this.getResourceImageConfig("Resources.Images.More");
				},

				setValidationConfig: function() {
					this.callParent(arguments);
					this.addColumnValidator("SocialChannel", this.validateChannel);
				},

				validateNewSocialMessageData: function() {
					var textValidationResult = this.validateSocialMessageText();
					var channelValidationResult = this.validateChannel();
					return (textValidationResult.isValid && channelValidationResult.isValid);
				},

				destroy: function() {
					this.Terrasoft.ServerChannel.un(this.Terrasoft.EventName.ON_MESSAGE, this.onSocialMessageReceived,
						this);
					this.callParent(arguments);
				},

				subscribeViewModelEvents: function() {
					this.callParent(arguments);
				},

				onLastActionOnSortClick: function() {
					this.set("sortColumnName", this.get("SocialMessageSortColumns")[1]);
					this.onSortClick();
				},

				onCreatedOnSortClick: function() {
					this.set("sortColumnName", this.get("SocialMessageSortColumns")[0]);
					this.onSortClick();
				}

				//endregion

			},
			diff: [
				{
					"operation": "insert",
					"parentName": "SocialChannelEditContainer",
					"name": "EmployeeRoleEdit",
					"propertyName": "items",
					"values": {
						"generateId": false,
						"itemType": Terrasoft.ViewItemType.MODEL_ITEM,
						"dataValueType": Terrasoft.DataValueType.ENUM,
						"enabled": true,
						"controlConfig": {
							"visible": {bindTo: "postPublishActionsVisible"}
						},
						"value": {bindTo: "EmployeeRole"},
						"list": {bindTo: "EmployeeRoleList"},
						"prepareList": {bindTo: "setEmployeeRoleList"},
						//"placeholder": {bindTo: "getSocialChannelEditPlaceholder"},
						//"focused": {bindTo: "ChannelEditFocused"},
						//"blur": {bindTo: "onChannelEditBlur"},
						"classes": {
							wrapClass: ["inlineBlock", "channel", "placeholderOpacity"]
						},
						"markerValue": {bindTo: "getSocialMessageEditMarkerValue"},
						"labelConfig": {"visible": false}
					}
				},
				{
					"operation": "merge",
					"name": "SocialChannelEdit",
					"values": {
						"controlConfig": {
							"visible": false
						}
					}
				}
			]
		};
	});
