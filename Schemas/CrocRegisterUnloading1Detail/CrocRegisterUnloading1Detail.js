define("CrocRegisterUnloading1Detail", [], function() {
	return {
		entitySchemaName: "CrocRegisterUnloading",
		messages: {
			SelectDocumentForUnloading: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attributes: {
			ExternalId: {
				"dataValueType": Terrasoft.DataValueType.TEXT,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			filterName: {
				dataValueType: Terrasoft.DataValueType.TEXT,
				value: "DocumentIDFilter"
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
			getCopyRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				var masterRecId = this.get("MasterRecordId");
				if (masterRecId) {
					this.sandbox.subscribe("SelectDocumentForUnloading", this.onSelectDocumentForUnloadingSubscribe, this, [masterRecId]);
				}
			},

			onSelectDocumentForUnloadingSubscribe: function(val) {
				var masterRecId = this.get("MasterRecordId");
				if(val && masterRecId === val.masterRecId) {
					this.$ExternalId = val.externId;
					this.reloadGridData();
				}
			},

			getFilters: function() {
				var filterName = this.$filterName;
				const filters = this.callParent(arguments);
				if(filters.contains(filterName)) {
					filters.removeByKey(filterName);
				}
				if(this.$ExternalId) {
					var filter = this.Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,"CrocGuid", this.$ExternalId, Terrasoft.DataValueType.GUID);
					filter.key = filterName
					filters.addItem(filter);
				}
				return filters;
			},
		}
	};
});
