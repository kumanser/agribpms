define("CrocSetDocument1Detail", ["CrocConstantsJS","RightUtilities","ProcessModuleUtilities", "ServiceHelper", "ConfigurationEnums"],
	function(CrocConstantsJS, RightUtilities, ProcessModuleUtilities, ServiceHelper, enums) {
	return {
		entitySchemaName: "CrocSetDocument",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		attributes: {
			"CrocIsAcceptVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"CrocIsAddCommentVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsAcceptDocumentEnable": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				dependencies: [{
					columns: ["ActiveRow"],
					methodName: "onActiveRowChange"
				}],
				value: false
			}
		},
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {

			initData: function() {
				this.callParent(arguments);
				this.setAcceptVisible();
				this.setAddCommentVisible();
			},

			addGridDataColumns: function(esq) {
				this.callParent(arguments);
				esq.addColumn("CrocDocument");
			},
			
			setActionsEnabled: function() {
				var selected = this.getSelectedItems();
				return selected;
			},
			
			onActiveRowChange: function() {
				var gridData = this.getGridData();
				var activeRow = this.get("ActiveRow");
				if (gridData && activeRow) {
					this.checkAcceptedInRow(function(result){
					}, this);					
				}
			},
			
			checkAcceptedInRow: function(callback, scope) {
				var activeRow = this.get("ActiveRow");
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocSetDocument"});
					esq.addColumn("CrocDocument.CrocIsAccepted");
					esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Id", activeRow));
					esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "CrocDocument.CrocIsAccepted", false));
					esq.getEntityCollection(function(response) {
						if (response.success && response.collection.getCount()=== 1) {
							callback.call(scope, this.$IsAcceptDocumentEnable = true);
						}
						else {
							callback.call(scope, this.$IsAcceptDocumentEnable = false);
						}
					}, this);
			},
			
			
			setAddCommentVisible: function(){
				RightUtilities.checkCanExecuteOperation({operation: "CrocIsAddCommentRights"}, function(result) {
					this.$CrocIsAddCommentVisible = result && (this.get("CardPageName") === "CrocDocument1Page");
				},this);
			},
			
			setAcceptVisible: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocIsAcceptVisibleRights"}, function(result) {
					this.$CrocIsAcceptVisible = result;
				}, this);
			},
			

			addToolsButtonMenuItems: function(toolsButtonMenu) {
				toolsButtonMenu.addItem(this.getAcceptDocumentAction());
				toolsButtonMenu.addItem(this.getButtonMenuSeparator());
				toolsButtonMenu.addItem(this.getAddCommentAction());
				toolsButtonMenu.addItem(this.getButtonMenuSeparator());
				toolsButtonMenu.addItem(this.getAddDocumentKitAction());
				toolsButtonMenu.addItem(this.getButtonMenuSeparator());
				toolsButtonMenu.addItem(this.getAddExistDocumentAction());
				toolsButtonMenu.addItem(this.getButtonMenuSeparator());
				this.callParent(arguments);
			},
			
			getAddDocumentKitAction: function() {
				return this.getButtonMenuItem({
					Caption: {"bindTo":"Resources.Strings.AddDocumentKitActionCaption"},
					Visible: true,
					Click: {"bindTo":"onAddDocumentKitActionClick"}
				});
			},
			
			getAcceptDocumentAction: function() {
				return this.getButtonMenuItem({
					Caption: {"bindTo":"Resources.Strings.AcceptDocumentActionCaption"},
					Visible: {"bindTo": "CrocIsAcceptVisible"},
					Click: {"bindTo":"onAcceptDocumentActionClick"},
					Enabled: {"bindTo": "IsAcceptDocumentEnable"}			
				});
			},
			
			getAddCommentAction: function() {
				return this.getButtonMenuItem({
					Caption: {"bindTo":"Resources.Strings.AddCommentActionCaption"},
					Visible: {"bindTo": "CrocIsAddCommentVisible"},
					Click: {"bindTo":"onAddCommentActionClick"},
					Enabled: {"bindTo": "setActionsEnabled"}
				});
			},
			
			onAddDocumentKitActionClick: function() {
				console.log(this.get("CardPageName"));
				if (this.get("CardPageName") === "CrocContract2Page")
				{
					var contractId = this.get("MasterRecordId");
					if (contractId) {
						var contractArgs = {
						sysProcessName: "CrocProcessContractDocumentKitAdd",
						parameters: {
							ContractId: contractId
						}
					};
					ProcessModuleUtilities.executeProcess(contractArgs);
					}
				}
				else if (this.get("CardPageName") === "CrocDocument1Page") {
					var documentId = this.get("MasterRecordId");
					if (documentId) {
						var documentArgs = {
							sysProcessName: "CrocProcessDocumentKitAdd",
							parameters: {
								DocumentId: documentId
							}
						};
					ProcessModuleUtilities.executeProcess(documentArgs);
					}
				}
				else if (this.get("CardPageName") === "ActivityPageV2"){
					var activityId = this.get("MasterRecordId");
					if (activityId) {
						var activityArgs = {
							sysProcessName: "CrocProcessActivityDocumentKitAdd",
							parameters: {
								ActivityId: activityId
							}
						};
					ProcessModuleUtilities.executeProcess(activityArgs);
					}
				}
			},
			
			onAddCommentActionClick: function() {
				var documentSetId = this.get("ActiveRow");
				if (documentSetId) {
					var args = {
						sysProcessName: "CrocProcessDocumentSetCommentAdd",
						parameters: {
							DocumentSetID: documentSetId
						}
					};
					ProcessModuleUtilities.executeProcess(args);
				}
			},

			onAcceptDocumentActionClick: function() {
				var activeRowId = this.get("ActiveRow");
				if (activeRowId) {
					var args = {
						sysProcessName: "CrocProcessAcceptDocumentFromDetail",
						parameters: {
							ActiveRowId: activeRowId
						}
					};
					ProcessModuleUtilities.executeProcess(args);
				}
				this.$IsAcceptDocumentEnable = false;
			},

			getAddExistDocumentAction: function() {
				return this.getButtonMenuItem({
					Caption: {"bindTo":"Resources.Strings.AddExistDocumentActionCaption"},
					Visible: true,
					Click: {"bindTo":"onAddExistDocumentActionClick"},
					Enabled: {"bindTo": "AddExistDocumentActionEnabled"}
				});
			},

			onAddExistDocumentActionClick: function() {
				var documentSetId = this.get("ActiveRow");
				if(!documentSetId) { return; }
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: this.entitySchemaName});
				esq.addColumn("CrocDocument.CrocAccount", "Account");
				esq.addColumn("CrocDocument.CrocDocumentType", "DocumentType");
				esq.addColumn("CrocDocument.Id", "DocumentId");
				esq.getEntity(documentSetId, function(response) {
					if (response && response.success) {
						var entity = response.entity;
						if(entity) {
							var accountId = entity.get("Account").value;
							var typeId = entity.get("DocumentType").value;
							var currentDocId = entity.get("DocumentId");
							this.LoadLookupIfFindDocs(accountId, typeId, currentDocId);
						}
					}
				}, this);
			},

			AddExistDocumentActionEnabled: function() {
				var activeRow = this.getActiveRow();
				if(activeRow) return true;
				else return false;
			},

			LoadLookupIfFindDocs: function(accountId, typeId, currentDocId) {
				if(accountId && typeId) {
					var lookupConfig = {
						entitySchemaName: "CrocDocument",
						multiSelect: false,
						columns: ["CrocAccount", "CrocDocumentType", "CrocDocumentStatus"],
						actionsButtonVisible: false
					};
					var gridData = this.getGridData();
					var documents = [];
					if(gridData) {
						Terrasoft.each(gridData, function(item) {
							documents.push(item.get("CrocDocument").value)
						});
					}
					if(documents.length === 0) { documents.push(currentDocId); }
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocDocument"});
					esq.addColumn("CrocAccount");
					esq.addColumn("CrocDocumentType");
					esq.addColumn("CrocDocumentStatus");
					var filters = Terrasoft.createFilterGroup();
					filters.name = "DocumentsToSetFilter";
					filters.addItem(Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "CrocAccount", accountId, Terrasoft.DataValueType.GUID));
					filters.addItem(Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "CrocDocumentType", typeId, Terrasoft.DataValueType.GUID));
					filters.addItem(Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.NOT_EQUAL, "CrocDocumentStatus", CrocConstantsJS.CrocDocumentStatus.Archive.value , Terrasoft.DataValueType.GUID));
					documents.forEach(function(documentId) {
						filters.addItem(Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.NOT_EQUAL, "Id", documentId, Terrasoft.DataValueType.GUID));
					}, this)
					lookupConfig.filters = filters;
					esq.filters = filters;
					esq.getEntityCollection(function(response) {
						if (response.success && response.collection.getCount() > 0) {
							this.openLookup(lookupConfig, this.addDocumentCallBack, this);
						}
						else {
							this.showConfirmationDialog(this.get("Resources.Strings.CreateNewDocumentToSet"), function(result) {
								if (result === Terrasoft.MessageBoxButtons.YES.returnCode) {
									this.createNewDocumentToSet(currentDocId);
								}
							}, ["yes", "no"]);
						}
					}, this);
				}
			},

			addDocumentCallBack: function(args) {
				var bq = this.Ext.create("Terrasoft.BatchQuery");
				this.selectedRows = args.selectedRows.getItems();
				this.selectedRows.forEach(function(item) {
					item.CrocDocument = item.value;
					item.Name = item.displayValue;
					bq.add(this.getDocumentUpdateQuery(item));
				}, this);
				if (bq.queries.length) {
					bq.execute(this.reloadGridData, this);
				}
			},

			getDocumentUpdateQuery: function(item) {
				var documentSetId = this.get("ActiveRow");
				var update = Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: this.entitySchemaName});
				update.filters.add("IdFilter", update.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
					"Id", documentSetId))
				update.setParameterValue("CrocDocument", item.CrocDocument, this.Terrasoft.DataValueType.GUID);
				update.setParameterValue("CrocName", item.Name, this.Terrasoft.DataValueType.TEXT);
				return update;
			},

			getDocumentInsertQuery: function(item) {
				var masterRecordId = this.$MasterRecordId;
				var masterColumn = this.$DetailColumnName;
				var insert = Ext.create("Terrasoft.InsertQuery", {rootSchemaName: this.entitySchemaName});
				insert.setParameterValue(masterColumn, masterRecordId, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("CrocDocument", item.CrocDocument, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("CrocName", item.Name, this.Terrasoft.DataValueType.TEXT);
				return insert;
			},

			createNewDocumentToSet: function(currentDocId) {
				var config = {
					serviceName: "CrocToolsService",
					methodName: "CreateNewDocumentOnExist",
					data: {
						sourceDocumentId: currentDocId,
						masterRecordId: this.$MasterRecordId,
						masterColumn: this.$DetailColumnName
					},
					callback: this.createNewDocumentToSetCallback,
					scope: this
				};
				ServiceHelper.callService(config);
			},

			createNewDocumentToSetCallback: function(response) {
				if(response && response.CreateNewDocumentOnExistResult) {
					this.reloadGridData();
					this.openCard(enums.CardStateV2.EDIT, null, response.CreateNewDocumentOnExistResult);
				}
			}
		}
	};
});
