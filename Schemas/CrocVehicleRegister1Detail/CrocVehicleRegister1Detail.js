define("CrocVehicleRegister1Detail", [], function() {
	return {
		entitySchemaName: "CrocVehicleRegister",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			addRecordOperationsMenuItems: this.Terrasoft.emptyFn,
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
			getAddTypedRecordButtonVisible: this.Terrasoft.emptyFn,
		}
	};
});
