define("CrocDocumenta9252732Section", ["CrocExcelTemplateConst", "ServiceHelper", "RightUtilities", "CrocExcelHelperJS", "CrocConstantsJS"], function(CrocExcelTemplateConst,
																																   ServiceHelper, RightUtilities,
																																				   CrocExcelHelperJS, CrocConstantsJS) {
	return {
		entitySchemaName: "CrocDocument",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "SendToExporterButton",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.SendToExporterButtonCaption"
					},
					"click": {
						"bindTo": "onCardAction"
					},
					"tag": "onSendToExporterButtonClick",
					"enabled": true,
					"visible": {"bindTo": "SendToExporterButtonVisible"},
					"style": "green",
					"styles": {
						"textStyle": {
							"margin": "0px 9px 0px 0px"
						}
					},
					"layout": {
						"column": 1,
						"row": 6,
						"colSpan": 1
					}
				},
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "ApproveApplicationButton",
				"index": 1,
				"values": {
					"tag": "onApproveApplicationButtonClick",
					"visible": {"bindTo": "ApproveApplicationButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					"caption": { "bindTo": "Resources.Strings.ApproveApplicationButtonCaption" },
					"click": { "bindTo": "onCardAction" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			},
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "RejectApplicationButton",
				"index": 2,
				"values": {
					"tag": "onRejectApplicationButtonClick",
					"visible": {"bindTo": "RejectApplicationButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.RED,
					"caption": { "bindTo": "Resources.Strings.RejectApplicationButtonCaption" },
					"click": { "bindTo": "onCardAction" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			},
		]/**SCHEMA_DIFF*/,
		messages: {
			"AcceptDocumentActionVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"CancelAccreditationActionVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"CancelAccreditationActionEnabledChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"SendToExporterButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"ApproveApplicationButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"RejectApplicationButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attributes: {
			"CrocIsAcceptVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsCancelAccreditationVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsCancelAccreditationEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"SendToExporterButtonVisible":{
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"ApproveApplicationButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"RejectApplicationButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"CanExportFile": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			}
		},
		methods: {
			init: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("AcceptDocumentActionVisibleChanged", function(isVisible) {
					this.$CrocIsAcceptVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);

				this.sandbox.subscribe("CancelAccreditationActionVisibleChanged", function(isVisible) {
					this.$IsCancelAccreditationVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);

				this.sandbox.subscribe("CancelAccreditationActionEnabledChanged", function(isEnabled) {
					this.$IsCancelAccreditationEnabled = isEnabled;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
				
				this.sandbox.subscribe("SendToExporterButtonVisibleChanged", function(isVisible) {
					this.$SendToExporterButtonVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
				
				this.sandbox.subscribe("ApproveApplicationButtonVisibleChanged", function(isVisible) {
					this.$ApproveApplicationButtonVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
				
				this.sandbox.subscribe("RejectApplicationButtonVisibleChanged", function(isVisible) {
					this.$RejectApplicationButtonVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);

				this.setCanExportExcel();
			},

			getGridDataColumns: function() {
				var config = this.callParent(arguments);
				config.CrocDocumentType = {path: "CrocDocumentType"};
				return config;
			},

			setCanExportExcel: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanExportToExcel"},
					function(result) {
						this.$CanExportFile = result;
					}, this);
			},

			getSectionActions: function () {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					Type: "Terrasoft.MenuSeparator",
					Caption: ""
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Enabled": {bindTo: "CanExportFile"},
					"Visible": {bindTo: "CanExportFile"},
					"Caption" : {bindTo: "Resources.Strings.CrocExcelRailwayButtonCaption"},
					"Click": {bindTo: "exportToExcelRailway"},
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Enabled": {bindTo: "CanExportFile"},
					"Visible": {bindTo: "CanExportFile"},
					"Caption" : {bindTo: "Resources.Strings.CrocExcelGU12ButtonCaption"},
					"Click": {bindTo: "exportToExcelGU12"},
				}));
				return actionMenuItems;
			},

			exportToExcelRailway: function() {
				var filter = this.getFilters();
				var config = {
					serviceName: "CrocExcelGeneratorService",
					methodName: "GenerateExcel",
					data: {
						model: JSON.stringify(CrocExcelTemplateConst.CrocExcelTemplates.ExcelRailWay),
						filter: filter.serialize()
					}
				};

				ServiceHelper.callService(config.serviceName, config.methodName, function(response) {
					var cacheKey = response.GenerateExcelResult;
					CrocExcelHelperJS.CrocExcelHelper.downloadFile(CrocExcelTemplateConst.CrocExcelTemplates.ExcelRailWay.excelReportName, cacheKey);
				}, config.data, this);
			},

			exportToExcelGU12: function() {
				var filter = this.getFilters();
				var config = {
					serviceName: "CrocExcelGeneratorService",
					methodName: "GenerateExcel",
					data: {
						model: JSON.stringify(CrocExcelTemplateConst.CrocExcelTemplates.ExcelGU12),
						filter: filter.serialize()
					}
				};

				ServiceHelper.callService(config.serviceName, config.methodName, function(response) {
					var cacheKey = response.GenerateExcelResult;
					CrocExcelHelperJS.CrocExcelHelper.downloadFile(CrocExcelTemplateConst.CrocExcelTemplates.ExcelGU12.excelReportName, cacheKey);
				}, config.data, this);
			},

		}
	};
});
