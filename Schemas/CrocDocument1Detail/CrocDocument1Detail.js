define("CrocDocument1Detail", [], function() {
	return {
		entitySchemaName: "CrocDocument",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: Terrasoft.emptyFn,

			updateDetail: function(config) {
				config.reloadAll = true;
				this.callParent(arguments);
			}
		}
	};
});
