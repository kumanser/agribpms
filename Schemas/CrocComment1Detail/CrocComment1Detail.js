define("CrocComment1Detail", ["RightUtilities", "ServiceHelper"], function(RightUtilities, ServiceHelper) {
	return {
		entitySchemaName: "CrocComment",
		attributes: {
			"CrocCommentActionVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			initData: function() {
				this.callParent(arguments);
				this.setCommentActionVisible();
			},
			
			setActionsEnabled: function() {
				var activeRowId = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				return activeRowId ? true : selectedRows ? (selectedRows.length > 0) : false;
			},
			
			setCommentActionVisible: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanReturnOrAcceptComment"}, function(result) {
					this.$CrocCommentActionVisible = result;
				}, this);
			},
			
			onCommentFixedActionClick: function() {
				var activeRowId = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				var commentId = "";
				commentId = (selectedRows && selectedRows.length) ? selectedRows.join(';') : (activeRowId + ";");
				var params = {
					commentsStr: commentId
				};
				var serviceConfig = {
					serviceName: "CrocCommentService",
					methodName: "CommentFixed",
					callback: function(response){
						if (response) {
							console.log("OK");
						}
					},
					data: params,
					scope: this,
					timeout: 250000
				};
				ServiceHelper.callService(serviceConfig);
				this.reloadGridData();
				this.unSetMultiSelect();
			},
			
			onCorrectionAcceptedActionClick: function() {
				var activeRowId = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				var commentId = "";
				commentId = (selectedRows && selectedRows.length) ? selectedRows.join(';') : (activeRowId + ";");
				var params = {
					commentsStr: commentId
				};
				var serviceConfig = {
					serviceName: "CrocCommentService",
					methodName: "AcceptCorrection",
					callback: function(response){
						if (response) {
							console.log("OK");
						}
					},
					data: params,
					scope: this,
					timeout: 250000
				};
				ServiceHelper.callService(serviceConfig);
				this.reloadGridData();
				this.unSetMultiSelect();
			},
			
			onReturnToRevisionClick: function() {
				var activeRowId = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				var commentId = "";
				commentId = (selectedRows && selectedRows.length) ? selectedRows.join(';') : (activeRowId + ";");
				var params = {
					commentsStr: commentId
				};
				var serviceConfig = {
					serviceName: "CrocCommentService",
					methodName: "ReturnToRevision",
					callback: function(response){
						if (response) {
							console.log("OK");
						}
					},
					data: params,
					scope: this,
					timeout: 250000
				};
				ServiceHelper.callService(serviceConfig);
				this.reloadGridData();
				this.unSetMultiSelect();
			},
			
			addToolsButtonMenuItems: function(toolsButtonMenu) {
				toolsButtonMenu.addItem(this.getReturnToRevisionAction());
				toolsButtonMenu.addItem(this.getButtonMenuSeparator());
				toolsButtonMenu.addItem(this.getCorrectionAcceptedAction());
				toolsButtonMenu.addItem(this.getButtonMenuSeparator());
				toolsButtonMenu.addItem(this.getCommentFixedAction());
				toolsButtonMenu.addItem(this.getButtonMenuSeparator());
				this.callParent(arguments);
			},
			
			getCommentFixedAction: function() {
				return this.getButtonMenuItem({
					Caption: {"bindTo":"Resources.Strings.CommentFixedActionCaption"},
					Visible: true,
					Click: {"bindTo":"onCommentFixedActionClick"},
					Enabled: {"bindTo": "setActionsEnabled"}
				});
			},
			
			getCorrectionAcceptedAction: function() {
				return this.getButtonMenuItem({
					Caption: {"bindTo":"Resources.Strings.CorrectionAcceptedActionCaption"},
					Visible: {"bindTo": "CrocCommentActionVisible"},
					Click: {"bindTo":"onCorrectionAcceptedActionClick"},
					Enabled: {"bindTo": "setActionsEnabled"}			
				});
			},
			
			getReturnToRevisionAction: function() {
				return this.getButtonMenuItem({
					Caption: {"bindTo":"Resources.Strings.ReturnToRevisionActionCaption"},
					Visible: {"bindTo": "CrocCommentActionVisible"},
					Click: {"bindTo":"onReturnToRevisionClick"},
					Enabled: {"bindTo": "setActionsEnabled"}
				});
			}
		}
	};
});
