define("CrocRegisterUnloading1Page", [], function() {
	return {
		entitySchemaName: "CrocRegisterUnloading",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocNumber88d1c935-1931-44c0-8540-4f865df99be1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocNumber",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocSender6cc18fd1-dd7e-4069-b3f1-9fa91bb64b3a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocSender",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocProduct44d9b7ab-5838-402a-a964-877f161d2ed6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocProduct",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocProvider65fc14aa-20c1-42d8-99da-414871d3ab59",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocProvider",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocProductClass608237b2-c52a-4204-afad-11a5a87bfda0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocProductClass",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocChP6ee1c5fd-023a-47be-88cd-c03c5067b6c8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocChP",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocNetWeightAcceptanceTon582974bc-4b3d-460e-a181-d246d40e9153",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocNetWeightAcceptanceTon",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocWeedImpurity83b7ada8-2984-4291-9363-8bc7dc626630",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocWeedImpurity",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocGrossWeightAcceptanceTond46aefb0-ea0b-45ba-9fe3-d1f3dd931f1c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocGrossWeightAcceptanceTon",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "CrocInfected6d69a20d-f7e8-4854-8a33-ac33c7bc2c3a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocInfected",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "CrocShipmentDate04475e38-b042-4515-9e8f-46c15e91e4e5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "CrocShipmentDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "CrocProtein203bc3ff-b559-4bbf-bf8d-52138df63048",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "CrocProtein",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "CrocUploadDate20db0a4d-1f2f-47d4-b102-2127274df203",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "CrocUploadDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "CrocNaturb33312ae-7b44-4e59-99c2-7669923dead1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "CrocNatur",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "LOOKUP28d44d80-42c5-4c00-a708-c12529bbe9a6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Header"
					},
					"bindTo": "CrocPointLoading",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "insert",
				"name": "CrocGluten2846eea7-04c7-4c8b-99a2-ddd1a64f5d00",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 7,
						"layoutName": "Header"
					},
					"bindTo": "CrocGluten",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 15
			},
			{
				"operation": "insert",
				"name": "CrocUnloadingPointf6c30291-525b-4a71-ba0e-880f36787d9d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "Header"
					},
					"bindTo": "CrocUnloadingPoint",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 16
			},
			{
				"operation": "insert",
				"name": "CrocIDK5a2b2afb-270f-4c9d-8593-e86dcf9fe33a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 8,
						"layoutName": "Header"
					},
					"bindTo": "CrocIDK",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 17
			},
			{
				"operation": "insert",
				"name": "CrocTransportType102ee64d-28b2-4f13-bcbc-753e43e99b11",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 9,
						"layoutName": "Header"
					},
					"bindTo": "CrocTransportType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 18
			},
			{
				"operation": "insert",
				"name": "CrocGrainAdmixture8798b309-bda7-439c-8034-8605857fa412",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 9,
						"layoutName": "Header"
					},
					"bindTo": "CrocGrainAdmixture",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 19
			},
			{
				"operation": "insert",
				"name": "CrocNumberTransport6fbc46a4-8a9f-4d30-b227-428d7f30adf0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 10,
						"layoutName": "Header"
					},
					"bindTo": "CrocNumberTransport",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 20
			},
			{
				"operation": "insert",
				"name": "CrocHumiditydf09c0a2-d109-4119-aae6-8749096d6be9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 10,
						"layoutName": "Header"
					},
					"bindTo": "CrocHumidity",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 21
			},
			{
				"operation": "insert",
				"name": "CrocNumberConsignment5db8f05c-ee70-4186-9297-c1b1b21d7afb",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 11,
						"layoutName": "Header"
					},
					"bindTo": "CrocNumberConsignment",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 22
			},
			{
				"operation": "insert",
				"name": "CrocBrokenc128d0a9-824e-4ebe-ab51-5957cce7a063",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 11,
						"layoutName": "Header"
					},
					"bindTo": "CrocBroken",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 23
			},
			{
				"operation": "insert",
				"name": "CrocExportere607e5d1-f259-4b16-a198-9aef945e9cf0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 12,
						"layoutName": "Header"
					},
					"bindTo": "CrocExporter",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 24
			},
			{
				"operation": "insert",
				"name": "CrocSZPKdb9c56db-5e78-4272-b563-599defd71a22",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 12,
						"layoutName": "Header"
					},
					"bindTo": "CrocSZPK",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 25
			},
			{
				"operation": "insert",
				"name": "FLOAT6ff1c478-439e-4c06-9fb8-5e610df18c37",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 13,
						"layoutName": "Header"
					},
					"bindTo": "CrocDamaged",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 26
			},
			{
				"operation": "insert",
				"name": "FLOATb05d2b21-1f84-4722-a521-e56ac5f904b8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 13,
						"layoutName": "Header"
					},
					"bindTo": "CrocBugEated",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 27
			},
			{
				"operation": "insert",
				"name": "LOOKUP07223800-ab22-4749-bc0b-634c641230a0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 14,
						"layoutName": "Header"
					},
					"bindTo": "CrocAssignment",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 28
			},
			{
				"operation": "insert",
				"name": "FLOAT9b00b491-527b-40ec-b6fa-4b6098d7ded5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 14,
						"layoutName": "Header"
					},
					"bindTo": "CrocGrainsDifferentColor",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 29
			}
		]/**SCHEMA_DIFF*/
	};
});
