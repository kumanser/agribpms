define("CrocChartc374e954Section", ["ConfigurationEnums","ProcessModuleUtilities"], function(enums,ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocChart",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		messages: {
			"ApproveChartButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"LoadGU12ButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attributes: {
			"ApproveChartButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"LoadGU12ButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"value": false
			},
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "ApproveChartButton",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.ApproveChartButtonCaption"
					},
					"click": {
						"bindTo": "onCardAction"
					},
					"tag": "onApproveChartButtonClick",
					"enabled": true,
					"visible": {"bindTo": "ApproveChartButtonVisible"},
					"style": "green",
					"styles": {
						"textStyle": {
							"margin": "0px 9px 0px 0px"
						}
					},
					"layout": {
						"column": 1,
						"row": 6,
						"colSpan": 1
					}
				},
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardContainer",
				"propertyName": "items",
				"name": "LoadGU12Button",
				"index": 4,
				"values": {
					"visible": {"bindTo": "LoadGU12ButtonVisible"},
					"tag": "onLoadGU12ButtonClick",
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"caption": { "bindTo": "Resources.Strings.LoadGU12ButtonCaption" },
					"click": { "bindTo": "onCardAction" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
					"layout": {
						"column": 12,
						"row": 6,
						"colSpan": 6
					},
					"fileUpload": true,
					"filesSelected": { "bindTo": "onFileSelect" },
					"fileUploadMultiSelect": false,
				}
			},
		]/**SCHEMA_DIFF*/,
		methods: {
			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("ApproveChartButtonVisibleChanged", function(isVisible) {
					this.$ApproveChartButtonVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
				this.sandbox.subscribe("LoadGU12ButtonVisibleChanged", function(isVisible) {
					this.$LoadGU12ButtonVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
			},
			onFileSelect: function(files) {
				if (files.length <= 0) {
					return;
				}
				const config = this.getUploadConfig(files);
				this.upload(config, function() {
					this.set("FileUploadConfig", null);
				});
			},

			performValues: function(){
				var chartId = this.cachedActiveRow;
				return {
					chartId: chartId
				}
			},

			getUploadConfig: function(files) {
				const config = this.performValues();
				return {
					scope: this,
					onUpload: this.onUpload,
					onComplete: this.onComplete,
					uploadWebServicePath: "CrocDocumentGU12FilesService/ImportFile",
					entitySchemaName: "CrocDocumentFile",
					columnName: "Data",
					parentColumnName: "CrocDocumentFile",
					parentColumnValue: this.cachedActiveRow,
					files: files,
					isChunkedUpload: false,
					data: config
				};
			},

			onUpload: function() {
				this.showBodyMask();
			},

			onComplete: function(error, xhr) {
				this.hideBodyMask();
				var documentId = xhr.response ? JSON.parse(xhr.response) : null;
				if (documentId) {
					var args = {
						sysProcessName: "CrocProcessShowNewGU12DocumentPage",
						parameters: {
							NewDocumentId: documentId
						},
						scope: this
					};
					ProcessModuleUtilities.executeProcess(args);
				}
			},

			upload: function(config) {
				this.Terrasoft.ConfigurationFileApi.upload(config);
			},
		}
	};
});
