 define("UserPageV2", ["ProcessModuleUtilities"],
	function(ProcessModuleUtilities) {
		return {
			entitySchemaName: "VwSysAdminUnit",
			messages: {},
			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
			diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
			attributes: {},
			rules: {},
			methods: {
				onSaveUser: function(response, next) {
					if (response) {
						response.message = response[this.getSaveUserMethodName() + "Result"];
						response.success = this.Ext.isEmpty(response.message);
						if (this.validateResponse(response)) {
							this.isNew = false;
							this.changedValues = null;
							this.cardSaveResponse = response;
							this.runContactMessageProcess(next);
						}
					}
				},

				runContactMessageProcess: function(callback) {
					let id = this.$Id;
					if(id) {
						this.showBodyMask();
						ProcessModuleUtilities.executeProcess({
							sysProcessName: "CrocProcessUserCreateSampleMessage",
							parameters: {
								UserId: id
							},
							callback: function() {
								this.hideBodyMask();
								callback.call(this);
							},
							scope: this
						});
					}
				},
			}
		};
	});