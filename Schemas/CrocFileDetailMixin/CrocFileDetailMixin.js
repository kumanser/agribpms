 define("CrocFileDetailMixin", ["CrocConstantsJS"], function(CrocConstantsJS) {
	/**
	 * @class Terrasoft.configuration.mixins.AccountPageMixin
	 * Implements common business logic for AccountPageV2 and AccountMiniPage.
	 */
	Ext.define("Terrasoft.configuration.mixins.CrocFileDetailMixin", {
		alternateClassName: "Terrasoft.CrocFileDetailMixin",

		getAllowS3Sections: function() {
			return ["CrocDocument", "CrocContract", "KnowledgeBase", "Account", "Activity", "CrocGrainSaleApplication", "CrocRequest", "CrocHelp"];
		},

		getAllowChooseType: function() {
			return ["CrocDocument", "CrocContract", "Account", "CrocGrainSaleApplication", "CrocRequest"];
		},

		getAllowVersionS3Sections: function() {
			return ["CrocDocument"];
		},

		getCardValues: function(entityName) {
			let cardValues = null;
			if (entityName === "CrocDocument") {
				cardValues = this.sandbox.publish("GetColumnsValues", ["CrocAccount", "CrocDocumentType"], [this.sandbox.id]);
			}
			else if (entityName === "CrocContract") {
				cardValues = this.sandbox.publish("GetColumnsValues", ["CrocAccount"], [this.sandbox.id]);
			}
			else if (entityName === "Account") {
				let account = this.sandbox.publish("GetColumnsValues", ["Id", "Name"], [this.sandbox.id]);
				cardValues = {
					CrocAccount: {
						value: account.Id,
						displayValue: account.Name,
					}
				}
			}
			else if (entityName === "CrocGrainSaleApplication") {
				cardValues = this.sandbox.publish("GetColumnsValues", ["CrocAccount"], [this.sandbox.id]);
			}
			else if (entityName === "CrocRequest") {
				cardValues = this.sandbox.publish("GetColumnsValues", ["CrocAccount"], [this.sandbox.id]);
			}
			return cardValues;
		},

		callSaveS3FileService: function(serviceConfig, callback, scope) {
			let request = {
				serviceName: "FilesS3Service",
				methodName: "SaveFile",
				data: {
					data: serviceConfig
				}
			};
			this.callService(request, callback, scope);
		},

		getFileTypeByDocumentType: function(documentType, callback, scope) {
			let esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
				rootSchemaName: "CrocFileType"
			});
			esq.addColumn("Name");
			if (documentType) {
				esq.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL,
					"CrocDocumentType", documentType.value));
			}
			esq.getEntityCollection(function (result) {
				if (result.success && result.collection.getCount() > 0) {
					let entities = result.collection.getItems();
					if (callback) callback.call(scope, {value: entities[0].$Id, displayValue: entities[0].$Name});
				}
				else callback.call(scope, null);
			}, this);
		},

		performDocumentSection: function(data, fileId, callback, scope) {
			let cardValues = this.sandbox.publish("GetColumnsValues", ["CrocAccount"], [this.sandbox.id]);
			let accountName = (cardValues.CrocAccount) ? cardValues.CrocAccount.displayValue : null;
			this.getFileTypeByDocumentType(cardValues.CrocDocumentType, callback, scope);
		},

		executeDocumentService: function(serviceConfig, accountName, fileType, fileId, callback, scope) {
			if (fileType) serviceConfig.file.columns = JSON.stringify({"CrocFileTypeId": fileType.value});
			serviceConfig.path = Terrasoft.getFormattedString("Контрагенты/{0}/{1}",
				(accountName) ? accountName.displayValue : "Общие файлы",
				(fileType) ? fileType.displayValue : "Общие документы");
			serviceConfig.version = {
				entitySchemaName : "CrocDocumentVersion",
				parentEntitySchemaName : "CrocDocumentFile",
				parentColumnName: "CrocDocumentFile",
				parentColumnValue: fileId,
				columnName: "CrocData",
				s3ColumnName: "CrocS3Link",
				versionColumnName: "CrocVersion",
				displayColumnName: "CrocName"
			};
			this.callSaveS3FileService(serviceConfig, callback, scope);
		},

		executeAccountService: function(serviceConfig, accountName, fileType, fileId, callback, scope) {
			if (fileType) serviceConfig.file.columns = JSON.stringify({"CrocFileTypeId": fileType.value});
			serviceConfig.path = Terrasoft.getFormattedString("Контрагенты/{0}/{1}",
				(accountName) ? accountName.displayValue : "Общие файлы",
				(fileType) ? fileType.displayValue : "Общие документы");
			this.callSaveS3FileService(serviceConfig, callback, scope);
		},

		executeSavingEntity: function(entityName, data, cardValues, fileId, callback, scope) {
			let serviceConfig = {file: data};
			serviceConfig.file.s3ColumnName = "CrocS3Link";
			if (entityName === "CrocDocument") {
				if (cardValues.CrocFileType) {
					this.executeDocumentService(serviceConfig, cardValues.CrocAccount, cardValues.CrocFileType, fileId, callback, scope);
				}
				this.getFileTypeByDocumentType(cardValues.CrocDocumentType, function(fileType) {
					this.executeDocumentService(serviceConfig, cardValues.CrocAccount, fileType, fileId, callback, scope);
				}, scope);
			}
			else if (entityName === "CrocContract") {
				let fileType = (cardValues.CrocFileType) ? cardValues.CrocFileType : {
					value: CrocConstantsJS.CrocFileType.Contract,
					displayValue: "Договор"
				}
				this.executeAccountService(serviceConfig, cardValues.CrocAccount, fileType, null, callback, scope);
			}
			else if (entityName === "KnowledgeBase") {
				serviceConfig.path = "База знаний";
				this.callSaveS3FileService(serviceConfig, callback, scope);
			}
			else if (entityName === "Account") {
				this.executeAccountService(serviceConfig, cardValues.CrocAccount, cardValues.CrocFileType, null, callback, scope);
			}
			else if (entityName === "Activity") {
				serviceConfig.path = "Активности";
				this.callSaveS3FileService(serviceConfig, callback, scope);
			}
			else if (entityName === "CrocGrainSaleApplication") {
				let fileType = (cardValues.CrocFileType) ? cardValues.CrocFileType : {
					value: CrocConstantsJS.CrocFileType.GrainSale
				}
				this.executeAccountService(serviceConfig, cardValues.CrocAccount, fileType, null, callback, scope);
			}
			else if (entityName === "CrocRequest") {
				let fileType = (cardValues.CrocFileType) ? cardValues.CrocFileType : {
					value: CrocConstantsJS.CrocFileType.Request
				}
				this.executeAccountService(serviceConfig, cardValues.CrocAccount, fileType, null, callback, scope);
			}
			else if (entityName === "CrocHelp") {
				serviceConfig.path = "Помощь";
				this.callSaveS3FileService(serviceConfig, callback, scope);
			}
			else callback.call(scope, null);
		}

	});
});