define("CrocPlannedRefund1Page", ["CrocConstantsJS"], function(CrocConstantsJS) {
	return {
		entitySchemaName: "CrocPlannedRefund",
		attributes: {
			TotalSettlement: {
				dataType: Terrasoft.DataValueType.MONEY,
				value: 0
			}
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocManager": {
				"fd420fcf-6a01-478b-9515-8b6d91c73451": {
					"uId": "fd420fcf-6a01-478b-9515-8b6d91c73451",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "CrocContract",
							"attributePath": "CrocAssignedManager"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocContract"
							}
						}
					]
				}
			},
			"CrocAccount": {
				"8050d32d-3f7f-458d-9cea-276831f38e6e": {
					"uId": "8050d32d-3f7f-458d-9cea-276831f38e6e",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "CrocContract",
							"attributePath": "CrocAccount"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocContract"
							}
						}
					]
				}
			},
			"CrocProduct": {
				"2f0eae72-97f7-4842-be1d-157d032922f1": {
					"uId": "2f0eae72-97f7-4842-be1d-157d032922f1",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "CrocContract",
							"attributePath": "CrocProduct"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocContract"
							}
						}
					]
				}
			},
			"CrocReturnDate": {
				"6d62309c-ea62-4c6e-abad-bf2e9c013754": {
					"uId": "6d62309c-ea62-4c6e-abad-bf2e9c013754",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocIsBlocked"
							},
							"rightExpression": {
								"type": 0,
								"value": false,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"CrocRefundAmount": {
				"ef837223-6258-41f0-a574-32caccfa0942": {
					"uId": "ef837223-6258-41f0-a574-32caccfa0942",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocIsBlocked"
							},
							"rightExpression": {
								"type": 0,
								"value": false,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"CrocRefundReason": {
				"df5f0d58-dad1-496c-8ddf-c0fab391f739": {
					"uId": "df5f0d58-dad1-496c-8ddf-c0fab391f739",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocIsBlocked"
							},
							"rightExpression": {
								"type": 0,
								"value": false,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"CrocComment": {
				"544b7fe8-cf7f-405e-8c63-abf10bfe8672": {
					"uId": "544b7fe8-cf7f-405e-8c63-abf10bfe8672",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocIsBlocked"
							},
							"rightExpression": {
								"type": 0,
								"value": false,
								"dataValueType": 12
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.fillSettlement();
			},

			fillSettlement: function() {
				var contract = this.get("CrocContract");
				if(contract) {
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "CrocMutualSettlements"
					});
					esq.addAggregationSchemaColumn("CrocAmount", Terrasoft.AggregationType.SUM, "SAmountSum");
					esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
						"CrocContractDS", contract.value, Terrasoft.DataValueType.GUID));
					esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
						"CrocSettlementType", CrocConstantsJS.CrocSettlementType.Incoming, Terrasoft.DataValueType.GUID));
					esq.getEntityCollection(function(response) {
						if (response.success) {
							if (response.collection.getCount() > 0) {
								var selectResult = response.collection.getByIndex(0);
								var amount = selectResult.get("SAmountSum");
								if (amount > 0) {
									this.$TotalSettlement = amount;
								}
							}
						}
					}, this);
				}
			},

			asyncValidate: function(callback, scope) {
				this.callParent([function(response) {
					if (!this.validateResponse(response)) {
						return;
					}
					Terrasoft.chain(
						function(next) {
							this.validateAccounting(function(response) {
								if (this.validateResponse(response)) {
									next();
								}
							}, this);
						},
						function(next) {
							callback.call(scope, response);
							next();
						}, this);
				}, this]);
			},

			validateAccountingOLD:  function(callback, scope) {
				var result = {
					success: true
				};
				var contract = this.get("CrocContract");
				var currentAmount = this.get("CrocRefundAmount");
				if (this.isNewMode() || this.changedValues.CrocRefundAmount) {
					var selectRefund = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "CrocPlannedRefund"
					});
					selectRefund.addAggregationSchemaColumn("CrocRefundAmount", Terrasoft.AggregationType.SUM, "AmountSum");
					selectRefund.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
						"CrocContract", contract.value, Terrasoft.DataValueType.GUID));
					selectRefund.getEntityCollection(function(response) {
						var totalAmount = currentAmount;
						if (response.success) {
							if (response.collection.getCount() > 0) {
								var selectResult = response.collection.getByIndex(0);
								var otherRefundAmount = selectResult.get("AmountSum");
								if (otherRefundAmount > 0) {
									totalAmount += otherRefundAmount;
								}
							}
						}
						if(totalAmount > this.$TotalSettlement)
						{
							result.message = this.get("Resources.Strings.RefundLimitError");
							result.success = false;
						}
						callback.call(scope || this, result);
					}, this);
				} else {
					callback.call(scope || this, result);
				}
			},

			validateAccounting:  function(callback, scope) {
				var result = {
					success: true
				};
				var contract = this.get("CrocContract");
				var currentAmount = this.get("CrocRefundAmount");
				if (this.isNewMode() || this.changedValues.CrocRefundAmount) {
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "CrocMutualSettlements"
					});
					esq.addColumn("CrocAmount");
					esq.addColumn("CrocSettlementType");
					esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
						"CrocContractDS", contract.value, Terrasoft.DataValueType.GUID));
					esq.getEntityCollection(function(response) {
						if (response.success) {
							if (response.collection.getCount() > 0) {
								let collection = response.collection;
								let amountIn = 0;
								let amountOut = 0
								Terrasoft.each(collection, function(item) {
									if(item.get("CrocSettlementType").value === CrocConstantsJS.CrocSettlementType.Incoming) {
										amountIn += item.get("CrocAmount");
									} else {
										amountOut += item.get("CrocAmount");
									}
								}, this)
								var amount = amountIn - amountOut;
								if(currentAmount > amount)
								{
									result.message = this.get("Resources.Strings.RefundLimitError");
									result.success = false;
								}
							}
							callback.call(scope || this, result);
						}
					}, this);
				} else {
					callback.call(scope || this, result);
				}
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocNumber824d82b1-b51b-42ac-a65f-708faa525526",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocNumber",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocClosingDate664f313b-2399-4d60-a9c3-7ba6804cd71b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocClosingDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocReturnDatef42d8586-98da-46bb-a9b5-48d47fa66ea5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocReturnDate",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocIsClosed31436727-9ddd-41e6-9f1a-c74c65825284",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocIsClosed",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocRefundAmount35a6b9b9-01df-4dc0-8784-16b1ff982906",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocRefundAmount",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocRefundCloseReason7d66c479-e6fd-4d2a-90ae-43b4027a400b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocRefundCloseReason",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocRefundReason8fd5eb24-9f0b-4e60-9efe-36ba2b6ef6ed",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocRefundReason"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocManagera19bdb41-11b9-4453-bf33-2952286d10c4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocManager",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocCommenta2a16124-afc3-4b83-90d4-003430cd746d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 3,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocComment",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "CrocProducte0f44e42-00c5-4ac7-8166-611ea0fcd93c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocProduct",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "CrocAccount27360a34-92b8-409d-8784-ec59d168696f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "CrocAccount",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "CrocIsBlockedd4208068-1cb0-4761-b2f9-2694c68ec6f5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "CrocIsBlocked",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			}
		]/**SCHEMA_DIFF*/
	};
});
