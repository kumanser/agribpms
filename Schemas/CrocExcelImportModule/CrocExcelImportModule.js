define("CrocExcelImportModule", ["ext-base", "terrasoft"],
    function(Ext, Terrasoft) {

        function GetTemplateImportParameters() {
            let config = {
                NeedSendNotify: true,
                rootSchemaUId: "",
                notImportedRowsCount: 0,
                importedRowsCount: 0,
                totalRowsCount: 0,
                chunkSize: 0,
                authorId: "00000000-0000-0000-0000-000000000000",
                authorTimeZoneInfo: null,
                columns: [],
                importObject: {
                    uId: "", //SchemaUId
                    name: "", //SchemaName
                    caption: "", //SchemaCaption
                    isOtherObject: true
                },
                canUseImportEntitiesStorage: false,
                fileName: "",
                headerColumnsValues: null,
                importTags: null
            };
            return config;
        }

        function GetTemplateColumnConfig() {
            let config = {
                index: "", //Excel column
                source: "", //Excel column caption
                destinations: [] //Columns where put value
            }
            return config;
        }

        function GetTemplateDestinationConfig() {
            let config = {
                schemaUId: "", //Column parent schemaUId
                columnName: "",
                columnValueName: "",
                isKey: true,
                properties: {}, // {"TypeUId": ""} Column type UId
                attributes: {}
            }
            return config;
        }

        function GetTemplateDetailConfig() {
            let config = {
                masterRecord: "",
                masterColumn: "",
                clearOldRows: false
            }
            return config;
        }

        function FindSchemaByName(name) {
            var filteredCollection = Terrasoft.EntitySchemaManager.items.filterByFn(function(item) {
                return (!item.getExtendParent() && (item.getName() === name));
            });
            return filteredCollection.isEmpty() ? null : filteredCollection.getByIndex(0);
        }

        function GetModalBoxConfig() {
            let config = {
                heightPixels: 450,
                widthPixels: 650,
                disableScroll: true
            };
            return config;
        }

        return {
            getTemplateImportParameters: GetTemplateImportParameters,
            getTemplateColumnConfig: GetTemplateColumnConfig,
            getTemplateDestinationConfig: GetTemplateDestinationConfig,
            findSchemaByName: FindSchemaByName,
            getTemplateDetailConfig: GetTemplateDetailConfig,
            getModalBoxConfig: GetModalBoxConfig
        }
    });