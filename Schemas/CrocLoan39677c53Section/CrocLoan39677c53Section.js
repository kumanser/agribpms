define("CrocLoan39677c53Section", ["ProcessModuleUtilities"], function(ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocLoan",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {

			getSectionActions: function() {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					Type: "Terrasoft.MenuSeparator"
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					Caption: this.get("Resources.Strings.RequestLoanButtonCaption"),
					Click: {bindTo: "onRequestLoanClicked"},
					Enabled: true
				}));
				return actionMenuItems;
			},

			onRequestLoanClicked: function() {
				var args = {
					sysProcessName: "CrocProcessLoanRequestActivityCreation",
				};
				ProcessModuleUtilities.executeProcess(args);
			}

		}
	};
});
