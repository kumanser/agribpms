define("CrocErrand1Page", ["ServiceHelper"], function(ServiceHelper) {
	return {
		entitySchemaName: "CrocErrand",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"CrocRegisterEmptyCar1Detailf477c702": {
				"schemaName": "CrocRegisterEmptyCar1Detail",
				"entitySchemaName": "CrocRegisterEmptyCar",
				"filter": {
					"detailColumn": "CrocErrand",
					"masterColumn": "Id"
				}
			},
			"CrocVehicleRegister1Detail56aa9157": {
				"schemaName": "CrocVehicleRegister1Detail",
				"entitySchemaName": "CrocVehicleRegister",
				"filter": {
					"detailColumn": "CrocErrand",
					"masterColumn": "Id"
				}
			},
			"CrocQuoteRequest1Detail779c1942": {
				"schemaName": "CrocQuoteRequest1Detail",
				"entitySchemaName": "CrocQuoteRequest",
				"filter": {
					"detailColumn": "CrocErrand",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocReasonRejection": {
				"b12d4156-1411-4609-8330-59a543d969cb": {
					"uId": "b12d4156-1411-4609-8330-59a543d969cb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeviation"
							},
							"rightExpression": {
								"type": 0,
								"value": 0,
								"dataValueType": 5
							}
						}
					]
				},
				"16296166-ff45-453f-8d16-6f3eca6e2a30": {
					"uId": "16296166-ff45-453f-8d16-6f3eca6e2a30",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeviation"
							},
							"rightExpression": {
								"type": 0,
								"value": 0,
								"dataValueType": 5
							}
						}
					]
				}
			},
			"CrocPaymentStatus": {
				"7bf3f1a9-9b8f-4e57-ab05-e64c49058da2": {
					"uId": "7bf3f1a9-9b8f-4e57-ab05-e64c49058da2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			onSendEmptyVehiclesClick: function() {
				var rowId = this.get("Id");
				ServiceHelper.callService({
					serviceName: "ExchangeIntegrationService",
					methodName: "SendEmptyVehicles",
					data: {
						recordId: rowId
					},
					scope: this
				});
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocNumberf1c7cc8e-446d-406a-a5d0-99a22f570881",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocNumber",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDate23fb7cb3-2f61-4954-81c3-bf37c5026acd",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocDate",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocStatus92de8022-358c-4e6a-add6-12fe916e37c3",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocPaymentStatus83671944-c3a6-40f6-bb59-b8249978e9c1",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocPaymentStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tabfc798b51TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabfc798b51TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabfc798b51TabLabelGroup9565012c",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabfc798b51TabLabelGroup9565012cGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabfc798b51TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabfc798b51TabLabelGroup9565012c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocLoadingAddress386854cb-a459-47ee-98a2-8a05d7b73882",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocLoadingAddress",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocUnloadingAddressa420aba3-5add-4eeb-a322-a23fd784bb49",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocUnloadingAddress",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocProductcc38d5bb-d953-47c6-bf5b-280439e3bbea",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocProduct",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP3518c39a-06f0-4702-8899-c6ff3ec1f2cf",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocProductClass",
					"enabled": false,
					"contentType": 3
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocLoadingPlanTon0a7623b5-1e40-495e-a1ab-99cc4936f5b5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocLoadingPlanTon",
					"enabled": false
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocFreightRate1892672c-e090-45fc-a540-bb346a986e00",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocFreightRate",
					"enabled": false
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocLoadingFactTon67125fd0-60ee-4700-8387-61fdeeab8d7a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocLoadingFactTon",
					"enabled": false
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocCargoCost274bf24f-5e16-430b-b825-a0f1a06e2b7a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocCargoCost",
					"enabled": false
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocUnloadingTon5f1563f2-34e0-4751-ab69-28f18efee085",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocUnloadingTon",
					"enabled": false
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "CrocExecutionbcee197c-3326-46e9-ba53-e60de4b409be",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocExecution",
					"enabled": false
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "CrocDeviation982cc9e3-1ad1-463d-88aa-73d21a737498",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocDeviation",
					"enabled": false
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "CrocReasonRejectionf747dc07-3427-4e0c-99ab-29eddd0ff413",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Tabfc798b51TabLabelGridLayoutf438c32b"
					},
					"bindTo": "CrocReasonRejection",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tabfc798b51TabLabelGridLayoutf438c32b",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "CrocRegisterEmptyCar1Detailf477c702",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabfc798b51TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocVehicleRegister1Detail56aa9157",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabfc798b51TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocQuoteRequest1Detail779c1942",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabfc798b51TabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
