define("LinkPageV2", [], function () {
    return {
        details: /**SCHEMA_DETAILS*/{
            "CrocDocumentVersionDetail": {
                "schemaName": "CrocDocumentVersionDetail",
                "entitySchemaName": "CrocDocumentVersion",
                "filter": {
                    "masterColumn": "Id",
                    "detailColumn": "CrocDocumentFile"
                }
            },
        }/**SCHEMA_DETAILS*/,
        attributes: {},
        messages: {},
        modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
        diff: /**SCHEMA_DIFF*/[
            {
                "operation": "insert",
                "name": "TabsContainer",
                "parentName": "HeaderContainer",
                "propertyName": "items",
                "values": {
                    "itemType": Terrasoft.ViewItemType.CONTAINER,
                    "items": []
                }
            },
            {
                "operation": "insert",
                "name": "CrocDocumentVersionDetail",
                "values": {
                    "itemType": 2,
                    "markerValue": "added-detail"
                },
                "parentName": "TabsContainer",
                "propertyName": "items",
                "index": 0
            },
        ], /**SCHEMA_DIFF*/
        methods: {},
        rules: {}
    };
});