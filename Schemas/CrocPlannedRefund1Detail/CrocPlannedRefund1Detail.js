define("CrocPlannedRefund1Detail", ["CrocPlannedRefund1DetailResources","ServiceHelper"], function(resources, ServiceHelper) {
	return {
		entitySchemaName: "CrocPlannedRefund",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			/*
			addToolsButtonMenuItems: function(toolsButtonMenu) {
				toolsButtonMenu.addItem(this.getIntegrationServiceButton());
				toolsButtonMenu.addItem(this.getButtonMenuSeparator());
				this.callParent(arguments);
			},
			 */

			getIntegrationServiceButton: function() {
				return this.getButtonMenuItem({
					Caption: {"bindTo": "Resources.Strings.IntegrationServiceButtonCaption"},
					Click: {"bindTo": "setIntegrationServiceRun"},
					Enabled: {"bindTo": "setIntegrationServiceMenuActive"}
				});
			},

			setIntegrationServiceMenuActive: function() {
				var selected = this.getSelectedItems();
				return selected;
			},

			setIntegrationServiceRun: function() {
				var activeRow = this.getActiveRow();
				if(activeRow) {
					var rowId = activeRow.get("Id");
					ServiceHelper.callService({
						serviceName: "ExchangeIntegrationService",
						methodName: "SendPlanRefund",
						data: {
							refundId: rowId
						},
						scope: this
					});
				}
			}
		}
	};
});
