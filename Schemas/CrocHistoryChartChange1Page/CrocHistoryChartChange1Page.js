define("CrocHistoryChartChange1Page", [], function() {
	return {
		entitySchemaName: "CrocHistoryChartChange",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"CrocDeliveryScheduleHistory1Detail19979a64": {
				"schemaName": "CrocDeliveryScheduleHistory1Detail",
				"entitySchemaName": "CrocDeliveryScheduleHistory",
				"filter": {
					"detailColumn": "CrocHistoryChartChange",
					"masterColumn": "Id"
				}
			},
			"CrocLoadingScheduleHistory1Detail99144c04": {
				"schemaName": "CrocLoadingScheduleHistory1Detail",
				"entitySchemaName": "CrocLoadingScheduleHistory",
				"filter": {
					"detailColumn": "CrocHistoryChartChange",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CreatedOn40b46ce7-8beb-47ed-af40-605853113bcf",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CreatedOn",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocChartStatuse43bbce8-1ed5-41de-b34e-550fc5223c9f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocChartStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocChartChangeReasonbe19b527-c752-4635-96ba-4fe2622ea6ba",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocChartChangeReason",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab121561b1TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab121561b1TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryScheduleHistory1Detail19979a64",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab121561b1TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocLoadingScheduleHistory1Detail99144c04",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab121561b1TabLabel",
				"propertyName": "items",
				"index": 1
			}
		]/**SCHEMA_DIFF*/
	};
});
