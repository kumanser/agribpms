namespace Terrasoft.Core.Process.Configuration
{
	using DocumentFormat.OpenXml.Office2010.Excel;
	using DocumentFormat.OpenXml.Presentation;
	using FastReport.Export.Dxf.Sections.Entities;
	using Nest;
	using System;
	using System.Data;
	using Terrasoft.Common;
	using Terrasoft.Configuration;
	using Terrasoft.Configuration.Utils;
	using Terrasoft.Core.DB;
	using Terrasoft.Core.Entities;
	using Terrasoft.Core.Process;

	#region Class: CrocCreateTextByTemplate

	/// <exclude/>
	public partial class CrocCreateTextByTemplate
	{


		#region Methods: Protected

		public (string, string) GetTemplateData(Guid templateId)
		{
			(string EntityName, string Body) result = (string.Empty, string.Empty);
			EntitySchemaQuery esq = new EntitySchemaQuery(UserConnection.EntitySchemaManager, "EmailTemplate");
			esq.UseAdminRights = false;
			esq.AddColumn("Object.Name");
			esq.AddColumn("CrocTemplateBody");
			esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal, "Id", templateId));
			EntityCollection collection = esq.GetEntityCollection(UserConnection);
			if (collection.Count > 0)
			{
				result.EntityName = collection[0].GetTypedColumnValue<string>("Object_Name");
				result.Body = collection[0].GetTypedColumnValue<string>("CrocTemplateBody");
			}
			return result;
		}

		protected override bool InternalExecute(ProcessExecutingContext context) {
			(string EntityName, string Body) result = GetTemplateData(CrocEmailTemplate);
			MacrosHelperV2 macroHelper = new GlobalMacrosHelper();
			macroHelper.UserConnection = UserConnection;
			MacrosHelperService service = new MacrosHelperService(macroHelper, UserConnection);
			MacrosHelperServiceResponse resonse = service.GetTemplate(new MacrosHelperServiceRequest()
			{
				EntityId = CrocRecordId,
				EntityName = result.EntityName,
				TemplateId = CrocEmailTemplate,
				TextTemplate = result.Body
			});
			CrocText = resonse.TextTemplate;
			return true;
		}

		#endregion

		#region Methods: Public

		public override bool CompleteExecuting(params object[] parameters) {
			return base.CompleteExecuting(parameters);
		}

		public override void CancelExecuting(params object[] parameters) {
			base.CancelExecuting(parameters);
		}

		public override string GetExecutionData() {
			return string.Empty;
		}

		public override ProcessElementNotification GetNotificationData() {
			return base.GetNotificationData();
		}

		#endregion

	}

	#endregion

}

