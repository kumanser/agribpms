define("CrocReplacementEmployeeClientUnit", ["CrocConstantsJS"], function(CrocConstantsJS) {
	return {
		entitySchemaName: "",
		attributes: {
			"CrocExporterEmployeeRole": {
				"lookupListConfig": {
					"filters": [
						function() {
							var filterGroup = Ext.create("Terrasoft.FilterGroup");
							var idList = [CrocConstantsJS.CrocEmployeeRole.PurchasingManager.value, CrocConstantsJS.CrocEmployeeRole.ManagerSTO.value];
							filterGroup.addItem(Terrasoft.createColumnInFilterWithParameters("Id", idList));
							return filterGroup;
						}
					]
				}
			},
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocCurrentEmployee": {
				"ba697b37-bbf8-40c5-9d0a-771156d81fb1": {
					"uId": "ba697b37-bbf8-40c5-9d0a-771156d81fb1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 5,
								"attribute": "CrocStringContractId"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "Button-be6148b819154a0791eaee8f1635d859",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.SaveDataButtonCaption"
					},
					"click": {
						"bindTo": "onSaveButtonClick"
					},
					"enabled": true
				}
			},
			{
				"operation": "insert",
				"name": "CrocCurrentEmployee",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocCurrentEmployee",
					"enabled": true,
					"contentType": 5,
					"isRequired": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocStringContractId",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocStringContractId",
					"enabled": true,
					"visible": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocReplacementEmployee",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocReplacementEmployee",
					"enabled": true,
					"contentType": 5,
					"isRequired": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocExporterEmployeeRole",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocExporterEmployeeRole",
					"enabled": true,
					"contentType": 3,
					"isRequired": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocReplacementEndDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocReplacementEndDate",
					"enabled": true,
					"isRequired": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			}
		]/**SCHEMA_DIFF*/
	};
});
