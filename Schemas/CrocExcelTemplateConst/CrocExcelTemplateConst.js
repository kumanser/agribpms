define("CrocExcelTemplateConst", ["CrocConstantsJS"], function (CrocConstantsJS) {
	var excelTemplates = {
		ExcelCPT: {
			entitySchemaName: "CrocContract",
			excelReportName: "Отчет по СРТ поставке (АВТО)",
			rowOffset: 0,
			columns: [
				{ path: "CrocAccount.Name", displayName: "Поставщик", position: 0, isVisible: true},
				{ path: "CrocContractNumber", displayName: "ДС", position: 1, isVisible: true},
				{ path: "CrocTerminal.Name", displayName: "Склад получатель", position: 2, isVisible: true},
				{ path: "CrocProduct.Name", displayName: "Номенклатура", position: 3, isVisible: true},
				{ path: "CrocQuantityPlanTon", displayName: "Объем по контракту, тн", position: 4, isVisible: true},
				{ path: "CrocQuantityFactTon", displayName: "Поставлено", position: 5, isVisible: true},
				{ path: "CrocProteinPlan", displayName: "Среднее качество, протеин", position: 6, isVisible: true},
				{ path: "CrocBalanceDeliveredTon", displayName: "Остаток к поставке", position: 7, isVisible: true},
				{
					path: "[Activity:CrocCrocContract:Id].CrocCarRequest",
					displayName: "Квота запрос, ТС",
					func: "1",
					isAggregate: true,
					position: 8,
					isVisible: true
				},
				{
					path: "[Activity:CrocCrocContract:Id].CrocCarAllocate",
					displayName: "Квота согласована, ТС",
					func: "1",
					isAggregate: true,
					position: 9,
					isVisible: true
				},
				{ path: "CrocFactDeliveryTransport", displayName: "Факт завоза, ТС", position: 10, isVisible: true},
				{
					path: "[CrocChart:CrocContract:Id].CrocChartDeviationTon",
					displayName: "Отклонение от графика первичного от Поставщика",
					func: "1",
					isAggregate: true,
					position: 11,
					isVisible: true
				}
			]
		},
		ExcelFCA: {
			entitySchemaName: "CrocContract",
			excelReportName: "Отчет по FCA поставке (АВТО)",
			rowOffset: 0,
			columns: [
				{ path: "CrocAccount.Name", displayName: "Поставщик", position: 0, isVisible: true},
				{ path: "CrocContractNumber", displayName: "ДС", position: 1, isVisible: true},
				{ path: "CrocTerminal.Name", displayName: "Склад получатель", position: 2, isVisible: true},
				{ path: "CrocProduct.Name", displayName: "Номенклатура", position: 3, isVisible: true},
				{ path: "CrocQuantityPlanTon", displayName: "Объм по контракту, тн", position: 4, isVisible: true},
				{ path: "CrocQuantityFactTon", displayName: "Отгружено", position: 5, isVisible: true},
				{ path: "CrocProteinPlan", displayName: "Качество по контракту", position: 6, isVisible: true},
				{ path: "CrocRemainingExportedTon", displayName: "Остаток к отгрузке", position: 7, isVisible: true},
			]
		},
		ExcelRailWay: {
			entitySchemaName: "CrocDocument",
			excelReportName: "Отчет ЖД",
			rowOffset: 2,
			templateFilter: [
				{
					path: "CrocDocumentType.Id",
					value: CrocConstantsJS.CrocDocumentType.ApplicationGU12.value
				}
			],
			columns: [
				{ path: "CrocChart.CrocDepartureStation.Name", displayName: "Станция отправления", position: 0, isVisible: true},
				{ path: "CrocChart.CrocDeliveryCondition.Name", displayName: "Базис поставки", position: 1, isVisible: true},
				{ path: "CrocChart.CrocShipper.Name", displayName: "Грузоотправитель", position: 2, isVisible: true},
				{ path: "CrocChart.CrocConsignee.Name", displayName: "Грузополучатель", position: 3, isVisible: true},
				{ path: "CrocGU12Number", displayName: "№ ГУ-12", position: 4, isVisible: true},
				{ path: "CrocGU12StartDate", displayName: "Начало действия заявки", dateFormat: "dd.MM.yyyy", position: 5, isVisible: true},
				{ path: "CrocGU12EndDate", displayName: "Завершение действия заявки", dateFormat: "dd.MM.yyyy", position: 6, isVisible: true},
				{ path: "CrocChart.CrocContract.CrocProduct.Name", displayName: "Наименование груза", position: 7, isVisible: true},
				{ path: "CrocChart.CrocContract.CrocProductClass.Name", displayName: "Класс груза", position: 8, isVisible: true},
				{ path: "CrocChart.CrocContract.CrocProteinPlan", displayName: "Протеин", position: 9, isVisible: true},
				{
					path: "[CrocLoadingSchedule:CrocChart:CrocChart].CrocLoadingTonPlan",
					displayName: "Масса груза по ГУ, кг",
					position: 10,
					concatenateColumns: [],
					func: "1",
					isAggregate: true,
					isVisible: true
				},
				{
					path: "[CrocLoadingSchedule:CrocChart:CrocChart].CrocLoadingTransporPlan",
					displayName: "Количество вагонов по ГУ",
					position: 11,
					concatenateColumns: [],
					func: "1",
					isAggregate: true,
					isVisible: true
				},
				{
					path: "[CrocRegisterLoading:CrocChart:CrocChart].CrocShippingDate",
					displayName: "Даты отгрузки",
					position: 12,
					concatenateColumns: [],
					concatenateValues: true,
					dateFormat: "dd.MM.yyyy",
					separator: ',',
					isVisible: true
				},
				{
					path: "[CrocRegisterLoading:CrocChart:CrocChart].CrocGrossWeightTon",
					displayName: "Отгружено по жд накладной, за прошедшие сутки, тн",
					position: 13,
					concatenateColumns: [],
					concatenateValues: false,
					isVisible: true,
					isRailWayColumn: true
				},
				{
					path: "[CrocRegisterLoading:CrocChart:CrocChart].CrocGrossWeightTon",
					displayName: "Отгружено по жд накладной с нарастающим итогом, тн",
					position: 14,
					concatenateColumns: [],
					func: "1",
					isAggregate: true,
					isVisible: true
				},
				{ path: "", displayName: "Доступно к перевозке", position: 15, isOperationColumn: true, operationColumns: [10,14], operator: 2, isVisible: true},
				{
					path: "[CrocRegisterUnloading:CrocChart:CrocChart].CrocNetWeightAcceptanceTon",
					displayName: "Принято в порту, тн",
					position: 16,
					concatenateColumns: [],
					func: "1",
					isAggregate: true,
					isVisible: true
				},
				{ path: "", displayName: "В пути, тн", position: 17, isOperationColumn: true,operationColumns: [14,16], operator: 2, isVisible: true},
				{ path: "CurrentDate", displayName: "Дата формирования отчета", position: -1, isTechnicalColumn: true}
			]
		},
		ExcelGU12: {
			entitySchemaName: "CrocDocument",
			excelReportName: "Согласование ГУ-12",
			hasIdColumn: true,
			templateFilter: [
				{
					path: "CrocDocumentType.Id",
					value: CrocConstantsJS.CrocDocumentType.ApplicationGU12.value
				}
			],
			columns: [
				{ path: "CrocChart.CrocDepartureStation.CrocCode", displayName: "Код станции отправления", position: 1, isVisible: true},
				{ path: "CrocChart.CrocDepartureStation.Name", displayName: "Наименование станции отправления", position: 2, isVisible: true},
				{ path: "CrocGU12Number", displayName: "ГУ-12 заявка №", position: 3, isVisible: true},
				{ path: "CrocGU12Status.Name", displayName: "Статус согласования", position: 4, isVisible: true},
				{ path: "CrocChart.CrocProduct.Name", displayName: "Наименование груза", position: 5, isVisible: true},
				{ path: "CrocChart.CrocConsignorGU12", displayName: "Грузоотправитель", position: 6, isVisible: true},
				{ path: "CrocChart.CrocWeight", displayName: "Количество тонн/ваг", position: 7, isVisible: true},
				{
					path: "[CrocLoadingSchedule:CrocChart:CrocChart].CrocLoadingTransporFact",
					displayName: "Колличество вагонов",
					position: 8,
					concatenateColumns: [],
					func: "1",
					isAggregate: true,
					isVisible: true
				},
				{ path: "[CrocSetDocument:CrocDocument:Id].CrocContract.CrocProteinPlan", displayName: "Протеин", position: 9, isVisible: true},
				{ path: "", displayName: "Период завоза", position: 10, concatenateColumns: [13,14], separator: '-', dateFormat: "MMMM yyyy",isVisible: true},
				{ isExcelFormula: true, formula: "SUM", position: 11, formulaPosition: 7, isVisible: true},
				{ isExcelFormula: true, formula: "SUM", position: 12, formulaPosition: 8, isVisible: true},
				{
					path: "[CrocLoadingSchedule:CrocChart:CrocChart].CrocShipmentDate",
					displayName: "Факт погрузки, ТС",
					position: 13,
					func: "3",
					isAggregate: true,
					isVisible: false
				},
				{
					path: "[CrocLoadingSchedule:CrocChart:CrocChart].CrocShipmentDate",
					displayName: "Факт погрузки, ТС",
					position: 14,
					func: "4",
					isAggregate: true,
					isVisible: false
				},
			]
		},
		ExcelVED: {
			entitySchemaName: "CrocPositionPlan",
			excelReportName: "План позиций ВЭД",
			hasMonthColumn: true,
			rowOffset: 1,
			columns: [
				{ path: "CrocExporter.Name", displayName: "Shipper", position: 1, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocCulture.Name", displayName: "Cargo", position: 2, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocStartLoadingDate", dateFormat: "dd.MM.yyyy",displayName: "Loading start date", position: 3, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocEndLoadingDate", dateFormat: "dd.MM.yyyy", displayName: "Loading end date ", position: 4, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocPlannedVolume", displayName: "Plnnd Pos. Qnt, mt", position: 5, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocCustomer.Name", displayName: "Buyer/CAK#", position: 6, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocStartSaleDate", dateFormat: "dd.MM.yyyy", displayName: "Sales start date", position: 7, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocEndSaleDate", dateFormat: "dd.MM.yyyy", displayName: "Sales end date", position: 8, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocContractVolume", displayName: "Contract Qnt, mt", position: 9, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocPlannedArrivalDate", dateFormat: "dd.MM.yyyy", displayName: "ETA/NOR", position: 10, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocShipCapacity", displayName: "Cargo plan", position: 11, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocShipName", displayName: "Vessel Name", position: 12, isVisible: true},
				{ path: "[CrocPosition:CrocPositionPlan:Id].CrocShipStatus.Name", displayName: "Status", position: 13, isVisible: true},
			]
		},
	};
	
    return {
        CrocExcelTemplates: excelTemplates
    };
});
