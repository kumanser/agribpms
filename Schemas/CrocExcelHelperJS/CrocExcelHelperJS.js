 define("CrocExcelHelperJS", [], function () {
	
	var excelHelper = {
		downloadFile: function (caption, key) {
			 var report = document.createElement("a");
			 report.href = "../rest/CrocExcelGeneratorService/GetFile/" + key;
			 report.download = caption + "xlsx";
			 document.body.appendChild(report);
			 report.click();
			 document.body.removeChild(report);
	 	}
	};
	
    return {
        CrocExcelHelper: excelHelper
    };
});
