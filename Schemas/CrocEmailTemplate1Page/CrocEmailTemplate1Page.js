define("CrocEmailTemplate1Page", ["StructureExplorerUtilities", "CrocTemplateTextEdit"], function(StructureExplorerUtilities) {
	return {
		entitySchemaName: "EmailTemplate",
		mixins: {},
		attributes: {
			"Object": {
				lookupListConfig: {
					columns: ["Name"]
				}
			}
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			openColumnExplorer: function(callback, scope) {
				var config = {
					useBackwards: false,
					firstColumnsOnly: false,
					schemaName: this.$Object.Name
				};
				var handler = function(args) {
					callback.call(scope, args);
				};
				StructureExplorerUtilities.Open(this.sandbox, config, handler, this.renderTo, this);
			},

			onOpenMacrosTemplate: function(control) {
				if (this.$Object) {
					this.openColumnExplorer(function (result) {
						let macro = Terrasoft.getFormattedString("[#{0}#]", result.leftExpressionColumnPath);
						this.$CrocTemplateBody += macro;
					}, this);
				}
				else this.showInformationDialog(this.get("Resources.Strings.MissingObjectMessage"));
			},

			/**
			 * On select item callback.
			 * @param {Object} args
			 * @param {Object} result
			 * @protected
			 */
			onOpenMacrosTemplateCallback: function(args, result) {
				var selectedItems = args.selectedRows.getItems();
				if (!selectedItems.length) {
					return;
				}
				var item = {
					body: selectedItems[0].Body,
					value: selectedItems[0].Name,
					isFromButton: true
				};
				const extendedHtmlEditModule = result.control;
				extendedHtmlEditModule.setTrackingValue(item, function(bodyValue) {
					bodyValue = bodyValue || extendedHtmlEditModule.getBodyValue() || Terrasoft.emptyString;
					this.set("Body", bodyValue.trim());
				}, this);
			}

		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "Namea758afdf-01ff-4ee8-8503-b092f81b83a9",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "Name"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Objectb4eb11d4-5a4d-4c2d-860d-1dc2431804ee",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "Object"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "TemplateTypefcf6c693-6c83-4673-bafb-8318aa8df295",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "TemplateType"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabc28e655dTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "TemplateControlGroup",
				"values": {
					"itemType": 15,
					"items": []
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocTemplateBody",
				"values": {
					"generateId": false,
					"className": "Terrasoft.CrocTemplateTextEdit",
					"itemType": 3,
					"dataValueType": 1,
					"contentType": 0,
					"labelConfig": {
						"visible": false
					},
					"value": {
						"bindTo": "CrocTemplateBody"
					},
					"placeholder": {
						"bindTo": "CrocTemplateBody"
					},
					"openMacrosTemplate": {
						"bindTo": "onOpenMacrosTemplate"
					},
					"height": "200px"
				},
				"parentName": "TemplateControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 1
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
