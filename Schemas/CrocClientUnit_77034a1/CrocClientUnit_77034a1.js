define("CrocClientUnit_77034a1", ["CrocConstantsJS"], function(CrocConstantsJS) {
	return {
		entitySchemaName: "",
		attributes: {
			"CrocLoanCategory": {
				lookupListConfig: {
					filter: function () {
						var filterGroup = new this.Terrasoft.createFilterGroup();
						filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.OR;
						filterGroup.add("FirstId", Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "Id", CrocConstantsJS.CrocLoanCategory.ReleaseLoanRequest));
						filterGroup.add("SecondId", Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "Id", CrocConstantsJS.CrocLoanCategory.ReceiveLoanRequest));
						return filterGroup;
					}
				}
			}
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			setValidationConfig: function() {
				this.callParent(arguments);
				this.addColumnValidator("CrocMaturityDate", this.validateIssueDate);
			},

			validateIssueDate: function() {
				var invalidMessage = "";
				var issueDate = new Date(this.get("CrocIssueDate"));
				issueDate.setHours(0);
				issueDate.setMinutes(0);
				issueDate.setSeconds(0);

				var maturityDate = new Date(this.get("CrocMaturityDate"));
				maturityDate.setHours(0);
				maturityDate.setMinutes(0);
				maturityDate.setSeconds(0);

				if ((this.get("CrocMaturityDate") != null && this.get("CrocIssueDate") != null) && issueDate >= maturityDate) {
					invalidMessage = this.get("Resources.Strings.CrocIssueDateError");
				}

				return {
					fullInvalidMessage: invalidMessage,
					invalidMessage: invalidMessage
				};
			}

		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "Button-3a8ac667899d4aa68021a07eb1c7c49c",
				"values": {
					"style": "red",
					"caption": {
						"bindTo": "Resources.Strings.ClosePageButtonCaption"
					},
					"click": {
						"bindTo": "onCancelProcessElementClick"
					},
					"enabled": true
				}
			},
			{
				"operation": "insert",
				"name": "LOOKUPce54978e-1872-4e46-9d80-b90fb05b8586",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocLoanCategory",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPbea3cad8-fd7c-44ed-a213-106bc2927ace",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocAccount",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUP661e4e23-8ca6-4c6a-b75d-945ee3924cbe",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocPort",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP30890f87-b047-446b-80c2-4d05be2319c0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocProduct",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "FLOATe14be985-6a00-4a54-a2d9-d937fae9a356",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocTonVolume",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "DATETIMEc6e7cf50-2a78-4fc1-800b-fb0295da342a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "CrocIssueDate",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "DATETIME1045653d-7637-497a-895c-3ac9b44fdf55",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "CrocMaturityDate",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "remove",
				"name": "NewTab1"
			},
			{
				"operation": "remove",
				"name": "NewTab1Group1"
			},
			{
				"operation": "remove",
				"name": "NewTab1GridLayout1"
			}
		]/**SCHEMA_DIFF*/
	};
});
