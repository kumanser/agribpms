define("CrocDocumentUnloadings1Detail", [], function() {
	return {
		entitySchemaName: "CrocDocumentUnloadings",
		messages: {
			SelectDocumentForUnloading: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
			getCopyRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,

			addGridDataColumns: function(esq) {
				this.callParent(arguments);
				esq.addColumn("CrocGuid");
			},

			rowSelected: function(primaryColumnValue) {
				this.callParent(arguments);
				this.getSelectedRow(primaryColumnValue);
			},

			getSelectedRow: function (primaryColumnValue) {
				if(primaryColumnValue) {
					var masterRecId = this.get("MasterRecordId");
					var activeRow = this.getGridData().get(primaryColumnValue);
					if(activeRow && masterRecId) {
						var rowId = activeRow.get("CrocGuid");
						let result = {
							masterRecId: masterRecId,
							externId: rowId
						}
						this.sandbox.publish("SelectDocumentForUnloading", result, [masterRecId]);
					}
				}
			}
		}
	};
});
