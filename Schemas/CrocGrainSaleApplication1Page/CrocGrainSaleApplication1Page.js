define("CrocGrainSaleApplication1Page", ["CrocConstantsJS"], function(CrocConstantsJS) {
	return {
		entitySchemaName: "CrocGrainSaleApplication",
		attributes: {
			"CrocExporterRole": {
				dataValueType: this.Terrasoft.DataValueType.GUID,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"CurrentStatus": {
				dataValueType: Terrasoft.DataValueType.LOOKUP
			},
			"IsExporterRights": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				value: true
			},
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocGrainSaleApplicationFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocGrainSaleApplication"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocManager": {
				"5111929c-50a7-4bce-96d8-d5ee88e0fb46": {
					"uId": "5111929c-50a7-4bce-96d8-d5ee88e0fb46",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "CrocAccount",
							"attributePath": "CrocPinnedManager"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocAccount"
							}
						}
					]
				}
			},
			"CrocStock": {
				"58b5a4f5-8f6c-4b56-b83d-88b0aa8c3c79": {
					"uId": "58b5a4f5-8f6c-4b56-b83d-88b0aa8c3c79",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "CrocAccount",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "CrocAccount"
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.initSettings();
				this.initExporterRights();
				this.$CurrentStatus = this.$CrocStatus;
			},

			initSysSettingsValue: function(settingCode, callback, scope) {
				this.Terrasoft.SysSettings.querySysSettingsItem(settingCode,
					function(value) {
						this.set(settingCode, value);
						if (this.Ext.isFunction(callback)) {callback.call(scope);}
					}, this);
			},

			initExporterRights: function () {
				Terrasoft.chain(
					function(next) {
						this.initSysSettingsValue("CrocExporterRole", function (){
							next();
						}, this);
					},
					function() {
						this.getIsCurrentUserExporter(function(isCurrentUserExporter) {
							this.$IsExporterRights = isCurrentUserExporter;
						}, this );
					}, this);
			},

			initSettings: function() {
				if (this.isNewMode()) {
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "Contact"
						});
					esq.addColumn("Account.Id","AccountID");
					esq.addColumn("Account.Name","AccountName");
					var esqFirstFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Id", Terrasoft.SysValue.CURRENT_USER_CONTACT.value);
					esq.filters.add(esqFirstFilter);
					esq.getEntityCollection(function(response){
						if (response && response.success) {
							var collection = response.collection;
							if (collection && collection.getCount() > 0){
								var acc = collection.getByIndex(0);
								var currentAccount = {
									value: acc.get("AccountID"),
									displayValue: acc.get("AccountName")
								};
								this.loadLookupDisplayValue("CrocAccount", currentAccount.value);
							}
						}
					},this);
				}
			},

			asyncValidate: function(callback, scope) {
				this.callParent([function(response) {
					if (!this.validateResponse(response)) {
						return;
					}
					Terrasoft.chain(
						function(next) {
							this.checkChangeStatusRights(function(response) {
								if (this.validateResponse(response)) {
									next();
								}
							}, this);
						},
						function(next) {
							callback.call(scope, response);
							next();
						}, this);
				}, this]);
			},

			getIsCurrentUserExporter: function(callback, scope) {
				var currentUser = Terrasoft.SysValue.CURRENT_USER.value;
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "SysUserInRole"});
				esq.addColumn("SysRole");
				esq.addColumn("SysUser");
				esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "SysUser", currentUser));
				esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "SysRole", this.$CrocExporterRole));
				esq.getEntityCollection(function(response) {
					if (response && response.success) {
						var result = response.collection;
						var isCurrentUserExporter = (result.collection.length !== 0);
						callback.call(scope, isCurrentUserExporter);
					}
				}, this);
			},

			checkChangeStatusRights: function(callback, scope) {
				var result = {success: true};
				if (this.$CurrentStatus.value === this.$CrocStatus.value || this.$IsExporterRights ||
					(this.$CurrentStatus.value === CrocConstantsJS.CrocGrainSaleApplicationStatus.New
						&& this.$CrocStatus.value === CrocConstantsJS.CrocGrainSaleApplicationStatus.Canceled)) {
					if (callback) callback.call(scope || this, result);
				}
				else {
					result.message = this.get("Resources.Strings.ExporterRightsError");
					result.success = false;
					this.set("CrocStatus", this.$CurrentStatus);
					if (callback) callback.call(scope || this, result);
				}
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocName81727b5b-ee2f-4829-b7f1-795655c93741",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocName",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocAccountcbc68853-5780-4f53-905e-3ff4ddacbf65",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocAccount"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUP6a2fbedb-43c2-463c-81f4-1edd19794814",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocDeliveryType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP7fa7664e-6127-4a76-afca-e1b6b941c945",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocManager",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP7b488648-2172-4b0c-bdb5-5a7e0573c31c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocStock",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "DATETIME60c3cc9c-fcab-48b5-81fc-b2349caf1808",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocDeliveryToTheTerminalFrom",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryDate6535eb5c-7477-42fb-9099-f16f3a312929",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocDeliveryToTheTerminalUntil",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tab4550e6ecTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4550e6ecTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab4550e6ecTabLabelGroupc20b8489",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab4550e6ecTabLabelGroupc20b8489GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab4550e6ecTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab4550e6ecTabLabelGroupc20b8489",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocTerminal81606f43-a1cd-4fcd-938b-11bc3107dd1a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab4550e6ecTabLabelGridLayout7f9fe885"
					},
					"bindTo": "CrocTerminal"
				},
				"parentName": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocBasis5d40bdd3-35d8-4b94-b17d-206194b59c56",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab4550e6ecTabLabelGridLayout7f9fe885"
					},
					"bindTo": "CrocBasis"
				},
				"parentName": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocProduct5616faaa-f687-4060-b428-6bd868d69cf2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab4550e6ecTabLabelGridLayout7f9fe885"
					},
					"bindTo": "CrocProduct"
				},
				"parentName": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocVolume7f4a5190-0875-4a76-9207-75ffc290fb9b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab4550e6ecTabLabelGridLayout7f9fe885"
					},
					"bindTo": "CrocVolume"
				},
				"parentName": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUPad97d92f-2fc0-4771-a65b-dd2de759ff3b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab4550e6ecTabLabelGridLayout7f9fe885"
					},
					"bindTo": "CrocGoodClass",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "FLOAT9ede5b4c-a517-444a-97de-54ce6c3e0854",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab4550e6ecTabLabelGridLayout7f9fe885"
					},
					"bindTo": "CrocProtein",
					"enabled": true
				},
				"parentName": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "INTEGER9acb2ee2-28b0-47dc-924e-61398abaa11c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tab4550e6ecTabLabelGridLayout7f9fe885"
					},
					"bindTo": "CrocNatura",
					"enabled": true
				},
				"parentName": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocProductFeaturesba19b0df-0e22-4063-a594-a8517dde34e7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tab4550e6ecTabLabelGridLayout7f9fe885"
					},
					"bindTo": "CrocProductFeatures",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocComment9654ba51-c86a-4f85-9ba6-3ed380203765",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Tab4550e6ecTabLabelGridLayout7f9fe885"
					},
					"bindTo": "CrocComment",
					"enabled": true
				},
				"parentName": "Tab4550e6ecTabLabelGridLayout7f9fe885",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
