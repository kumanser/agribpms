define("CrocRegisterLoading1Detail", ["ModalBox","CrocExcelImportModule", "ServiceHelper"], function(ModalBox, ImportModule, ServiceHelper) {
	return {
		entitySchemaName: "CrocRegisterLoading",
		messages: {
			BoxInfoReturnExcelImportResult: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			RunRegisterLoadingExcelImport: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			AfterRegisterLoadingImport: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			SelectDocumentForLoading: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			UpdateCrocRegisterLoading: {
				mode: Terrasoft.MessageMode.BROADCAST,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attributes: {
			ExternalId: {
				"dataValueType": Terrasoft.DataValueType.TEXT,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			filterName: {
				dataValueType: Terrasoft.DataValueType.TEXT,
				value: "DocumentIDFilter"
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
			getCopyRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,

			addGridDataColumns: function(esq) {
				this.callParent(arguments);
				esq.addColumn("CrocGuid");
			},

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("BoxInfoReturnExcelImportResult", this.onCloseImportModalCallback, this, [this.getModuleIdfn(this.sandbox)]);
				this.sandbox.subscribe("RunRegisterLoadingExcelImport", this.onAddFileClick, this, [this.getRunnerModule()]);
				var masterRecId = this.get("MasterRecordId");
				if (masterRecId) {
					this.sandbox.subscribe("SelectDocumentForLoading", this.onSelectDocumentForLoadingSubscribe, this, [masterRecId]);
					this.sandbox.subscribe("UpdateCrocRegisterLoading", this.onAfterImport, this);
				}
			},

			onSelectDocumentForLoadingSubscribe: function(val) {
				var masterRecId = this.get("MasterRecordId");
				if(val && masterRecId === val.masterRecId) {
					this.$ExternalId = val.externId;
					this.reloadGridData();
				}
			},

			getFilters: function() {
				var filterName = this.$filterName;
				var filters = this.callParent(arguments);
				if(filters.contains(filterName)) {
					filters.removeByKey(filterName);
				}
				if(this.$ExternalId) {
					var filter = this.Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,"CrocGuid", this.$ExternalId, Terrasoft.DataValueType.GUID);
					filter.key = filterName;
					filters.addItem(filter);
				}
				return filters;
			},

			getModuleIdfn: function(sandbox) {
				return sandbox.id + "CrocExcelImportModalBoxModule";
			},

			getRunnerModule: function() {
				var id = this.get("MasterRecordId");
				return "RunRegisterLoadingExcelImport_" + id;
			},

			getAfterModule: function() {
				var id = this.get("MasterRecordId");
				return "AfterRegisterLoadingImport_" + id;
			},

			onAddFileClick: function () {
				var sandbox = this.sandbox;
				var renderTo = ModalBox.show(ImportModule.getModalBoxConfig(), this.onCloseImportModalCallback, this);
				var modalConfig = {
					id: this.getModuleIdfn(sandbox),
					renderTo: renderTo,
					parameters: {
						workParam : {
							importConfig: this.fillImportConfig(),
							detailConfig: this.fillDetailConfig()
						}
					}
				};
				sandbox.loadModule("CrocExcelImportModalBoxModule", modalConfig);
			},

			onCloseImportModalCallback: function(arg) {
				if(arg) {
					this.reloadGridData();
					this.sandbox.publish("AfterRegisterLoadingImport", null, [this.getAfterModule()]);
				}
			},

			onAfterImport: function(args) {
				var masterRecId = this.get("MasterRecordId");
				if(args && args.masterId === masterRecId) {
					this.onCloseImportModalCallback(true);
				}
			},

			fillDetailConfig: function() {
				var masterRecId = this.get("MasterRecordId");
				var masterColumn = this.get("DetailColumnName");
				var detailConfig = ImportModule.getTemplateDetailConfig();
				detailConfig.clearOldRows = false;
				detailConfig.masterColumn = masterColumn;
				detailConfig.masterRecord = masterRecId;
				return detailConfig;
			},

			fillImportConfig: function () {
				var importConfig = ImportModule.getTemplateImportParameters();
				importConfig.authorId = this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value;
				var schema = ImportModule.findSchemaByName(this.entitySchemaName);
				if(schema) {
					importConfig.rootSchemaUId = schema.uId;
					importConfig.importObject.uId = schema.uId;
					importConfig.importObject.caption = schema.caption;
					importConfig.importObject.name = schema.name;
					var config = [
						{colName: "CrocNumber", colExcel: "A", source: "№"},
						{colName: "CrocTransportNumber",colExcel: "B", source: "Номер вагона, номер ТС"},
						{colName: "CrocTTN", colExcel: "C", source: "Номер накладной"},
						{colName: "CrocGrossWeightTTNTon", colExcel: "D", source: "Вес брутто по накладной, тонны"},
						{colName: "CrocWeightContainerTTNTon",colExcel: "E", source: "Вес тара, по наклад-ной, кг"},
						{colName: "CrocGrossWeightTon",colExcel: "F", source: "Вес по наклад-ной, тонны"},
						{colName: "CrocShippingDate",colExcel: "G", source: "Дата отгрузки"},
						{colName: "CrocPointLoading",colExcel: "H", source: "Пункт погрузки"},
						{colName: "CrocDepartureStation",colExcel: "I", source: "Станция отправления"},
						{colName: "CrocShipper",colExcel: "J", source: "Грузоотправитель"},
						{colName: "CrocGU12",colExcel: "K", source: "ГУ-12"},
						{colName: "CrocUnloadingPoint",colExcel: "L", source: "Порт выгрузки"},
						{colName: "CrocProduct",colExcel: "M", source: "Груз"},
						{colName: "CrocProductClass",colExcel: "N", source: "Класс"},
						{colName: "CrocProtein",colExcel: "O", source: "Протеин"},
						{colName: "CrocNatur",colExcel: "P", source: "Натура"},
						{colName: "CrocChP",colExcel: "Q", source: "ЧП"},
						{colName: "CrocWeedImpurity",colExcel: "R", source: "Сорная смесь, %"},
						{colName: "CrocHumidity",colExcel: "S", source: "Влажность, %"},
						{colName: "CrocBroken",colExcel: "T", source: "Битые"},
						{colName: "CrocDamaged",colExcel: "U", source: "Поврежденные"},
						{colName: "CrocGluten",colExcel: "V", source: "Клейковина, %"},
						{colName: "CrocIDK",colExcel: "W", source: "ИДК, ед"},
						{colName: "CrocBugEated",colExcel: "X", source: "Клоп Черепашка"},
						{colName: "CrocForeignGrain",colExcel: "Y", source: "Зерновая примесь"},
						{colName: "CrocGrainsDifferentColor",colExcel: "Z", source: "Зерна другого цвета"}
					];
					config.forEach((item) => {
						var col = this.getColumnByName(item.colName);
						var columnConfig = ImportModule.getTemplateColumnConfig();
						if (col) {
							var destinationConfig = ImportModule.getTemplateDestinationConfig();
							destinationConfig.isKey = false;
							destinationConfig.schemaUId = schema.uId;
							if (col.isLookup) {
								destinationConfig.columnValueName = col.name + "Id";
								destinationConfig.properties = {
									"TypeUId": this.Terrasoft.convertToServerDataValueType(col.dataValueType),
									"ReferenceSchemaName": col.referenceSchemaName
								};
							}
							else {
								destinationConfig.columnValueName = col.name;
								destinationConfig.properties = {"TypeUId": this.Terrasoft.convertToServerDataValueType(col.dataValueType)};
							}
							destinationConfig.columnName = col.name;
							columnConfig.index = item.colExcel;
							columnConfig.source = item.source;
							columnConfig.destinations.push(destinationConfig);
						}
						importConfig.columns.push(columnConfig);
					});
				}
				return importConfig;
			}
		}
	};
});