define("CrocContract1Detail", [], function() {
	return {
		entitySchemaName: "CrocContract",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "remove",
				"name": "addTypedRecordButton"
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			getAddTypedRecordButtonVisible: Terrasoft.emptyFn,
			getDeleteRecordMenuItem: Terrasoft.emptyFn,
			getCopyRecordMenuItem: Terrasoft.emptyFn
		}
	};
});
