 define("ContactProfileSchema", [],
		function() {
			return {
				entitySchemaName: "Contact",
				attributes: {
					"MasterAccountValue": {
						dataValueType: Terrasoft.DataValueType.GUID,
						value: null
					}
				},
				messages: {},
				mixins: {},
				methods: {
					getLookupConfig: function() {
						var accId = this.$MasterAccountValue;
						var config = this.callParent(arguments);
						if(config !== undefined && accId) {
							var accountFilter = this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,"Account",accId, Terrasoft.DataValueType.GUID);
							if(!config.filters)
							{
								config.filters = this.Terrasoft.createFilterGroup();
							}
							config.filters.addItem(accountFilter);
						}
						return config;
					}
				},
				diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
			};
		}
);
