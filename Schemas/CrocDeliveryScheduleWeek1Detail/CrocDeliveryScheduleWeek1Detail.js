define("CrocDeliveryScheduleWeek1Detail", [], function() {
	return {
		entitySchemaName: "CrocDeliveryScheduleWeek",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "EditRecordMenu",
				"values": {
					"visible": false
				}
			},
			{
				"operation": "merge",
				"name": "EnableMultiSelect",
				"values": {
					"visible": false
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
			getCopyRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,
			getEditRecordMenuItem: this.Terrasoft.emptyFn,

			editRecord: function(record) {
				return;
			},


		}
	};
});
