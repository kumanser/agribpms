 define("CrocExcelImportModalBoxPage", ["FileImportStartPageResources", "ModalBox", "ServiceHelper", "ConfigurationFileApi", "css!CrocExcelImportModalBoxModule"],
    function(resources, ModalBox, ServiceHelper) {
        return {
            attributes: {
                // Common ModalBox parameters ->
                modalWorkParameter: {
                    dataValueType: Terrasoft.DataValueType.CUSTOM_OBJECT,
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
                },
                ImportSessionId: {
                    dataValueType: Terrasoft.DataValueType.GUID
                },
                ImportObject: {
                    dataValueType: Terrasoft.DataValueType.CUSTOM_OBJECT
                },
                ImportParameters: {
                    dataValueType: Terrasoft.DataValueType.CUSTOM_OBJECT
                },
                DetailParameters: {
                    dataValueType: Terrasoft.DataValueType.CUSTOM_OBJECT
                },
                FileUploadWebServicePath: {
                    dataValueType: Terrasoft.DataValueType.TEXT,
                    value: "FileImportService/SaveFile"
                },
                File: {dataValueType: Terrasoft.DataValueType.CUSTOM_OBJECT},
                SaveFileResult: {dataValueType: Terrasoft.DataValueType.BOOLEAN},
                ErrorMessage: {dataValueType: Terrasoft.DataValueType.TEXT},
                PrepareImportService: {
                    dataValueType: Terrasoft.DataValueType.TEXT,
                    value: "CrocToolsService"
                }
            },
            messages: {
                GetModuleBoxInfo: {
                    mode: Terrasoft.MessageMode.PTP,
                    direction: Terrasoft.MessageDirectionType.PUBLISH
                },
                BoxInfoReturnExcelImportResult: {
                    mode: Terrasoft.MessageMode.PTP,
                    direction: Terrasoft.MessageDirectionType.PUBLISH
                }
            },
            mixins: {},
            methods: {

                getFileUploadConfig: function(file, importObject) {
                    var data = {
                        importSessionId: this.$ImportSessionId,
                        fileId: this.$ImportSessionId
                    };
                    if (importObject) {
                        data.entitySchemaUId = importObject.uId;
                        data.entitySchemaName = importObject.name;
                        data.isOtherObject = importObject.isOtherObject;
                    }
                    return {
                        onComplete: this.onFileUploadEnd,
                        entitySchemaName: "FileImportParameters",
                        data: data,
                        columnName: "FileData",
                        parentColumnName: "Id",
                        parentColumnValue: this.$ImportSessionId,
                        files: [file],
                        maxFileSizeSysSettingsName: "FileImportMaxFileSize",
                        uploadWebServicePath: "FileImportUploadFileService/SaveFile",
                        isChunkedUpload: true,
                        scope: this
                    };
                },

                onFileUploadEnd: function(error, xhr) {
                    if (error) {
                        this.setErrorMessage(error);
                        throw new Terrasoft.UnknownException({message: error});
                    }
                    var response = this.Terrasoft.decode(xhr.responseText);
                    var result = response.SaveFileResult;
                    var saveFileResult = result.success;
                    if (!saveFileResult) {
                        this.onRequestLoadFileError(result.errorInfo);
                        return;
                    }
                    var file = xhr.files[0];
                    this.set("File", file);
                    this.checkIsLoadedFileValid(this.onValidateLoadedFile);
                },

                checkIsLoadedFileValid: function(callback) {
                    this.callService({
                        methodName: "CheckIsFileValid",
                        data: {
                            importSessionId: this.$ImportSessionId
                        },
                        serviceName: "FileImportUploadFileService"
                    }, callback, this);
                },

                onRequestLoadFileError: function (errorInfo) {
                    this.hideBodyMask();
                    this.clearFileSelection();
                },

                onValidateLoadedFile: function(validateFileResponse) {
                    var validateResult = validateFileResponse.CheckIsFileValidResult;
                    if (!validateResult.success) {
                        this.onRequestLoadFileError(validateResult.errorInfo);
                    } else {
                        this.hideBodyMask();
                        this.set("SaveFileResult", true);
                    }
                },

                setFileInfo: function(callback, fileName) {
                    var requestConfig = {
                        contractName: "SetFileInfo",
                        importSessionId: this.$ImportSessionId,
                        fileName: fileName
                    };
                    var request = this.getFileImportServiceRequest(requestConfig);
                    request.execute(callback, this);
                },

                getFileImportServiceRequest: function(requestConfig) {
                    return this.Ext.create(Terrasoft.configuration.fileImport.FileImportServiceRequest, requestConfig);
                },

                init: function() {
                    this.callParent(arguments);
                    var workPar = this.get("modalWorkParameter");
                    if(workPar) {
                        this.$DetailParameters = workPar.detailConfig;
                        this.$ImportParameters = workPar.importConfig;
                        this.$ImportObject = this.$ImportParameters.importObject;
                        this.initImportSession()
                    } else { this.closePage(); }
                },

                initImportSession: function() {
                    ServiceHelper.callService({
                            serviceName: this.$PrepareImportService,
                            methodName: "CreateNewImportParameters",
                            data: {
                                config: JSON.stringify(this.$ImportParameters)
                            },
                            callback: this.initImportSessionCallback,
                            scope: this
                        }
                    );
                },

                initImportSessionCallback: function(response) {
                    if (!response || !response.CreateNewImportParametersResult) {
                        var message = resources.localizableStrings.EmptyImportSessionIdMessage;
                        this.showInformationDialog(message);
                        throw new this.Terrasoft.NullOrEmptyException({
                            message: message
                        });
                    } else {
                        this.$ImportSessionId = response.CreateNewImportParametersResult;
                    }
                },

                onRender: function() {
                    this.callParent(arguments);
                    this.initDropzoneEvents();
                },

                onCloseButtonClick: function() {
                    this.closePage();
                },

                closePage: function() {
                    if(this.$SaveFileResult) {
                        this.clearFileSelection(this.closeModal())
                    } else {
                        this.closeModal()
                    }
                },

                closeModal: function() {
                    if(ModalBox) {ModalBox.close();}
                },

                close: function() {
                    this.closePage();
                },

                onUploadButtonClick: function() {
                    try {
                        var sessionId = this.$ImportSessionId;
                        var detailParameters = this.$DetailParameters;
                        var importObject = this.$ImportObject;
                        ServiceHelper.callService({
                                serviceName: this.$PrepareImportService,
                                methodName: "RunImport",
                                data: {
                                    sessionId: sessionId,
                                    schemaName: importObject.name,
                                    detailColumn: detailParameters.masterColumn,
                                    detailValue: detailParameters.masterRecord,
                                    clearOldRows: detailParameters.clearOldRows
                                },
                                callback: this.onUploadButtonClickCallback,
                                scope: this
                            }
                        );
                    }
                    catch {
                        this.closePage();
                    }
                },

                onUploadButtonClickCallback: function() {
                    this.$SaveFileResult = false;
                    this.closePage();
                },

                validateFileType: function(file) {
                    var validationResult = false;
                    if (file) {
                        var fileType = this.getFileType(file);
                        if (fileType) {
                            var excelMediaTypeRegExp = /application\/vnd\.openxmlformats-officedocument\.spreadsheetml/;
                            validationResult = excelMediaTypeRegExp.test(fileType);
                        } else {
                            var fileName = file.name;
                            var excelExtensionRegExp = /.xlsx$/;
                            validationResult = excelExtensionRegExp.test(fileName);
                        }
                    }
                    var message = (validationResult) ? "" : resources.localizableStrings.InvalidFileTypeMessage; //this.get("Resources.Strings.InvalidFileTypeMessage");
                    this.setErrorMessage(message);
                    return validationResult;
                },

                initDropzoneEvents: function() {
                    var dropzone = this.getDropzone();
                    this.Terrasoft.ConfigurationFileApi.initDropzoneEvents(dropzone, this.onDropzoneHover.bind(this),
                        this.onFilesSelected.bind(this));
                },

                onFilesSelected: function(files) {
                    this.clearFileSelection(function(response) {
                        if (!response.success) {
                            this.logFailedResponse(response);
                            return;
                        }
                        var file = files[0];
                        if (!this.validateFileType(file)) {
                            return;
                        }
                        var importObject = this.get("ImportObject");
                        this.upload(file, importObject);
                    });
                },

                clearFileSelection: function(callback) {
                    if (!this.$File) {
                        Ext.callback(callback, this, [{success: true}]);
                        return;
                    }
                    this.set("SaveFileResult", false);
                    this.set("File", null);
                    this.executeRequestToDeleteFile(callback);
                },

                setErrorMessage: function(message) {
                    this.set("ErrorMessage", message);
                },

                onDropzoneHover: function(over, event) {
                    var dropzone = this.getDropzone();
                    if (over) {
                        dropzone.classList.add("dropzone-hover");
                    } else {
                        dropzone.classList.remove("dropzone-hover");
                        this.setFileType(event);
                    }
                },

                handleFileUploadError: function(errorInfo) {
                    this.logRequestError(errorInfo, this.Terrasoft.LogMessageType.INFORMATION);
                    var errorCode = errorInfo.errorCode;
                    var errorMessage = errorInfo.message;
                    var exception = this.Terrasoft.FileImportConstants.Exceptions[errorCode];
                    if (exception) {
                        errorMessage = exception.message;
                    }
                    this.setErrorMessage(errorMessage);
                },

                setFileType: function(event) {
                    var dataTransfer = event.dataTransfer;
                    var file = dataTransfer.files[0];
                    if (file) {
                        var fileType = this.getFileType(file);
                        this.set("FileType", fileType);
                    }
                },

                getFileType: function(file) {
                    return ((file && file.type) || this.get("FileType"));
                },

                getDropzone: function() {
                    var element = this.Ext.get("DragAndDropContainer");
                    return element.dom;
                },

                upload: function(file, importObject) {
                    this.showBodyMask();
                    var config = this.getFileUploadConfig(file, importObject);
                    this.Terrasoft.ConfigurationFileApi.upload(config);
                },

                getEmptyFileContainerVisibility: function() {
                    return !this.get("SaveFileResult");
                },

                getErrorFileContainerVisibility: function() {
                    var message = this.get("ErrorMessage");
                    return !this.Ext.isEmpty(message);
                },

                getUploadFileContainerVisibility: function() {
                    return (!this.getErrorFileContainerVisibility() && this.getEmptyFileContainerVisibility());
                },

                getFileName: function() {
                    var file = this.get("File");
                    return (file && file.name);
                },

                getEmptyFileImage: function() {
                    var image = resources.localizableImages.EmptyFileImage;
                    return Terrasoft.ImageUrlBuilder.getUrl(image);
                },

                getErrorFileImage: function() {
                    var image = resources.localizableImages.ErrorFileImage;
                    return Terrasoft.ImageUrlBuilder.getUrl(image);
                },

                getExcelFileImage: function() {
                    var image = resources.localizableImages.ExcelFileImage;
                    return Terrasoft.ImageUrlBuilder.getUrl(image);
                },

                logFailedResponse: function(response) {
                    if (!response.success) {
                        this.logRequestError(response.errorInfo);
                    }
                },

                onFileDeleteButtonClick: function() {
                    this.clearFileSelection(this.logFailedResponse);
                },

                executeRequestToDeleteFile: function (callback) {
                    this.callService({
                        methodName: "DeleteFile",
                        data: {importSessionId: this.$ImportSessionId},
                        serviceName: "FileImportUploadFileService"
                    }, function(deleteResponse) {
                        Ext.callback(callback, this, [deleteResponse.DeleteFileResult]);
                    }, this);
                }
            },
            diff: [
                {
                    "operation": "insert",
                    "name": "MainContainer",
                    "values": {
                        "itemType": Terrasoft.ViewItemType.CONTAINER,
                        "generateId": false,
                        "classes": {"wrapClassName": ["main-container"]},
                        "items": []
                    }
                },
                {
                    "operation": "insert",
                    "name": "DragAndDropContainer",
                    "parentName": "MainContainer",
                    "propertyName": "items",
                    "values": {
                        "id": "DragAndDropContainer",
                        "selectors": {
                            "wrapEl": "#DragAndDropContainer"
                        },
                        "itemType": Terrasoft.ViewItemType.CONTAINER,
                        "wrapClass": ["dropzone"],
                        "items": []
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "DragAndDropContainer",
                    "name": "EmptyFileContainer",
                    "propertyName": "items",
                    "values": {
                        "id": "EmptyFileContainer",
                        "itemType": Terrasoft.ViewItemType.CONTAINER,
                        "wrapClass": ["flex-vertical-center"],
                        "items": [],
                        "visible": {"bindTo": "getEmptyFileContainerVisibility"}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "EmptyFileContainer",
                    "name": "UploadFileContainer",
                    "propertyName": "items",
                    "values": {
                        "id": "UploadFileContainer",
                        "itemType": Terrasoft.ViewItemType.CONTAINER,
                        "wrapClass": ["flex-vertical-center"],
                        "items": [],
                        "visible": {"bindTo": "getUploadFileContainerVisibility"}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "UploadFileContainer",
                    "name": "EmptyFileImage",
                    "propertyName": "items",
                    "values": {
                        "readonly": true,
                        "getSrcMethod": "getEmptyFileImage",
                        "generator": "ImageCustomGeneratorV2.generateCustomImageControl",
                        "classes": {
                            "wrapClass": ["empty-file-image-wrapper"]
                        },
                        "markerValue": "EmptyFileImage"
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "UploadFileContainer",
                    "name": "EmptyFileCaption",
                    "propertyName": "items",
                    "values": {
                        "generateId": false,
                        "itemType": Terrasoft.ViewItemType.LABEL,
                        "caption": resources.localizableStrings.DragAndDropContainerCaption,
                        "classes": {
                            "wrapClassName": ["upload-caption"],
                            "labelClass": ["upload-caption"]
                        },
                        "markerValue": "EmptyFileCaption"
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "EmptyFileContainer",
                    "name": "ErrorFileContainer",
                    "propertyName": "items",
                    "values": {
                        "id": "ErrorFileContainer",
                        "itemType": Terrasoft.ViewItemType.CONTAINER,
                        "wrapClass": ["flex-vertical-center"],
                        "items": [],
                        "visible": {"bindTo": "getErrorFileContainerVisibility"}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ErrorFileContainer",
                    "name": "ErrorFileImage",
                    "propertyName": "items",
                    "values": {
                        "readonly": true,
                        "defaultImage": Terrasoft.ImageUrlBuilder.getUrl(resources.localizableImages.ErrorFileImage),
                        "getSrcMethod": "getErrorFileImage",
                        "generator": "ImageCustomGeneratorV2.generateCustomImageControl",
                        "classes": {
                            "wrapClass": ["empty-file-image-wrapper"]
                        },
                        "markerValue": "ErrorFileImage"
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ErrorFileContainer",
                    "name": "ErrorFileCaption",
                    "propertyName": "items",
                    "values": {
                        "generateId": false,
                        "itemType": Terrasoft.ViewItemType.LABEL,
                        "caption": {"bindTo": "ErrorMessage"},
                        "classes": {
                            "wrapClassName": ["error-caption"],
                            "labelClass": ["error-caption"]
                        },
                        "markerValue": "ErrorFileCaption"
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "EmptyFileContainer",
                    "name": "UploadFileButton",
                    "propertyName": "items",
                    "values": {
                        "generateId": false,
                        "caption": resources.localizableStrings.UploadFileButtonCaption,
                        "itemType": Terrasoft.ViewItemType.BUTTON,
                        "classes": {
                            "textClass": ["upload-button"],
                            "wrapperClass": ["upload-button"]
                        },
                        "fileTypeFilter": ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"],
                        "fileUpload": true,
                        "filesSelected": {"bindTo": "onFilesSelected"},
                        "fileUploadMultiSelect": false,
                        "style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
                        "markerValue": "UploadFileButton"
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "DragAndDropContainer",
                    "name": "ExcelFileContainer",
                    "propertyName": "items",
                    "values": {
                        "id": "ExcelFileContainer",
                        "itemType": Terrasoft.ViewItemType.CONTAINER,
                        "wrapClass": ["flex-vertical-center"],
                        "items": [],
                        "visible": {"bindTo": "SaveFileResult"}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ExcelFileContainer",
                    "name": "ExcelFileCaption",
                    "propertyName": "items",
                    "values": {
                        "generateId": false,
                        "itemType": Terrasoft.ViewItemType.LABEL,
                        "caption": resources.localizableStrings.ExcelFileCaption,
                        "classes": {
                            "wrapClassName": ["excel-file-caption"],
                            "labelClass": ["excel-file-caption"]
                        },
                        "markerValue": "ExcelFileCaption"
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ExcelFileContainer",
                    "name": "ExcelFileImage",
                    "propertyName": "items",
                    "values": {
                        "readonly": true,
                        "defaultImage": Terrasoft.ImageUrlBuilder.getUrl(resources.localizableImages.ExcelFileImage),
                        "getSrcMethod": "getExcelFileImage",
                        "generator": "ImageCustomGeneratorV2.generateCustomImageControl",
                        "classes": {
                            "wrapClass": ["excel-file-image-wrapper"]
                        },
                        "markerValue": "ExcelFileImage"
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ExcelFileContainer",
                    "name": "ExcelFileNameContainer",
                    "propertyName": "items",
                    "values": {
                        "id": "ExcelFileNameContainer",
                        "selectors": {
                            "wrapEl": "#ExcelFileNameContainer"
                        },
                        "itemType": Terrasoft.ViewItemType.CONTAINER,
                        "wrapClass": ["excel-file-name-wrapper"],
                        "items": []
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ExcelFileNameContainer",
                    "name": "ExcelFileName",
                    "propertyName": "items",
                    "values": {
                        "generateId": false,
                        "itemType": Terrasoft.ViewItemType.BUTTON,
                        "caption": {"bindTo": "getFileName"},
                        "classes": {
                            "wrapperClass": ["excel-file-name-wrapper"],
                            "textClass": ["excel-file-name-caption"]
                        },
                        "style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
                        "markerValue": "ExcelFileName"
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ExcelFileNameContainer",
                    "name": "ExcelFileDeleteButton",
                    "propertyName": "items",
                    "values": {
                        "generateId": false,
                        "itemType": Terrasoft.ViewItemType.BUTTON,
                        "caption": "",
                        "click": {"bindTo": "onFileDeleteButtonClick"},
                        "classes": {
                            "wrapperClass": ["excel-file-delete-button-wrapper"]
                        },
                        "imageConfig": resources.localizableImages.ExcelFileDeleteImage,
                        "iconAlign": Terrasoft.controls.ButtonEnums.iconAlign.RIGHT,
                        "style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
                        "markerValue": "ExcelFileDeleteButton"
                    }
                },
                {
                    "operation": "insert",
                    "name": "ButtonContainer",
                    "propertyName": "items",
                    "values": {
                        "itemType": Terrasoft.ViewItemType.CONTAINER,
                        "items": []
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ButtonContainer",
                    "propertyName": "items",
                    "name": "ButtonGrid",
                    "values": {
                        "itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
                        "items": []
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ButtonGrid",
                    "name": "OKButton",
                    "propertyName": "items",
                    "values": {
                        "itemType": Terrasoft.ViewItemType.BUTTON,
                        "style": Terrasoft.controls.ButtonEnums.style.BLUE,
                        "click": {"bindTo": "onUploadButtonClick"},
                        "markerValue": "CloseButton",
                        "caption": "Загрузить",
                        "layout": { "column": 4, "row": 1, "colSpan": 6 },
                        "enabled": {"bindTo": "SaveFileResult"}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ButtonGrid",
                    "name": "CloseButton",
                    "propertyName": "items",
                    "values": {
                        "itemType": Terrasoft.ViewItemType.BUTTON,
                        "style": Terrasoft.controls.ButtonEnums.style.RED,
                        "click": {bindTo: "onCloseButtonClick"},
                        "markerValue": "CloseButton",
                        "caption": "Отмена",
                        "layout": { "column": 16, "row": 1, "colSpan": 4 }
                    }
                }
            ]
        };
    });