define("KnowledgeBasePageV2", [], function() {
	return {
		entitySchemaName: "KnowledgeBase",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocSubtype": {
				"e38f1db1-83e9-4d19-b087-7b9340d1d033": {
					"uId": "e38f1db1-83e9-4d19-b087-7b9340d1d033",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "CrocType",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "Type"
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "STRING435d84a0-6efe-48b4-8479-3eff2daaf13f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocContent",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "Type",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "insert",
				"name": "LOOKUPc0ebeefa-2e14-475a-96eb-17fdaa39b013",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocSubtype",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "ModifiedBy",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "ModifiedOn",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "FilesTab",
				"values": {
					"order": 1
				}
			},
			{
				"operation": "remove",
				"name": "PlaybookTab"
			},
			{
				"operation": "remove",
				"name": "PlaybookDetail"
			},
			{
				"operation": "remove",
				"name": "LikeContainer"
			},
			{
				"operation": "remove",
				"name": "Like"
			},
			{
				"operation": "move",
				"name": "AmountSymbolsc3370bef-813a-4721-aa4e-a684d5311a43",
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "move",
				"name": "AmountFragments1366f32e-7dc8-4488-ba04-06bb883542d0",
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			}
		]/**SCHEMA_DIFF*/
	};
});
