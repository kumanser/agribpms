 define("CrocConstantsJS", ["ext-base", "terrasoft", "CrocConstantsJSResources"],
	function(Ext, Terrasoft, resources) {
		
	 	var crocDocumentType = {
			Accreditation: {
				displayValue: resources.localizableStrings.CrocAccreditationName,
				value: "7cda430d-acbc-40e2-ace8-6d05a660527a"
			},
			ApplicationGU12: {
				displayValue: resources.localizableStrings.CrocApplicationGU12Name,
				value: "ed86e071-6526-42d4-914a-b3c91ed6939d"
			}
		};
	 
	 	var crocApplicationGU12Status = {
			WaitingToApprove:{
				displayValue: resources.localizableStrings.CrocGU12WaitingToApprove,
				value: "5130636d-a9dd-4875-9817-3818dff67665"
			},
			Approved: {
				displayValue: resources.localizableStrings.CrocGU12Approved,
				value: "17807d8b-7f77-4e27-94ac-4cbe9ee6ffc6"
			},
			Rejected: {
				displayValue: resources.localizableStrings.CrocGU12Rejected,
				value: "0f1327a4-d0bf-4286-87f8-db98068e66e9"
			}
		};
	 	
	 	var crocCheckStatus = {
			SendToCheck: {
				displayValue: resources.localizableStrings.CrocSendToCheck,
				value: "0f5395f1-43ad-48d7-8655-ef97186038b3"
			},
			Checked: {
				displayValue: resources.localizableStrings.CrocSendToCheck,
				value: "0f5395f1-43ad-48d7-8655-ef97186038b3"
			},
			Draft: {
				displayValue: resources.localizableStrings.CrocDraft,
				value: "ed61d89c-8155-42db-b1c6-6e77cd13a77a"
			}
		};
	 
	 	var crocAccountType = {
	 		Port: {
				displayValue: resources.localizableStrings.CrocAccountTypePort,
				value: "7cc6f423-18b1-4af7-bd9d-59de34e98c84"
			},
			Elevator: {
				displayValue: resources.localizableStrings.CrocAccountTypeElevator,
				value: "a7810de5-a458-419f-b6e6-79792c706298"
			}
		};
		
	 	var crocChartStatus = {
			Approved: {
				displayValue: resources.localizableStrings.CrocApprovedName,
				value: "c95757ca-1df7-4c0e-bb44-b59599917271"
			},
			Plan: {
				displayValue: resources.localizableStrings.CrocPlanName,
				value: "48ae599f-6993-4fd4-b6a4-4297c5bae0ba"
			},
			WaitingExporterApprovement: {
				displayValue: resources.localizableStrings.CrocWaitingExporterApprovementName,
				value: "b3cfed4b-e043-45e1-901f-f11ce7604803"
			},
			WaitingProviderApprovement: {
				displayValue: resources.localizableStrings.CrocWaitingProviderApprovementName,
				value: "e375dfa2-401d-44fb-849a-45b37782c316" 
			},
			ProviderApprovement: {
				displayValue: resources.localizableStrings.CrocWaitingProviderApprovementName,
				value: "b7363ade-e980-4b5a-b6be-0577dd3b70eb" 
			},
			ProviderRejected: {
				displayValue: resources.localizableStrings.CrocWaitingProviderApprovementName,
				value: "3de65c2b-82d0-4071-a748-bc9194fb9b9c" 
			},
		};
	 	
	 	var crocDocumentStatus = {
			Actual: {
				displayValue: resources.localizableStrings.CrocActual,
				value: "7936b7e6-5ac7-4d4a-8461-969c1cf5f873"
			},
			Archive: {
				displayValue: resources.localizableStrings.CrocArchive,
				value: "3b76a485-0c13-4663-9c1b-c23dbbc96e83"
			},
			Canceled: {
				displayValue: resources.localizableStrings.CrocCanceled,
				value: "94e35301-4c95-46b0-ab3b-55e341de0ee6"
			},
			Prepared: {
				displayValue: resources.localizableStrings.CrocPrepared,
				value: "b1db7257-1c2d-4af5-b749-7035ea616238"
			},
		};
	 
	 	var crocChartExecutionStatus = {
			CompletionWaiting: {
				displayValue: resources.localizableStrings.CrocCompletionWaiting,	
				value: "023751f2-883d-4a68-85e3-4a43b114669c"
			},
			Closed: {
				displayValue: resources.localizableStrings.CrocClosed,
				value: "2b8e623a-d3be-410b-bbc0-cf92f926852a"
			}
		};

		var crocEmployeeRole = {
			PurchasingManager: {
				displayValue: resources.localizableStrings.CrocRolePurchasingManager,
				value: "d8fac9dc-d264-4a5e-ad47-5a436cb77b29"
			},
			ManagerSTO: {
				//displayValue: resources.localizableStrings.CrocRolePurchasingManager,
				value: "22c28cfb-b081-4566-b7f6-78da27e3804e"
			},
		};

		var crocApplicationStatus = {
			RequestSended: "ad3800f2-c9ec-4398-8e1b-f3b227b8e8c9",// Запрос отправлен
			RequestAccepted: "f6e761c9-ab8b-4d03-a72e-79e76bbc055e",// Заявка принята
			RequestCanceled: "e8ebbc0a-4e6d-4eeb-9945-6cc6810b7618",// Заявка отклонена
		};

		var crocProgressReportStatus = {
			Accepted: "1e251493-cc54-4c7c-84da-7de52a0cf575",// Принят
			UnderReview: "a81b53c3-4faf-4773-a4d0-a7d4ee725c7a",// На проверке
			Draft: "cb5ec5ba-03c1-47e3-a06d-bd00ab16a604",// Черновик
			ReturnedForRevision: "37a93684-98cf-4e0e-91b5-a9efdc132f0c",// Возвращен на доработку
		};

		var crocFileType = {
			Contract: "6ed7cc16-b596-4a5c-a680-78841d0b5159", // Договор
			GrainSale: "de18746f-fffb-411c-aef3-11e0f85f9e4c", // Заявка на продажу зерна
			Request: "1bdc40e9-853d-403c-8f10-f68a99f578db", // Заявка
		};

		var crocMailingStatus = {
			Launched: "2e736da9-4812-4fff-ae68-44fabe4a29d0", // Запущена
			Planned: "ba7a58fb-ab96-4fd8-a15d-f75230c40c44", //В планах
		};

		var crocSettlementType = {
			Incoming: "17b1735f-d443-4b01-bd79-b98e01923c04", //Поступление
			Outgoing: "57130a04-f79f-400e-8d61-a9c8fc8c4b5f" //Расход
		};

		var crocActivityCategory = {
			DocumentsRequest: "bc9f5cad-954f-4d52-9959-169cfca7c980",
			QuoteRequest: "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
			LoanRequest: "5706b8be-662d-4b56-85ff-8fa787db7bc0",
			IssuanceRequest: "71d81d0b-d515-470f-95c7-ad15593b776c"
		};

		var crocActivityResult = {
			Loan: "8906ccb6-2d40-4a05-8a9c-2b24f91cce73",
			NotLoan: "34f91f56-4b5c-4042-8271-d12f4eb31d35"
		};

		 var crocActivityStatus = {
			 Completed: "4bdbb88f-58e6-df11-971b-001d60e938c6"
		 };

		 var crocLoanCategory = {
			 ReleaseLoanRequest: "71d81d0b-d515-470f-95c7-ad15593b776c",
			 ReceiveLoanRequest: "5706b8be-662d-4b56-85ff-8fa787db7bc0"
		 };

		var crocDeliveryCondition = {
			CPT: "ba3fb45a-2a01-49bd-bb0e-2b01bc011cd0", //CPT
			FCA: "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",//FCA
			EXW: "0b5f5318-738c-49fc-890c-f481739f4d0f" //EXW
		};

		var crocTransportType = {
			Auto: "0bb95c3f-fba1-4baa-99c9-bacdaee127e3",//Автомобильный
			Railway: "efa3fcd2-6eba-406b-962b-3eb6b82be6cc" //ЖД
		};

		var crocGrainSaleApplicationStatus = {
			Canceled: "35f5173d-4736-4182-941a-9cfe37afad0c",//отменена
			New: "1b887099-2a04-4536-9e3f-6c243b110ce5" //новая
		};

		return {
			CrocCheckStatus: crocCheckStatus,
			CrocGrainSaleApplicationStatus: crocGrainSaleApplicationStatus,
			CrocChartExecutionStatus: crocChartExecutionStatus,
			CrocDocumentType: crocDocumentType,
			CrocChartStatus: crocChartStatus,
			CrocDocumentStatus: crocDocumentStatus,
			CrocAccountType: crocAccountType,
			CrocApplicationGU12Status:crocApplicationGU12Status,
			CrocEmployeeRole: crocEmployeeRole,
			CrocApplicationStatus: crocApplicationStatus,
			CrocProgressReportStatus: crocProgressReportStatus,
			CrocMailingStatus: crocMailingStatus,
			CrocFileType: crocFileType,
			CrocSettlementType: crocSettlementType,
			CrocActivityCategory: crocActivityCategory,
			CrocActivityResult: crocActivityResult,
			CrocActivityStatus: crocActivityStatus,
			CrocLoanCategory: crocLoanCategory,
			CrocDeliveryCondition: crocDeliveryCondition,
			CrocTransportType:crocTransportType
		};
	});
