define("CrocChart2Page", ["CrocConstantsJS", "ServiceHelper", "ProcessModuleUtilities"], function(CrocConstantsJS, ServiceHelper,ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocChart",
		attributes: {
			"ApproveChartButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"LoadGU12ButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"value": false
			},
			"CrocIsApproveRights": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
			},
			"CrocApproveChartRightsRole": {
				dataValueType: this.Terrasoft.DataValueType.GUID,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"CrocNumberGU12": {
				dependencies: [
					{
						columns: ["CrocNumberGU12", "CrocContract"],
						methodName: "onCrocGU12NumberChanged"
					}
				]
			},
			"CrocSmartseedsUrl": {
				"dataValueType": Terrasoft.DataValueType.TEXT,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"caption": { bindTo: "Resources.Strings.SmartseedsUrlCaption"}
			}
		},
		messages: {
			"ApproveChartButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"LoadGU12ButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},
		modules: /**SCHEMA_MODULES*/{
			"Indicatore086c8f3-5fee-4317-8845-a19538e502bb": {
				"moduleId": "Indicatore086c8f3-5fee-4317-8845-a19538e502bb",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicatore086c8f3-5fee-4317-8845-a19538e502bb",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicatorcbae26d1-af16-495b-8091-ed714e1e5fdb": {
				"moduleId": "Indicatorcbae26d1-af16-495b-8091-ed714e1e5fdb",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicatorcbae26d1-af16-495b-8091-ed714e1e5fdb",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicator4c8f799d-2a6d-40e8-b8ee-2c9daeb35040": {
				"moduleId": "Indicator4c8f799d-2a6d-40e8-b8ee-2c9daeb35040",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicator4c8f799d-2a6d-40e8-b8ee-2c9daeb35040",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicator0c1acce8-68be-43e7-9152-c437bc20df7b": {
				"moduleId": "Indicator0c1acce8-68be-43e7-9152-c437bc20df7b",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicator0c1acce8-68be-43e7-9152-c437bc20df7b",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicator81351b4c-4e89-43af-9af9-f528d26f0ca8": {
				"moduleId": "Indicator81351b4c-4e89-43af-9af9-f528d26f0ca8",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicator81351b4c-4e89-43af-9af9-f528d26f0ca8",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicator32f68d29-6d37-468c-9956-e5544377b964": {
				"moduleId": "Indicator32f68d29-6d37-468c-9956-e5544377b964",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicator32f68d29-6d37-468c-9956-e5544377b964",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicatorffc99b11-492b-4bb1-b4ef-18206f77af26": {
				"moduleId": "Indicatorffc99b11-492b-4bb1-b4ef-18206f77af26",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicatorffc99b11-492b-4bb1-b4ef-18206f77af26",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicatorad17d7c0-d592-4df1-8642-e3fcae895aae": {
				"moduleId": "Indicatorad17d7c0-d592-4df1-8642-e3fcae895aae",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicatorad17d7c0-d592-4df1-8642-e3fcae895aae",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicator97dff6e8-261c-4cba-8320-b8055be05e61": {
				"moduleId": "Indicator97dff6e8-261c-4cba-8320-b8055be05e61",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicator97dff6e8-261c-4cba-8320-b8055be05e61",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicatora0faf893-cfe7-4143-b3b3-8ff2668b4553": {
				"moduleId": "Indicatora0faf893-cfe7-4143-b3b3-8ff2668b4553",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicatora0faf893-cfe7-4143-b3b3-8ff2668b4553",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			},
			"Indicator05b47c3e-8d8d-4d74-b047-bd21a7f6c3ee": {
				"moduleId": "Indicator05b47c3e-8d8d-4d74-b047-bd21a7f6c3ee",
				"moduleName": "CardWidgetModule",
				"config": {
					"parameters": {
						"viewModelConfig": {
							"widgetKey": "Indicator05b47c3e-8d8d-4d74-b047-bd21a7f6c3ee",
							"recordId": "6fb9ca3c-1a9e-4162-af3a-8d43fcf56867",
							"primaryColumnValue": {
								"getValueMethod": "getPrimaryColumnValue"
							}
						}
					}
				}
			}
		}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocChartFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocChart"
				}
			},
			"CrocDeliverySchedule1Detail7a28a292": {
				"schemaName": "CrocDeliverySchedule1Detail",
				"entitySchemaName": "CrocDeliverySchedule",
				"filter": {
					"detailColumn": "CrocChart",
					"masterColumn": "Id"
				}
			},
			"CrocLoadingSchedule1Detail5970fbd6": {
				"schemaName": "CrocLoadingSchedule1Detail",
				"entitySchemaName": "CrocLoadingSchedule",
				"filter": {
					"detailColumn": "CrocChart",
					"masterColumn": "Id"
				}
			},
			"CrocHistoryChartChange1Detail1e9491f2": {
				"schemaName": "CrocHistoryChartChange1Detail",
				"entitySchemaName": "CrocHistoryChartChange",
				"filter": {
					"detailColumn": "CrocChart",
					"masterColumn": "Id"
				}
			},
			"CrocDeviationReasonSchedule1Detail8afc6bbc": {
				"schemaName": "CrocDeviationReasonSchedule1Detail",
				"entitySchemaName": "CrocDeviationReasonSchedule",
				"filter": {
					"detailColumn": "CrocChart",
					"masterColumn": "Id"
				}
			},
			"CrocDocumentRegistry1Detailb849f636": {
				"schemaName": "CrocDocumentRegistry1Detail",
				"entitySchemaName": "CrocDocumentRegistry",
				"filter": {
					"detailColumn": "CrocChart",
					"masterColumn": "Id"
				}
			},
			"CrocRegisterLoading1Detailb849f636": {
				"schemaName": "CrocRegisterLoading1Detail",
				"entitySchemaName": "CrocRegisterLoading",
				"filter": {
					"detailColumn": "CrocChart",
					"masterColumn": "Id"
				}
			},
			"CrocRegisterUnloading1Detail6fc7a3bc": {
				"schemaName": "CrocRegisterUnloading1Detail",
				"entitySchemaName": "CrocRegisterUnloading",
				"filter": {
					"detailColumn": "CrocChart",
					"masterColumn": "Id"
				}
			},
			"CrocDocumentUnloadings1Detail6fc7a3bc": {
				"schemaName": "CrocDocumentUnloadings1Detail",
				"entitySchemaName": "CrocDocumentUnloadings",
				"filter": {
					"detailColumn": "CrocChart",
					"masterColumn": "Id"
				}
			},
			"CrocDocument3Detail37e7d9d0": {
				"schemaName": "CrocDocument3Detail",
				"entitySchemaName": "CrocDocument",
				"filter": {
					"detailColumn": "CrocChart",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"Tab5bd53612TabLabelGroupa06ab748": {
				"b86820db-1ae9-4f93-bb8a-0e9bdb2dd8a3": {
					"uId": "b86820db-1ae9-4f93-bb8a-0e9bdb2dd8a3",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba3fb45a-2a01-49bd-bb0e-2b01bc011cd0",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						}
					]
				},
				"c1f6e848-80bb-4690-9149-74c79154ce43": {
					"uId": "c1f6e848-80bb-4690-9149-74c79154ce43",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0bb95c3f-fba1-4baa-99c9-bacdaee127e3",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab5bd53612TabLabelGroupff58bfbe": {
				"e3966bf3-c650-49a3-be30-fdf7e7f42ed7": {
					"uId": "e3966bf3-c650-49a3-be30-fdf7e7f42ed7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab5bd53612TabLabelGroup6678b015": {
				"18525484-6a56-414a-b0db-bb2ce169e8cb": {
					"uId": "18525484-6a56-414a-b0db-bb2ce169e8cb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba3fb45a-2a01-49bd-bb0e-2b01bc011cd0",
								"dataValueType": 10
							}
						}
					]
				},
				"159858e2-2dcd-474c-9c56-0cc3682c70d8": {
					"uId": "159858e2-2dcd-474c-9c56-0cc3682c70d8",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab5bd53612TabLabelGroup6e712507": {
				"a84b6da4-9568-4891-a971-64a21ed176bd": {
					"uId": "a84b6da4-9568-4891-a971-64a21ed176bd",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNumberGU12": {
				"7ccafd6c-f3c5-4cb4-8495-9a7750b47482": {
					"uId": "7ccafd6c-f3c5-4cb4-8495-9a7750b47482",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocIsRATWagonOwner"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"d4da7e3f-565d-4674-b7f5-5d7c96086124": {
					"uId": "d4da7e3f-565d-4674-b7f5-5d7c96086124",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "b3cfed4b-e043-45e1-901f-f11ce7604803",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "c95757ca-1df7-4c0e-bb44-b59599917271",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNumberRequestRAT": {
				"10b91873-2498-4906-b2d3-42bf6bbb2cd5": {
					"uId": "10b91873-2498-4906-b2d3-42bf6bbb2cd5",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocIsRATWagonOwner"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"2b306b6d-87b1-42e1-ada5-8c92117d7c85": {
					"uId": "2b306b6d-87b1-42e1-ada5-8c92117d7c85",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "b3cfed4b-e043-45e1-901f-f11ce7604803",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "c95757ca-1df7-4c0e-bb44-b59599917271",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDeliveryTime": {
				"0bf02221-ad4d-4a42-ac94-c2a3d9d40487": {
					"uId": "0bf02221-ad4d-4a42-ac94-c2a3d9d40487",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0bb95c3f-fba1-4baa-99c9-bacdaee127e3",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocQuantityFactTon": {
				"19cf4f3c-fb38-45ff-9dca-84b703250d81": {
					"uId": "19cf4f3c-fb38-45ff-9dca-84b703250d81",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0bb95c3f-fba1-4baa-99c9-bacdaee127e3",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocFactDeliveryTransport": {
				"5194f6c6-9d9b-4187-a42c-92b9a0470b7d": {
					"uId": "5194f6c6-9d9b-4187-a42c-92b9a0470b7d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0bb95c3f-fba1-4baa-99c9-bacdaee127e3",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocBalanceDeliveredTon": {
				"bab61a54-65ea-4b72-ba90-5f45cfaf7cd6": {
					"uId": "bab61a54-65ea-4b72-ba90-5f45cfaf7cd6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0bb95c3f-fba1-4baa-99c9-bacdaee127e3",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocChartDeviationTon": {
				"2ad99994-0d82-4c8c-b978-f63016a125e8": {
					"uId": "2ad99994-0d82-4c8c-b978-f63016a125e8",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0bb95c3f-fba1-4baa-99c9-bacdaee127e3",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocFactLoadingTon": {
				"28af3485-144a-4cb4-bc20-b730dd72e23f": {
					"uId": "28af3485-144a-4cb4-bc20-b730dd72e23f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDateTransferOwnership": {
				"41cc854f-de7b-4a7d-9ca2-3c04da71c6ad": {
					"uId": "41cc854f-de7b-4a7d-9ca2-3c04da71c6ad",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocRemainingExportedTon": {
				"a721b5ec-facb-46aa-bf09-96c1c7c8e078": {
					"uId": "a721b5ec-facb-46aa-bf09-96c1c7c8e078",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDeliveryTimeFrom": {
				"c4cf684b-ea7e-47db-a55c-4af4abf46cb2": {
					"uId": "c4cf684b-ea7e-47db-a55c-4af4abf46cb2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0bb95c3f-fba1-4baa-99c9-bacdaee127e3",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDateTransferOwnershipFrom": {
				"ec72a911-feea-4df7-9d3e-613f05e5256a": {
					"uId": "ec72a911-feea-4df7-9d3e-613f05e5256a",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDeliverySchedule1Detail7a28a292": {
				"1c6cb8a2-60a0-4cc4-9efd-d4e899c90ecd": {
					"uId": "1c6cb8a2-60a0-4cc4-9efd-d4e899c90ecd",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba3fb45a-2a01-49bd-bb0e-2b01bc011cd0",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						}
					]
				},
				"99f6a7b0-f4a9-4021-8901-d6b5975ee837": {
					"uId": "99f6a7b0-f4a9-4021-8901-d6b5975ee837",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocWarehouseUnloading": {
				"903f5c4b-a226-4585-82cf-ff114ae38544": {
					"uId": "903f5c4b-a226-4585-82cf-ff114ae38544",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocRegisterUnloading1Detail6fc7a3bc": {
				"180f804a-4af7-4b48-a5cb-11722c244c68": {
					"uId": "180f804a-4af7-4b48-a5cb-11722c244c68",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				},
				"97d23135-d815-4b6f-b376-8ebb83f6ab04": {
					"uId": "97d23135-d815-4b6f-b376-8ebb83f6ab04",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab5f8288b7TabLabelGroupb3a4c346": {
				"42dd43c0-55fa-478c-a630-e887d1f4fc61": {
					"uId": "42dd43c0-55fa-478c-a630-e887d1f4fc61",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				},
				"3ca2ad7b-0966-4dfe-89bd-d4f755b742c3": {
					"uId": "3ca2ad7b-0966-4dfe-89bd-d4f755b742c3",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocConsignee": {
				"fbbbb77a-d0b4-4a04-95c9-f80fbed775f3": {
					"uId": "fbbbb77a-d0b4-4a04-95c9-f80fbed775f3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				},
				"49ecbf15-c723-4f44-9e03-a685659a1473": {
					"uId": "49ecbf15-c723-4f44-9e03-a685659a1473",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNormDeliveryOneDayTon": {
				"3df1699a-f0e2-4ccd-90ab-e03110f22bea": {
					"uId": "3df1699a-f0e2-4ccd-90ab-e03110f22bea",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				},
				"38998616-d14e-47a4-b948-34f307160b5b": {
					"uId": "38998616-d14e-47a4-b948-34f307160b5b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocTotalTransportNumber": {
				"9e1ba347-0bbd-4738-99fe-02633f9a4480": {
					"uId": "9e1ba347-0bbd-4738-99fe-02633f9a4480",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				},
				"437f4231-0cb6-42df-bda1-ddfc860b5775": {
					"uId": "437f4231-0cb6-42df-bda1-ddfc860b5775",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab376bf607TabLabel": {
				"4db2d13a-0c06-4c16-bb07-baa76a0250fe": {
					"uId": "4db2d13a-0c06-4c16-bb07-baa76a0250fe",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				},
				"4053af2d-4f96-49a6-bac5-6a4fbcde7084": {
					"uId": "4053af2d-4f96-49a6-bac5-6a4fbcde7084",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba3fb45a-2a01-49bd-bb0e-2b01bc011cd0",
								"dataValueType": 10
							}
						}
					]
				},
				"fca734b1-2246-4ad5-a97c-67b5c383953d": {
					"uId": "fca734b1-2246-4ad5-a97c-67b5c383953d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				},
				"ba002115-26a4-427c-8386-bafe8a0ef634": {
					"uId": "ba002115-26a4-427c-8386-bafe8a0ef634",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocHistoryChartChange1Detail1e9491f2": {
				"cb1b0736-7923-46ff-ba18-bc3cec0bbc41": {
					"uId": "cb1b0736-7923-46ff-ba18-bc3cec0bbc41",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocChartStatusGeneral": {
				"d00bd275-d944-47c6-b83a-8b5461f4c3da": {
					"uId": "d00bd275-d944-47c6-b83a-8b5461f4c3da",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocExporter"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocExporter"
							}
						}
					]
				}
			},
			"CrocWarehouseLoading": {
				"703baf0d-e53b-4255-9999-8e5478881121": {
					"uId": "703baf0d-e53b-4255-9999-8e5478881121",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba3fb45a-2a01-49bd-bb0e-2b01bc011cd0",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						}
					]
				},
				"c206016a-3dd4-4cc7-8b4e-acf8dbd4ba18": {
					"uId": "c206016a-3dd4-4cc7-8b4e-acf8dbd4ba18",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "0bb95c3f-fba1-4baa-99c9-bacdaee127e3",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocConsignorGU12": {
				"a1e45867-7204-41dc-96d5-9c2f0e040046": {
					"uId": "a1e45867-7204-41dc-96d5-9c2f0e040046",
					"enabled": true,
					"removed": true,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "CrocShipper",
							"attributePath": "Name"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocConsignorGU12"
							}
						}
					]
				},
				"8fb196a9-a326-4bcd-9fb0-050dde626d1f": {
					"uId": "8fb196a9-a326-4bcd-9fb0-050dde626d1f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "b3cfed4b-e043-45e1-901f-f11ce7604803",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "c95757ca-1df7-4c0e-bb44-b59599917271",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocAverageWeightTransport": {
				"42efe497-8e37-492c-9cc5-6a4163025fcd": {
					"uId": "42efe497-8e37-492c-9cc5-6a4163025fcd",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDeliveryDateFrom": {
				"30320071-ba60-4189-827d-2bf123303c91": {
					"uId": "30320071-ba60-4189-827d-2bf123303c91",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDeliveryDateTo": {
				"1feb8a57-8432-401b-a6b9-8e75ba64bfe3": {
					"uId": "1feb8a57-8432-401b-a6b9-8e75ba64bfe3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocLoadingSchedule1Detail5970fbd6": {
				"52767dd0-8ae4-43b7-840d-4af5f0c91ece": {
					"uId": "52767dd0-8ae4-43b7-840d-4af5f0c91ece",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocRegisterLoading1Detailb849f636": {
				"26740910-5867-4299-ae8c-30149e5fb266": {
					"uId": "26740910-5867-4299-ae8c-30149e5fb266",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab5f8288b7TabLabelGroup9f05448f": {
				"0564911d-ffbd-47c9-a138-93dc1849f9a0": {
					"uId": "0564911d-ffbd-47c9-a138-93dc1849f9a0",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocContract": {
				"a9b72d73-c5df-4536-8b44-049014d8cf1b": {
					"uId": "a9b72d73-c5df-4536-8b44-049014d8cf1b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"CrocConsigneeGU12": {
				"a18f6476-6996-4591-8cba-4a7cd0fb622b": {
					"uId": "a18f6476-6996-4591-8cba-4a7cd0fb622b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "b3cfed4b-e043-45e1-901f-f11ce7604803",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "c95757ca-1df7-4c0e-bb44-b59599917271",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocIsRATWagonOwner": {
				"aee2aeab-3abc-4349-8f02-79d282c3989f": {
					"uId": "aee2aeab-3abc-4349-8f02-79d282c3989f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "b3cfed4b-e043-45e1-901f-f11ce7604803",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "c95757ca-1df7-4c0e-bb44-b59599917271",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDestinationStation": {
				"9eb0a719-6be6-44e4-b122-e0ddf2aaee4f": {
					"uId": "9eb0a719-6be6-44e4-b122-e0ddf2aaee4f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "b3cfed4b-e043-45e1-901f-f11ce7604803",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "c95757ca-1df7-4c0e-bb44-b59599917271",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDepartureStation": {
				"67c29103-328f-4908-925c-8f39697110ba": {
					"uId": "67c29103-328f-4908-925c-8f39697110ba",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "b3cfed4b-e043-45e1-901f-f11ce7604803",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocChartStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "c95757ca-1df7-4c0e-bb44-b59599917271",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDocumentUnloadings1Detail6fc7a3bc": {
				"acefb825-1019-4853-a948-e3175d4005ec": {
					"uId": "acefb825-1019-4853-a948-e3175d4005ec",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDocumentRegistry1Detailb849f636": {
				"b854174d-94b3-46b0-b68d-4c8c52669973": {
					"uId": "b854174d-94b3-46b0-b68d-4c8c52669973",
					"enabled": false,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"TabLoadingTabLabel": {
				"b5c089e5-ac93-408a-bab7-242f633187c8": {
					"uId": "b5c089e5-ac93-408a-bab7-242f633187c8",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTransportType"
							},
							"rightExpression": {
								"type": 0,
								"value": "efa3fcd2-6eba-406b-962b-3eb6b82be6cc",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab5f8288b7TabLabel": {
				"cc25eef0-ecbe-4935-8f6a-ab3e717f471e": {
					"uId": "cc25eef0-ecbe-4935-8f6a-ab3e717f471e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			init: function() {
				this.callParent(arguments);
				this.loadSettingUrl();
			},

			initSysSettingsValue: function(settingCode, callback, scope) {
				this.Terrasoft.SysSettings.querySysSettingsItem(settingCode,
					function(value) {
						this.set(settingCode, value);
						if (this.Ext.isFunction(callback)) {callback.call(scope);}
					}, this);
			},

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.initApproveButtonVisible();
				this.setLoadGU12ButtonVisible();
			},

			initApproveButtonVisible: function() {
				this.Terrasoft.chain(
					function(next) {
						this.initSysSettingsValue("CrocApproveChartRightsRole", function (){
							next();
						}, this);
					},
					function() {
						this.getApproveRights(function(isApproveRights) {
							this.$CrocIsApproveRights = isApproveRights;
							this.setApproveChartButtonVisible();
						}, this);
					}, this);
			},

			onCrocGU12NumberChanged: function() {
				this.setLoadGU12ButtonVisible();
			},

			setApproveChartButtonVisible: function() {
				this.$ApproveChartButtonVisible = this.$CrocIsApproveRights && !this.isNewMode() && this.get("CrocChartStatus").value !== CrocConstantsJS.CrocChartStatus.Approved.value;
				this.sandbox.publish("ApproveChartButtonVisibleChanged", this.$ApproveChartButtonVisible, [this.sandbox.id]);
			},

			setLoadGU12ButtonVisible: function () {
				this.getGU12Exists(function (result) {
					if (!result) {
						this.$LoadGU12ButtonVisible = this.get("CrocNumberGU12") && this.get("CrocContract") && !this.isNewMode();
						this.sandbox.publish("LoadGU12ButtonVisibleChanged", this.$LoadGU12ButtonVisible, [this.sandbox.id]);
					} else {
						this.$LoadGU12ButtonVisible = false;
						this.sandbox.publish("LoadGU12ButtonVisibleChanged", this.$LoadGU12ButtonVisible, [this.sandbox.id]);
					}
				}, this);
			},

			getGU12Exists: function(callback, scope) {
				var chart = this.$Id;
				if (chart) {
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocDocument"});
					esq.addColumn("CrocChart");
					esq.addColumn("CrocChartStatus");
					esq.addColumn("CrocDocumentType");
					esq.filters.add("CrocChart", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "CrocChart", chart));
					esq.filters.add("CrocDocumentType", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "CrocDocumentType", CrocConstantsJS.CrocDocumentType.ApplicationGU12.value));
					esq.filters.add("CrocDocumentStatus", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "CrocDocumentStatus", CrocConstantsJS.CrocDocumentStatus.Actual.value));
					esq.getEntityCollection(function (response) {
						if (response && response.success) {
							var result = response.collection;
							var isGU12Exists = (result.collection.length !== 0);
							callback.call(scope, isGU12Exists);
						}
					});
				}
			},

			getApproveRights: function (callback, scope) {
				var currentUser = Terrasoft.SysValue.CURRENT_USER.value;
				var elevatorSpecialist = this.$CrocApproveChartRightsRole.value;
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "SysUserInRole"});
				esq.addColumn("SysRole");
				esq.addColumn("SysUser");
				esq.filters.add("SysUser", Terrasoft.createColumnFilterWithParameter(
					Terrasoft.ComparisonType.EQUAL, "SysUser", currentUser));
				esq.filters.add("SysRole", Terrasoft.createColumnFilterWithParameter(
					Terrasoft.ComparisonType.EQUAL, "SysRole", elevatorSpecialist));
				esq.getEntityCollection(function (response) {
					if (response && response.success) {
						var result = response.collection;
						var isElevatorSpecialist = (result.collection.length !== 0);
						callback.call(scope, isElevatorSpecialist);
					}
				}, this);
			},

			onApproveChartButtonClick: function() {
				var chartStatus = {
					value: CrocConstantsJS.CrocChartStatus.Approved.value,
					displayValue: CrocConstantsJS.CrocChartStatus.Approved.displayValue
				};
				this.loadLookupDisplayValue("CrocChartStatus", chartStatus);
				this.save({isSilent: true});
				this.getApproveRights(function(isApproveRights) {
					this.$CrocIsApproveRights = isApproveRights;
					this.setApproveChartButtonVisible();
				}, this);
			},

			onLoadGU12ButtonClick: function() {
				this.save({isSilent: true});
			},

			onFileSelect: function(files) {
				if (files.length <= 0) {
					return;
				}
				const config = this.getUploadConfig(files);
				this.upload(config, function() {
					this.set("FileUploadConfig", null);
				});
			},

			performValues: function(){
				var chartId = this.$Id;
				return {
					chartId: chartId
				}
			},

			getUploadConfig: function(files) {
				const config = this.performValues();
				return {
					scope: this,
					onUpload: this.onUpload,
					onComplete: this.onComplete,
					uploadWebServicePath: "CrocDocumentGU12FilesService/ImportFile",
					entitySchemaName: "CrocDocumentFile",
					columnName: "Data",
					parentColumnName: "CrocDocumentFile",
					parentColumnValue: this.$Id,
					files: files,
					data: config,
					isChunkedUpload: false,
				};
			},

			onUpload: function() {
				this.showBodyMask();
			},

			onComplete: function(error, xhr) {
				this.hideBodyMask();
				var documentId = xhr.response ? JSON.parse(xhr.response) : null;
				if (documentId) {
					var args = {
						sysProcessName: "CrocProcessShowNewGU12DocumentPage",
						parameters: {
							NewDocumentId: documentId
						},
						scope: this
					};
					ProcessModuleUtilities.executeProcess(args);
				}
			},

			upload: function(config) {
				this.Terrasoft.ConfigurationFileApi.upload(config);
			},

			onSendRegisterLoadingClick: function() {
				var rowId = this.get("Id");
				ServiceHelper.callService({
					serviceName: "ExchangeIntegrationService",
					methodName: "SendRegisterLoading",
					data: {
						recordId: rowId
					},
					scope: this
				});
			},

			onSendLoadingPlanClick: function() {
				var rowId = this.get("Id");
				ServiceHelper.callService({
					serviceName: "ExchangeIntegrationService",
					methodName: "SendLoadingPlan",
					data: {
						recordId: rowId
					},
					scope: this
				});
			},

			onSendDeliveryPlanClick: function() {
				var rowId = this.get("Id");
				ServiceHelper.callService({
					serviceName: "ExchangeIntegrationService",
					methodName: "SendDeliveryPlan",
					data: {
						recordId: rowId
					},
					scope: this
				});
			},

			getSmartseedsLink: function(value) {
				return {
					url: value,
					caption: "Smartseeds"
				};
			},

			loadSettingUrl: function() {
				let sysSettings = ["CrocSmartseedsURL"];
				Terrasoft.SysSettings.querySysSettings(sysSettings, function(settings) {
					this.$CrocSmartseedsUrl = settings.CrocSmartseedsURL;
				}, this);
			},

			SSCalculateButtonEnabled: function() {
				if(this.isNewMode()) return false;
				return (this.$CrocWarehouseLoading && this.$CrocWarehouseUnloading && this.$CrocDeliveryDateFrom &&
					this.$CrocProduct && this.$CrocWeight)
			},

			onSSCalculateButtonClick: function() {
				var config = {
					serviceName: "ExchangeIntegrationService",
					methodName: "SendSmartSeedsChartRequest",
					data: {
						config: JSON.stringify({
							recordId: this.$Id,
							originId: this.$CrocWarehouseLoading.value,
							destinationId: this.$CrocWarehouseUnloading.value,
							productId: this.$CrocProduct.value,
							deliveryDate: this.$CrocDeliveryDateFrom,
							weight: this.$CrocWeight
						})
					},
					callback: this.SSCalculateButtonCallback,
					scope: this
				};
				ServiceHelper.callService(config);
			},

			SSCalculateButtonCallback: function(response) {
				if(response && response.SendSmartSeedsChartRequestResult) {
					var result = response.SendSmartSeedsChartRequestResult;
					if(result.success) {
						var fields = [
							"CrocDistanceSmartseeds",
							"CrocNumberFlightSmartseeds",
							"CrocTonCarSmartseeds",
							"CrocTravelTimeSmartseeds",
							"CrocAmountTonSmartseeds",
							"CrocTotalAmountSmartseeds"
						];
						this.reloadCardFromPage(fields, function() {}, this);
					}
					else {
						if(result.errorInfo) {
							this.showInformationDialog(result.errorInfo.message);
						}
						else { console.log(result); }
					}
				}
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "ApproveChartButton",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.ApproveChartButtonCaption"
					},
					"click": {
						"bindTo": "onApproveChartButtonClick"
					},
					"enabled": true,
					"visible": {
						"bindTo": "ApproveChartButtonVisible"
					},
					"style": "green",
					"styles": {
						"textStyle": {
							"margin": "0px 9px 0px 0px"
						}
					},
					"layout": {
						"column": 1,
						"row": 6,
						"colSpan": 1
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LoadGU12Button",
				"values": {
					"visible": {
						"bindTo": "LoadGU12ButtonVisible"
					},
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.LoadGU12ButtonCaption"
					},
					"click": {
						"bindTo": "onLoadGU12ButtonClick"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					},
					"layout": {
						"column": 12,
						"row": 6,
						"colSpan": 6
					},
					"fileUpload": true,
					"filesSelected": {
						"bindTo": "onFileSelect"
					},
					"fileUploadMultiSelect": false
				},
				"parentName": "ActionButtonsContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocNumber823b5612-e354-42bc-a23f-08338377506f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocNumber",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocTransportType366b5e76-c8b6-4078-9802-87ba8abf4b4b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocTransportType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryConditionecf2de18-eea0-416a-9319-fd1c1dca932d",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocDeliveryCondition",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocChartStatus88dc0f7a-82c7-4ec4-95ca-5e7a6bd3e040",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocChartStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocExporter58f96218-3d99-4387-9a74-a821fce17847",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocExporter",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocContractab130066-b8f7-4738-8570-63fafb925b66",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocContract"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocChartStatusGeneral7123a107-94c8-48a2-abe3-bd3c4f8e4842",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocChartStatusGeneral"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Indicatora0faf893-cfe7-4143-b3b3-8ff2668b4553",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 3,
						"column": 0,
						"row": 7,
						"layoutName": "ProfileContainer",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocChartStatusGeneral5a47f2e3-3c18-4831-8f94-1e2235f311dc",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 10,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocChartStatusGeneral",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "Indicator05b47c3e-8d8d-4d74-b047-bd21a7f6c3ee",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 3,
						"column": 0,
						"row": 11,
						"layoutName": "ProfileContainer",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5bd53612TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGroup09b71d65",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5bd53612TabLabelGroup09b71d65GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab5bd53612TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab5bd53612TabLabelGroup09b71d65",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocProduct8c093e41-df1b-4f2f-8bb2-a55809d03a28",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocProduct",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryDateFromb05cf59c-7311-4b46-afb6-893ff71592f2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocDeliveryDateFrom",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocProductClass690edfaa-1635-4da3-a2a1-1c5ea07fb4f6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocProductClass",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryDateTo97e985c9-de0c-4559-aa71-a361d01f1232",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocDeliveryDateTo",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocNormDeliveryOneDayTon3720d7b5-bd41-4fc6-b09f-c6282b83645e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocNormDeliveryOneDayTon",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocShipper485f8383-2b2e-455f-8028-68bde15c00c4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocShipper",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocWeight2d325a98-90d0-4e68-b6ec-4ea02f9934b0",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocWeight",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocConsignee4eb5fa6c-5d02-4051-9079-59f121782736",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocConsignee",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocRegion551b957d-452f-46d8-b811-4558aea281f9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocRegion",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "CrocWarehouseLoadingb9d4b4b9-c272-42f3-850c-03fce6c85b04",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocWarehouseLoading",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "CrocCropYear26d051a1-7210-444a-b1bb-e75a110915e6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocCropYear",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "CrocWarehouseUnloading9d08256c-81b9-4b6c-8621-e75fcc5bd0dc",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocWarehouseUnloading",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "CrocTotalTransportNumber8b1c63a2-e9f7-4dac-af83-e990b3a4390e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocTotalTransportNumber",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "CrocAverageWeightTransporte2932372-7f93-4928-94be-e9d6b26db324",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocAverageWeightTransport",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "CrocFactLoadingTon7a8c0a84-b1a9-431f-8ddd-05c83d2902e2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Tab5bd53612TabLabelGridLayoute8a55b7b"
					},
					"bindTo": "CrocFactLoadingTon"
				},
				"parentName": "Tab5bd53612TabLabelGridLayoute8a55b7b",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGroup6e712507",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5bd53612TabLabelGroup6e712507GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab5bd53612TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGridLayout2ba85024",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab5bd53612TabLabelGroup6e712507",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocNumberGU1202e5a3c6-9793-462d-8a20-f95118848ba1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayout2ba85024"
					},
					"bindTo": "CrocNumberGU12",
					"enabled": true
				},
				"parentName": "Tab5bd53612TabLabelGridLayout2ba85024",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDestinationStationce5a1aab-ed40-4bee-b55e-76feed6a8b63",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayout2ba85024"
					},
					"bindTo": "CrocDestinationStation",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab5bd53612TabLabelGridLayout2ba85024",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocConsignorGU12801b23f9-6109-43b4-be07-f42188cc1938",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab5bd53612TabLabelGridLayout2ba85024"
					},
					"bindTo": "CrocConsignorGU12",
					"enabled": true
				},
				"parentName": "Tab5bd53612TabLabelGridLayout2ba85024",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocDepartureStation3caaeca7-2a55-4efc-a06e-7f58100755a7",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab5bd53612TabLabelGridLayout2ba85024"
					},
					"bindTo": "CrocDepartureStation",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab5bd53612TabLabelGridLayout2ba85024",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRING84848841-2ab4-4205-940d-000ea47b7c4e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab5bd53612TabLabelGridLayout2ba85024"
					},
					"bindTo": "CrocConsigneeGU12",
					"enabled": true
				},
				"parentName": "Tab5bd53612TabLabelGridLayout2ba85024",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocNumberRequestRATe7481001-4bb9-4dd6-8830-f6ce110a466c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab5bd53612TabLabelGridLayout2ba85024"
					},
					"bindTo": "CrocNumberRequestRAT",
					"enabled": true
				},
				"parentName": "Tab5bd53612TabLabelGridLayout2ba85024",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocIsRATWagonOwner17d6c59c-901c-4743-95e9-85e75c17dedc",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab5bd53612TabLabelGridLayout2ba85024"
					},
					"bindTo": "CrocIsRATWagonOwner",
					"enabled": true
				},
				"parentName": "Tab5bd53612TabLabelGridLayout2ba85024",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGroupa06ab748",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5bd53612TabLabelGroupa06ab748GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab5bd53612TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGridLayout11420ada",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab5bd53612TabLabelGroupa06ab748",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIMEaac6c9ff-bdb4-4180-b74a-15aa40919960",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayout11420ada"
					},
					"bindTo": "CrocDeliveryTimeFrom",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayout11420ada",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryTime3d67f769-af94-4427-8e5a-db87521259b3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayout11420ada"
					},
					"bindTo": "CrocDeliveryTime",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayout11420ada",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocDateTransferOwnershipFrome885dea1-16a6-45cd-a9e3-839f3bb40653",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab5bd53612TabLabelGridLayout11420ada"
					},
					"bindTo": "CrocDateTransferOwnershipFrom",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayout11420ada",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocDateTransferOwnership99e5fee0-118e-4e10-920f-0e87e707bd6b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab5bd53612TabLabelGridLayout11420ada"
					},
					"bindTo": "CrocDateTransferOwnership",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayout11420ada",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocQuantityFactTondd99edeb-d422-4482-a0fe-8529ad5e33cb",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab5bd53612TabLabelGridLayout11420ada"
					},
					"bindTo": "CrocQuantityFactTon",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayout11420ada",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocBalanceDeliveredTona8b8d67e-4165-4618-b93a-e1f776312e86",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab5bd53612TabLabelGridLayout11420ada"
					},
					"bindTo": "CrocBalanceDeliveredTon",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayout11420ada",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocFactDeliveryTransportb3d2402f-d49d-48a1-92cd-d5aa71ef0b4c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab5bd53612TabLabelGridLayout11420ada"
					},
					"bindTo": "CrocFactDeliveryTransport",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayout11420ada",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocRemainingExportedTon49ac2642-7c33-4eda-95a4-3aab5237e27e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tab5bd53612TabLabelGridLayout11420ada"
					},
					"bindTo": "CrocRemainingExportedTon",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayout11420ada",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocChartDeviationTona398a1a3-b386-4565-945f-c005b224a80e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab5bd53612TabLabelGridLayout11420ada"
					},
					"bindTo": "CrocChartDeviationTon",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayout11420ada",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGroupff58bfbe",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5bd53612TabLabelGroupff58bfbeGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab5bd53612TabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGridLayoutf5add94b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab5bd53612TabLabelGroupff58bfbe",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIMEe067666e-b5d8-4f08-b25a-d0c4079ac26e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayoutf5add94b"
					},
					"bindTo": "CrocDateTransferOwnershipFrom",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoutf5add94b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDateTransferOwnershipd2e31dd1-acb1-469d-a9ab-30690a1bc359",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayoutf5add94b"
					},
					"bindTo": "CrocDateTransferOwnership",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoutf5add94b",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocFactLoadingTon6d3cef9f-eb08-4ac6-a3e6-837b21ca7c02",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab5bd53612TabLabelGridLayoutf5add94b"
					},
					"bindTo": "CrocFactLoadingTon",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoutf5add94b",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocRemainingExportedTon3450c8b6-6166-4f68-bf2c-f696999d20a8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab5bd53612TabLabelGridLayoutf5add94b"
					},
					"bindTo": "CrocRemainingExportedTon",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoutf5add94b",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGroup6678b015",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5bd53612TabLabelGroup6678b015GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab5bd53612TabLabel",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab5bd53612TabLabelGridLayoutb0f90904",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab5bd53612TabLabelGroup6678b015",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDateTransferOwnershipFrom7e56d54b-4e25-4681-ba0b-3fa7e41f96f1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayoutb0f90904"
					},
					"bindTo": "CrocDateTransferOwnershipFrom",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoutb0f90904",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDateTransferOwnership6db5012e-476c-412c-bda2-1a50e0e0b8a8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab5bd53612TabLabelGridLayoutb0f90904"
					},
					"bindTo": "CrocDateTransferOwnership",
					"enabled": false
				},
				"parentName": "Tab5bd53612TabLabelGridLayoutb0f90904",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocLoadingSchedule1Detail5970fbd6",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab5bd53612TabLabel",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocDeliverySchedule1Detail7a28a292",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab5bd53612TabLabel",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "TabLoadingTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabLoadingTabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocDocumentRegistry1Detailb849f636",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "TabLoadingTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocRegisterLoading1Detailb849f636",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "TabLoadingTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab5f8288b7TabLabelGroup9f05448f",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5f8288b7TabLabelGroup9f05448fGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "TabLoadingTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab5f8288b7TabLabelGridLayout010a440c",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab5f8288b7TabLabelGroup9f05448f",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Indicatore086c8f3-5fee-4317-8845-a19538e502bb",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 4,
						"column": 0,
						"row": 0,
						"layoutName": "Tab5f8288b7TabLabelGridLayout010a440c",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab5f8288b7TabLabelGridLayout010a440c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Indicatorcbae26d1-af16-495b-8091-ed714e1e5fdb",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 4,
						"column": 6,
						"row": 0,
						"layoutName": "Tab5f8288b7TabLabelGridLayout010a440c",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab5f8288b7TabLabelGridLayout010a440c",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Indicator4c8f799d-2a6d-40e8-b8ee-2c9daeb35040",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 4,
						"column": 12,
						"row": 0,
						"layoutName": "Tab5f8288b7TabLabelGridLayout010a440c",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab5f8288b7TabLabelGridLayout010a440c",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Indicator0c1acce8-68be-43e7-9152-c437bc20df7b",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 4,
						"column": 18,
						"row": 0,
						"layoutName": "Tab5f8288b7TabLabelGridLayout010a440c",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab5f8288b7TabLabelGridLayout010a440c",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab5f8288b7TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5f8288b7TabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocDocumentUnloadings1Detail6fc7a3bc",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab5f8288b7TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocRegisterUnloading1Detail6fc7a3bc",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab5f8288b7TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab5f8288b7TabLabelGroupb3a4c346",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab5f8288b7TabLabelGroupb3a4c346GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab5f8288b7TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab5f8288b7TabLabelGridLayoutb2056eec",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab5f8288b7TabLabelGroupb3a4c346",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Indicator81351b4c-4e89-43af-9af9-f528d26f0ca8",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 4,
						"column": 0,
						"row": 0,
						"layoutName": "Tab5f8288b7TabLabelGridLayoutb2056eec",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab5f8288b7TabLabelGridLayoutb2056eec",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Indicator32f68d29-6d37-468c-9956-e5544377b964",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 4,
						"column": 6,
						"row": 0,
						"layoutName": "Tab5f8288b7TabLabelGridLayoutb2056eec",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab5f8288b7TabLabelGridLayoutb2056eec",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Indicatorffc99b11-492b-4bb1-b4ef-18206f77af26",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 4,
						"column": 12,
						"row": 0,
						"layoutName": "Tab5f8288b7TabLabelGridLayoutb2056eec",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab5f8288b7TabLabelGridLayoutb2056eec",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Indicator97dff6e8-261c-4cba-8320-b8055be05e61",
				"values": {
					"layout": {
						"colSpan": 6,
						"rowSpan": 4,
						"column": 18,
						"row": 0,
						"layoutName": "Tab5f8288b7TabLabelGridLayoutb2056eec",
						"useFixedColumnHeight": true
					},
					"itemType": 4,
					"classes": {
						"wrapClassName": [
							"card-widget-grid-layout-item"
						]
					}
				},
				"parentName": "Tab5f8288b7TabLabelGridLayoutb2056eec",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab13acf66fTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab13acf66fTabLabelTabCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocDeviationReasonSchedule1Detail8afc6bbc",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab13acf66fTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocHistoryChartChange1Detail1e9491f2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab13acf66fTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocDocument3Detail37e7d9d0",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab13acf66fTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab376bf607TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab376bf607TabLabelTabCaption"
					},
					"items": [],
					"order": 4
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab376bf607TabLabelGroup572b841f",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab376bf607TabLabelGroup572b841fGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab376bf607TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab376bf607TabLabelGridLayout450d06de",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab376bf607TabLabelGroup572b841f",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocWarehouseLoadingSmart",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocWarehouseLoading",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocProductSmart",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocProduct",
					"contentType": 5,
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocWarehouseUnloadingSmart",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocWarehouseUnloading",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryDateFromSmart",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocDeliveryDateFrom",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocWeightSmart",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocWeight",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocSmartseedsCalculateButton",
				"values": {
					"itemType": 5,
					"style": "blue",
					"click": {
						"bindTo": "onSSCalculateButtonClick"
					},
					"markerValue": "SSCalculateButton",
					"caption": {
						"bindTo": "Resources.Strings.CrocSmartseedsCalculateButtonCaption"
					},
					"enabled": {
						"bindTo": "SSCalculateButtonEnabled"
					},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					}
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocNumberFlightSmartseeds1463125b-6ee0-45c4-a720-4a917307a624",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocNumberFlightSmartseeds",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocDistanceSmartseedsf55fea0f-e1ce-4a14-ac7c-22c2d07bca6a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocDistanceSmartseeds",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocTonCarSmartseedse418f343-b287-4e9a-9ac9-315374ed6106",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocTonCarSmartseeds",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "CrocTravelTimeSmartseeds8ec66787-4881-42f8-b7d6-de5e934bd0ff",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocTravelTimeSmartseeds",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "CrocAmountTonSmartseedscc229fcb-e4bb-4504-a95c-46c06f33d65f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocAmountTonSmartseeds",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "CrocTotalAmountSmartseedsb11f68d9-06d3-4755-9c8d-5bea865a4de2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 7,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocTotalAmountSmartseeds",
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "CrocSmartseedsUrl",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocSmartseedsUrl",
					"controlConfig": {
						"className": "Terrasoft.TextEdit"
					},
					"showValueAsLink": true,
					"href": {
						"bindTo": "CrocSmartseedsUrl",
						"bindConfig": {
							"converter": "getSmartseedsLink"
						}
					},
					"linkclick": {
						"bindTo": "openNewTab"
					},
					"enabled": false
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "CrocComment5b00ab79-4207-445b-bcd2-25eec26ca38e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab376bf607TabLabelGridLayout450d06de"
					},
					"bindTo": "CrocComment",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tab376bf607TabLabelGridLayout450d06de",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 5
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 6
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
