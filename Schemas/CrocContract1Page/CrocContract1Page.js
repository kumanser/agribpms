define("CrocContract1Page", [], function() {
	return {
		entitySchemaName: "CrocContract",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocContractFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocContract"
				}
			},
			"CrocContract1Detail694e1a8d": {
				"schemaName": "CrocContract1Detail",
				"entitySchemaName": "CrocContract",
				"filter": {
					"detailColumn": "CrocParentContract",
					"masterColumn": "Id"
				}
			},
			"CrocContractDocument1Detail940b0a77": {
				"schemaName": "CrocContractDocument1Detail",
				"entitySchemaName": "CrocContractDocument",
				"filter": {
					"detailColumn": "CrocContract",
					"masterColumn": "Id"
				}
			},
			"CrocMutualSettlements1Detail56cff1ed": {
				"schemaName": "CrocMutualSettlements1Detail",
				"entitySchemaName": "CrocMutualSettlements",
				"filter": {
					"detailColumn": "CrocContract",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		messages: {
			"SendToExporterButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
		},
		methods: {

			onEntityInitialized: function (arguments) {
				this.callParent(arguments);
				this.sandbox.publish("SendToExporterButtonVisibleChanged", false, ["SectionModuleV2_InvoiceSectionV2"]);
			},

			loadESNFeed: function() {
				var moduleId = this.getSocialFeedSandboxId();
				var rendered = this.sandbox.publish("RerenderModule", {
					renderTo: "ESNFeed"
				}, [moduleId]);
				if (!rendered) {
					var esnFeedModuleConfig = {
						renderTo: "ESNFeed",
						id: moduleId
					};
					var activeSocialMessageId = this.get("ActiveSocialMessageId") ||
						this.getDefaultValueByName("ActiveSocialMessageId");
					if (!Ext.isEmpty(activeSocialMessageId)) {
						esnFeedModuleConfig.parameters = {activeSocialMessageId: activeSocialMessageId};
					}
					this.sandbox.loadModule("CrocFeedModule", esnFeedModuleConfig);
				}
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocContractNumber1e9bfa81-97b8-4022-bdc6-dc4782495472",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocContractNumber",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP81554193-cebc-4485-b4e6-2dd5651b6b8b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 11,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocContractStatus",
					"enabled": false,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUPf674d038-14a1-40da-9827-f24818379c16",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocContractType",
					"enabled": false,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DATETIME227221e4-0b8f-40cb-9d7f-51418122967a",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocStartDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocAccount4b97d65f-9412-4fec-a713-27b243929e8c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 11,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocAccount",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "DATETIME64782972-7bf2-43a0-99dc-c960141426f5",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocEndDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocAssignedManager7e99d022-0641-4fc5-b121-8ccfe3334cd9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 11,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocAssignedManager",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "DATETIME841e147a-f4c4-47db-8115-d9b160bbcbdb",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 11,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocContractDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "Tab633724eaTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab633724eaTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocContract1Detail694e1a8d",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab633724eaTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocContractDocument1Detail940b0a77",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab633724eaTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocMutualSettlements1Detail56cff1ed",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab633724eaTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
