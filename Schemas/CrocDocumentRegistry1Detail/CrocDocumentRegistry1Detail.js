define("CrocDocumentRegistry1Detail", ["ModalBox","CrocExcelImportModule", "ServiceHelper"], function(ModalBox, ImportModule, ServiceHelper) {
	return {
		entitySchemaName: "CrocDocumentRegistry",
		messages: {
			RunRegisterLoadingExcelImport: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			AfterRegisterLoadingImport: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			SelectDocumentForLoading: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "CrocLoadRegisterButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onAddFileClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": "Загрузить реестр погрузок",
					"fileTypeFilter":[".xls",".xlsx"],
					enabled: true,
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
			getCopyRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,

			addGridDataColumns: function(esq) {
				this.callParent(arguments);
				esq.addColumn("CrocGuid");
			},

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("AfterRegisterLoadingImport", this.onImportComplead, this, [this.getAfterModule()]);
			},

			getRunnerModule: function() {
				var id = this.get("MasterRecordId");
				return "RunRegisterLoadingExcelImport_" + id;
			},

			getAfterModule: function() {
				var id = this.get("MasterRecordId");
				return "AfterRegisterLoadingImport_" + id;
			},

			onImportComplead: function() {
				this.reloadGridData();
			},

			onAddFileClick: function () {
				this.sandbox.publish("RunRegisterLoadingExcelImport", null, [this.getRunnerModule()])
			},

			rowSelected: function(primaryColumnValue) {
				this.callParent(arguments);
				this.getSelectedRow(primaryColumnValue);
			},

			getSelectedRow: function (primaryColumnValue) {
				if(primaryColumnValue) {
					var masterRecId = this.get("MasterRecordId");
					var activeRow = this.getGridData().get(primaryColumnValue);
					if(activeRow && masterRecId) {
						var rowId = activeRow.get("CrocGuid");
						let result = {
							masterRecId: masterRecId,
							externId: rowId
						}
						this.sandbox.publish("SelectDocumentForLoading", result, [masterRecId]);
					}
				}
			}
		}
	};
});
