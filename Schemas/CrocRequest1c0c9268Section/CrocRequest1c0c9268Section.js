define("CrocRequest1c0c9268Section", ["RightUtilities", "ServiceHelper"], function(RightUtilities, ServiceHelper) {
	return {
		entitySchemaName: "CrocRequest",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		messages: {
			"AcceptRequestButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"CancelRequestButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
		},
		attributes: {
			"AcceptRequestButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"CancelRequestButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsUnsetIsCheckActButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"AllSelectedRowsCount": {
				"dataValueType": Terrasoft.DataValueType.INTEGER,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": 0
			},
		},
		methods: {

			unSetMultiSelect: function() {
				this.callParent(arguments);
				this.set("MultiSelect", false);
			},

			createSelectAllButton: function() {
				return this.getButtonMenuItem({
					"Caption": {"bindTo": "Resources.Strings.SelectAllButtonCaption"},
					"Click": {"bindTo": "selectAllItems"},
					"Visible": {"bindTo": "isSelectAllModeVisible"},
					"IsEnabledForSelectedAll": true
				});
			},

			onSelectedRowsChange: function() {
				if (this.get("MultiSelect")) {
					if (this.get("SelectAll")) {
						var selectedRows = this.get("SelectedRows");
						if (this.$AllSelectedRowsCount < selectedRows.length) this.$AllSelectedRowsCount = selectedRows.length
						else this.set("SelectAll", false);
					}
				} else this.set("SelectAll", false);
			},

			unSelectRecords: function() {
				this.callParent(arguments);
				this.set("SelectAll", false);
			},

			selectAllItems: function () {
				this.set("IsPageable", false);
				this.set("RowCount", -1);
				this.set("SelectAll", true);
				this.getGridData().clear();
				this.reloadGridData();
			},

			onGridDataLoaded: function(response) {
				this.callParent(arguments);
				if (this.get("SelectAll")) {
					if (!this.get("MultiSelect")) {
						this.setMultiSelect();
					}
					this.set('SelectedRows', response.collection.collection.keys);
				}
			},

			init: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("AcceptRequestButtonVisibleChanged",  function(isVisible){
					this.$AcceptRequestButtonVisible = isVisible;
				}, this, ["SectionModuleV2_InvoiceSectionV2"]);
				this.sandbox.subscribe("CancelRequestButtonVisibleChanged",  function(isVisible){
					this.$CancelRequestButtonVisible = isVisible;
				}, this, ["SectionModuleV2_InvoiceSectionV2"]);
				this.canUnsetIsCheckAct();
			},

			isUnsetIsCheckActButtonEnabled: function() {
				var activeRowId = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				return activeRowId ? true : selectedRows ? (selectedRows.length > 0) : false;
			},

			canUnsetIsCheckAct: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanUnsetIsCheckActRequest"},
					function(result) {
						this.$IsUnsetIsCheckActButtonVisible = result;
					}, this);
			},

			checkIsCheckActInSelectedRows: function(callback, scope) {
				var activeRow = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocRequest"});
				esq.addColumn("CrocIsCheckAct");
				var filterGroup = Terrasoft.createFilterGroup();
				filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.OR;
				if (!!activeRow) filterGroup.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Id", activeRow));
				if (!!selectedRows) filterGroup.addItem(Terrasoft.createColumnInFilterWithParameters("Id", selectedRows));
				esq.filters.addItem(filterGroup);
				esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "CrocIsCheckAct", true));
				esq.getEntityCollection(function(response) {
					if (response && response.success) {
						callback.call(scope, response.collection.collection.getCount());
					} else callback.call(scope, 0);
				}, this);
			},

			onUnsetIsCheckActClick: function() {
				this.checkIsCheckActInSelectedRows(function(count) {
					if (count !== 0) {
						this.runUnsetRequestIsCheckAct();
					} else {
						this.showConfirmationDialog(this.get("Resources.Strings.EmptyIsCheckActError"));
					}
				}, this);
			},

			runUnsetRequestIsCheckAct: function() {
				var activeRowId = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				var commentId = (selectedRows && selectedRows.length) ? selectedRows.join(';') : (activeRowId + ";");
				var serviceConfig = {
					serviceName: "CrocRequestService",
					methodName: "UnsetRequestIsCheckAct",
					callback: function(response){
						if (response) {
							console.log("CrocRequestService : UnsetRequestIsCheckAct - OK");
						}
					},
					data: {
						requestsStr: commentId
					},
					scope: this,
					timeout: 250000
				};
				ServiceHelper.callService(serviceConfig);
				this.reloadGridData();
				// this.unSetMultiSelect();
				this.set("MultiSelect", false);
			},
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "SeparateModeActionButtonsLeftContainer",
				"propertyName": "items",
				"name": "UnsetIsCheckActButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"caption": { bindTo: "Resources.Strings.UnsetIsCheckActButtonCaption" },
					"click": { bindTo: "onUnsetIsCheckActClick" },
					"enabled": { bindTo: "isUnsetIsCheckActButtonEnabled" },
					"visible": { bindTo: "IsUnsetIsCheckActButtonVisible" },
					"layout": {
						"column": 1,
						"row": 6,
						"colSpan": 1
					}
				}
			},
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "AcceptRequestButton",
				"index": 0,
				"values": {
					"tag": "onAcceptRequestButtonClick",
					"visible": {"bindTo": "AcceptRequestButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					"caption": { "bindTo": "Resources.Strings.AcceptRequestButtonCaption" },
					"click": { "bindTo": "onCardAction" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			},
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "CancelRequestButton",
				"index": 1,
				"values": {
					"tag": "onCancelRequestButtonClick",
					"visible": {"bindTo": "CancelRequestButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.RED,
					"caption": { "bindTo": "Resources.Strings.CancelRequestButtonCaption" },
					"click": { "bindTo": "onCardAction" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			},
		]/**SCHEMA_DIFF*/,
	};
});
