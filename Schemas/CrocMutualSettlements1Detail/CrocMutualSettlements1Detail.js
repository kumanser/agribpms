define("CrocMutualSettlements1Detail", [], function() {
	return {
		entitySchemaName: "CrocMutualSettlements",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "remove",
				"name": "addRecordButton"
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: Terrasoft.emptyFn,
			getDeleteRecordMenuItem: Terrasoft.emptyFn,
			getCopyRecordMenuItem: Terrasoft.emptyFn
		}
	};
});
