define("CrocVwApplicationGU1Section", [], function() {
	return {
		entitySchemaName: "CrocVwApplicationGU12",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
			"operation": "remove",
            "name": "SeparateModeAddRecordButton",
			},
			{
				"operation": "insert",
				"name": "SendToExporterButton",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.SendToExporterButtonCaption"
					},
					"click": {
						"bindTo": "onCardAction"
					},
					"tag": "onSendToExporterButtonClick",
					"enabled": true,
					"visible": {"bindTo": "SendToExporterButtonVisible"},
					"style": "green",
					"styles": {
						"textStyle": {
							"margin": "0px 9px 0px 0px"
						}
					},
					"layout": {
						"column": 1,
						"row": 6,
						"colSpan": 1
					}
				},
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "ApproveApplicationButton",
				"index": 1,
				"values": {
					"tag": "onApproveApplicationButtonClick",
					"visible": {"bindTo": "ApproveApplicationButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					"caption": { "bindTo": "Resources.Strings.ApproveApplicationButtonCaption" },
					"click": { "bindTo": "onCardAction" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			},
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "RejectApplicationButton",
				"index": 2,
				"values": {
					"tag": "onRejectApplicationButtonClick",
					"visible": {"bindTo": "RejectApplicationButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.RED,
					"caption": { "bindTo": "Resources.Strings.RejectApplicationButtonCaption" },
					"click": { "bindTo": "onCardAction" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			},
		]/**SCHEMA_DIFF*/,
		messages: {
			"AcceptDocumentActionVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"SendToExporterButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"ApproveApplicationButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"RejectApplicationButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attributes: {
			"CrocIsAcceptVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"SendToExporterButtonVisible":{
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"ApproveApplicationButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"RejectApplicationButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
		},
		methods: {
			init: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("AcceptDocumentActionVisibleChanged", function(isVisible) {
					this.$CrocIsAcceptVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
				
				this.sandbox.subscribe("SendToExporterButtonVisibleChanged", function(isVisible) {
					this.$SendToExporterButtonVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
				
				this.sandbox.subscribe("ApproveApplicationButtonVisibleChanged", function(isVisible) {
					this.$ApproveApplicationButtonVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
				
				this.sandbox.subscribe("RejectApplicationButtonVisibleChanged", function(isVisible) {
					this.$RejectApplicationButtonVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
			}
		}
	};
});