define("ActivitySectionV2", [], function() {
	return {
		entitySchemaName: "Activity",
		attributes: {
			"CrocFormSubmitted": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				value: false
			},
		},
		messages: {
			"FormSubmittedVisible": {
				"mode": Terrasoft.MessageMode.BROADCAST,
				"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
			},
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
		{
			"operation": "insert",
			"parentName": "CombinedModeActionButtonsCardLeftContainer",
			"propertyName": "items",
			"name": "LoanPossibleButton",
			"index": 10,
			"values": {
				"tag": "onLoanPossibleButtonClicked",
				"visible": {"bindTo": "CrocFormSubmitted"},
				"itemType": Terrasoft.ViewItemType.BUTTON,
				"style": Terrasoft.controls.ButtonEnums.style.GREEN,
				"caption": { "bindTo": "Resources.Strings.LoanPossibleButtonCaption" },
				"click": { "bindTo": "onCardAction" },
				"classes": {
					"textClass": ["actions-button-margin-right"],
					"wrapperClass": ["actions-button-margin-right"]
				},
			}
		},
		{
			"operation": "insert",
			"parentName": "CombinedModeActionButtonsCardLeftContainer",
			"propertyName": "items",
			"name": "LoanNotPossibleButton",
			"index": 11,
			"values": {
				"tag": "onLoanNotPossibleButtonClicked",
				"visible": {"bindTo": "CrocFormSubmitted"},
				"itemType": Terrasoft.ViewItemType.BUTTON,
				"style": Terrasoft.controls.ButtonEnums.style.RED,
				"caption": { "bindTo": "Resources.Strings.LoanNotPossibleButtonCaption" },
				"click": { "bindTo": "onCardAction" },
				"classes": {
					"textClass": ["actions-button-margin-right"],
					"wrapperClass": ["actions-button-margin-right"]
				},
			}
		}
		]/**SCHEMA_DIFF*/,
		methods: {

			init: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("FormSubmittedVisible", function(data) {
					this.$CrocFormSubmitted = data.formSubmitted;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
			}
			
		}
	};
});
