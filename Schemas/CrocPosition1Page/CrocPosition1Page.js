define("CrocPosition1Page", ["CrocConstantsJS"], function(CrocConstantsJS) {
	return {
		entitySchemaName: "CrocPosition",
		attributes: {
			"PlanPositionFieldGroupVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.ATTRIBUTE,
				"value": false
			},
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"PositionPlanTabLabelGroup": {
				"462021f0-ebcf-4c24-9283-9f70d748c2fa": {
					"uId": "462021f0-ebcf-4c24-9283-9f70d748c2fa",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "PlanPositionFieldGroupVisible"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			onEntityInitialized: function () {
				this.callParent(arguments);
				this.setPlanPositionFieldGroupVisible();
			},

			setPlanPositionFieldGroupVisible: function() {
				let accountId = Terrasoft.SysValue.CURRENT_USER_ACCOUNT.value;

				let esq = this.Ext.create(Terrasoft.EntitySchemaQuery, {
					rootSchemaName: "CrocAccountTypification"
				});
				esq.filters.addItem(esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
					"CrocType.Id", CrocConstantsJS.CrocAccountType.Port.value));
				esq.filters.addItem(esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
					"CrocAccount.Id", accountId));
				esq.addAggregationSchemaColumn("CrocType", Terrasoft.AggregationType.COUNT, "TypeCount", Terrasoft.AggregationEvalType.DISTINCT);

				esq.getEntityCollection(function(response) {
					if (response.success) {
						let queryResult = response.collection.getItems();
						if(queryResult.length > 0) {
							let accountCount = queryResult[0].get("TypeCount");
							this.$PlanPositionFieldGroupVisible = accountCount > 0 ? false : true;
						}
					}
				}, this)

			}

		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "LOOKUP896c885a-f93d-433a-bb09-5241f62ca9a7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocPositionPlan",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "PositionTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.PositionTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "PositionInPortTabLabelGroup",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.PositionInPortTabLabelGroupGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "PositionTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "PositionTabLabelGridLayout3d8160cc",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "PositionInPortTabLabelGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPb2c789c5-5bc4-47f1-9881-eb72bb7887d8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "PositionTabLabelGridLayout3d8160cc"
					},
					"bindTo": "CrocCulture",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "PositionTabLabelGridLayout3d8160cc",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FLOAT823e11fc-5a7a-4273-91e0-73b50fd5f266",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "PositionTabLabelGridLayout3d8160cc"
					},
					"bindTo": "CrocProtein",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayout3d8160cc",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "DATETIMEc393b2f7-0802-4206-81c0-f7664f71085d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "PositionTabLabelGridLayout3d8160cc"
					},
					"bindTo": "CrocStartLoadingDate",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayout3d8160cc",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DATETIME0cfb44e2-6f9c-4eb2-a096-03467eb9108e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "PositionTabLabelGridLayout3d8160cc"
					},
					"bindTo": "CrocEndLoadingDate",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayout3d8160cc",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "FLOATe94beb0d-bf9a-43c2-b982-e3448cb359ec",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "PositionTabLabelGridLayout3d8160cc"
					},
					"bindTo": "CrocPlannedVolume",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayout3d8160cc",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "PositionPlanTabLabelGroup",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.PositionPlanTabLabelGroupGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "PositionTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "PositionTabLabelGridLayout41014e4d",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "PositionPlanTabLabelGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIME240ca907-bbad-4b02-9410-fab85d499e49",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "PositionTabLabelGridLayout41014e4d"
					},
					"bindTo": "CrocStartSaleDate",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayout41014e4d",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIME8e6fad50-16bf-49e4-a875-037825115ed8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "PositionTabLabelGridLayout41014e4d"
					},
					"bindTo": "CrocEndSaleDate",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayout41014e4d",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "FLOATe65b5245-3241-4b8d-91f3-523d96ba5502",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "PositionTabLabelGridLayout41014e4d"
					},
					"bindTo": "CrocContractVolume",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayout41014e4d",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING7fd1a982-73b4-4967-936c-48a8f65a2807",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "PositionTabLabelGridLayout41014e4d"
					},
					"bindTo": "CrocBuyer",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayout41014e4d",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "ShipCategoryTabLabelGroup",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.ShipCategoryTabLabelGroupGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "PositionTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "PositionTabLabelGridLayoutdd3731ae",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "ShipCategoryTabLabelGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING7b806799-7ec9-4ed5-8a72-d9806e9a4db0",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "PositionTabLabelGridLayoutdd3731ae"
					},
					"bindTo": "CrocShipName",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayoutdd3731ae",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FLOAT0f3f15dc-22d9-416f-8c7d-d46f355c8567",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "PositionTabLabelGridLayoutdd3731ae"
					},
					"bindTo": "CrocShipCapacity",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayoutdd3731ae",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUPe01cd9d5-5883-497b-8364-684362927c43",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 8,
						"row": 1,
						"layoutName": "PositionTabLabelGridLayoutdd3731ae"
					},
					"bindTo": "CrocShipStatus",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "PositionTabLabelGridLayoutdd3731ae",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DATETIME173bc405-eeb0-4ee5-a900-78656dd89c1f",
				"values": {
					"layout": {
						"colSpan": 8,
						"rowSpan": 1,
						"column": 16,
						"row": 1,
						"layoutName": "PositionTabLabelGridLayoutdd3731ae"
					},
					"bindTo": "CrocPlannedArrivalDate",
					"enabled": true
				},
				"parentName": "PositionTabLabelGridLayoutdd3731ae",
				"propertyName": "items",
				"index": 3
			}
		]/**SCHEMA_DIFF*/
	};
});
