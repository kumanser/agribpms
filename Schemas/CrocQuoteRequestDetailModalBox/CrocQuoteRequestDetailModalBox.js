 define("CrocQuoteRequestDetailModalBox", ["ServiceHelper", "ProcessModuleUtilities", "ConfigurationGrid", "ConfigurationGridGenerator",
		"ConfigurationGridUtilities", "css!SummaryModuleV2"], function(ServiceHelper, ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocVWQuoteRequst",
		mixins: {
			ConfigurationGridUtilites: "Terrasoft.ConfigurationGridUtilities",
			GridUtilities: "Terrasoft.GridUtilities"
		},
		messages: {
			"ReloadForAllocatedValuesInfo": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},
		attributes: {
			IsEditable: {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			},
			MinWidth: {
				dataValueType: Terrasoft.DataValueType.INTEGER,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: 1320
			},
			MinHeight: {
				dataValueType: Terrasoft.DataValueType.INTEGER,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: 100
			},
			MaxGridHeight: {
				dataValueType: Terrasoft.DataValueType.INTEGER,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: 450
			}
		},
		methods: {

			initData: function(callback, scope) {
				this.set("Collection", this.Ext.create("Terrasoft.BaseViewModelCollection"));
				this.initGridRowViewModel(function() {
					this.initGridData();
					this.mixins.GridUtilities.init.call(this);
					this.loadGridData();
					callback.call(scope);
				}, this);
			},

			getCellControlsConfig: function(entitySchemaColumn) {
				var config = this.mixins.ConfigurationGridUtilites.getCellControlsConfig.apply(this, arguments);
				return config;
			},

			getGridData: function() {
				return this.get("Collection");
			},

			init: function(callback, scope) {
				this.initEditPages();
				this.callParent([function() {
					this.initData(function() {
						callback.call(scope);
					}, this);
				}, this]);
			},

			initGridData: function() {
				this.set("ActiveRow", "");
				this.set("IsEditable", false);
				this.set("MultiSelect", false);
				this.set("IsPageable", false);
				this.set("IsClearGridData", false);
			},

			loadGridData: function() {
				if (!this.get("IsDetailCollapsed") && !this.get("IsGridLoading")) {
					this.set("IsGridLoading", true);
					this.mixins.GridUtilities.loadGridData.call(this);
				}
			},

			getFilters: function() {
				var filters = this.mixins.GridUtilities.getFilters.call(this);
				if (filters) {
					var moduleInfo = this.getModuleInfo();
					var crocDeliveryScheduleId = moduleInfo && moduleInfo.crocDeliveryScheduleId;
					if (crocDeliveryScheduleId) {
						filters.add("deliveryScheduleFilter",
							this.Terrasoft.createColumnFilterWithParameter(
								this.Terrasoft.ComparisonType.EQUAL, "CrocDeliverySchedule", crocDeliveryScheduleId
							)
						);
					}
				}
				return filters;
			},

			getGridDataColumns: function() {
				var gridDataColumns = this.mixins.GridUtilities.getGridDataColumns(arguments);
				var requiredColumns = {
					Title: {path: "Title"},
					CrocNomenclature: {path: "CrocNomenclature"},
					"CrocTerminal.Name": {path: "CrocTerminal.Name"},
					CrocOrder: {path: "CrocOrder"},
					CrocAccountRequest: {path: "CrocAccountRequest"},
					CrocTonRequest: {path: "CrocTonRequest"},
					CrocCarRequest: {path: "CrocCarRequest"},
					CrocDatet: {path: "CrocDatet"},
					CrocTonAllocate: {path: "CrocTonAllocate"},
					CrocCarAllocate: {path: "CrocCarAllocate"},
					CrocRefusalDate: {path: "CrocRefusalDate"},
					CreatedOn: {
						path: "CreatedOn",
						orderPosition: 0,
						orderDirection: this.Terrasoft.OrderDirection.DESC
					}
				};
				return Ext.apply(gridDataColumns, requiredColumns);
			},

			initGridRowViewModel: function(callback, scope) {
				this.initEditableGridRowViewModel(callback, scope);
			},

			getModuleInfo: function() {
				return this.get("moduleInfo");
			},

			getActiveRow: function() {
				var activeRow = null;
				var primaryColumnValue = this.get("ActiveRow");
				if (primaryColumnValue) {
					var gridData = this.getGridData();
					activeRow = gridData.find(primaryColumnValue) ? gridData.get(primaryColumnValue) : null;
				}
				return activeRow;
			},

			getModalWindowSize: function() {
				var gridContainerEl = this.Ext.get("gridContainer");
				var fixedContainerEl = this.Ext.get("fixedAreaContainer");
				if (!gridContainerEl || !fixedContainerEl) {
					return null;
				}
				var totalHeight = fixedContainerEl.dom.clientHeight +
					Math.min(gridContainerEl.dom.clientHeight, this.get("MaxGridHeight")) + this.get("MinHeight");
				return {
					width: this.get("MinWidth"),
					height: totalHeight
				};
			},

			onGridDataLoaded: function() {
				this.mixins.GridUtilities.onGridDataLoaded.apply(this, arguments);
				var modalWindowSize = this.getModalWindowSize();
				if (modalWindowSize) {
					this.updateSize(modalWindowSize.width, modalWindowSize.height);
				}
			},

			prepareResponseCollectionItem: function(item) {
				this.mixins.GridUtilities.prepareResponseCollectionItem.apply(this, arguments);
			},

			onCtrlEnterKeyPressed: function() {
				var activeRow = this.getActiveRow();
				this.unfocusRowControls(activeRow);
				this.setActiveRow(null);
			},

			onEnterKeyPressed: function() {
				var activeRow = this.getActiveRow();
				this.unfocusRowControls(activeRow);
			},

			onTabKeyPressed: function() {
				var activeRow = this.getActiveRow();
				this.currentActiveColumnName = this.getCurrentActiveColumnName(activeRow, this.columnsConfig);
				return true;
			},

			onRender: function() {
				var modalBoxInnerBoxEl = this.Ext.get(this.renderTo);
				if (modalBoxInnerBoxEl && this.Ext.isFunction(modalBoxInnerBoxEl.parent)) {
					var modalBoxEl = modalBoxInnerBoxEl.parent();
					if (modalBoxEl) {
						modalBoxEl.addCls("quote-request-modal-box");
					}
				}
				this.updateSize(this.get("MinWidth"), this.get("MinHeight") + 50);
			},

			closeWindow: function() {
				this.destroyModule();
			},

			setQuoteButtonEnabled: function() {
				var activeRow = this.getActiveRow();
				if(activeRow) {
					var tonAllocate = activeRow.get("CrocTonAllocate");
					var сarAllocate = activeRow.get("CrocCarAllocate");
					if(tonAllocate !== 0 && сarAllocate !== 0) return true;
					else return false;
				}
				else return false;
			},

			onRefusalQuoteButtonClick: function() {
				var activeRow = this.getActiveRow();
				if(activeRow) {
					var config = {
						sysProcessName: "CrocProcessRefusalQuotePage",
						parameters: {
							ActivityId: activeRow.get("Id")
						},
						callback: this.refusalQuoteCallback()
					};
					ProcessModuleUtilities.executeProcess(config);
				}
			},

			onRefusalQuoteButtonClickOLD: function() {
				var activeRow = this.getActiveRow();
				if(activeRow) {
					this.showBodyMask();
					var currDate = new Date();
					var update = Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "Activity"});
					update.filters.add("IdFilter", update.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "Id", activeRow.get("Id")))
					update.setParameterValue("CrocRefusalDate", currDate, this.Terrasoft.DataValueType.DATE_TIME);
					var bq = this.Ext.create("Terrasoft.BatchQuery");
					bq.add(update);
					bq.execute(this.refusalQuoteCallback, this);
				}
			},

			refusalQuoteCallback: function() {
				//this.hideBodyMask();
				this.reloadGridData()
				this.sandbox.publish("ReloadForAllocatedValuesInfo", null, [this.sandbox.id]);
				this.closeWindow();
			},

			generateActiveRowControlsConfig: function(id, columnsConfig, rowConfig) {
				this._setActiveRowConfig(id);
				this.columnsConfig = columnsConfig;
				var gridLayoutItems = [];
				var currentColumnIndex = 0;
				Terrasoft.each(columnsConfig, function(columnConfig) {
					var cellConfig = this.getActiveRowCellConfig(columnConfig, currentColumnIndex);
					cellConfig.enabled = false;
					if (!cellConfig.hasOwnProperty("isNotFound")) {
						gridLayoutItems.push(cellConfig);
					}
					currentColumnIndex += cellConfig.layout.colSpan;
				}, this);
				this.applyBusinessRulesForActiveRow(id, gridLayoutItems);
				var viewGenerator = Ext.create(this.getRowViewGeneratorClassName());
				viewGenerator.viewModelClass = this;
				var gridLayoutConfig = viewGenerator.generateGridLayout({
					name: this.name,
					items: gridLayoutItems
				});
				rowConfig.push(gridLayoutConfig);
			},

		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "gridContainer",
				"index": 0,
				"propertyName": "items",
				"values": {
					"id": "gridContainer",
					"selectors": {"wrapEl": "#gridContainer"},
					"wrapClass": ["grid-container"],
					"markerValue": "quoteRequestGridContainer",
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"items": []
				}
			},
			{
				"operation": "insert",
				"name": "DataGrid",
				"parentName": "gridContainer",
				"propertyName": "items",
				"values": {
					"itemType": Terrasoft.ViewItemType.GRID,
					"className": "Terrasoft.ConfigurationGrid",
					"generator": "ConfigurationGridGenerator.generatePartial",
					"generateControlsConfig": {"bindTo": "generateActiveRowControlsConfig"},
					"multiSelect": false,
					"rowDataItemMarkerColumnName": "Title",
					"unSelectRow": {"bindTo": "unSelectRow"},
					"onGridClick": {"bindTo": "onGridClick"},
					"listedZebra": true,
					"initActiveRowKeyMap": {"bindTo": "initActiveRowKeyMap"},
					"collection": {"bindTo": "Collection"},
					"activeRow": {"bindTo": "ActiveRow"},
					"primaryColumnName": "Id",
					"isEmpty": {"bindTo": "IsGridEmpty"},
					"isLoading": {"bindTo": "IsGridLoading"},
					"selectedRows": {"bindTo": "SelectedRows"},
					"type": "listed",
					"listedConfig": {
						"name": "DataGridListedConfig",
						"items": [
							//{
							//	"name": "TitleListedGridColumn",
							//	"bindTo": "Title",
							//	"position": {"column": 1, "colSpan": 5}
							//},
							{
								"name": "CrocTerminalListedGridColumn",
								"bindTo": "CrocTerminal.Name",
								"position": {"column": 1, "colSpan": 6},
								"caption": "Терминал"
							},
							{
								"name": "CrocDatetListedGridColumn",
								"bindTo": "CrocDatet",
								"position": {"column": 7, "colSpan": 3}
							},
							{
								"name": "CrocTonRequestListedGridColumn",
								"bindTo": "CrocTonRequest",
								"position": {"column": 10, "colSpan": 3},
								"caption": "Запрошено, тонн"
							},
							{
								"name": "CrocCarRequestListedGridColumn",
								"bindTo": "CrocCarRequest",
								"position": {"column": 13, "colSpan": 3},
								"caption": "Запрошено транспорта"
							},
							{
								"name": "CrocTonAllocateListedGridColumn",
								"bindTo": "CrocTonAllocate",
								"position": {"column": 16, "colSpan": 3},
								"caption": "Выделено, тонн"
							},
							{
								"name": "CrocCarAllocateListedGridColumn",
								"bindTo": "CrocCarAllocate",
								"position": {"column": 19, "colSpan": 3},
								"caption": "Выделено транспорта"
							},
							{
								"name": "CrocRefusalDateListedGridColumn",
								"bindTo": "CrocRefusalDate",
								"position": {"column": 22, "colSpan": 3}
							}
						]
					},
					"tiledConfig": {
						"name": "DataGridTiledConfig",
						"grid": {"columns": 24, "rows": 1},
						"items": [
							{
								"name": "TitleTiledGridColumn",
								"bindTo": "Title",
								"position": {"row": 1, "column": 1, "colSpan": 8}
							},
							{
								"name": "CrocNomenclatureTiledGridColumn",
								"bindTo": "CrocNomenclature",
								"position": {"row": 1, "column": 9, "colSpan": 8}
							},
							{
								"name": "CrocTerminalTiledGridColumn",
								"bindTo": "CrocTerminal",
								"position": {"row": 1, "column": 17, "colSpan": 8}
							},
							{
								"name": "CrocOrderTiledGridColumn",
								"bindTo": "CrocOrder",
								"position": {"row": 2, "column": 1, "colSpan": 8}
							},
							{
								"name": "CrocAccountRequestTiledGridColumn",
								"bindTo": "CrocAccountRequest",
								"position": {"row": 2, "column": 9, "colSpan": 8}
							},
							{
								"name": "CrocTonRequestTiledGridColumn",
								"bindTo": "CrocTonRequest",
								"position": {"row": 2, "column": 17, "colSpan": 8}
							},
							{
								"name": "CrocCarRequestTiledGridColumn",
								"bindTo": "CrocCarRequest",
								"position": {"row": 3, "column": 1, "colSpan": 8}
							},
							{
								"name": "CrocDatetTiledGridColumn",
								"bindTo": "CrocDatet",
								"position": {"row": 3, "column": 9, "colSpan": 8}
							}
						]
					}
				}
			},
			{
				"operation": "insert",
				"name": "fixedAreaContainer",
				"index": 1,
				"values": {
					"id": "fixedAreaContainer",
					"selectors": {"wrapEl": "#fixedAreaContainer"},
					"wrapClass": ["fixed-area-container"],
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"items": []
				}
			},
			{
				"operation": "insert",
				"propertyName": "items",
				"name": "headContainer",
				"parentName": "fixedAreaContainer",
				"values": {
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"wrapClass": ["header"],
					"items": []
				}
			},
			{
				"operation": "insert",
				"parentName": "headContainer",
				"propertyName": "items",
				"name": "headerNameContainer",
				"values": {
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"wrapClass": ["header-name-container", "header-name-container-full"],
					"items": []
				}
			},
			{
				"operation": "insert",
				"parentName": "headContainer",
				"propertyName": "items",
				"name": "closeIconContainer",
				"values": {
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"wrapClass": ["header-name-container", "header-name-container-full"],
					"items": []
				}
			},
			{
				"operation": "insert",
				"name": "closeIconButton",
				"parentName": "closeIconContainer",
				"propertyName": "items",
				"values": {
					"click": {"bindTo": "closeWindow"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
					"hint": {"bindTo": "Resources.Strings.CloseButtonHint"},
					"imageConfig": {"bindTo": "Resources.Images.CloseIcon"},
					"markerValue": "CloseIconButton",
					"classes": {"wrapperClass": ["close-btn-user-class"]},
					"selectors": {"wrapEl": "#headContainer"}
				}
			},
			{
				"operation": "insert",
				"parentName": "headerNameContainer",
				"propertyName": "items",
				"name": "HeaderLabel",
				"values": {
					"itemType": Terrasoft.ViewItemType.LABEL,
					"caption": {"bindTo": "Resources.Strings.HeaderCaption"}
				}
			},
			{
				"operation": "insert",
				"name": "buttonsContainer",
				"parentName": "fixedAreaContainer",
				"propertyName": "items",
				"values": {
					"id": "buttonsContainer",
					"selectors": {"wrapEl": "#buttonsContainer"},
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"items": []
				}
			},
			{
				"operation": "insert",
				"name": "refusalQuoteButton",
				"parentName": "buttonsContainer",
				"propertyName": "items",
				"values": {
					"caption": {"bindTo": "Resources.Strings.RefusalQuoteButtonCaption"},
					"click": {"bindTo": "onRefusalQuoteButtonClick"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"enabled": { "bindTo": "setQuoteButtonEnabled"},
					"markerValue": "ButtonRefusalQuote"
				}
			},
			{
				"operation": "insert",
				"name": "cancelButton",
				"parentName": "buttonsContainer",
				"propertyName": "items",
				"values": {
					"caption": {"bindTo": "Resources.Strings.CancelButtonCaption"},
					"click": {"bindTo": "closeWindow"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"markerValue": "ButtonCancel"
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
