define("ActivityPageV2", ["ServiceHelper", "CrocConstantsJS"], function(ServiceHelper, CrocConstantsJS) {
	return {
		entitySchemaName: "Activity",
		attributes: {
			"CrocTonRequest": {
				dataValueType: Terrasoft.DataValueType.FLOAT,
				dependencies: [
					{
						columns: ["CrocCarRequest"],
						methodName: "onRequestTon"
					}
				]
			},
			"CrocCarRequest": {
				dataValueType: Terrasoft.DataValueType.INTEGER,
				dependencies: [
					{
						columns: ["CrocTonRequest"],
						methodName: "onRequestCar"
					}
				]
			},
			isVisibleElementOnCategory: {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				value: false
			},
			"CrocFormSubmitted": {
				value: false,
				dependencies: [
					{
						columns: ["CrocButtonLoanClicked", "Status", "ActivityCategory"],
						methodName: "onFormSubmittedChange"
					}
				]
			},
			"CrocButtonLoanClicked": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				value: false,
			},
			"CrocLoanButtonVisible": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				value: false,
			},
		},
		messages: {
			"RelevanceDateEmptyNotification": {
				"mode": Terrasoft.MessageMode.BROADCAST,
             	"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"UpdateCrocDocumentKitDetail": {
             	"mode": Terrasoft.MessageMode.BROADCAST,
             	"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
        	},
			"FormSubmittedVisible": {
				"mode": Terrasoft.MessageMode.BROADCAST,
				"direction": Terrasoft.MessageDirectionType.PUBLISH
			},
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"CrocSetDocument1Detailec7dde1f": {
				"schemaName": "CrocSetDocument1Detail",
				"entitySchemaName": "CrocSetDocument",
				"filter": {
					"detailColumn": "CrocActivity",
					"masterColumn": "Id"
				}
			},
			"VisaDetailV2577f96e4": {
				"schemaName": "VisaDetailV2",
				"entitySchemaName": "CrocActivityVisa",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocActivity"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"GeneralInfoTabGroupcc3e2b7d": {
				"bc798143-628d-4e73-ac5b-357e28e43fd1": {
					"uId": "bc798143-628d-4e73-ac5b-357e28e43fd1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocRefusalDate": {
				"39f24b68-25c4-41ff-aacd-03eb8e634286": {
					"uId": "39f24b68-25c4-41ff-aacd-03eb8e634286",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 7,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTonAllocate"
							},
							"rightExpression": {
								"type": 0,
								"value": 0,
								"dataValueType": 5
							}
						}
					]
				}
			},
			"GeneralInfoTabGroup4849c929": {
				"884f26aa-bcdb-4c29-93a4-94aa49ee7410": {
					"uId": "884f26aa-bcdb-4c29-93a4-94aa49ee7410",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "5706b8be-662d-4b56-85ff-8fa787db7bc0",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "71d81d0b-d515-470f-95c7-ad15593b776c",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocAccountRequest": {
				"8e832eac-9761-483e-9499-06a8de713745": {
					"uId": "8e832eac-9761-483e-9499-06a8de713745",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "bc9f5cad-954f-4d52-9959-169cfca7c980",
								"dataValueType": 10
							}
						}
					]
				},
				"62888276-962f-4da7-8120-c7a59e530b0f": {
					"uId": "62888276-962f-4da7-8120-c7a59e530b0f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "bc9f5cad-954f-4d52-9959-169cfca7c980",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocSetDocument1Detailec7dde1f": {
				"ff62febe-9676-49bc-9caa-d58675d561a0": {
					"uId": "ff62febe-9676-49bc-9caa-d58675d561a0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "bc9f5cad-954f-4d52-9959-169cfca7c980",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"GeneralInfoTabGroup144dc970": {
				"c5864793-fe9c-437f-9448-10d31cd3193e": {
					"uId": "c5864793-fe9c-437f-9448-10d31cd3193e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "142b0327-fca6-41fa-b33a-3997be2d366c",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocLogin": {
				"e97082e3-248e-4a61-b11c-3ca3a430ef50": {
					"uId": "e97082e3-248e-4a61-b11c-3ca3a430ef50",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "4bdbb88f-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "142b0327-fca6-41fa-b33a-3997be2d366c",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocPassword": {
				"30110b8b-0597-4c79-84a5-49fbbc19751e": {
					"uId": "30110b8b-0597-4c79-84a5-49fbbc19751e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "4bdbb88f-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "142b0327-fca6-41fa-b33a-3997be2d366c",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"GeneralInfoTabGroup31bd0f65": {
				"c0dd0dac-eac9-4658-831e-ea89fc057cd6": {
					"uId": "c0dd0dac-eac9-4658-831e-ea89fc057cd6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "276a29f5-28ac-4bfc-a809-f81525a6a1a9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Contact": {
				"7b734c75-0ab6-43e7-8ec4-f639b786067b": {
					"uId": "7b734c75-0ab6-43e7-8ec4-f639b786067b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "276a29f5-28ac-4bfc-a809-f81525a6a1a9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocTonRequest": {
				"7ff5a6c9-d384-4594-85db-042e1f0eba55": {
					"uId": "7ff5a6c9-d384-4594-85db-042e1f0eba55",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "384d4b84-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocCarRequest": {
				"73073a1a-adac-4943-92b2-8552fb330820": {
					"uId": "73073a1a-adac-4943-92b2-8552fb330820",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "384d4b84-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocTerminal": {
				"79547599-4207-44e6-a762-0e19bbfdd20a": {
					"uId": "79547599-4207-44e6-a762-0e19bbfdd20a",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "384d4b84-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNomenclature": {
				"4cdc81d2-4da0-446e-9502-0c43a9899593": {
					"uId": "4cdc81d2-4da0-446e-9502-0c43a9899593",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "384d4b84-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Status": {
				"e41bcd37-cf98-421e-bf81-04d47b0da4cd": {
					"uId": "e41bcd37-cf98-421e-bf81-04d47b0da4cd",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				},
				"4459be6a-41da-4b05-ba04-210a502af913": {
					"uId": "4459be6a-41da-4b05-ba04-210a502af913",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "71d81d0b-d515-470f-95c7-ad15593b776c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "5706b8be-662d-4b56-85ff-8fa787db7bc0",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ActivityParticipantTab": {
				"3b6d158c-0993-4f92-8858-5a7ba726112c": {
					"uId": "3b6d158c-0993-4f92-8858-5a7ba726112c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isVisibleElementOnCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				},
				"a9fad1ee-3c0c-4212-bf80-e93bb7d32810": {
					"uId": "a9fad1ee-3c0c-4212-bf80-e93bb7d32810",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "bc9f5cad-954f-4d52-9959-169cfca7c980",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ActivityFileNotesTab": {
				"a492bfd0-a228-49e0-a22e-7f3733d271fa": {
					"uId": "a492bfd0-a228-49e0-a22e-7f3733d271fa",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isVisibleElementOnCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"CallTab": {
				"7188da54-d7b9-4943-a9fe-287a73e6c95a": {
					"uId": "7188da54-d7b9-4943-a9fe-287a73e6c95a",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isVisibleElementOnCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"EmailTab": {
				"734ef370-12c1-4e24-9e29-9d8056ea888c": {
					"uId": "734ef370-12c1-4e24-9e29-9d8056ea888c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isVisibleElementOnCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ESNTab": {
				"051d08b1-e770-49ea-9407-1e0e43adea44": {
					"uId": "051d08b1-e770-49ea-9407-1e0e43adea44",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isVisibleElementOnCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Tab91a338e0TabLabel": {
				"5eb686c9-2ebe-42b8-ad46-80a7baf3d788": {
					"uId": "5eb686c9-2ebe-42b8-ad46-80a7baf3d788",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isVisibleElementOnCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"ResultControlGroup": {
				"52cc35b5-3ec4-4cae-a64c-bf303700f92c": {
					"uId": "52cc35b5-3ec4-4cae-a64c-bf303700f92c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isVisibleElementOnCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"EntityConnections": {
				"6284c8e1-d875-4126-9aaf-de64cc17225a": {
					"uId": "6284c8e1-d875-4126-9aaf-de64cc17225a",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isVisibleElementOnCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"RemindControlGroup": {
				"5ad65938-65eb-42e9-aabe-28a2ee6b080b": {
					"uId": "5ad65938-65eb-42e9-aabe-28a2ee6b080b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isVisibleElementOnCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"Result": {
				"db529fd5-37f4-4189-84ab-67d056249277": {
					"uId": "db529fd5-37f4-4189-84ab-67d056249277",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				},
				"a0b4e614-05ef-4197-a98d-c4926e2814b2": {
					"uId": "a0b4e614-05ef-4197-a98d-c4926e2814b2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "4bdbb88f-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"OwnerRole": {
				"0fc85be7-66a1-4c1f-874a-c4533851736b": {
					"uId": "0fc85be7-66a1-4c1f-874a-c4533851736b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "71d81d0b-d515-470f-95c7-ad15593b776c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "5706b8be-662d-4b56-85ff-8fa787db7bc0",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Priority": {
				"d589a32f-e77f-4e4d-912c-cfea8d87b5bb": {
					"uId": "d589a32f-e77f-4e4d-912c-cfea8d87b5bb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "71d81d0b-d515-470f-95c7-ad15593b776c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "5706b8be-662d-4b56-85ff-8fa787db7bc0",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"ShowInScheduler": {
				"f268b349-ebb9-4c35-831a-f92fafdfc769": {
					"uId": "f268b349-ebb9-4c35-831a-f92fafdfc769",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "5706b8be-662d-4b56-85ff-8fa787db7bc0",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "71d81d0b-d515-470f-95c7-ad15593b776c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocPort": {
				"e8d27fb8-f9b5-4dbe-a6f1-9bb1af0bc8fb": {
					"uId": "e8d27fb8-f9b5-4dbe-a6f1-9bb1af0bc8fb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedOn"
							}
						}
					]
				}
			},
			"StartDate": {
				"da769ebd-d0e9-4e8c-bb80-7b0cffe9b340": {
					"uId": "da769ebd-d0e9-4e8c-bb80-7b0cffe9b340",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e52bd583-7825-e011-8165-00155d043204",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "03df85bf-6b19-4dea-8463-d5d49b80bb28",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "8038a396-7825-e011-8165-00155d043204",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "42c74c49-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "2365ae4f-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "f51c4643-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "142b0327-fca6-41fa-b33a-3997be2d366c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "bc9f5cad-954f-4d52-9959-169cfca7c980",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "276a29f5-28ac-4bfc-a809-f81525a6a1a9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"DueDate": {
				"de96998d-ba1b-4f39-a365-f2b6eaca9e3e": {
					"uId": "de96998d-ba1b-4f39-a365-f2b6eaca9e3e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e52bd583-7825-e011-8165-00155d043204",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "03df85bf-6b19-4dea-8463-d5d49b80bb28",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "8038a396-7825-e011-8165-00155d043204",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "42c74c49-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "2365ae4f-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "f51c4643-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "142b0327-fca6-41fa-b33a-3997be2d366c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "bc9f5cad-954f-4d52-9959-169cfca7c980",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "276a29f5-28ac-4bfc-a809-f81525a6a1a9",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Author": {
				"a4085ac0-8b68-4013-904b-e5df218ba278": {
					"uId": "a4085ac0-8b68-4013-904b-e5df218ba278",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e52bd583-7825-e011-8165-00155d043204",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "03df85bf-6b19-4dea-8463-d5d49b80bb28",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "8038a396-7825-e011-8165-00155d043204",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "42c74c49-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "2365ae4f-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "f51c4643-58e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "142b0327-fca6-41fa-b33a-3997be2d366c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "bc9f5cad-954f-4d52-9959-169cfca7c980",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "276a29f5-28ac-4bfc-a809-f81525a6a1a9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocExporter": {
				"4c55dd88-4842-4269-93ce-7dc3f5ebc29c": {
					"uId": "4c55dd88-4842-4269-93ce-7dc3f5ebc29c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "71d81d0b-d515-470f-95c7-ad15593b776c",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "5706b8be-662d-4b56-85ff-8fa787db7bc0",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocLoadingPoint": {
				"412331b1-c5a0-4bc0-82f1-f92860ca52b5": {
					"uId": "412331b1-c5a0-4bc0-82f1-f92860ca52b5",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "ActivityCategory"
							},
							"rightExpression": {
								"type": 0,
								"value": "e3b490a8-4323-4765-93f3-8c4e3ba5ad36",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			init: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("RelevanceDateEmptyNotification", this.showNotification, this);
				this.sandbox.subscribe("UpdateCrocDocumentKitDetail", this.updateCrocDocumentKitDetail, this);
			},

			onRender: function(esq) {
				this.callParent(arguments);
				this.updateCrocDocumentKitDetail();
			},

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.fillVisibleElements();
				this.onFormSubmittedChange();
			},

			fillVisibleElements: function() {
				let category = this.$ActivityCategory;
				if(category) {
					this.$isVisibleElementOnCategory = category.value !== CrocConstantsJS.CrocActivityCategory.DocumentsRequest &&
						category.value !== CrocConstantsJS.CrocActivityCategory.IssuanceRequest &&
						category.value !== CrocConstantsJS.CrocActivityCategory.LoanRequest &&
						category.value !== CrocConstantsJS.CrocActivityCategory.QuoteRequest
				}
			},

			onRequestTon: function () {
				if (this.get("CrocTonRequest") === 0) {
					var carRequest = this.get("CrocCarRequest");
					var carVolume = this.get("CrocCarVolume");
					if (this.get("CrocTonRequest") !== (carRequest * carVolume)) {
						this.set("CrocTonRequest", carRequest * carVolume);
					}
				}
			},
			onRequestCar: function() {
				if (this.get("CrocCarRequest") === 0) {
					var tonRequest = this.get("CrocTonRequest");
					var carVolume = this.get("CrocCarVolume");
					var result = Math.ceil(tonRequest / carVolume);
					if (this.get("CrocCarRequest") !== result) {
						this.set("CrocCarRequest", result);
					}
				}
			},

			showNotification: function(args) {
				if (args && args.addDocumentId && args.documentNumber){
					let message = this.Ext.String.format(this.get("Resources.Strings.RelevanceDateEmptyNotification"), args.documentNumber);
					this.showInformationDialog(message);
				}
			},
			
			updateCrocDocumentKitDetail: function(args) {
				if (args && args.documentId) {
      				this.updateDetail({detail: "CrocSetDocument1Detailec7dde1f",reloadAll:true});
   				}
			},

			onFormSubmittedChange: function() {
				this.$CrocFormSubmitted = this.$Status?.value !== CrocConstantsJS.CrocActivityStatus.Completed ? true : false;
				let category = this.$ActivityCategory?.value;
				this.$CrocLoanButtonVisible =
					(CrocConstantsJS.CrocLoanCategory.ReleaseLoanRequest == category|| CrocConstantsJS.CrocLoanCategory.ReceiveLoanRequest == category)
					&& (this.$CrocFormSubmitted && !this.$CrocButtonLoanClicked)
				this.sandbox.publish("FormSubmittedVisible", {
					formSubmitted: this.$CrocLoanButtonVisible
				}, [this.sandbox.id])
			},

			onLoanPossibleButtonClicked: function() {
				this.loadLookupDisplayValue("Result", CrocConstantsJS.CrocActivityResult.Loan);
				this.loadLookupDisplayValue("Status", CrocConstantsJS.CrocActivityStatus.Completed);
				this.$CrocButtonLoanClicked = true;
			},

			onLoanNotPossibleButtonClicked: function() {
				this.loadLookupDisplayValue("Result", CrocConstantsJS.CrocActivityResult.NotLoan);
				this.loadLookupDisplayValue("Status", CrocConstantsJS.CrocActivityStatus.Completed);
				this.$CrocButtonLoanClicked = true;
			}

		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "LoanPossibleButton",
				"values": {
					"itemType": 5,
					"style": "green",
					"caption": {
						"bindTo": "Resources.Strings.LoanPossibleButtonCaption"
					},
					"visible": {
						"bindTo": "CrocLoanButtonVisible"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					},
					"click": {
						"bindTo": "onLoanPossibleButtonClicked"
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "LoanNotPossibleButton",
				"values": {
					"itemType": 5,
					"style": "red",
					"caption": {
						"bindTo": "Resources.Strings.LoanNotPossibleButtonCaption"
					},
					"visible": {
						"bindTo": "CrocLoanButtonVisible"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					},
					"click": {
						"bindTo": "onLoanNotPossibleButtonClicked"
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "merge",
				"name": "InformationOnStepButtonContainer",
				"values": {
					"layout": {
						"colSpan": 4,
						"rowSpan": 1,
						"column": 20,
						"row": 0
					}
				}
			},
			{
				"operation": "move",
				"name": "InformationOnStepButtonContainer",
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "TitleInformationContainer",
				"values": {
					"layout": {
						"colSpan": 20,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "ActivityCategory",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "move",
				"name": "ActivityCategory",
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "merge",
				"name": "StartDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "Owner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					},
					"enabled": true,
					"contentType": 5
				}
			},
			{
				"operation": "move",
				"name": "Owner",
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "merge",
				"name": "DueDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2
					}
				}
			},
			{
				"operation": "insert",
				"name": "CrocExporter653f0069-cf0b-4a83-81a6-9209c3b6a073",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocExporter",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Status48588635-a9e3-480c-81b7-d05982b87484",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "Status"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "merge",
				"name": "Author",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "Priority",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4
					},
					"enabled": true
				}
			},
			{
				"operation": "merge",
				"name": "OwnerRole",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5
					},
					"enabled": true,
					"contentType": 5
				}
			},
			{
				"operation": "move",
				"name": "OwnerRole",
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "merge",
				"name": "ShowInScheduler",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5
					},
					"enabled": true
				}
			},
			{
				"operation": "insert",
				"name": "CrocAccountRequestdd1bbdc4-54f8-419b-944f-060f391eed5e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "CrocAccountRequest",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "merge",
				"name": "CallDirection",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6
					}
				}
			},
			{
				"operation": "move",
				"name": "CallDirection",
				"parentName": "Header",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "insert",
				"name": "Resultdeecc6f5-1ac0-41f9-b8a0-c0c764caa43d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Header"
					},
					"bindTo": "Result"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 14
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "merge",
				"name": "GeneralInfoTab",
				"values": {
					"order": 0
				}
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGroupcc3e2b7d",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GeneralInfoTabGroupcc3e2b7dGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGridLayoutab99ee04",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GeneralInfoTabGroupcc3e2b7d",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocCrocContract91d63a35-eebf-421f-a7fd-4bbac0ebcd12",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocCrocContract",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP536c7e65-d4a0-4c7b-8793-97fdf667c7ac",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocTerminal",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUPe9759229-794b-4fbb-9f9d-4d62b0b3d803",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocPort",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP4359fb53-4741-4e82-8dba-a4b24e3cff67",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocNomenclature",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "DATETIME924c4621-cecc-4308-9e56-c7a5457a20e2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocDatet",
					"enabled": false
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "FLOATafaba04b-7aa9-4c59-be7a-26ceac92b55d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocTonRequest",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "FLOAT4252b94e-2c39-44f7-8230-c7b722e5445f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocTonAllocate",
					"enabled": false
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "INTEGER4ac22909-3cf5-4381-be20-d3d41db424c5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocCarRequest",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "INTEGER531c899f-0a68-417a-9ee2-00849c4f2822",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocCarAllocate",
					"enabled": false
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "FLOATafc8e08d-f102-4bf9-a914-0118e80a9cce",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocCarVolume",
					"enabled": false
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "DATETIMEd1c4b03f-ce49-4f5c-8c2f-6aa2a4b8dd76",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocRefusalDate",
					"enabled": false
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "STRINGd98ac4d9-6374-4de7-839d-a14e1408770a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "GeneralInfoTabGridLayoutab99ee04"
					},
					"bindTo": "CrocLoadingPoint",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayoutab99ee04",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGroup4849c929",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GeneralInfoTabGroup4849c929GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGridLayoutca42752b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GeneralInfoTabGroup4849c929",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Account2c7cb4d7-7cbe-4c4d-8350-1f6b58f095a9",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayoutca42752b"
					},
					"bindTo": "Account"
				},
				"parentName": "GeneralInfoTabGridLayoutca42752b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocTerminal7a69604b-4f19-42bb-9415-9d6ec2fc0dc5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayoutca42752b"
					},
					"bindTo": "CrocTerminal"
				},
				"parentName": "GeneralInfoTabGridLayoutca42752b",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocNomenclature6bc4351d-4edb-4ff0-b592-4bf4aec23236",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "GeneralInfoTabGridLayoutca42752b"
					},
					"bindTo": "CrocNomenclature",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.CrocNomenclature6bc4351d4edb4ff0b5924bf4aec23236LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "GeneralInfoTabGridLayoutca42752b",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "FLOAT3662732e-0bf5-4a4e-b00a-3a9e9b853fb6",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "GeneralInfoTabGridLayoutca42752b"
					},
					"bindTo": "CrocVolume",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayoutca42752b",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocDatetb504ddbd-dbe4-407d-8737-b78fdc55a592",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "GeneralInfoTabGridLayoutca42752b"
					},
					"bindTo": "CrocDatet",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.CrocDatetb504ddbddbe4407d8737b78fdc55a592LabelCaption"
						}
					},
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayoutca42752b",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "DATETIME07d40b69-f890-4fc9-8cd2-7ba5c8e4ff74",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "GeneralInfoTabGridLayoutca42752b"
					},
					"bindTo": "CrocRepaymentDate",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayoutca42752b",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocPort5e537d52-48ab-49d9-869b-989f6ff327e4",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "GeneralInfoTabGridLayoutca42752b"
					},
					"bindTo": "CrocPort",
					"enabled": true,
					"contentType": 5,
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.CrocPort5e537d5248ab49d9869b989f6ff327e4LabelCaption"
						},
						"visible": false
					}
				},
				"parentName": "GeneralInfoTabGridLayoutca42752b",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocSetDocument1Detailec7dde1f",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGroup144dc970",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GeneralInfoTabGroup144dc970GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGridLayout97551de1",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GeneralInfoTabGroup144dc970",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Contact9c76d540-7aaa-482e-9a5e-4d7c7141b462",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayout97551de1"
					},
					"bindTo": "Contact"
				},
				"parentName": "GeneralInfoTabGridLayout97551de1",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Accountd79dc86a-894c-4703-9c3e-c6a424357d7e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayout97551de1"
					},
					"bindTo": "Account"
				},
				"parentName": "GeneralInfoTabGridLayout97551de1",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRINGdc1b08b9-6487-4f8c-93d8-1ef99e19afe8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayout97551de1"
					},
					"bindTo": "CrocLogin",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayout97551de1",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRINGde773a19-7b3e-4620-901d-4ffe9d4662da",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayout97551de1"
					},
					"bindTo": "CrocPassword",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayout97551de1",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "STRING88977855-3fad-4c96-8d86-2fad8dd6c5eb",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "GeneralInfoTabGridLayout97551de1"
					},
					"bindTo": "CrocRole",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayout97551de1",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGroup31bd0f65",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GeneralInfoTabGroup31bd0f65GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGridLayoutc548e174",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GeneralInfoTabGroup31bd0f65",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Contact92443ca1-1433-4ec5-9afd-e71725c3db17",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayoutc548e174"
					},
					"bindTo": "Contact"
				},
				"parentName": "GeneralInfoTabGridLayoutc548e174",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "CustomActionSelectedResultControlGroup",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Result",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToOwner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToOwnerDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToAuthor",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToAuthorDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "ActivityParticipantTab",
				"values": {
					"order": 1
				}
			},
			{
				"operation": "merge",
				"name": "ActivityFileNotesTab",
				"values": {
					"order": 2
				}
			},
			{
				"operation": "merge",
				"name": "EmailTab",
				"values": {
					"order": 3
				}
			},
			{
				"operation": "insert",
				"name": "Tab91a338e0TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabVisaCaption"
					},
					"items": [],
					"order": 5
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "VisaDetailV2577f96e4",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab91a338e0TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "CallTab",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "remove",
				"name": "Status"
			},
			{
				"operation": "remove",
				"name": "EntityConnections"
			}
		]/**SCHEMA_DIFF*/
	};
});
