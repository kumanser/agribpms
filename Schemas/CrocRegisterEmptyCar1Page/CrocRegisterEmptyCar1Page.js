define("CrocRegisterEmptyCar1Page", ["ServiceHelper"], function(ServiceHelper) {
	return {
		entitySchemaName: "CrocRegisterEmptyCar",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			onSaved: function() {
				this.callParent(arguments);
				try { this.onSendEmptyVehiclesClick(); }
				catch {}
			},

			onSendEmptyVehiclesClick: function() {
				var masterId;
				var masterColumn;
				var defaultValues = this.$DefaultValues;
				var masterObj = defaultValues.filter(function(item) {
					var parent = item.displayValue;
					if(parent !== undefined) { return true; }
				});
				if(masterObj[0]) {
					masterColumn = masterObj[0].name;
					masterId = masterObj[0].value;
				}
				if(masterId) {
					ServiceHelper.callService({
						serviceName: "ExchangeIntegrationService",
						methodName: "SendEmptyVehiclesA",
						data: {
							recordId: masterId,
							masterColumnName: masterColumn
						},
						scope: this
					});
				}
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocVehicle4578ebf5-c955-429a-a9c0-c3d07c79e917",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocVehicle",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocTrailerNumber36466014-c557-4c2b-b54c-4afe76f98f40",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocTrailerNumber"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocBrandVehiclec2b75334-b03d-49ea-8c4b-b43fecb2f492",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocBrandVehicle"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocCarrier717e6e50-8f61-4abe-bf7f-d51c8c259b2b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocCarrier"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocCarrierINNa0ab4725-4534-4dad-9763-ddb8ef876a22",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocCarrierINN"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocCarrierOwner8102ef72-e93e-487c-a3a5-f2accab7e2ac",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocCarrierOwner"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocCarrierOwnerINNed99fa9d-3529-4afa-a63e-0f48ac37ed1e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocCarrierOwnerINN"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocDriverFullNamec07d45dc-a8a1-47cf-b9d9-b5d69bc92abd",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocDriverFullName"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocDriverINNb1cf7c46-d235-4ca4-b9f8-d16043887545",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocDriverINN"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "CrocDriverPhone36e30e5a-765c-43ea-bf48-ec52f7c0960b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "CrocDriverPhone"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "CrocPriceNetPerTon4d220731-0ea4-4db2-b03e-b1506483d6e8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "Header"
					},
					"bindTo": "CrocPriceNetPerTon"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "CrocContractCode14b0a983-10c7-4bbf-85ae-963b435a8124",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "CrocContractCode"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "CrocCommissionPerTon90f8cc33-0cdd-4309-a79d-4feac690ccda",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Header"
					},
					"bindTo": "CrocCommissionPerTon"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "CrocOwnershipTypeIntb03ff09f-940f-41b8-8751-d1432ed5e638",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocOwnershipTypeInt"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 13
			}
		]/**SCHEMA_DIFF*/
	};
});
