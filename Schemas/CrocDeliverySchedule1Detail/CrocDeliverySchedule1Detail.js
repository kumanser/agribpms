define("CrocDeliverySchedule1Detail", ["ProcessModuleUtilities", "RightUtilities", "CrocQuoteRequestGridButtonUtility", "ConfigurationGrid", "ConfigurationGridGenerator",
		"ConfigurationGridUtilities", "css!CrocQuoteRequestGridButtonUtility"],
	function(ProcessModuleUtilities, RightUtilities,GridButtonsUtil) {
	return {
		entitySchemaName: "CrocDeliverySchedule",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		attributes: {
			IsNotifyRedirectVisible: {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			},
			"IsEditable": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: true
			},
			"IsEnabled": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			}
		},
		messages: {
			"GetModuleInfo": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"ReloadForAllocatedValuesInfo": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		mixins: {
			ConfigurationGridUtilities: "Terrasoft.ConfigurationGridUtilities"
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "NotifyDeliveryRedirectButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onNotifyDeliveryRedirectButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": {"bindTo": "Resources.Strings.NotifyDeliveryRedirectButton"},
					"enabled": { "bindTo": "setNotifyRedirectEnabled" },
					"visible": {bindTo: "IsNotifyRedirectVisible"}
				},
			},
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "CreateQuoteRequestButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onCreateQuoteRequestButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": {"bindTo": "Resources.Strings.CreateQuoteRequestButtonCaption"},
					"enabled": { "bindTo": "setCreateQuoteRequestEnabled" },
					"visible": true
				}
			},
			{
				"operation": "merge",
				"name": "DataGrid",
				"values": {
					"className": "Terrasoft.ConfigurationGrid",
					"generator": "ConfigurationGridGenerator.generatePartial",
					"generateControlsConfig": {"bindTo": "generateActiveRowControlsConfig"},
					"changeRow": {"bindTo": "changeRow"},
					"unSelectRow": {"bindTo": "unSelectRow"},
					"onGridClick": {"bindTo": "onGridClick"},
					"sortColumnIndex": null,
					"listedZebra": true,
					"useLinks": true,
					"activeRowActions": [],
					"initActiveRowKeyMap": {"bindTo": "initActiveRowKeyMap"},
					"activeRowAction": {"bindTo": "onActiveRowAction"},
					"multiSelect": {"bindTo": "MultiSelect"}
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
			getCopyRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,

			getOpenCardConfig: function(operation, typeColumnValue, recordId) {
				var config = this.callParent(arguments);
				if(config && config.schemaName) { config.schemaName = "CrocDeliverySchedule1Page"; }
				return config;
			},

			initData: function() {
				this.callParent(arguments);
				this.setNotifyRedirectVisible();
			},


			setNotifyRedirectEnabled: function() {
				var selected = this.getSelectedItems();
				return selected;
			},

			setCreateQuoteRequestEnabled: function() {
				var activeRowId = this.get("ActiveRow");
				if(activeRowId) return true;
				else return false;
			},

			setNotifyRedirectVisible: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanNotifyRedirect"}, function(result) {
					this.$IsNotifyRedirectVisible = result;
				}, this);
			},

			onNotifyDeliveryRedirectButtonClick: function () {
				var activeRowId = this.get("ActiveRow");
				var args = {
					sysProcessName: "CrocProcessForwarding",
					parameters: {
						DeliveryScheduleId: activeRowId
					}
				};
				ProcessModuleUtilities.executeProcess(args);
			},

			onCreateQuoteRequestButtonClick: function() {
				var activeRowId = this.get("ActiveRow");
				var gridData = this.getGridData();
				var activeRowModel = gridData.get(activeRowId);
				var contract = activeRowModel.get("CrocContract");
				if(contract) {
					var config = {
						sysProcessName: "CrocProcessQuoteRequest",
						parameters: {
							idDeliverySchedule: activeRowId,
							isDC: true,
							IdContract: contract.value,
							DeliveryPlanTon: activeRowModel.get("CrocDeliveryTonPlan"),
							DeliveryPlanCar: activeRowModel.get("CrocDeliveryTransportPlan")
						}
					};
					ProcessModuleUtilities.executeProcess(config);
				}
				else {
					this.showInformationDialog(this.get("Resources.Strings.EmptyContractError"));
				}
			},

			///CrocQuoteRequest
			generateActiveRowControlsConfig: function(id, columnsConfig, rowConfig) {
				this._setActiveRowConfig(id);
				this.columnsConfig = columnsConfig;
				var gridLayoutItems = [];
				var currentColumnIndex = 0;
				Terrasoft.each(columnsConfig, function(columnConfig) {
					var cellConfig = this.getActiveRowCellConfig(columnConfig, currentColumnIndex);
					cellConfig.enabled = false;
					if(cellConfig.name === "CrocDeliveryTransportPlan" || cellConfig.name === "CrocDeliveryTonPlan")
						cellConfig.enabled = true;
					if (!cellConfig.hasOwnProperty("isNotFound")) {
						gridLayoutItems.push(cellConfig);
					}
					currentColumnIndex += cellConfig.layout.colSpan;
				}, this);
				this.applyBusinessRulesForActiveRow(id, gridLayoutItems);
				var viewGenerator = Ext.create(this.getRowViewGeneratorClassName());
				viewGenerator.viewModelClass = this;
				var gridLayoutConfig = viewGenerator.generateGridLayout({
					name: this.name,
					items: gridLayoutItems
				});
				rowConfig.push(gridLayoutConfig);
			},

			addGridDataColumns: function(esq) {
				this.callParent(arguments);
				this.addRequiredColumns(esq);
				GridButtonsUtil.instance.addGridDataColumns(esq);
			},

			addRequiredColumns: function(esq) {
				esq.addColumn("CrocChart.CrocContract", "CrocContract");
				esq.addColumn("CrocDeliveryTransportPlan");
				esq.addColumn("CrocDeliveryTonPlan");
			},

			prepareResponseCollectionItem: function(item) {
				this.mixins.GridUtilities.prepareResponseCollectionItem.apply(this, arguments);
				GridButtonsUtil.instance.prepareResponseCollectionItem(item, this);
			},

			getCellControlsConfig: function(entitySchemaColumn) {
				var controlsConfig = GridButtonsUtil.instance.getCellControlsConfig(entitySchemaColumn);
				if (!controlsConfig) {
					controlsConfig =
						this.mixins.ConfigurationGridUtilities.getCellControlsConfig.apply(this, arguments);
				}
				return controlsConfig;
			},

			init: function(callback, scope) {
				GridButtonsUtil.init({
					Ext: this.Ext,
					Terrasoft: this.Terrasoft
				});
				this.callParent(arguments);
				this.sandbox.subscribe("ReloadForAllocatedValuesInfo", this.quoteRequestsWindowCallback, this, [this.getModalBoxQuoteRequestDetailId()]);
			},

			onCrocQuoteRequestsButtonClick: function(item) {
				this.set("CurrentRowId", this.get("ActiveRow"));
				this.openQuoteRequestsWindow();
			},

			linkClicked: function(recordId, columnName) {
				var eventResult = false;
				try {
					this.set("CurrentRowId", recordId);
					if (columnName === "CrocQuoteRequests") {
						this.openQuoteRequestsWindow();
					}
				} catch (exception) {
					this.log(exception, this.Terrasoft.LogMessageType.ERROR);
				}
				this.set("IsButtonClicked", true);
				return eventResult;
			},

			onGridClick: function() {
				if (this.get("IsButtonClicked")) {
					this.set("IsButtonClicked", false);
				} else {
					this.mixins.ConfigurationGridUtilities.onGridClick.apply(this, arguments);
				}
			},

			openQuoteRequestsWindow: function() {
				this.sandbox.loadModule("ModalBoxSchemaModule", {id: this.getModalBoxQuoteRequestDetailId()});
			},

			quoteRequestsWindowCallback: function() {
				this.reloadGridData();
			},

			getModalBoxQuoteRequestDetailId: function() {
				return this.sandbox.id + "_CrocQuoteRequestDetailModalBox";
			},

			getModuleInfoResponse: function() {
				const activeRowId = this.get("CurrentRowId");
				if (activeRowId) {
					return {
						schemaName: "CrocQuoteRequestDetailModalBox",
						crocDeliveryScheduleId: activeRowId
					};
				}
			},

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("GetModuleInfo", this.getModuleInfoResponse,
					this, [this.getModalBoxQuoteRequestDetailId()]);
			},
		}
	};
});
