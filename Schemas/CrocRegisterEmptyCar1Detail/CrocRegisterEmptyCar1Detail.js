define("CrocRegisterEmptyCar1Detail", ["ServiceHelper", "ModalBox", "CrocExcelImportModule"], function(ServiceHelper, ModalBox, ImportModule) {
	return {
		entitySchemaName: "CrocRegisterEmptyCar",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		messages: {
			BoxInfoReturnExcelImportResult: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "NotifyDeliveryRedirectButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onUploadEmptyVehicleButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": "Загрузить транспортные средства",
					"enabled": true
				},
			}
		]/**SCHEMA_DIFF*/,
		methods: {

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("BoxInfoReturnExcelImportResult", this.onCloseImportModalCallback, this, [this.getModuleIdfn(this.sandbox)]);
			},

			getModuleIdfn: function(sandbox) {
				return sandbox.id + "CrocExcelImportModalBoxModule";
			},

			onUploadEmptyVehicleButtonClick: function() {
				var sandbox = this.sandbox;
				var renderTo = ModalBox.show(ImportModule.getModalBoxConfig(), this.onCloseImportModalCallback, this);
				var modalConfig = {
					id: this.getModuleIdfn(sandbox),
					renderTo: renderTo,
					parameters: {
						workParam : {
							importConfig: this.fillImportConfig(),
							detailConfig: this.fillDetailConfig()
						}
					}
				};
				sandbox.loadModule("CrocExcelImportModalBoxModule", modalConfig);
			},

			onCloseImportModalCallback: function(arg) {
				if(arg) {
					this.reloadGridData();
					this.onSendEmptyVehiclesClick();
				}
			},

			init: function() {
				this.Terrasoft.ServerChannel.on(this.Terrasoft.EventName.ON_MESSAGE, this.onMessageReceive, this);
				this.callParent(arguments);
			},

			onMessageReceive: function(channel, message) {
				if (this.Ext.isEmpty(message)) {
					return;
				}
				var header = message.Header;
				if (header.Sender !== this.entitySchemaName + "_" + this.get("MasterRecordId")) {
					return;
				}
				var messageObject = Terrasoft.decode(message.Body || "{}");
				if (messageObject.LoadStatus) {
					this.onCloseImportModalCallback(true);
				}
			},

			onSendEmptyVehiclesClick: function() {
				ServiceHelper.callService({
					serviceName: "ExchangeIntegrationService",
					methodName: "SendEmptyVehiclesA",
					data: {
						recordId: this.$MasterRecordId,
						masterColumnName: this.$DetailColumnName
					},
					scope: this
				});
			},

			fillDetailConfig: function() {
				var masterRecId = this.get("MasterRecordId");
				var masterColumn = this.get("DetailColumnName");
				var detailConfig = ImportModule.getTemplateDetailConfig();
				detailConfig.clearOldRows = true;
				detailConfig.masterColumn = masterColumn;
				detailConfig.masterRecord = masterRecId;
				return detailConfig;
			},

			fillImportConfig: function () {
				var importConfig = ImportModule.getTemplateImportParameters();
				importConfig.authorId = this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value;
				var schema = ImportModule.findSchemaByName(this.entitySchemaName);
				if(schema) {
					importConfig.rootSchemaUId = schema.uId;
					importConfig.importObject.uId = schema.uId;
					importConfig.importObject.caption = schema.caption;
					importConfig.importObject.name = schema.name;
					var mappingConfig = [
						{ colName: "CrocVehicle", colExcel: "A", source: "Номер ТС" },
						{ colName: "CrocTrailerNumber", colExcel: "B", source: "Номер прицепа" },
						{ colName: "CrocDriverFullName", colExcel: "C", source: "ФИО Водителя" },
						{ colName: "CrocCarrier", colExcel: "D", source: "Факт. перевозчик" },
						{ colName: "CrocCarrierINN", colExcel: "E", source: "ИНН Факт. перевозчика" },
						{ colName: "CrocBrandVehicle", colExcel: "F", source: "Марка автомобиля" },
						{ colName: "CrocDriverPhone", colExcel: "G", source: "Тел. водителя" },
						{ colName: "CrocDriverINN", colExcel: "H", source: "ИНН водителя" },
						{ colName: "CrocOwnershipTypeInt", colExcel: "I", source: "Вид ЗВ" },
						{ colName: "CrocCarrierOwner", colExcel: "J", source: "Собственник ТС" },
						{ colName: "CrocCarrierOwnerINN", colExcel: "K", source: "ИНН собственника" },
						{ colName: "CrocPriceNetPerTon", colExcel: "L", source: "Тариф перевозки" },
						{ colName: "CrocCommissionPerTon", colExcel: "M", source: "Тариф вознаграждения" },
						{ colName: "CrocContractCode", colExcel: "N", source: "Договор" }
					];
					mappingConfig.forEach((item) => {
						var col = this.getColumnByName(item.colName);
						if (col) {
							var destinationConfig = ImportModule.getTemplateDestinationConfig();
							var columnConfig = ImportModule.getTemplateColumnConfig();
							destinationConfig.isKey = false;
							destinationConfig.schemaUId = schema.uId;
							if (col.isLookup) {
								destinationConfig.columnValueName = col.name + "Id";
								destinationConfig.properties = {
									"TypeUId": this.Terrasoft.convertToServerDataValueType(col.dataValueType),
									"ReferenceSchemaName": col.referenceSchemaName
								};
							}
							else {
								destinationConfig.columnValueName = col.name;
								destinationConfig.properties = {"TypeUId": this.Terrasoft.convertToServerDataValueType(col.dataValueType)};
							}
							destinationConfig.columnName = col.name;
							columnConfig.index = item.colExcel;
							columnConfig.source = item.source;
							columnConfig.destinations.push(destinationConfig);
							importConfig.columns.push(columnConfig);
						}
					});
				}
				return importConfig;
			}
		}
	};
});
