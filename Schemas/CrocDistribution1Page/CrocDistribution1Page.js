define("CrocDistribution1Page", ["ProcessModuleUtilities", "CrocConstantsJS"],
	function(ProcessModuleUtilities, CrocConstantsJS) {
	return {
		entitySchemaName: "CrocDistribution",
		messages: {
			"ReloadContactIndistributionDetail": {
				"mode": Terrasoft.MessageMode.BROADCAST,
				"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"UnPublishDistributionButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"PublishDistributionButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"UpdateDistribution1Page": {
				"mode": Terrasoft.MessageMode.BROADCAST,
				"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
			},
		},
		attributes: {
			"CrocTemplate": {
				dependencies: [
					{
						columns: ["CrocTemplate"],
						methodName: "onTemplateChanged"
					}
				]
			},
			"UnPublishButtonVisible": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"PublishButtonVisible": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
		},
		methods: {
			init: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("ReloadContactIndistributionDetail", this.reloadPage, this);
				this.sandbox.subscribe("UpdateDistribution1Page", this.reloadPage, this);
			},

			initCardActionHandler: function() {
				this.callParent(arguments);
				this.on("change:ShowDiscardButton", function(model, value) {
					this.publishPropertyValueToSection("ShowDiscardButton", value);
					this.set("ShowCloseButton", !value);
					this.$PublishButtonVisible = !value && !this.$UnPublishButtonVisible && (this.$CrocMailingStatus.value === CrocConstantsJS.CrocMailingStatus.Planned);
					this.publishPropertyValueToSection("PublishButtonVisible", !value && !this.$UnPublishButtonVisible && (this.$CrocMailingStatus.value === CrocConstantsJS.CrocMailingStatus.Planned))
				}, this);
				this.on("change:ShowCloseButton", function(model, value) {
					this.$PublishButtonVisible = value;
					this.publishPropertyValueToSection("ShowCloseButton", value);
					this.publishPropertyValueToSection("PublishButtonVisible", value)
				}, this);

			},

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.setPublishButtonsVisible();
			},

			setPublishButtonsVisible: function () {
				this.$UnPublishButtonVisible = (this.$CrocMailingStatus.value === CrocConstantsJS.CrocMailingStatus.Launched);
				this.$PublishButtonVisible = this.$ShowCloseButton && !this.$UnPublishButtonVisible && (this.$CrocMailingStatus.value === CrocConstantsJS.CrocMailingStatus.Planned);
				this.sandbox.publish("UnPublishDistributionButtonVisibleChanged", this.$UnPublishButtonVisible, ["SectionModuleV2_InvoiceSectionV2"]);
				this.sandbox.publish("PublishDistributionButtonVisibleChanged", this.$PublishButtonVisible, ["SectionModuleV2_InvoiceSectionV2"]);
			},

			onTemplateChanged: function() {
				if (this.$CrocTemplate) this.$CrocNotificationText = "";
			},

			reloadPage: function (args) {
				if (args && args.distributionId === this.$Id) {
					this.reloadEntity();
				}
			},

			onPublishButtonClick: function() {
				this.runPublishProcess("CrocProcessStartDistribution");
			},

			onUnPublishButtonClick: function() {
				this.runPublishProcess("CrocProcessCancelDistribution");
			},

			runPublishProcess: function(processName) {
				this.getRecepientsCount(function (result) {
					if (result > 0) {
						var args = {
							sysProcessName: processName,
							parameters: {
								DistributionId: this.$Id
							},
							scope: this
						};
						ProcessModuleUtilities.executeProcess(args);
					} else this.showConfirmationDialog(this.get("Resources.Strings.EmptyRecipientError"));
				}, this);
			},

			getRecepientsCount: function(callback, scope) {
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocContactInDistribution"});
				esq.addColumn("CrocContact");
				esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "CrocDistribution", this.$Id));
				esq.getEntityCollection(function(response) {
					if (response && response.success) {
						var length = response.collection.collection.length;
						callback.call(scope, length);
					}
				}, this);
			},
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "PublishButton",
				"values": {
					"tag": "onPublishButtonClick",
					"itemType": 5,
					"style": "green",
					"visible": {
						"bindTo": "PublishButtonVisible"
					},
					"caption": {
						"bindTo": "Resources.Strings.PublishButtonCaption"
					},
					"click": {
						"bindTo": "onPublishButtonClick"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "UnPublishButton",
				"values": {
					"tag": "onUnPublishButtonClick",
					"visible": {
						"bindTo": "UnPublishButtonVisible"
					},
					"itemType": 5,
					"style": "red",
					"caption": {
						"bindTo": "Resources.Strings.UnPublishButtonCaption"
					},
					"click": {
						"bindTo": "onUnPublishButtonClick"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocName6daa642d-eefa-4195-9122-a620dd172e43",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocName"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocNotificationTyped981b0c6-2ee8-41de-8b0f-83a4396a4471",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocNotificationType"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocNotificationChannel28274287-adb4-4905-8adc-1cd777069e34",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocNotificationChannel"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocNotificationSubjectc4a0a197-f725-4a31-a37a-482e4704ae7e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocNotificationSubject"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocTemplate75bbdb01-a786-454b-a799-1347e6400dd1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocTemplate"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocNotificationText214f8602-26fe-45f5-8b5b-8ea9aecfaced",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocNotificationText",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CreatedOn454280f8-e2ea-48fc-b1c8-9b75a2597cc3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CreatedOn",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CreatedByca83bc67-8e74-45b2-8764-315ac86ed779",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CreatedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "RecepientsTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.RecepientsTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocAccountInDistribution1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "RecepientsTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocContactInDistribution1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "RecepientsTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/,
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocDistributionFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocDistribution"
				}
			},
			"CrocAccountInDistribution1Detail": {
				"schemaName": "CrocAccountInDistribution1Detail",
				"entitySchemaName": "CrocAccountInDistribution",
				"filter": {
					"detailColumn": "CrocDistribution",
					"masterColumn": "Id"
				}
			},
			"CrocContactInDistribution1Detail": {
				"schemaName": "CrocContactInDistribution1Detail",
				"entitySchemaName": "CrocContactInDistribution",
				"filter": {
					"detailColumn": "CrocDistribution",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocNotificationText": {
				"102af696-c71a-4986-b61d-5fbd049a25cb": {
					"uId": "102af696-c71a-4986-b61d-5fbd049a25cb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocMailingStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba7a58fb-ab96-4fd8-a15d-f75230c40c44",
								"dataValueType": 10
							}
						}
					]
				},
				"1df3dc8f-481c-4763-a388-8c86d6e44fc0": {
					"uId": "1df3dc8f-481c-4763-a388-8c86d6e44fc0",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTemplate"
							}
						}
					]
				},
				"010da67d-f6cb-4212-b9bb-7fe94023e064": {
					"uId": "010da67d-f6cb-4212-b9bb-7fe94023e064",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTemplate"
							}
						}
					]
				}
			},
			"CrocNotificationSubject": {
				"be7fbc48-aa8c-4b0e-8333-6d69f76e8850": {
					"uId": "be7fbc48-aa8c-4b0e-8333-6d69f76e8850",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocMailingStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba7a58fb-ab96-4fd8-a15d-f75230c40c44",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNotificationType": {
				"83ffbcd7-659f-47fa-bbd3-069aeeffb984": {
					"uId": "83ffbcd7-659f-47fa-bbd3-069aeeffb984",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocMailingStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba7a58fb-ab96-4fd8-a15d-f75230c40c44",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNotificationChannel": {
				"d565a5b7-82b2-45a8-987b-193c17cde369": {
					"uId": "d565a5b7-82b2-45a8-987b-193c17cde369",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocMailingStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba7a58fb-ab96-4fd8-a15d-f75230c40c44",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
	};
});
