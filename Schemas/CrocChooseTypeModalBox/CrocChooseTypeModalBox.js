define("CrocChooseTypeModalBox", ["ServiceHelper", "RightUtilities", "NetworkUtilities", "CrocFileDetailMixin", "css!CrocChooseTypeModalBoxModuleCss"],
	function(ServiceHelper, RightUtilities, NetworkUtilities) {
		return {
			mixins: {
				CrocFileDetailMixin: "Terrasoft.CrocFileDetailMixin"
			},
			messages: {
				"UpdateFileDetail": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.PUBLISH
				},
			},
			attributes: {
				"FileType": {
					dataValueType: Terrasoft.DataValueType.LOOKUP,
					isRequired: true
				},
				"DocumentTypeId": {
					dataValueType: Terrasoft.DataValueType.GUID
				},
				"MasterRecordId": {
					dataValueType: Terrasoft.DataValueType.GUID
				}
			},
			methods: {

				/**
				 * @inheritdoc Terrasoft.ModalBoxSchemaModule#init.
				 * @override
				 */
				init: function(callback, scope) {
					this.callParent([function() {
						this.set("FilesUploadErrorLog", this.Ext.create("Terrasoft.Collection"));
						const moduleInfo = this.get("moduleInfo");
						this.$DocumentTypeId = moduleInfo.documentTypeId;
						this.$MasterRecordId = moduleInfo.masterRecordId;
						this.$FileTypeName = moduleInfo.fileTypeName;
						this.$SingleFileTypeId = moduleInfo.fileTypeId;
						this.$AccountName = moduleInfo.account;
						this.$RootSchemaName = moduleInfo.rootSchemaName;
						this.getDefTypeValue();
						callback.call(scope);
					}, this]);
				},

				getHeader: function() {
					//return this.get("Resources.Strings.DetailModalBoxHeader");
					return "Выбор вида файла";
				},

				getDefTypeValue: function() {
					this.getFileTypeQuery(function (response) {
						let items = response.collection.getItems();
						if (response.collection.getCount() > 0) this.$FileType = {displayValue: items[0].$Name, value: items[0].$Id};
					}, this);
				},

				getFileTypeQuery: function(callback, scope) {
					let esq = Ext.create("Terrasoft.EntitySchemaQuery", { rootSchemaName:"CrocFileType" });
					esq.addColumn("Name");
					esq.addColumn("Id");
					esq.getEntityCollection(function (response) {
						callback.call(scope, response);
					}, this);
				},

				prepareColumnCollection: function(filter, list) {
					if (Terrasoft.isEmptyObject(list)) {
						return;
					}
					list.clear();
					this.getFileTypeQuery(function (response) {
						if(response.success && response.collection.getCount() > 0) {
							let items = this.Ext.create("Terrasoft.Collection");
							response.collection.each(function (item) {
								var id = item.$Id;
								var name = item.$Name;
								items.add(id, {
									value: id,
									displayValue: name
								});
							}, this);
							list.loadAll(items);
						}
					}, this);
				},

				/**
				 * @inheritdoc Terrasoft.ModalBoxSchemaModule#onRender.
				 * @override
				 */
				onRender: function() {
					this.callParent(arguments);
					this.updateSize(580, 200);
				},

				/**
				 * Handler for the "OK" button.
				 * @protected
				 */
				closeButtonClick: function() {
					//this.sandbox.publish("NavigateToAllowedStep", null, [this.sandbox.id]);
					this.close();
				},

				onFileSelect: function(files) {
					if (files.length <= 0) {
						return;
					}
					const config = this.getUploadConfig(files);
					this.set("FileUploadConfig", config);
					this.upload(config, function() {
						this.set("FileUploadConfig", null);
					});
				},

				getUploadConfig: function(files) {
					var config =  {
						scope: this,
						onUpload: this.onUpload,
						onComplete: this.onComplete,
						onFileComplete: this.onFileComplete,
						uploadWebServicePath: "FilesS3Service/ImportFile",
						onFileProgress: this.onFileProgress,
						entitySchemaName: this.$RootSchemaName + "File",
						columnName: "Data",
						parentColumnName: this.$RootSchemaName,
						parentColumnValue: this.$MasterRecordId,
						files: files,
						isChunkedUpload: true,
						oversizeErrorHandler: this.onFilesOversized
					};
					return config;
				},

				upload: function(config, callback) {
					this.validateEmptyFilesSize(config.files, function (files) {
						this.Terrasoft.ConfigurationFileApi.upload(config, callback);
					}, this);
				},

				getEmptyFiles: function(files) {
					const filesOversize = [];
					const length = files.length;
					for (let i = length - 1; i >= 0; i--) {
						if (files[i].size === 0) {
							filesOversize.push(files[i]);
							files.splice(i, 1);
						}
					}
					return filesOversize;
				},

				generateDefaultErrorMessage: function(excludedFiles) {
					let errorMessage = "";
					const length = excludedFiles.length;
					for (let i = 0; i < length; i++) {
						errorMessage += (errorMessage === "") ? excludedFiles[i].name
							: (", " + excludedFiles[i].name);
					}
					return errorMessage;
				},

				validateEmptyFilesSize: function(files, callback, scope) {
					const filesEmpty = this.getEmptyFiles(files);
					if (filesEmpty.length > 0) {
						Terrasoft.utils.showInformation(
							Ext.String.format("{0}\n{1}", this.get("Resources.Strings.EmptyFilesErrorMessage"),
								this.generateDefaultErrorMessage(filesEmpty)
							)
						);
					}
					Ext.callback(callback, scope || this, [files]);
				},

				onUpload: function(file, xhr, options) {
					this.$FileMaskId = Terrasoft.MaskHelper.showBodyMask({
						caption: "Загрузка"
					});
				},

				onFileProgress: function(event, file, xhr, options) {
					this.$FileMaskCaption = "Сохранение " + file.name;
					var caption = Terrasoft.getFormattedString("{0} ({1}%)", this.$FileMaskCaption, Math.floor(event.loaded * 100 / event.total));
					Terrasoft.MaskHelper.updateBodyMaskCaption(this.$FileMaskId, caption);
				},

				onFileComplete: function(error, xhr, file, options) {
					let cardValues = {
						CrocAccount: {displayValue: this.$AccountName},
						CrocFileType: this.$FileType
					};
					this.executeSavingEntity(this.$RootSchemaName, options.data, cardValues, null, function(result) {
						if (result) {
							this.sandbox.publish("UpdateFileDetail", null, [this.sandbox.id]);
							if (result.SaveFileResult) {
								this.showInformationDialog(Terrasoft.getFormattedString("{0} {1}", result.SaveFileResult));
								Terrasoft.MaskHelper.hideBodyMask(this.$FileMaskId);
							}
							else {
								if (options.data.fileName === options.files.files[options.files.files.length - 1].name) {
									Terrasoft.MaskHelper.hideBodyMask(this.$FileMaskId);
								}
							}
							this.close();
						}
					}, this);
				},

				onComplete: function() {
					this.hideBodyMask();
				}
			},
			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
			diff: /**SCHEMA_DIFF*/[
				// {
				// 	"operation": "remove",
				// 	"name": "HeaderContainer"
				// },
				{
					"operation": "insert",
					"parentName": "CardContentContainer",
					"propertyName": "items",
					"name": "DocumentTypeContainer",
					"index": 0,
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"items": [],
						"wrapClass": ["wizard-warning-body-container"]
					}
				},
				{
					"operation": "insert",
					"parentName": "DocumentTypeContainer",
					"propertyName": "items",
					"name": "FileType",
					"values": {
						"caption": "$Resources.Strings.DocumentTypeCaption",
						"bindTo": "FileType",
						"controlConfig": {
							"prepareList": {
								"bindTo": "prepareColumnCollection"
							}
						},
						"enabled": true,
						"contentType": 3,
					}
				},
				{
					"operation": "insert",
					"parentName": "CardContentContainer",
					"propertyName": "items",
					"name": "ButtonContainer",
					"index": 1,
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"items": [],
						"wrapClass": ["choose-modal-box-btn-container"]
					}
				},
				{
					"operation": "insert",
					"name": "ChooseButton",
					"parentName": "ButtonContainer",
					"propertyName": "items",
					"values": {
						//"click": {"bindTo": "openSystemSettings"},
						"fileUpload": true,
						"filesSelected": {"bindTo": "onFileSelect"},
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						"markerValue": "ChangeCurrentPackageButton",
						"caption": {"bindTo": "Resources.Strings.ChooseButtonCaption"}
					}
				},
				{
					"operation": "insert",
					"name": "CloseButton",
					"parentName": "ButtonContainer",
					"propertyName": "items",
					"values": {
						"click": {"bindTo": "closeButtonClick"},
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.BLUE,
						"markerValue": "CloseButton",
						"caption": {"bindTo": "Resources.Strings.CloseButtonCaption"}
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});
