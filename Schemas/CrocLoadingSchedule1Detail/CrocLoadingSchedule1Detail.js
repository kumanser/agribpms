define("CrocLoadingSchedule1Detail", ["ModalBox", "CrocExcelImportModule"],
	function(ModalBox, ImportModule) {
	return {
		entitySchemaName: "CrocLoadingSchedule",
		messages: {
			BoxInfoReturnExcelImportResult: {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "CrocLoadChartButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onAddFileClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": "Загрузить график",
					"fileTypeFilter":[".xls",".xlsx"],
					enabled: true,
				}
			},
		]/**SCHEMA_DIFF*/,
		methods: {
			getCopyRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("BoxInfoReturnExcelImportResult", this.onCloseImportModalCallback, this, [this.getModuleIdfn(this.sandbox)]);
			},

			getModuleIdfn: function(sandbox) {
				return sandbox.id + "CrocExcelImportModalBoxModule";
			},

			onAddFileClick: function () {
				var sandbox = this.sandbox;
				var renderTo = ModalBox.show(ImportModule.getModalBoxConfig(), this.onCloseImportModalCallback, this);
				var modalConfig = {
					id: this.getModuleIdfn(sandbox),
					renderTo: renderTo,
					parameters: {
						workParam : {
							importConfig: this.fillImportConfig(),
							detailConfig: this.fillDetailConfig()
						}
					}
				};
				sandbox.loadModule("CrocExcelImportModalBoxModule", modalConfig);
			},

			onCloseImportModalCallback: function(arg) {
				if(arg) {
					this.reloadGridData();
				}
			},

			init: function() {
				this.Terrasoft.ServerChannel.on(this.Terrasoft.EventName.ON_MESSAGE, this.onMessageReceive, this);
				this.callParent(arguments);
			},

			onMessageReceive: function(channel, message) {
				if (this.Ext.isEmpty(message)) {
					return;
				}
				var header = message.Header;
				if (header.Sender !== this.entitySchemaName + "_" + this.get("MasterRecordId")) {
					return;
				}
				var messageObject = Terrasoft.decode(message.Body || "{}");
				if (messageObject.LoadStatus) {
					this.onCloseImportModalCallback(true);
				}
			},

			fillDetailConfig: function() {
				var masterRecId = this.get("MasterRecordId");
				var masterColumn = this.get("DetailColumnName");
				var detailConfig = ImportModule.getTemplateDetailConfig();
				detailConfig.clearOldRows = false;
				detailConfig.masterColumn = masterColumn;
				detailConfig.masterRecord = masterRecId;
				return detailConfig;
			},

			fillImportConfig: function () {
				var importConfig = ImportModule.getTemplateImportParameters();
				importConfig.authorId = this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value;
				var schema = ImportModule.findSchemaByName(this.entitySchemaName);
				if(schema) {
					importConfig.rootSchemaUId = schema.uId;
					importConfig.importObject.uId = schema.uId;
					importConfig.importObject.caption = schema.caption;
					importConfig.importObject.name = schema.name;
					var config = [
						{colName: "CrocShipmentDate", colExcel: "A", source: "Дата погрузки"},
						{colName: "CrocLoadingTonPlan", colExcel: "B", source: "План погрузки, тонны"},
						{colName: "CrocLoadingTransporPlan", colExcel: "C", source: "План погрузки, ТС"},
					];
					config.forEach((item) => {
						var col = this.getColumnByName(item.colName);
						if (col) {
							var destinationConfig = ImportModule.getTemplateDestinationConfig();
							var columnConfig = ImportModule.getTemplateColumnConfig();
							destinationConfig.isKey = false;
							destinationConfig.schemaUId = schema.uId;
							if (col.isLookup) {
								destinationConfig.columnValueName = col.name + "Id";
								destinationConfig.properties = {
									"TypeUId": this.Terrasoft.convertToServerDataValueType(col.dataValueType),
									"ReferenceSchemaName": col.referenceSchemaName
								};
							}
							else {
								destinationConfig.columnValueName = col.name;
								destinationConfig.properties = {"TypeUId": this.Terrasoft.convertToServerDataValueType(col.dataValueType)};
							}
							destinationConfig.columnName = col.name;
							columnConfig.index = item.colExcel;
							columnConfig.source = item.source;
							columnConfig.destinations.push(destinationConfig);
							importConfig.columns.push(columnConfig);
						}
					});
				}
				return importConfig;
			},
		}
	};
});
