define("CrocDocument1Page", ["CrocConstantsJS","RightUtilities","ProcessModuleUtilities", "ServiceHelper"],
	function(CrocConstantsJS, RightUtilities, ProcessModuleUtilities, ServiceHelper) {
	return {
		entitySchemaName: "CrocDocument",
		attributes: {
			"CrocIsAcceptVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsCancelAccreditationVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsCancelAccreditationEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"SendToExporterButtonVisible":{
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"ApproveApplicationButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"RejectApplicationButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsModelItemsEnabled":{
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"value": true,
				dependencies: [{
					columns: ["CrocDocumentStatus"],
					methodName: "setDocumentLock"
				}]
			},
			IsPackage:{
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"value": false
			},
			CrocDocumentType: {
				"dependencies": [
					{
						"columns": ["CrocDocumentType"],
						"methodName": "updPackage"
					}
				]
			},
			CrocChart: {
				"lookupListConfig": {
					"columns": ["CrocChartStatus"]
				}
			},
		},
		messages: {
			"AcceptDocumentActionVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"CancelAccreditationActionVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"CancelAccreditationActionEnabledChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"SendToExporterButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"UpdateCrocDocumentKitDetail": {
             	"mode": Terrasoft.MessageMode.BROADCAST,
             	"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
        	},
			"ApproveApplicationButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"RejectApplicationButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"RelevanceDateEmptyNotification": {
				"mode": Terrasoft.MessageMode.BROADCAST,
             	"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocDocumentFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocDocument"
				}
			},
			"CrocSetDocument1Detail61a32329": {
				"schemaName": "CrocSetDocument1Detail",
				"entitySchemaName": "CrocSetDocument",
				"filter": {
					"detailColumn": "CrocMainDocument",
					"masterColumn": "Id"
				}
			},
			"CrocComment1Detail99e841a5": {
				"schemaName": "CrocComment1Detail",
				"entitySchemaName": "CrocComment",
				"filter": {
					"detailColumn": "CrocDocument",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocGU12Number": {
				"bb77371b-a2c8-45ad-b862-a096962f1875": {
					"uId": "bb77371b-a2c8-45ad-b862-a096962f1875",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocGU12Status": {
				"98ff65a6-92bb-45ab-8bb3-1da4561be224": {
					"uId": "98ff65a6-92bb-45ab-8bb3-1da4561be224",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocGU12RejectReason": {
				"d7a4ec7d-1307-4570-ac09-bef857df7e5f": {
					"uId": "d7a4ec7d-1307-4570-ac09-bef857df7e5f",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						}
					]
				},
				"667ebdde-6340-4ffe-9073-9b9812bc1e6b": {
					"uId": "667ebdde-6340-4ffe-9073-9b9812bc1e6b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocGU12Status"
							},
							"rightExpression": {
								"type": 0,
								"value": "0f1327a4-d0bf-4286-87f8-db98068e66e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocGU12StartDate": {
				"53b00f78-858b-4c73-a366-cf8420657a0b": {
					"uId": "53b00f78-858b-4c73-a366-cf8420657a0b",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab62f61bd1TabLabelGroupba194667": {
				"34d5defe-9f59-4704-b026-8c5cc7c7edcb": {
					"uId": "34d5defe-9f59-4704-b026-8c5cc7c7edcb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNumber": {
				"1a6391cc-006c-43f7-8941-4036bc370a9e": {
					"uId": "1a6391cc-006c-43f7-8941-4036bc370a9e",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocRelevanceDate": {
				"c89e2de5-5dcf-42b5-808b-9c2da63fe1ac": {
					"uId": "c89e2de5-5dcf-42b5-808b-9c2da63fe1ac",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "7936b7e6-5ac7-4d4a-8461-969c1cf5f873",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocIsPerpetual"
							}
						}
					]
				},
				"01ffd52a-57b1-48d2-b4bd-00c79466558c": {
					"uId": "01ffd52a-57b1-48d2-b4bd-00c79466558c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocIsPerpetual"
							},
							"rightExpression": {
								"type": 0,
								"value": false,
								"dataValueType": 12
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7cda430d-acbc-40e2-ace8-6d05a660527a",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						}
					]
				},
				"c47f6b7b-044b-4509-b3ba-cfc5b60c67de": {
					"uId": "c47f6b7b-044b-4509-b3ba-cfc5b60c67de",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "CrocGU12EndDate"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocGU12EndDate"
							}
						}
					]
				},
				"30179819-e053-4ea8-8ed3-42367e81c983": {
					"uId": "30179819-e053-4ea8-8ed3-42367e81c983",
					"enabled": true,
					"removed": true,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "CrocEndAccreditation"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7cda430d-acbc-40e2-ace8-6d05a660527a",
								"dataValueType": 10
							}
						}
					]
				},
				"5498ccbc-f515-40eb-9bba-f4f9fe6aec7b": {
					"uId": "5498ccbc-f515-40eb-9bba-f4f9fe6aec7b",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7cda430d-acbc-40e2-ace8-6d05a660527a",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocCheckStatus": {
				"bb946935-5f0b-440c-a7d1-a19d80f76bc9": {
					"uId": "bb946935-5f0b-440c-a7d1-a19d80f76bc9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7cda430d-acbc-40e2-ace8-6d05a660527a",
								"dataValueType": 10
							}
						}
					]
				},
				"66efa132-906a-4ab1-9a0f-9f7760fe56f3": {
					"uId": "66efa132-906a-4ab1-9a0f-9f7760fe56f3",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocCheckStatus"
							}
						}
					]
				}
			},
			"CrocAccreditationType": {
				"838a4260-63eb-4613-b760-ba6e09813ea9": {
					"uId": "838a4260-63eb-4613-b760-ba6e09813ea9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7cda430d-acbc-40e2-ace8-6d05a660527a",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocContact": {
				"db0b6dcd-e65e-464e-8e59-4b5e7692b43a": {
					"uId": "db0b6dcd-e65e-464e-8e59-4b5e7692b43a",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "CrocAccount"
				}
			},
			"CrocSetDocument1Detail61a32329": {
				"c9d9fbd3-991a-483c-a09f-9dfd6e5c312f": {
					"uId": "c9d9fbd3-991a-483c-a09f-9dfd6e5c312f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "IsPackage"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"CrocIsPerpetual": {
				"e70804e2-2e48-4458-9d81-e0537606b87f": {
					"uId": "e70804e2-2e48-4458-9d81-e0537606b87f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7cda430d-acbc-40e2-ace8-6d05a660527a",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocRelevanceDate"
							}
						}
					]
				},
				"06743c1a-31a1-4a32-9d50-abeba089974c": {
					"uId": "06743c1a-31a1-4a32-9d50-abeba089974c",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocRelevanceDate"
							}
						}
					]
				}
			},
			"CrocCommentStatus": {
				"3257329f-b28b-4c8c-b847-a586d822a6ff": {
					"uId": "3257329f-b28b-4c8c-b847-a586d822a6ff",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocCommentStatus"
							}
						}
					]
				}
			},
			"CrocChart": {
				"c9bcc286-fbdd-4aeb-bb19-88c7026b8e9e": {
					"uId": "c9bcc286-fbdd-4aeb-bb19-88c7026b8e9e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						}
					]
				},
				"bba7320b-922c-4220-867f-ff534104dba3": {
					"uId": "bba7320b-922c-4220-867f-ff534104dba3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocStartAccreditation": {
				"01918a04-5f2b-4858-bac2-dd0cdef710f3": {
					"uId": "01918a04-5f2b-4858-bac2-dd0cdef710f3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7cda430d-acbc-40e2-ace8-6d05a660527a",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocEndAccreditation": {
				"d1f9a053-be7b-4867-93ce-8c2b7402ca6d": {
					"uId": "d1f9a053-be7b-4867-93ce-8c2b7402ca6d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7cda430d-acbc-40e2-ace8-6d05a660527a",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocValidityPeriod": {
				"cf148fbe-7b53-4f2a-8be9-a761b72c0bee": {
					"uId": "cf148fbe-7b53-4f2a-8be9-a761b72c0bee",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "7cda430d-acbc-40e2-ace8-6d05a660527a",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocIsPerpetual"
							},
							"rightExpression": {
								"type": 0,
								"value": false,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"CrocAccount": {
				"8f0bf25f-fcf3-4f4f-ab27-e313439d3155": {
					"uId": "8f0bf25f-fcf3-4f4f-ab27-e313439d3155",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDocumentType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ed86e071-6526-42d4-914a-b3c91ed6939d",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "IsModelItemsEnabled"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
	
			init: function() {
				this.callParent(arguments);
				this.additionalDefaultValues();
				this.sandbox.subscribe("UpdateCrocDocumentKitDetail", this.updateCrocDocumentKitDetail, this);
				this.sandbox.subscribe("RelevanceDateEmptyNotification", this.showNotification, this);
			},

			onRender: function(esq) {
				this.callParent(arguments);
				this.updateCrocDocumentKitDetail();
			},

			updPackage: function() {
				this.setPackage();
			},

			updateCrocDocumentKitDetail: function (args) {
				if (args && args.documentId) {
      				this.updateDetail({detail: "CrocSetDocument1Detail61a32329",reloadAll:true});
   				}
			},
			
			showNotification: function(args) {
				if (args && args.addDocumentId && args.documentNumber){
					let message = this.Ext.String.format(this.get("Resources.Strings.RelevanceDateEmptyNotification"), args.documentNumber);
					this.showInformationDialog(message);
				}
			},

			additionalDefaultValues: function() {
				if(this.isNewMode() && this.sandbox.id && this.sandbox.id.indexOf("CrocDocument1DetailAccreditation") !== -1) {
					this.set("CrocDocumentType", CrocConstantsJS.CrocDocumentType.Accreditation);
				}
			},

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.setCancelAccreditationActionParams();
				this.isApplicationButtonVisible();
				this.isAcceptDocumentActionVisible();
				this.isSendToExporterButtonVisible();
				this.setDocumentLock();
				this.setPackage();
			},

			setPackage: function() {
				var docType = this.$CrocDocumentType;
				if(docType) {
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocDocumentType"});
					esq.addColumn("CrocIsPackage");
					esq.getEntity(docType.value, function (result) {
						if(result && result.success) {
							var item = result.entity;
							this.$IsPackage = item.get("CrocIsPackage");
						}
					}, this);
				}
				else { this.$IsPackage = false; }
			},
			
			isAcceptDocumentActionVisible: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocIsAcceptVisibleRights"},
													   function(result){
					this.$CrocIsAcceptVisible = result && !this.isNewMode() && this.get("CrocIsAccepted") !== true;
					this.sandbox.publish("AcceptDocumentActionVisibleChanged", this.$CrocIsAcceptVisible, [this.sandbox.id]);
				}, this);
			},

			setCancelAccreditationActionParams: function () {
				this.isCancelAccreditationActionEnabled();
				this.isCancelAccreditationActionVisible();
			},

			isCancelAccreditationActionVisible: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanCancelAccreditation"},
													   function(result){
					this.$IsCancelAccreditationVisible = result && !this.isNewMode();
					this.sandbox.publish("CancelAccreditationActionVisibleChanged", this.$IsCancelAccreditationVisible, [this.sandbox.id]);
				}, this);
			},

			isCancelAccreditationActionEnabled: function() {
				this.$IsCancelAccreditationEnabled = !this.isNewMode() && this.get("CrocDocumentStatus").value === CrocConstantsJS.CrocDocumentStatus.Prepared.value;
				this.sandbox.publish("CancelAccreditationActionEnabledChanged", this.$IsCancelAccreditationEnabled, [this.sandbox.id]);
			},
			
			isSendToExporterButtonVisible: function() {
				if (!this.isNewMode() && this.get("CrocDocumentType").value === CrocConstantsJS.CrocDocumentType.Accreditation.value
					&& this.get("CrocCheckStatus").value === CrocConstantsJS.CrocCheckStatus.Draft.value
					&& this.get("CrocDocumentStatus").value === CrocConstantsJS.CrocDocumentStatus.Prepared.value) //DTSRM-285
				{
					this.$SendToExporterButtonVisible = true;
				} else {
					this.$SendToExporterButtonVisible = false;
				}
				this.sandbox.publish("SendToExporterButtonVisibleChanged", this.$SendToExporterButtonVisible, [this.sandbox.id]);
			},
			
			isApplicationButtonVisible: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanManageApplicationRights"},
													   function(result) {
					this.$ApproveApplicationButtonVisible = result && !this.isNewMode() && this.get("CrocDocumentType").value === CrocConstantsJS.CrocDocumentType.ApplicationGU12.value && this.get("CrocGU12Status").value === CrocConstantsJS.CrocApplicationGU12Status.WaitingToApprove.value;
					this.$RejectApplicationButtonVisible = result && !this.isNewMode() && this.get("CrocDocumentType").value === CrocConstantsJS.CrocDocumentType.ApplicationGU12.value && this.get("CrocGU12Status").value === CrocConstantsJS.CrocApplicationGU12Status.WaitingToApprove.value;
					this.sandbox.publish("ApproveApplicationButtonVisibleChanged", this.$ApproveApplicationButtonVisible, [this.sandbox.id]);
					this.sandbox.publish("RejectApplicationButtonVisibleChanged", this.$RejectApplicationButtonVisible, [this.sandbox.id]);
				},this);
			},

			setDocumentLock: function () {
				if (this.isNewMode()) return;
				var isAccepted = !!this.$CrocIsAccepted && (this.$CrocCheckStatus?.value !== CrocConstantsJS.CrocCheckStatus.SendToCheck.value);
				var isGU12NotStatusWaiting = this.$CrocGU12Status?.value !== CrocConstantsJS.CrocApplicationGU12Status.WaitingToApprove.value;
				var isChartPlan = this.$CrocChart?.CrocChartStatus?.value === CrocConstantsJS.CrocChartStatus.Plan.value;
				if (isAccepted) this.$IsModelItemsEnabled = !isAccepted;
				else if (this.$CrocChart) {
					if (isGU12NotStatusWaiting) this.$IsModelItemsEnabled = isGU12NotStatusWaiting;
					else this.$IsModelItemsEnabled = isChartPlan;
				} else this.$IsModelItemsEnabled = !this.$CrocChart;
			},
			
			onAcceptDocumentActionClick: function() {
				if (this.get("CrocIsPerpetual") || this.get("CrocRelevanceDate")? true: false) {
						this.set("CrocIsAccepted", true);
						this.set("CrocDocumentStatus", CrocConstantsJS.CrocDocumentStatus.Actual);
						this.save({isSilent: true});
						this.isAcceptDocumentActionVisible();
				}
				else {
					let message = this.Ext.String.format(this.get("Resources.Strings.RelevanceDateEmptyNotification"), this.$CrocNumber);
					this.showInformationDialog(message);}
			},

			onCancelAccreditationActionClick: function() {
				this.set("CrocDocumentStatus", CrocConstantsJS.CrocDocumentStatus.Canceled);
				this.setCancelAccreditationActionParams();
				this.save({isSilent: true});
			},
			
			onSendToExporterButtonClick: function() {
				this.onSendAccreditationClick();
			},

			setDataAfterExport: function() {
				this.set("CrocCheckStatus", CrocConstantsJS.CrocCheckStatus.SendToCheck);
				this.save({isSilent: true});
				this.isSendToExporterButtonVisible();
				this.setDocumentLock();
			},
			
			onApproveApplicationButtonClick: function() {
				this.set("CrocGU12Status", CrocConstantsJS.CrocApplicationGU12Status.Approved);
				this.save({isSilent: true});
				this.isApplicationButtonVisible();
				//this.onSendGU12Click();
			},
			
			onRejectApplicationButtonClick: function() {
				this.set("CrocGU12Status", CrocConstantsJS.CrocApplicationGU12Status.Rejected);
				this.save({isSilent: true});
				//this.onSendGU12Click();
				var args = {
					sysProcessName: "CrocProcessSetGU12RejectReason",
					parameters: {
						DocumentId: this.$Id
					},
					scope: this
				};
				ProcessModuleUtilities.executeProcess(args);
			},

			onSendAccreditationClick: function() {
				var rowId = this.get("Id");
				ServiceHelper.callService({
					serviceName: "ExchangeIntegrationService",
					methodName: "SendAccreditation",
					data: {
						recordId: rowId
					},
					callback: this.onSendAccreditationClickCallback,
					scope: this
				});
			},

			onSendAccreditationClickCallback: function (response) {
				if(response && response.SendAccreditationResult) {
					var result = response.SendAccreditationResult;
					if(!result.success && result.errorInfo) {
						var errorInfo = result.errorInfo;
						if(errorInfo.message && errorInfo.message.indexOf("TCM:") != -1) {
							this.showInformationDialog(errorInfo.message.replace("TCM: ", ""));
						}
						else { this.setDataAfterExport() }
					} else { this.setDataAfterExport() }
				} else { this.setDataAfterExport() }
			},

			onSendGU12Click: function() {
				var rowId = this.get("Id");
				ServiceHelper.callService({
					serviceName: "ExchangeIntegrationService",
					methodName: "SendGU12A",
					data: {
						recordId: rowId
					},
					scope: this
				});
			},
						
			getActions: function() {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					Type: "Terrasoft.MenuSeparator",
                    Caption: ""	
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": { bindTo: "Resources.Strings.AcceptDocumentActionCaption" },
					"Tag": "onAcceptDocumentActionClick",
					"Visible": {bindTo: "CrocIsAcceptVisible"}
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					Type: "Terrasoft.MenuSeparator",
					Caption: ""
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": { bindTo: "Resources.Strings.CancelAccreditationActionCaption" },
					"Tag": "onCancelAccreditationActionClick",
					"Visible": {bindTo: "IsCancelAccreditationVisible"},
					"Enabled": {bindTo: "IsCancelAccreditationEnabled"}
				}));
				return actionMenuItems;
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "CardContentWrapper",
				"values": {
					"generator": "DisableControlsGenerator.generatePartial"
				}
			},
			{
				"operation": "insert",
				"name": "SendToExporterButton",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.SendToExporterButtonCaption"
					},
					"click": {
						"bindTo": "onSendToExporterButtonClick"
					},
					"enabled": true,
					"visible": {
						"bindTo": "SendToExporterButtonVisible"
					},
					"style": "green",
					"styles": {
						"textStyle": {
							"margin": "0px 9px 0px 0px"
						}
					},
					"layout": {
						"column": 1,
						"row": 6,
						"colSpan": 1
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ApproveApplicationButton",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.ApproveApplicationButtonCaption"
					},
					"click": {
						"bindTo": "onApproveApplicationButtonClick"
					},
					"enabled": true,
					"visible": {
						"bindTo": "ApproveApplicationButtonVisible"
					},
					"style": "green",
					"styles": {
						"textStyle": {
							"margin": "0px 9px 0px 0px"
						}
					},
					"layout": {
						"column": 1,
						"row": 6,
						"colSpan": 1
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "RejectApplicationButton",
				"values": {
					"itemType": 5,
					"caption": {
						"bindTo": "Resources.Strings.RejectApplicationButtonCaption"
					},
					"click": {
						"bindTo": "onRejectApplicationButtonClick"
					},
					"enabled": true,
					"visible": {
						"bindTo": "RejectApplicationButtonVisible"
					},
					"style": "red",
					"styles": {
						"textStyle": {
							"margin": "0px 9px 0px 0px"
						}
					},
					"layout": {
						"column": 1,
						"row": 6,
						"colSpan": 1
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocNumber810dd7a7-b673-4b0b-89fd-a8e3fa680ab0",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocNumber",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDocumentType523c5cae-733f-4f61-b1a0-46f86fb7ff77",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocDocumentType",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocDocumentStatus0a93f711-073f-48a7-b042-78a3fa9fda2d",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocDocumentStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "DATETIME11724122-982c-4f16-a05f-81d701dfc44b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocPreparationDate",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "BOOLEAN140023fa-71b6-4af0-8572-0d3be449a998",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocIsAccepted",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "BOOLEAN8c1e5972-f752-43ed-9ee7-2441113874fa",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocIsPerpetual",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "DATETIME681c9cf1-dd62-4053-b8be-7f896f3ff80a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocRelevanceDate",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "INTEGERc7641f65-b947-4251-b8f3-df7daad891ec",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocValidityPeriod",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "LOOKUP6385aae1-37e7-42a0-ac09-709663a2e20a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocCommentStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "LOOKUP885eb58f-6f2c-496d-98e5-7c0d33fa36da",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 9,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocAccreditationType",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "LOOKUPb07c5f5b-88cc-4811-a468-51564cf0bc56",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 10,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocCheckStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "Tab62f61bd1TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab62f61bd1TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab62f61bd1TabLabelGroupba194667",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab62f61bd1TabLabelGroupba194667GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab62f61bd1TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab62f61bd1TabLabelGridLayout8057d859",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab62f61bd1TabLabelGroupba194667",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocGU12Number1aba25a7-9c7b-47dd-9a12-bf43765c4291",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab62f61bd1TabLabelGridLayout8057d859"
					},
					"bindTo": "CrocGU12Number"
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout8057d859",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocGU12StartDate047c09c4-9da4-4bec-a9e4-9f8a799b596d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab62f61bd1TabLabelGridLayout8057d859"
					},
					"bindTo": "CrocGU12StartDate"
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout8057d859",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocGU12EndDateed191112-ab9e-47b1-a2d9-662c023fd5ee",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab62f61bd1TabLabelGridLayout8057d859"
					},
					"bindTo": "CrocGU12EndDate"
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout8057d859",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocGU12Statuscde6420a-c003-4c11-b83d-1322dbcf19b3",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab62f61bd1TabLabelGridLayout8057d859"
					},
					"bindTo": "CrocGU12Status",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout8057d859",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocGU12RejectReasonde1478c0-2477-4ec4-8d5b-8ae434af4f53",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab62f61bd1TabLabelGridLayout8057d859"
					},
					"bindTo": "CrocGU12RejectReason",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout8057d859",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUP3c843c94-0820-44b1-88d0-700e068acfc1",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab62f61bd1TabLabelGridLayout8057d859"
					},
					"bindTo": "CrocPort",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout8057d859",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "LOOKUPc2d1804c-a03e-4735-9355-41f2a55f2740",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tab62f61bd1TabLabelGridLayout8057d859"
					},
					"bindTo": "CrocTerminal",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout8057d859",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tab62f61bd1TabLabelGroup7e0baf1d",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab62f61bd1TabLabelGroup7e0baf1dGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab62f61bd1TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab62f61bd1TabLabelGridLayout5884b84c",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab62f61bd1TabLabelGroup7e0baf1d",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocAccount32d805f6-2a7c-4550-ba56-857e4cd79453",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab62f61bd1TabLabelGridLayout5884b84c"
					},
					"bindTo": "CrocAccount",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout5884b84c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP88709093-46e9-4dc8-9980-1fc2de50af43",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab62f61bd1TabLabelGridLayout5884b84c"
					},
					"bindTo": "CrocContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout5884b84c",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocChart701d0640-858b-49b2-8161-18f3ca674842",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab62f61bd1TabLabelGridLayout5884b84c"
					},
					"bindTo": "CrocChart"
				},
				"parentName": "Tab62f61bd1TabLabelGridLayout5884b84c",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocSetDocument1Detail61a32329",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab62f61bd1TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocComment1Detail99e841a5",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab62f61bd1TabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
