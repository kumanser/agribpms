define("CrocChart1Detail", ["CrocConstantsJS", "ProcessModuleUtilities"], function(CrocConstantsJS, ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocChart",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "SendChartToExporterButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onSendToExporterButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": {"bindTo": "Resources.Strings.SendChartToExporterButton"},
					"visible": { "bindTo": "IsSendToExporterButtonVisible" }
				},
			},
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "AcceptChartButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onAcceptChartButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": {"bindTo": "Resources.Strings.AcceptChartButton"},
					"visible": { "bindTo": "IsAcceptChartButtonVisible" }
				},
			},
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "RejectChartButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onRejectChartButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": {"bindTo": "Resources.Strings.RejectChartButton"},
					"visible": { "bindTo": "IsRejectChartButtonVisible" }
				},
			}
		]/**SCHEMA_DIFF*/,
		attributes: {
			IsSendToExporterButtonVisible: {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				dependencies: [{
					columns: ["ActiveRow"],
					methodName: "onActiveRowChange"
				}],
				value: false
			},
			IsAcceptChartButtonVisible: {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				dependencies: [{
					columns: ["ActiveRow"],
					methodName: "onActiveRowChange"
				}],
				value: false
			},
			IsRejectChartButtonVisible: {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				dependencies: [{
					columns: ["ActiveRow"],
					methodName: "onActiveRowChange"
				}],
				value: false
			},
		},
		methods: {
			getAddRecordButtonVisible: Terrasoft.emptyFn,
			getDeleteRecordMenuItem: Terrasoft.emptyFn,
			getCopyRecordMenuItem: Terrasoft.emptyFn,

			getOpenCardConfig: function(operation, typeColumnValue, recordId) {
				var config = this.callParent(arguments);
				if(config && config.schemaName) { config.schemaName = "CrocChart2Page"; }
				return config;
			},

			onActiveRowChange: function() {
				var gridData = this.getGridData();
				var activeRow = this.get("ActiveRow");
				if (gridData && activeRow) {
					this.checkStatusInRow(function (result) {
						if (result === CrocConstantsJS.CrocChartStatus.Plan.value) {
							this.$IsSendToExporterButtonVisible = true;
							this.$IsAcceptChartButtonVisible = false;
							this.$IsRejectChartButtonVisible = false;
						}
						else if (result === CrocConstantsJS.CrocChartStatus.WaitingProviderApprovement.value) {
							this.$IsSendToExporterButtonVisible = false;
							this.$IsAcceptChartButtonVisible = true;
							this.$IsRejectChartButtonVisible = true;
						}
						else {
							this.$IsSendToExporterButtonVisible = false;
							this.$IsAcceptChartButtonVisible = false;
							this.$IsRejectChartButtonVisible = false;
						}
					}, this);
				} else {
					this.$IsSendToExporterButtonVisible = false;
					this.$IsAcceptChartButtonVisible = false;
					this.$IsRejectChartButtonVisible = false;
				}
			},

			checkStatusInRow: function(callback, scope) {
				var activeRow = this.get("ActiveRow");
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocChart"});
				esq.addColumn("CrocChartStatus.Id", "ChartStatus");
				esq.getEntity(activeRow, function(result){
					if (result && result.success) {
						var chartStatus = result.entity.get("ChartStatus");
						callback.call(scope, chartStatus);
					}
				});
			},

			getGU12Exists: function(callback, scope) {
				var chart = this.get("ActiveRow");
				if (chart) {
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocDocument"});
					esq.addColumn("CrocChart");
					esq.addColumn("CrocChartStatus");
					esq.addColumn("CrocDocumentType");
					esq.filters.add("CrocChart", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "CrocChart", chart));
					esq.filters.add("CrocDocumentType", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "CrocDocumentType", CrocConstantsJS.CrocDocumentType.ApplicationGU12.value));
					esq.filters.add("CrocDocumentStatus", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.NOT_EQUAL, "CrocDocumentStatus", CrocConstantsJS.CrocDocumentStatus.Canceled.value));
					esq.getEntityCollection(function (response) {
						if (response && response.success) {
							var result = response.collection;
							var isGU12Exists = (result.collection.length !== 0);
							callback.call(scope, isGU12Exists);
						}
					});
				}
			},

			getLoadingScheduleExists: function (callback, scope) {
				var chart = this.get("ActiveRow");
				if (chart) {
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocLoadingSchedule"});
					esq.addColumn("CrocChart");
					esq.addColumn("CrocChart.CrocDeliveryCondition");
					esq.addColumn("CrocChart.CrocTransportType");
					esq.filters.add("CrocChart", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "CrocChart", chart));
					esq.filters.add("CrocDeliveryCondition", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.NOT_EQUAL, "CrocChart.CrocDeliveryCondition", CrocConstantsJS.CrocDeliveryCondition.EXW));
					esq.filters.add("CrocTransportType", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "CrocChart.CrocTransportType", CrocConstantsJS.CrocTransportType.Railway));
					esq.getEntityCollection(function (response) {
						if (response && response.success) {
							var result = response.collection;
							var isLoadingScheduleExists = (result.collection.length !== 0);
							callback.call(scope, isLoadingScheduleExists);
						}
					});
				}
			},

			updateChartStatus: function (isGU12Exists, isLoadingScheduleExists) {
				if (isGU12Exists && isLoadingScheduleExists) {
					var activeRow = this.get("ActiveRow");
					var uq = Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "CrocChart"});
					uq.filters.addItem(uq.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "Id", activeRow));
					uq.setParameterValue("CrocChartStatus", CrocConstantsJS.CrocChartStatus.WaitingExporterApprovement.value, Terrasoft.DataValueType.GUID);
					uq.execute(function () {
						this.updateDetail({reloadAll: true});
						this.$IsSendToExporterButtonVisible = false;
					}, this);
				} else this.showInformationDialog(this.get("Resources.Strings.NeedToLoadGU12OrLoadingRegister"));
			},

			onSendToExporterButtonClick: function () {
				var isGU12Exists;
				var isLoadingScheduleExists;
				this.Terrasoft.chain(
					function (next) {
						this.getGU12Exists(function (result) {
							isGU12Exists = result;
							next();
						}, this);
					},
					function () {
						this.getLoadingScheduleExists(function (result) {
							isLoadingScheduleExists = result;
							this.updateChartStatus(isGU12Exists, isLoadingScheduleExists);
						}, this);
					}, this);
			},

			onAcceptChartButtonClick: function() {
				var activeRow = this.get("ActiveRow");
				var uq = Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "CrocChart"});
				uq.filters.addItem(uq.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "Id", activeRow));
				uq.setParameterValue("CrocChartStatus", CrocConstantsJS.CrocChartStatus.ProviderApprovement.value, Terrasoft.DataValueType.GUID);
				uq.execute(function() {
					this.updateDetail({reloadAll: true});
					this.$IsAcceptChartButtonVisible = false;
					this.$IsRejectChartButtonVisible = false;
				}, this);
			},

			onRejectChartButtonClick: function() {
				var activeRow = this.get("ActiveRow");
				var uq = Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "CrocChart"});
				uq.filters.addItem(uq.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "Id", activeRow));
				uq.setParameterValue("CrocChartStatus", CrocConstantsJS.CrocChartStatus.ProviderRejected.value, Terrasoft.DataValueType.GUID);
				uq.execute(function() {
					this.updateDetail({reloadAll: true});
					this.$IsAcceptChartButtonVisible = false;
					this.$IsRejectChartButtonVisible = false;
				}, this);
				var args = {
					sysProcessName: "CrocProcessChartRejectedEmail",
					parameters: {
						ChartId: activeRow
					}
				};
				ProcessModuleUtilities.executeProcess(args);
			},
		}
	};
});
