define("EmailPageV2", [], function() {
	return {
		entitySchemaName: "Activity",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "EmailFileDetailV2",
				"entitySchemaName": "ActivityFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "Activity"
				}
			},
			"EntityConnections": {
				"schemaName": "EmailEntityConnectionsDetailV2",
				"entitySchemaName": "EntityConnection",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "SysModuleEntity"
				}
			},
			"RelatedEmails": {
				"schemaName": "RelatedEmailsDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "Activity"
				}
			},
			"VisaDetailV23b82465c": {
				"schemaName": "VisaDetailV2",
				"entitySchemaName": "CrocActivityVisa",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocActivity"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "CardContentWrapper",
				"values": {
					"markerValue": {
						"bindTo": "getCardContentContainerMarkerValue"
					}
				}
			},
			{
				"operation": "merge",
				"name": "SaveButton",
				"values": {
					"style": "blue"
				}
			},
			{
				"operation": "move",
				"name": "ESNTab",
				"parentName": "Tabs",
				"propertyName": "tabs"
			},
			{
				"operation": "move",
				"name": "TimelineTab",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"propertyName": "tabs",
				"parentName": "Tabs",
				"name": "Tab7426ea31TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabVisaCaption"
					},
					"items": []
				}
			},
			{
				"operation": "insert",
				"propertyName": "items",
				"parentName": "Tab7426ea31TabLabel",
				"name": "VisaDetailV23b82465c",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
