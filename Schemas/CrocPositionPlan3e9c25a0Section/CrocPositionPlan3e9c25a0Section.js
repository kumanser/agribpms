define("CrocPositionPlan3e9c25a0Section", ["CrocExcelTemplateConst", "ServiceHelper", "RightUtilities", "CrocExcelHelperJS"], function(CrocExcelTemplateConst, ServiceHelper,
																																	   RightUtilities, CrocExcelHelperJS) {
	return {
		entitySchemaName: "CrocPositionPlan",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		attributes: {
			"CanExportFile": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			}
		},
		methods: {

			init: function() {
				this.callParent(arguments);
				this.setCanExportExcel();
			},

			setCanExportExcel: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanExportToExcel"},
					function(result) {
						this.$CanExportFile = result;
					}, this);
			},

			getSectionActions: function () {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					Type: "Terrasoft.MenuSeparator",
					Caption: ""
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Enabled": {bindTo: "CanExportFile"},
					"Visible": {bindTo: "CanExportFile"},
					"Caption" : {bindTo: "Resources.Strings.CrocExcelVEDButtonCaption"},
					"Click": {bindTo: "exportToExcelVED"},
				}));
				return actionMenuItems;
			},

			exportToExcelVED: function(config) {
				var filter = this.getFilters();
				var config = {
					serviceName: "CrocExcelGeneratorService",
					methodName: "GenerateExcel",
					data: {
						model: JSON.stringify(CrocExcelTemplateConst.CrocExcelTemplates.ExcelVED),
						filter: filter.serialize()
					}
				};

				ServiceHelper.callService(config.serviceName, config.methodName, function(response) {
					var cacheKey = response.GenerateExcelResult;
					CrocExcelHelperJS.CrocExcelHelper.downloadFile(CrocExcelTemplateConst.CrocExcelTemplates.ExcelVED.excelReportName, cacheKey);
				}, config.data, this);
			},

		}
	};
});
