define("CrocProgressReport1Detail", ["RightUtilities", "CrocConstantsJS", "ProcessModuleUtilities"],
	function(RightUtilities, CrocConstantsJS, ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocProgressReport",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		attributes: {
			"IsAcceptReportButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsSendToReviewButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsReturnForRevisionButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsAcceptReportRights": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsReturnForRevisionRights": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
		},
		messages: {},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "CrocAcceptReportButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onCrocAcceptReportButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": {"bindTo": "Resources.Strings.AcceptReportButtonCaption"},
					"visible": {"bindTo": "IsAcceptReportButtonVisible"}
				}
			},
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "CrocSendToReviewButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onCrocSendToReviewButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": {"bindTo": "Resources.Strings.SendToReviewButtonCaption"},
					"visible": {"bindTo": "IsSendToReviewButtonVisible"}
				}
			},
			{
				"operation": "insert",
				"parentName": "Detail",
				"propertyName": "tools",
				"name": "CrocReturnForRevisionButton",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"click": {"bindTo": "onCrocReturnForRevisionButtonClick"},
					"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
					"caption": {"bindTo": "Resources.Strings.ReturnForRevisionButtonCaption"},
					"visible": {"bindTo": "IsReturnForRevisionButtonVisible"}
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			onActiveRowChange: function() {
				var gridData = this.getGridData();
				var activeRow = this.get("ActiveRow");
				if (gridData && activeRow) {
					this.getReportStatus(function (result) {
						if (result === CrocConstantsJS.CrocProgressReportStatus.UnderReview) {
							this.$IsAcceptReportButtonVisible = this.$IsAcceptReportRights;
							this.$IsSendToReviewButtonVisible = false;
							this.$IsReturnForRevisionButtonVisible = this.$IsReturnForRevisionRights;
						}
						else if (result === CrocConstantsJS.CrocProgressReportStatus.Draft) {
							this.$IsAcceptReportButtonVisible = false;
							this.$IsSendToReviewButtonVisible = true;
							this.$IsReturnForRevisionButtonVisible = false;
						}
						else if (result === CrocConstantsJS.CrocProgressReportStatus.ReturnedForRevision) {
							this.$IsAcceptReportButtonVisible = false;
							this.$IsSendToReviewButtonVisible = true;
							this.$IsReturnForRevisionButtonVisible = false;
						}
						else {
							this.$IsAcceptReportButtonVisible = false;
							this.$IsSendToReviewButtonVisible = false;
							this.$IsReturnForRevisionButtonVisible = false;
						}
					}, this);
				} else {
					this.$IsAcceptReportButtonVisible = false;
					this.$IsSendToReviewButtonVisible = false;
					this.$IsReturnForRevisionButtonVisible = false;
				}
			},

			getReportStatus: function(callback, scope) {
				var activeRow = this.get("ActiveRow");
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocProgressReport"});
				esq.addColumn("CrocReportStatus.Id", "ReportStatus");
				esq.getEntity(activeRow, function(result){
					if (result && result.success) {
						var reportStatus = result.entity.get("ReportStatus");
						callback.call(scope, reportStatus);
					}
				});
			},

			onCrocAcceptReportButtonClick: function() {
				var status = CrocConstantsJS.CrocProgressReportStatus.Accepted;
				this.runProgressReportStatusProcess(status);
			},

			setRejectReasonValue: function (reason) {
				var insert = Ext.create("Terrasoft.InsertQuery", {rootSchemaName: "CrocProgressReportRejectReason"});
				insert.setParameterValue("CrocProgressReport", this.get("ActiveRow"), this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("CrocReason", reason, this.Terrasoft.DataValueType.TEXT);
				insert.execute();
			},

			onCrocReturnForRevisionButtonClick: function() {
				Terrasoft.utils.inputBox("Заполните причину возврата на доработку", function (result, args){
						if (result === Terrasoft.MessageBoxButtons.OK.returnCode) {
							var reason = args.reason.value;
							if (reason.length > 0 ) {
								this.setRejectReasonValue(reason);
								var status = CrocConstantsJS.CrocProgressReportStatus.ReturnedForRevision;
								this.runProgressReportStatusProcess(status);
							}
						}
					}, [{
						className: "Terrasoft.Button",
						caption: "OK",
						returnCode: "ok"
					}, "Cancel"],
					this, {
						reason: {
							dataValueType: Terrasoft.DataValueType.TEXT,
							caption: "Причина",
							customConfig: {
								className: "Terrasoft.MemoEdit",
								height: "80px"
							},
							value: ""
						},
					},{
						defaultButton: 0,
						style: {
							borderStyle: "ts-messagebox-border-style-blue visa-action",
							buttonStyle: "blue"
						}
					});
			},

			onCrocSendToReviewButtonClick: function() {
				var status = CrocConstantsJS.CrocProgressReportStatus.UnderReview;
				this.updateReportStatus(status,function() {
					this.updateDetail({reloadAll: true});
					this.runProgressReportSendingProcess();
				}, this);
			},

			updateReportStatus: function(status, callback, scope) {
				var activeRowId = this.get("ActiveRow");
				var uq = Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "CrocProgressReport"});
				uq.filters.addItem(uq.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "Id", activeRowId));
				uq.setParameterValue("CrocReportStatus", status, Terrasoft.DataValueType.GUID);
				uq.execute(function() {callback.call(scope);}, this);
			},

			runProgressReportSendingProcess: function(){
				var progressReportId = this.get("ActiveRow");
				var args = {
					sysProcessName: "CrocProcessProgressReportSending",
					parameters: {
						IdRequest: this.$MasterRecordId,
						IdProgressReport : progressReportId
					}
				};
				ProcessModuleUtilities.executeProcess(args);
			},

			runProgressReportStatusProcess: function(status){
				var progressReportId = this.get("ActiveRow");
				var args = {
					sysProcessName: "CrocProcessProgressReportStatus",
					parameters: {
						ReportId: progressReportId,
						StatusId: status
					}
				};
				ProcessModuleUtilities.executeProcess(args);
			},

			init: function(){
				this.callParent(arguments);
				this.checkRights();
			},

			checkRights: function(){
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanAcceptReportRequest"}, function(result) {
					this.$IsAcceptReportRights = result;
				},this);
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanReturnForRevisionReportRequest"}, function(result) {
					this.$IsReturnForRevisionRights = result;
				},this);
			},
		}
	};
});
