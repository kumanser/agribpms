define("CrocContract2Page", ["RightUtilities","ProcessModuleUtilities", "ServiceHelper"], function(RightUtilities, ProcessModuleUtilities, ServiceHelper) {
	return {
		entitySchemaName: "CrocContract",
		attributes: {
			"CrocIsRequestVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"CrocSmartseedsUrl": {
				"dataValueType": Terrasoft.DataValueType.TEXT,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"caption": { bindTo: "Resources.Strings.SmartseedsUrlCaption"}
			}
		},
		messages: {
			"RequestDocumentActionVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"RelevanceDateEmptyNotification": {
				"mode": Terrasoft.MessageMode.BROADCAST,
             	"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"UpdateCrocDocumentKitDetail": {
             	"mode": Terrasoft.MessageMode.BROADCAST,
             	"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
        	},
			"SendToExporterButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocContractFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocContract"
				}
			},
			"CrocContract1Detailf22ef53f": {
				"schemaName": "CrocContract1Detail",
				"entitySchemaName": "CrocContract",
				"filter": {
					"detailColumn": "CrocParentContract",
					"masterColumn": "Id"
				}
			},
			"CrocPlannedRefund1Detail2f8737f5": {
				"schemaName": "CrocPlannedRefund1Detail",
				"entitySchemaName": "CrocPlannedRefund",
				"filter": {
					"detailColumn": "CrocContract",
					"masterColumn": "Id"
				}
			},
			"CrocPurchaseReturns1Detail83ddd0d7": {
				"schemaName": "CrocPurchaseReturns1Detail",
				"entitySchemaName": "CrocPurchaseReturns",
				"filter": {
					"detailColumn": "CrocContract",
					"masterColumn": "Id"
				}
			},
			"CrocMutualSettlements1Detail8c5bd5a1": {
				"schemaName": "CrocMutualSettlements1Detail",
				"entitySchemaName": "CrocMutualSettlements",
				"filter": {
					"detailColumn": "CrocContractDS",
					"masterColumn": "Id"
				}
			},
			"CrocSetDocument1Detail2b1f2b40": {
				"schemaName": "CrocSetDocument1Detail",
				"entitySchemaName": "CrocSetDocument",
				"filter": {
					"detailColumn": "CrocContract",
					"masterColumn": "Id"
				}
			},
			"CrocChart1Detail8e2853af": {
				"schemaName": "CrocChart1Detail",
				"entitySchemaName": "CrocChart",
				"filter": {
					"detailColumn": "CrocContract",
					"masterColumn": "Id"
				}
			},
			"ActivityDetailV2720b166d": {
				"schemaName": "ActivityDetailV2",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "CrocCrocContract",
					"masterColumn": "Id"
				}
			},
			"CrocAdjustmentOrder1Detaile471f790": {
				"schemaName": "CrocAdjustmentOrder1Detail",
				"entitySchemaName": "CrocAdjustmentOrder",
				"filter": {
					"detailColumn": "CrocContract",
					"masterColumn": "Id"
				}
			},
			"CrocAdjustmentOrder1Detaila243ac8c": {
				"schemaName": "CrocAdjustmentOrder1Detail",
				"entitySchemaName": "CrocAdjustmentOrder",
				"filter": {
					"detailColumn": "CrocContract",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"Tab329e4d05TabLabelGroupc636d345": {
				"50a9ed18-ed79-4a74-a8e1-3af3e65353f7": {
					"uId": "50a9ed18-ed79-4a74-a8e1-3af3e65353f7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba3fb45a-2a01-49bd-bb0e-2b01bc011cd0",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab329e4d05TabLabelGroup47409585": {
				"372644ed-a4c8-4d35-a0c3-a7e7e633f2ff": {
					"uId": "372644ed-a4c8-4d35-a0c3-a7e7e633f2ff",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab329e4d05TabLabelGroupd5e4cbce": {
				"d6fd95e5-2102-42c5-8d6e-ef3ccb36b35d": {
					"uId": "d6fd95e5-2102-42c5-8d6e-ef3ccb36b35d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"Tab1a015db7TabLabel": {
				"70f94c05-35d4-4bfa-87a8-6c2e1e159bb7": {
					"uId": "70f94c05-35d4-4bfa-87a8-6c2e1e159bb7",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocFinalQualityUpload": {
				"4cdf6743-79a7-4e09-b718-97287a46f86a": {
					"uId": "4cdf6743-79a7-4e09-b718-97287a46f86a",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocTerminal": {
				"7b3577af-66d4-4d7f-a788-0a58a6cb9416": {
					"uId": "7b3577af-66d4-4d7f-a788-0a58a6cb9416",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				},
				"bde19749-190c-4770-ac75-adf2b4e3a4dd": {
					"uId": "bde19749-190c-4770-ac75-adf2b4e3a4dd",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocPortElevator": {
				"189aea6f-dac5-4f0b-9e39-c783e8c8d5bc": {
					"uId": "189aea6f-dac5-4f0b-9e39-c783e8c8d5bc",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						},
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "3ce25053-e06f-4c8b-85c5-8fd89ab886e9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNaturFact": {
				"a4d5f23f-bc29-48af-8e7b-6182dae07499": {
					"uId": "a4d5f23f-bc29-48af-8e7b-6182dae07499",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocProteinFact": {
				"b31a53ce-4df3-4f39-98af-dc7088030075": {
					"uId": "b31a53ce-4df3-4f39-98af-dc7088030075",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocWeedImpurityFact": {
				"0a3f4ae9-1d07-446c-aa6e-76ab083dc5c6": {
					"uId": "0a3f4ae9-1d07-446c-aa6e-76ab083dc5c6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocChPFact": {
				"52c1ae1e-64a0-4c52-95bd-9115ccd7f9d9": {
					"uId": "52c1ae1e-64a0-4c52-95bd-9115ccd7f9d9",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocContract1Detailf22ef53f": {
				"b6c5f737-eb36-4ac0-a676-a8de85d891d3": {
					"uId": "b6c5f737-eb36-4ac0-a676-a8de85d891d3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocPurchaseReturns1Detail83ddd0d7": {
				"9c362634-3b89-4cce-956a-18029e29168b": {
					"uId": "9c362634-3b89-4cce-956a-18029e29168b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocDeliveryCondition"
							},
							"rightExpression": {
								"type": 0,
								"value": "0b5f5318-738c-49fc-890c-f481739f4d0f",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			init: function() {
				this.callParent(arguments);
				this.loadSettingUrl();
				this.sandbox.subscribe("RelevanceDateEmptyNotification", this.showNotification, this);
				this.sandbox.subscribe("UpdateCrocDocumentKitDetail", this.updateCrocDocumentKitDetail, this);
			},

			showNotification: function(args) {
				if (args && args.addDocumentId && args.documentNumber){
					message = this.Ext.String.format(this.get("Resources.Strings.RelevanceDateEmptyNotification"), args.documentNumber);
					this.showInformationDialog(message);
				}
			},
			
			updateCrocDocumentKitDetail: function(args) {
				if (args && args.documentId) {
      				this.updateDetail({detail: "CrocSetDocument1Detail2b1f2b40",reloadAll:true});
   				}
			},
			
			loadSettingUrl: function() {
				let sysSettings = ["CrocSmartseedsURL"];
				Terrasoft.SysSettings.querySysSettings(sysSettings, function(settings) {
					this.$CrocSmartseedsUrl = settings.CrocSmartseedsURL;
				}, this);
			},

			getSmartseedsLink: function(value) {
				return {
					url: value,
					caption: "Smartseeds"
				};
			},

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.CrocIsRequestVisibleRights();
				this.sandbox.publish("SendToExporterButtonVisibleChanged", true, ["SectionModuleV2_InvoiceSectionV2"]);
			},

			onRender: function(esq) {
				this.callParent(arguments);
				this.updateDetail({detail: "CrocSetDocument1Detail2b1f2b40", reloadAll: true});
			},
			
			CrocIsRequestVisibleRights: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanRequestDocument"},
													   function(result){
					this.$CrocIsRequestVisible = result && !this.isNewMode();
					this.sandbox.publish("RequestDocumentActionVisibleChanged", this.$CrocIsRequestVisible, [this.sandbox.id]);
				}, this);
			},
			
			onRequestDocumentActionClick: function(){
				var args = {
					sysProcessName: "CrocProcessContractDocumentRequest",
					parameters: {
						Contract: this.$Id
					}
				};
				ProcessModuleUtilities.executeProcess(args);
			},
			
			getActions: function() {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					Type: "Terrasoft.MenuSeparator",
                    Caption: ""	
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": { bindTo: "Resources.Strings.RequestDocumentActionCaption" },
					"Tag": "onRequestDocumentActionClick",
					"Visible": {bindTo: "CrocIsRequestVisible"}
				}));
				return actionMenuItems;
			},

			loadESNFeed: function() {
				var moduleId = this.getSocialFeedSandboxId();
				var rendered = this.sandbox.publish("RerenderModule", {
					renderTo: "ESNFeed"
				}, [moduleId]);
				if (!rendered) {
					var esnFeedModuleConfig = {
						renderTo: "ESNFeed",
						id: moduleId
					};
					var activeSocialMessageId = this.get("ActiveSocialMessageId") ||
						this.getDefaultValueByName("ActiveSocialMessageId");
					if (!Ext.isEmpty(activeSocialMessageId)) {
						esnFeedModuleConfig.parameters = {activeSocialMessageId: activeSocialMessageId};
					}
					this.sandbox.loadModule("CrocFeedModule", esnFeedModuleConfig);
				}
			},

			onSSCalculateButtonClick: function() {
				var config = {
					serviceName: "ExchangeIntegrationService",
					methodName: "SendSmartSeedsCalculationRequest",
					data: {
						config: JSON.stringify({
							recordId: this.$Id,
							originId: this.$CrocSmartseedsAddressesFrom.value,
							destinationId: this.$CrocSmartseedsAddressesTo.value,
							productId: this.$CrocProductSmartseeds.value,
							deliveryDate: this.$CrocShipmentDateSmartseeds,
							weight: this.$CrocTonSmartseeds
						})
					},
					callback: this.SSCalculateButtonCallback,
					scope: this
				};
				ServiceHelper.callService(config);
			},

			SSCalculateButtonCallback: function(response) {
				if(response && response.SendSmartSeedsCalculationRequestResult) {
					var result = response.SendSmartSeedsCalculationRequestResult;
					if(result.success) {
						var fields = [
							"CrocDistanceSmartseeds",
							"CrocNumberFlightSmartseeds",
							"CrocTonCarSmartseeds",
							"CrocTravelTimeSmartseeds",
							"CrocAmountTonSmartseeds",
							"CrocTotalAmountSmartseeds",
							"CrocSmartseedsAddressesFromId",
							"CrocSmartseedsAddressesToId",
							"CrocShipmentDateSmartseeds",
							"CrocProductSmartseedsId",
							"CrocTonSmartseeds"
						];
						this.reloadCardFromPage(fields, null, this);
					}
					else {
						if(result.errorInfo) {
							this.showInformationDialog(result.errorInfo.message);
						}
						else { console.log(result); }
					}
				}
			},

			SSCalculateButtonEnabled: function() {
				/*
				if(this.isNewMode()) return false;
				return (this.$CrocSmartseedsAddressesFrom && this.$CrocSmartseedsAddressesTo && this.$CrocShipmentDateSmartseeds &&
					this.$CrocProductSmartseeds && this.$CrocTonSmartseeds)
				 */
				return false;
			},

			onSendToExporterButtonClick: function() {
				var args = {
					sysProcessName: "CrocProcessSendDocumentsToExporter",
					parameters: {
						ContractId: this.$Id
					},
					scope: this
				};
				ProcessModuleUtilities.executeProcess(args);
			},
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "SendToExporterButton",
				"values": {
					"tag": "onSendToExporterButtonClick",
					"itemType": 5,
					"style": "green",
					"visible": true,
					"caption": {
						"bindTo": "Resources.Strings.SendToExporterButtonCaption"
					},
					"click": {
						"bindTo": "onSendToExporterButtonClick"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocParentContracteff2249a-79da-430d-b27d-0a9fa82596b7",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocParentContract",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocContractNumber4747175f-0927-4c59-8382-bd0b55bb1279",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocContractNumber",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocContractStatus2c7a73de-8763-4761-8f0a-47d384fc19d2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocContractStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocContractTypecd621791-2605-4412-8184-a4e0a10b249d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocContractType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP79325f74-2f2d-4717-85a6-64aca7cde12c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocPurchaseType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocStartDatec6d00417-8501-457a-9cbd-980d55651c9c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocStartDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocEndDate22785856-f496-4365-912a-a04ea913d0ab",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocEndDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab329e4d05TabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGroupc28b49b7",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab329e4d05TabLabelGroupc28b49b7GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab329e4d05TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGridLayoutc6ad936f",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab329e4d05TabLabelGroupc28b49b7",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocAccount01fcfde3-e35f-4f84-97d7-070df73ccada",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayoutc6ad936f"
					},
					"bindTo": "CrocAccount",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayoutc6ad936f",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP846cb537-b9b4-45cc-a0e2-7dad622646e3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayoutc6ad936f"
					},
					"bindTo": "CrocExporter",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayoutc6ad936f",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGroup13fb278c",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab329e4d05TabLabelGroup13fb278cGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab329e4d05TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGridLayout18cdde38",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab329e4d05TabLabelGroup13fb278c",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryCondition3b02f9ee-6d25-4061-8f08-174cdb154d97",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayout18cdde38"
					},
					"bindTo": "CrocDeliveryCondition",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayout18cdde38",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP62896182-b7b6-4bf2-8dcb-6b1fc898b5c8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayout18cdde38"
					},
					"bindTo": "CrocTerminal",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayout18cdde38",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocPaymentTerm5e419295-7a35-4bf7-ae39-4a0d5ea59017",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayout18cdde38"
					},
					"bindTo": "CrocPaymentTerm",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayout18cdde38",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP418f11e5-2167-46a0-829f-1132a22a0cf9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayout18cdde38"
					},
					"bindTo": "CrocPortElevator",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayout18cdde38",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUPbd078988-b435-473e-9f6c-66161f5cba5f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab329e4d05TabLabelGridLayout18cdde38"
					},
					"bindTo": "CrocProduct",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayout18cdde38",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "FLOATd9f3ff10-4bd5-413b-9d2b-663b37b29972",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab329e4d05TabLabelGridLayout18cdde38"
					},
					"bindTo": "CrocAmount",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout18cdde38",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "LOOKUPddcb428f-20bd-4b57-8e7e-82e318a94235",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab329e4d05TabLabelGridLayout18cdde38"
					},
					"bindTo": "CrocProductClass",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayout18cdde38",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "FLOATb33c3e4a-9f29-45bc-9b51-a68530b93b99",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tab329e4d05TabLabelGridLayout18cdde38"
					},
					"bindTo": "CrocDebt",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout18cdde38",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "INTEGERb185e6a6-0888-4b94-bb64-48efaa2e107f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab329e4d05TabLabelGridLayout18cdde38"
					},
					"bindTo": "CrocCropYear",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout18cdde38",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGroupc636d345",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab329e4d05TabLabelGroupc636d345GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab329e4d05TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGridLayout12301082",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab329e4d05TabLabelGroupc636d345",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIMEc0ff1382-408d-4636-b328-ca07cb92a379",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayout12301082"
					},
					"bindTo": "CrocDeliveryTimeFrom",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout12301082",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryTimeToa134513e-1ed0-411b-a74a-379c09a2146e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayout12301082"
					},
					"bindTo": "CrocDeliveryTimeTo",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout12301082",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocQuantityPlanTond08f2b48-a87c-4d6b-a56d-e53791d00d09",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayout12301082"
					},
					"bindTo": "CrocQuantityPlanTon",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout12301082",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocQuantityFactTonfea5885f-f397-4ce9-b8a1-8be75afc981f",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayout12301082"
					},
					"bindTo": "CrocQuantityFactTon",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout12301082",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocBalanceDeliveredTon158b9bca-cef1-46ee-83da-f5c40553d5a4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab329e4d05TabLabelGridLayout12301082"
					},
					"bindTo": "CrocBalanceDeliveredTon",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout12301082",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "INTEGER4dc2d4b3-fa42-44e6-bc14-13a80781cc90",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab329e4d05TabLabelGridLayout12301082"
					},
					"bindTo": "CrocFactDeliveryTransport",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout12301082",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocExecutionPercentage8a465714-27a1-4f01-a5ce-755e753cef8c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab329e4d05TabLabelGridLayout12301082"
					},
					"bindTo": "CrocExecutionPercentage",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout12301082",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocAmountTonSmartseeds77e16cb6-b7fd-4753-a4ae-eff4c3250ed3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab329e4d05TabLabelGridLayout12301082"
					},
					"bindTo": "CrocAmountTonSmartseeds",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.CrocAmountTonSmartseeds77e16cb6b7fd4753a4aeeff4c3250ed3LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout12301082",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGroup47409585",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab329e4d05TabLabelGroup47409585GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab329e4d05TabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGridLayout536688d8",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab329e4d05TabLabelGroup47409585",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "DATETIME474057d3-0952-42b9-8ad6-fda6533772a3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayout536688d8"
					},
					"bindTo": "CrocDateTransferOwnershipFrom",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout536688d8",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDateTransferOwnershipFrom691fa457-fdd1-4648-be5f-84aac6039278",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayout536688d8"
					},
					"bindTo": "CrocDateTransferOwnershipFrom",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout536688d8",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRINGf97554ba-4202-43aa-a555-d6fa72a395f2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayout536688d8"
					},
					"bindTo": "CrocLoadingPoint",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tab329e4d05TabLabelGridLayout536688d8",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocQuantityPlanTon5e418d75-2a41-4a97-94a3-fae71e345226",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayout536688d8"
					},
					"bindTo": "CrocQuantityPlanTon",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout536688d8",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocFactLoadingTon167aebf4-9164-4815-a112-b2965efcc256",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab329e4d05TabLabelGridLayout536688d8"
					},
					"bindTo": "CrocFactLoadingTon",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout536688d8",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "STRING211289fe-63c9-4a99-9807-c0221e6af41a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab329e4d05TabLabelGridLayout536688d8"
					},
					"bindTo": "CrocFinalQualityLoading",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tab329e4d05TabLabelGridLayout536688d8",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocRemainingExportedTond65f8fff-6a6a-4ae6-bff1-2a98031f4dd7",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab329e4d05TabLabelGridLayout536688d8"
					},
					"bindTo": "CrocRemainingExportedTon",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout536688d8",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "STRINGd7e05dfd-e62e-444a-b8bf-e2d7cbc5ca0e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tab329e4d05TabLabelGridLayout536688d8"
					},
					"bindTo": "CrocFinalQualityUpload",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tab329e4d05TabLabelGridLayout536688d8",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocAmountTonSmartseeds9bf38322-a9cb-4fde-b4e0-3b9d317f5f64",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tab329e4d05TabLabelGridLayout536688d8"
					},
					"bindTo": "CrocAmountTonSmartseeds",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.CrocAmountTonSmartseeds9bf38322a9cb4fdeb4e03b9d317f5f64LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout536688d8",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGroupd5e4cbce",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab329e4d05TabLabelGroupd5e4cbceGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab329e4d05TabLabel",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGridLayoutc472c5bf",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab329e4d05TabLabelGroupd5e4cbce",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDateTransferOwnershipFrom0ca3bc40-39dd-43a2-a5c3-d9a6f57b8ac2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayoutc472c5bf"
					},
					"bindTo": "CrocDateTransferOwnershipFrom",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayoutc472c5bf",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDateTransferOwnershipToc7422d32-b232-4306-a871-65773b103b86",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayoutc472c5bf"
					},
					"bindTo": "CrocDateTransferOwnershipTo",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayoutc472c5bf",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRING0f66e00f-d242-4e17-ae8e-fe1ef6c9c688",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayoutc472c5bf"
					},
					"bindTo": "CrocWarehouseCensus",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tab329e4d05TabLabelGridLayoutc472c5bf",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocQuantityPlanToncc0675de-6543-4dc4-a3f7-0c4a2f35cdc4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayoutc472c5bf"
					},
					"bindTo": "CrocQuantityPlanTon",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayoutc472c5bf",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocAmountTonSmartseeds1b307df8-7ac6-4420-a240-6c028b58ba85",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab329e4d05TabLabelGridLayoutc472c5bf"
					},
					"bindTo": "CrocAmountTonSmartseeds",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.CrocAmountTonSmartseeds1b307df87ac64420a2406c028b58ba85LabelCaption"
						}
					},
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayoutc472c5bf",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGroupe83a0d78",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab329e4d05TabLabelGroupe83a0d78GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab329e4d05TabLabel",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGridLayout4f0c07dc",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab329e4d05TabLabelGroupe83a0d78",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FLOAT041dbc92-7e09-474d-aa9b-3b86769070fc",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayout4f0c07dc"
					},
					"bindTo": "CrocProteinPlan",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout4f0c07dc",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "FLOATa68c370c-9bdb-4e5a-a0d1-9a90ba33e0e4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayout4f0c07dc"
					},
					"bindTo": "CrocProteinFact",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout4f0c07dc",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "FLOAT684081d5-c4bd-48a4-8e8d-0b7c89ba2df1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayout4f0c07dc"
					},
					"bindTo": "CrocChPPlan",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout4f0c07dc",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "FLOAT27cca59d-c52c-4064-a4ff-47c1ae8e00ce",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tab329e4d05TabLabelGridLayout4f0c07dc"
					},
					"bindTo": "CrocChPFact",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout4f0c07dc",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "FLOAT6fe4cbe0-5bc5-437f-bd48-740cebadc78a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Tab329e4d05TabLabelGridLayout4f0c07dc"
					},
					"bindTo": "CrocNaturPlan",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout4f0c07dc",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "FLOATb842f7c1-3036-4340-9944-3c5b64d3948a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tab329e4d05TabLabelGridLayout4f0c07dc"
					},
					"bindTo": "CrocNaturFact",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout4f0c07dc",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "FLOAT82158205-59ed-475b-8a96-98801822db8d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tab329e4d05TabLabelGridLayout4f0c07dc"
					},
					"bindTo": "CrocWeedImpurityPlan",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout4f0c07dc",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "FLOAT7cc73ee4-92d6-4515-a7c7-992197a24b81",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Tab329e4d05TabLabelGridLayout4f0c07dc"
					},
					"bindTo": "CrocWeedImpurityFact",
					"enabled": false
				},
				"parentName": "Tab329e4d05TabLabelGridLayout4f0c07dc",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocSetDocument1Detail2b1f2b40",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab329e4d05TabLabel",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocAdjustmentOrder1Detaile471f790",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab329e4d05TabLabel",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGroup1261f48e",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab329e4d05TabLabelGroup1261f48eGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tab329e4d05TabLabel",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "Tab329e4d05TabLabelGridLayoute903bc43",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tab329e4d05TabLabelGroup1261f48e",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocAssignedManagerce4fb438-f53e-4095-a708-369ce4e4b173",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayoute903bc43"
					},
					"bindTo": "CrocAssignedManager",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayoute903bc43",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocAssignedSTO2b1cea6e-a0d1-4024-bf9a-a472b81ac935",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tab329e4d05TabLabelGridLayoute903bc43"
					},
					"bindTo": "CrocAssignedSTO",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tab329e4d05TabLabelGridLayoute903bc43",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Tab1a015db7TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab1a015db7TabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocChart1Detail8e2853af",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab1a015db7TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabb4514403TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabb4514403TabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocMutualSettlements1Detail8c5bd5a1",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabb4514403TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocPlannedRefund1Detail2f8737f5",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabb4514403TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocPurchaseReturns1Detail83ddd0d7",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabb4514403TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocContract1Detailf22ef53f",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabb4514403TabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "ActivityDetailV2720b166d",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabb4514403TabLabel",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			}
		]/**SCHEMA_DIFF*/
	};
});
