define("CrocNews06d835b0Section", ["ProcessModuleUtilities"], function(ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocNews",
		messages: {
			"PublishNewsButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"UnPublishNewsButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"RunPublishNewsProcess": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
		},
		attributes: {
			"PublishButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"UnPublishButtonVisible": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
		},
		methods: {

			init: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("PublishNewsButtonVisibleChanged",  function(isVisible){
					this.$PublishButtonVisible = isVisible;
				}, this, ["SectionModuleV2_InvoiceSectionV2"]);
				this.sandbox.subscribe("UnPublishNewsButtonVisibleChanged",  function(isVisible){
					this.$UnPublishButtonVisible = isVisible;
				}, this, ["SectionModuleV2_InvoiceSectionV2"]);
			},

			onPublishButtonClick: function() {
				this.runPublishProcess("CrocProcessPublishNews");
			},

			onUnPublishButtonClick: function() {
				this.runPublishProcess("CrocProcessUnPublishNews");
			},

			runPublishProcess: function(processName) {
				this.sandbox.publish("RunPublishNewsProcess", processName, ["SectionModuleV2_InvoiceSectionV2"]);
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "PublishButton",
				"index": 0,
				"values": {
					"tag": "onPublishButtonClick",
					"visible": {"bindTo": "PublishButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					"caption": { "bindTo": "Resources.Strings.PublishButtonCaption" },
					"click": { "bindTo": "onPublishButtonClick" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			},
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "UnPublishButton",
				"index": 1,
				"values": {
					"tag": "onUnPublishButtonClick",
					"visible": {"bindTo": "UnPublishButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.RED,
					"caption": { "bindTo": "Resources.Strings.UnPublishButtonCaption" },
					"click": { "bindTo": "onUnPublishButtonClick" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
