define("CrocRequest1Page", ["RightUtilities", "CrocConstantsJS","ProcessModuleUtilities"],
	function(RightUtilities, CrocConstantsJS,ProcessModuleUtilities) {
	return {
		entitySchemaName: "CrocRequest",
		attributes: {
			"AcceptRequestButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"CancelRequestButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"CrocApplicationStatus": {
				dependencies: [
					{
						columns: ["CrocApplicationStatus"],
						methodName: "onApplicationStatusChanged"
					}
				]
			},
		},
		messages: {
			"AcceptRequestButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"CancelRequestButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"UpdateCrocProgressReport1Detail": {
				"mode": Terrasoft.MessageMode.BROADCAST,
				"direction": Terrasoft.MessageDirectionType.SUBSCRIBE
			},
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocRequestFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocRequest"
				}
			},
			"VisaDetailV211b3b391": {
				"schemaName": "VisaDetailV2",
				"entitySchemaName": "CrocRequestVisa",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocRequest"
				}
			},
			"CrocLoadingSchedule1Detailf863cb6e": {
				"schemaName": "CrocLoadingSchedule1Detail",
				"entitySchemaName": "CrocLoadingSchedule",
				"filter": {
					"detailColumn": "CrocRequest",
					"masterColumn": "Id"
				}
			},
			"CrocMutualSettlements1Detail9954743c": {
				"schemaName": "CrocMutualSettlements1Detail",
				"entitySchemaName": "CrocMutualSettlements",
				"filter": {
					"detailColumn": "CrocRequest",
					"masterColumn": "Id"
				}
			},
			"CrocProgressReport1Detail235c1ff8": {
				"schemaName": "CrocProgressReport1Detail",
				"entitySchemaName": "CrocProgressReport",
				"filter": {
					"detailColumn": "CrocRequest",
					"masterColumn": "Id"
				}
			},
			"CrocListExecutor1Detailcbd32699": {
				"schemaName": "CrocListExecutor1Detail",
				"entitySchemaName": "CrocListExecutor",
				"filter": {
					"detailColumn": "CrocRequest",
					"masterColumn": "Id"
				}
			},
			"CrocVehicleRegister1Detaila45b73c7": {
				"schemaName": "CrocVehicleRegister1Detail",
				"entitySchemaName": "CrocVehicleRegister",
				"filter": {
					"detailColumn": "CrocRequest",
					"masterColumn": "Id"
				}
			},
			"CrocErrand1Detail4ed14b40": {
				"schemaName": "CrocErrand1Detail",
				"entitySchemaName": "CrocErrand",
				"filter": {
					"detailColumn": "CrocRequest",
					"masterColumn": "Id"
				}
			},
			"CrocApplicationHistory1Detailde5958e8": {
				"schemaName": "CrocApplicationHistory1Detail",
				"entitySchemaName": "CrocApplicationHistory",
				"filter": {
					"detailColumn": "CrocRequest",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocApplicationSubtype": {
				"d6baabf9-4df2-488f-a12d-cb65e178841b": {
					"uId": "d6baabf9-4df2-488f-a12d-cb65e178841b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocProgressReport1Detail235c1ff8": {
				"9a756694-17bf-4bac-9756-dbc90b9393fb": {
					"uId": "9a756694-17bf-4bac-9756-dbc90b9393fb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocSchema3dd8f4bfDetaila490c7d1": {
				"2f54c5bd-0fef-4fb9-85c9-aa695bd6e045": {
					"uId": "2f54c5bd-0fef-4fb9-85c9-aa695bd6e045",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocListExecutor1Detailcbd32699": {
				"8a612faf-e534-4016-88ca-2daa701a3659": {
					"uId": "8a612faf-e534-4016-88ca-2daa701a3659",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocErrand1Detail4ed14b40": {
				"c08f061b-a13f-47d2-b15e-45a7889b9aa1": {
					"uId": "c08f061b-a13f-47d2-b15e-45a7889b9aa1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e903341-13c6-47a9-b511-073be951bb54",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDistance": {
				"424fb32a-415b-465c-823b-f676b5511bfe": {
					"uId": "424fb32a-415b-465c-823b-f676b5511bfe",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocDeviation": {
				"b7719ede-d233-456c-89cc-9313386a9cbf": {
					"uId": "b7719ede-d233-456c-89cc-9313386a9cbf",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				},
				"859be8db-6925-4e74-94f4-48e1a0b22eee": {
					"uId": "859be8db-6925-4e74-94f4-48e1a0b22eee",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e903341-13c6-47a9-b511-073be951bb54",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocReasonRejection": {
				"f3805791-c98f-4d59-8fc0-80e0c83cfd85": {
					"uId": "f3805791-c98f-4d59-8fc0-80e0c83cfd85",
					"enabled": true,
					"removed": true,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				},
				"1cf73cf6-d2da-463a-a0de-adc1ca71361d": {
					"uId": "1cf73cf6-d2da-463a-a0de-adc1ca71361d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e903341-13c6-47a9-b511-073be951bb54",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocUnloadingAddress": {
				"eed24fbe-7ffd-4d68-9f5f-2783e83ec725": {
					"uId": "eed24fbe-7ffd-4d68-9f5f-2783e83ec725",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocVehicleRegister1Detaila45b73c7": {
				"63330390-ab26-49f5-a9ac-e9fe5c1c1bb6": {
					"uId": "63330390-ab26-49f5-a9ac-e9fe5c1c1bb6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocLoadingSchedule1Detailf863cb6e": {
				"a7c0a220-d9fb-4ede-ab22-615c4c95ab5f": {
					"uId": "a7c0a220-d9fb-4ede-ab22-615c4c95ab5f",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocVolumeTon": {
				"9ebfa546-abd7-46ec-94d2-49a7744f3b03": {
					"uId": "9ebfa546-abd7-46ec-94d2-49a7744f3b03",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocTotalVolumeUnloadingTon": {
				"0dd70360-135f-4c9a-9980-39da841a4c75": {
					"uId": "0dd70360-135f-4c9a-9980-39da841a4c75",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e903341-13c6-47a9-b511-073be951bb54",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocTotalVolumeLoadingTon": {
				"7cc7a2e7-9270-4e4f-993b-1a51157c0a2a": {
					"uId": "7cc7a2e7-9270-4e4f-993b-1a51157c0a2a",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocSelectedTonnage": {
				"f980426e-5759-4032-9b0c-3300fd9c5d74": {
					"uId": "f980426e-5759-4032-9b0c-3300fd9c5d74",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocShipper": {
				"a9b7a283-7bf9-498a-9426-fa60389736b3": {
					"uId": "a9b7a283-7bf9-498a-9426-fa60389736b3",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocConsignee": {
				"c9ce0d67-2dfd-4b0b-b4e2-91b9ebb114d1": {
					"uId": "c9ce0d67-2dfd-4b0b-b4e2-91b9ebb114d1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocPlaceSelection": {
				"5aae3070-ebe2-477c-a73c-e1a2288ec66b": {
					"uId": "5aae3070-ebe2-477c-a73c-e1a2288ec66b",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocLoadingAddress": {
				"ed71611d-6cd5-43bd-b38d-7a0419ac0508": {
					"uId": "ed71611d-6cd5-43bd-b38d-7a0419ac0508",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "f92a8cd7-3eb3-4c6d-bf33-a48a363301f4",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocTariff": {
				"b685fe42-acb8-42ff-b24e-580dcfc2b836": {
					"uId": "b685fe42-acb8-42ff-b24e-580dcfc2b836",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocApplicationType"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e903341-13c6-47a9-b511-073be951bb54",
								"dataValueType": 10
							}
						}
					]
				},
				"d6bacf1e-50eb-4073-85a3-8865d1fa140e": {
					"uId": "d6bacf1e-50eb-4073-85a3-8865d1fa140e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTariff"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocTariff"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			init: function(callback, scope) {
				this.callParent(arguments);
				this.sandbox.subscribe("UpdateCrocProgressReport1Detail",this.onUpdateCrocProgressReport1Detail, this);
			},

			onUpdateCrocProgressReport1Detail: function(args) {
				if (args && args.reportId) {
					this.updateDetail({detail: "CrocProgressReport1Detail235c1ff8", reloadAll: true});
				}
			},

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.canAcceptRequest();
				this.canCancelRequest();
			},

			onApplicationStatusChanged: function() {
				this.setAcceptRequestButtonVisible();
				this.setCancelRequestButtonVisible();
			},

			canAcceptRequest: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanAcceptRequest"},
					function(result) {
						this.$CrocIsAcceptRights = result;
						this.setAcceptRequestButtonVisible();
					}, this);
			},

			canCancelRequest: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanCancelRequest"},
					function(result) {
						this.$CrocIsCancelRights = result;
						this.setCancelRequestButtonVisible();
					}, this);
			},

			setAcceptRequestButtonVisible: function () {
				if (this.$CrocIsAcceptRights && !this.isNewMode()) {
					this.$AcceptRequestButtonVisible = (this.$CrocApplicationStatus?.value === CrocConstantsJS.CrocApplicationStatus.RequestSended);
				} else {
					this.$AcceptRequestButtonVisible = false;
				}
				this.sandbox.publish("AcceptRequestButtonVisibleChanged", this.$AcceptRequestButtonVisible, ["SectionModuleV2_InvoiceSectionV2"]);
			},

			setCancelRequestButtonVisible: function () {
				if (this.$CrocIsCancelRights && !this.isNewMode()) {
					this.$CancelRequestButtonVisible = (this.$CrocApplicationStatus?.value === CrocConstantsJS.CrocApplicationStatus.RequestSended);
				} else {
					this.$CancelRequestButtonVisible = false;
				}
				this.sandbox.publish("CancelRequestButtonVisibleChanged", this.$CancelRequestButtonVisible, ["SectionModuleV2_InvoiceSectionV2"]);
			},

			onAcceptRequestButtonClick: function() {
				this.loadLookupDisplayValue("CrocApplicationStatus", CrocConstantsJS.CrocApplicationStatus.RequestAccepted);
				this.save({isSilent:true});
			},

			setHistoryReasonValue: function (reason) {
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "CrocApplicationHistory"
				});
				var createdOnColumn = esq.addColumn("CreatedOn");
				createdOnColumn.orderDirection = Terrasoft.OrderDirection.DESC;
				createdOnColumn.orderPosition = 0;
				esq.filters.addItem(esq.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "CrocRequest", this.$Id));
				esq.rowCount = 1;
				esq.getEntityCollection(function(response) {
					if (response && response.success) {
						var collection = response.collection;
						if (collection.getCount() > 0) {
							var firstItem = collection.getByIndex(0).values.Id;
							var uq = Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "CrocApplicationHistory"});
							uq.filters.addItem(uq.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "Id", firstItem));
							uq.setParameterValue("CrocCancelReason", reason, Terrasoft.DataValueType.TEXT);
							uq.execute(function () {
								this.reloadEntity();
							}, this);
						}
					}
				}, this);

			},

			onCancelRequestButtonClick: function() {
				this.loadLookupDisplayValue("CrocApplicationStatus", CrocConstantsJS.CrocApplicationStatus.RequestCanceled);
				this.save({isSilent:true});
				Terrasoft.utils.inputBox("Заполните причину отказа", function (result, args){
					if (result === Terrasoft.MessageBoxButtons.OK.returnCode) {
						var reason = args.reason.value;
						if (reason.length > 0 ) {
							this.setHistoryReasonValue(reason);
						}
					}
				}, [{
					className: "Terrasoft.Button",
					caption: "OK",
					returnCode: "ok"
				}, "Cancel"],
					this, {
						reason: {
							dataValueType: Terrasoft.DataValueType.TEXT,
							caption: "Причина",
							customConfig: {
								className: "Terrasoft.MemoEdit",
								height: "80px"
							},
							value: ""
						},
					},{
					defaultButton: 0,
						style: {
						borderStyle: "ts-messagebox-border-style-blue visa-action",
							buttonStyle: "blue"
					}
				});
			},
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "AcceptRequestButton",
				"values": {
					"visible": {
						"bindTo": "AcceptRequestButtonVisible"
					},
					"itemType": 5,
					"style": "green",
					"caption": {
						"bindTo": "Resources.Strings.AcceptRequestButtonCaption"
					},
					"click": {
						"bindTo": "onAcceptRequestButtonClick"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CancelRequestButton",
				"values": {
					"visible": {
						"bindTo": "CancelRequestButtonVisible"
					},
					"itemType": 5,
					"style": "red",
					"caption": {
						"bindTo": "Resources.Strings.CancelRequestButtonCaption"
					},
					"click": {
						"bindTo": "onCancelRequestButtonClick"
					},
					"classes": {
						"textClass": [
							"actions-button-margin-right"
						],
						"wrapperClass": [
							"actions-button-margin-right"
						]
					}
				},
				"parentName": "LeftContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocNumberdcd46ecd-0711-4650-9a7b-33db89a8639e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocNumber",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocApplicationType65bbeb16-ec97-46cb-973a-b1975f6cd21e",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocApplicationType",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocApplicationSubtype18f59e6b-b7ba-4731-856a-bc9e91c1c0c2",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocApplicationSubtype",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocProviderb872b3f7-b366-4965-bcc6-1d445ecadb08",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocProvider",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocLogist5aaa7fa1-702b-478d-9a82-a0322a046dde",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocLogist",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocShipper3c3ccb6d-690a-47a6-9e09-12bc9824a973",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocShipper",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocConsigneea96e8090-fb1b-4f6b-b11c-815160727ade",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocConsignee",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "FLOAT0b174b34-b6df-44f5-ae12-bdd82ca4d2cb",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocTariff",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "Tabc01904baTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabc01904baTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabc01904baTabLabelGroupab4d760f",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabc01904baTabLabelGroupab4d760fGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "Tabc01904baTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tabc01904baTabLabelGridLayoutca5f7873",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "Tabc01904baTabLabelGroupab4d760f",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocProductff860fd8-cf1e-41c4-a827-13fc002a5914",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocProduct",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocContactPersond05104e2-67a9-4cb2-8274-5093f1fbcd2a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocContactPerson",
					"enabled": false
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocDistance983b57a6-3f64-4105-91e9-a8c5e3a6a0a8",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocDistance",
					"enabled": false
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocVolumeTon2f69dc4a-93ba-4b56-a663-4938436cc73d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocVolumeTon",
					"enabled": false
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "FLOAT18d16607-f5e6-4fa4-affb-d469cf258413",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocSelectedTonnage",
					"enabled": false
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "STRING19a4af6b-5023-4350-97c7-34c198a5dccc",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocPlaceSelection",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocLoadingAddress89d6c7ba-a508-4ae0-9ba0-6091342e59a6",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocLoadingAddress",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocUnloadingAddress87523f10-4e27-4edc-aae9-56879e2218a3",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocUnloadingAddress",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocTotalVolumeLoadingTon04671108-4311-4290-bc07-a61526062ca7",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocTotalVolumeLoadingTon",
					"enabled": false
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "CrocDeviation9c4d58cc-8b39-4abd-8b86-f94303180414",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocDeviation",
					"enabled": false
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "CrocTotalVolumeUnloadingTon1a68962d-5bc4-4c38-9e07-fc53f0da72be",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocTotalVolumeUnloadingTon",
					"enabled": false
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "CrocReasonRejection7809acab-9651-4c0a-8ba9-30771dbbce39",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 7,
						"layoutName": "Tabc01904baTabLabelGridLayoutca5f7873"
					},
					"bindTo": "CrocReasonRejection",
					"enabled": false,
					"contentType": 0
				},
				"parentName": "Tabc01904baTabLabelGridLayoutca5f7873",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "CrocListExecutor1Detailcbd32699",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabc01904baTabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocProgressReport1Detail235c1ff8",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabc01904baTabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocErrand1Detail4ed14b40",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tabc01904baTabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "Tab1979bbe3TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab1979bbe3TabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocVehicleRegister1Detaila45b73c7",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab1979bbe3TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocLoadingSchedule1Detailf863cb6e",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab1979bbe3TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocMutualSettlements1Detail9954743c",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab1979bbe3TabLabel",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocApplicationHistory1Detailde5958e8",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab1979bbe3TabLabel",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab08111951TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabVisaCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "VisaDetailV211b3b391",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab08111951TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "remove",
				"name": "ESNTab"
			},
			{
				"operation": "remove",
				"name": "ESNFeedContainer"
			},
			{
				"operation": "remove",
				"name": "ESNFeed"
			}
		]/**SCHEMA_DIFF*/
	};
});
