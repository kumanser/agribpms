 define("SectionActionsDashboard", [],
	function() {
		return {
			methods: {
				getExtendedConfig: function() {
					var config = this.callParent(arguments);
					if(config) {
						if(config.CallMessageTab) { config.CallMessageTab.Visible = false; }
						if(config.FacebookMessageTab) { config.FacebookMessageTab.Visible = false; }
						if(config.TelegramMessageTab) { config.TelegramMessageTab.Visible = false; }
						if(config.WhatsAppMessageTab) { config.WhatsAppMessageTab.Visible = false; }
					};
					return config;
				},
			},
			diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
		};
	}
);
 