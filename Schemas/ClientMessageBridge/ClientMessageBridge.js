define("ClientMessageBridge", ["ConfigurationConstants"],
function(ConfigurationConstants) {
    return {
        messages: {
            "ReloadCrocNewsPage": {
                "mode": Terrasoft.MessageMode.BROADCAST,
                "direction": Terrasoft.MessageDirectionType.PUBLISH
            },
			"ReloadContactIndistributionDetail": {
                "mode": Terrasoft.MessageMode.BROADCAST,
                "direction": Terrasoft.MessageDirectionType.PUBLISH
            },
			"UpdateCrocDocumentKitDetail": {
                "mode": Terrasoft.MessageMode.BROADCAST,
                "direction": Terrasoft.MessageDirectionType.PUBLISH
            },
			"ShowDocumentKitNotFoundNotification": {
				"mode": Terrasoft.MessageMode.BROADCAST,
                "direction": Terrasoft.MessageDirectionType.PUBLISH
			},
			"RelevanceDateEmptyNotification": {
				"mode": Terrasoft.MessageMode.BROADCAST,
                "direction": Terrasoft.MessageDirectionType.PUBLISH
			},
			"UpdateCrocProgressReport1Detail": {
				"mode": Terrasoft.MessageMode.BROADCAST,
                "direction": Terrasoft.MessageDirectionType.PUBLISH
			},
			"UpdateDistribution1Page": {
				"mode": Terrasoft.MessageMode.BROADCAST,
                "direction": Terrasoft.MessageDirectionType.PUBLISH
			},
			"UpdateCrocRegisterLoading": {
				"mode": Terrasoft.MessageMode.BROADCAST,
                "direction": Terrasoft.MessageDirectionType.PUBLISH
			}
        },
        methods: {
            init: function() {
                this.callParent(arguments);
                this.addMessageConfig({
                    sender: "ReloadCrocNewsPage",
                    messageName: "ReloadCrocNewsPage"
                });
				this.addMessageConfig({
                    sender: "ReloadContactIndistributionDetail",
                    messageName: "ReloadContactIndistributionDetail"
                });
				this.addMessageConfig({
                    sender: "UpdateCrocDocumentKitDetail",
                    messageName: "UpdateCrocDocumentKitDetail"
                });
				this.addMessageConfig({
                    sender: "ShowDocumentKitNotFoundNotification",
                    messageName: "ShowDocumentKitNotFoundNotification"
                });
				this.addMessageConfig({
                    sender: "RelevanceDateEmptyNotification",
                    messageName: "RelevanceDateEmptyNotification"
                });
				this.addMessageConfig({
                    sender: "UpdateCrocProgressReport1Detail",
                    messageName: "UpdateCrocProgressReport1Detail"
                });
				this.addMessageConfig({
                    sender: "UpdateDistribution1Page",
                    messageName: "UpdateDistribution1Page"
                });
				this.addMessageConfig({
                    sender: "UpdateCrocRegisterLoading",
                    messageName: "UpdateCrocRegisterLoading"
                });
            },
            afterPublishMessage: function(sandboxMessageName, webSocketBody, result, publishConfig) {
                if (sandboxMessageName === "ReloadCrocNewsPage") {
                    var id = webSocketBody.id;
                }
				if (sandboxMessageName === "ReloadContactIndistributionDetail" || sandboxMessageName === "UpdateDistribution1Page") {
                    var distributionId = webSocketBody.distributionId;
                }
				if (sandboxMessageName === "UpdateCrocDocumentKitDetail") {
                    var documentId = webSocketBody.documentId;
                }
				if (sandboxMessageName === "ShowDocumentKitNotFoundNotification") {
                    var accreditationType = webSocketBody.accreditationType;
                }
				if (sandboxMessageName === "RelevanceDateEmptyNotification") {
                    var addDocumentId = webSocketBody.addDocumentId;
					var documentNumber = webSocketBody.documentNumber;
                }
				if (sandboxMessageName === "UpdateCrocProgressReport1Detail") {
                    var reportId = webSocketBody.reportId;
                }
				if (sandboxMessageName === "UpdateCrocRegisterLoading") {
                    var masterId = webSocketBody.masterId;
                }
            }
        }
    };
});
