using CrocIntegrations.Api;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Terrasoft.Web.Common;
using Terrasoft.Core.Factories;
using System.Reflection;

namespace Terrasoft.Configuration
{
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public partial class IntegrationDWHService : BaseService
	{
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendContragent(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendEmployee(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendAccreditation(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendContract(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendMutualSettlements(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendQuoteRequest(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendPlannedRefund(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendPortQuota(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendPositionPlan(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendLoan(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendRejectedTransport(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendRegisterLoading(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendChart(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendRegisterUnloading(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendPurchaseReturns(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendRequest(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendErrand(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendLookup(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendRegistryVehicle(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
		public void SendAdditionalAgreement(Stream data)
		{
            var helper = ClassFactory.Get<IIntegrationHelper>(
				new ConstructorArgument("userConnection", UserConnection),
				new ConstructorArgument("serviceName", MethodBase.GetCurrentMethod().DeclaringType.Name),
				new ConstructorArgument("methodName", MethodBase.GetCurrentMethod().Name)
			);
			helper.Execute(data);
		}
		
	}

}