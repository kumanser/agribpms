 define("CrocImageListViewModel", ["ServiceHelper", "ImageListViewModel"],
	function(ServiceHelper) {

		Ext.define("Terrasoft.controls.CrocImageListViewModel", {
			extend: "Terrasoft.ImageListViewModel",
			alternateClassName: "Terrasoft.CrocImageListViewModel",

			getS3FileLink: function(entitySchema, fileId) {
				var urlPattern = "{0}/rest/FilesS3Service/GetS3File/{1}/{2}/{3}/{4}";
				var workspaceBaseUrl = Terrasoft.utils.uri.getConfigurationWebServiceBaseUrl();
				var url = Ext.String.format(urlPattern, workspaceBaseUrl, entitySchema, fileId, "Data", "CrocS3Link");
				return url;
			},

			getEntityImage: function() {
				var result = this.callParent(arguments);
				if (this.$CrocS3Link) {
					var imageUrl = this.get("imageUrl");
					result = imageUrl.includes("FileExtension/Data") ? imageUrl : this.getS3FileLink(this.entitySchema.name, this.$Id);
				}
				return result;
			},

			insertEntityLink: function() {
				this.callParent(arguments);
				if (this.$CrocS3Link) {
					var entityText = this.getEntityText();
					var href = "";
					var target = "";
					if (this.isFile) {
						var urlPattern = "{0}/rest/FilesS3Service/GetS3File/{1}/{2}/{3}/{4}";
						var workspaceBaseUrl = Terrasoft.utils.uri.getConfigurationWebServiceBaseUrl();
						href = Ext.String.format(urlPattern, workspaceBaseUrl, this.entitySchema.name, this.entityId, "Data", "CrocS3Link");
						target = "_self";
					}
					var linkHtmlConfig = this.Ext.DomHelper.createHtml({
						tag: "a",
						target: target,
						html: Terrasoft.utils.string.encodeHtml(entityText),
						href: href
					});
					var container = this.getLinkContainer();
					if (this.isEntityLink) {
						container.wrapEl.el.on("click", this.onClickEntityLink, this);
					}
					container.wrapEl.update(linkHtmlConfig);
				}
			},

		})
	});
