define("CrocHelp1Page", [], function() {
	return {
		entitySchemaName: "CrocHelp",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocHelpFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocHelp"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocAppealTopic": {
				"858650d6-1a9c-4c0b-8cae-a5a54b1cbcb8": {
					"uId": "858650d6-1a9c-4c0b-8cae-a5a54b1cbcb8",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocAppealStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "16d688eb-3d1e-456a-88b7-b2ced1e426b9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocAppealContent": {
				"c0d8c807-df59-42b9-bcc3-2535e6261bb5": {
					"uId": "c0d8c807-df59-42b9-bcc3-2535e6261bb5",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocAppealStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "16d688eb-3d1e-456a-88b7-b2ced1e426b9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocAppealSolution": {
				"77a58984-8143-4c23-8d01-af2e1afc50b1": {
					"uId": "77a58984-8143-4c23-8d01-af2e1afc50b1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocAppealStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "d0f4e724-b15c-4f2d-952b-c3e8815e5ce8",
								"dataValueType": 10
							}
						}
					]
				},
				"942e0a3f-e47a-48f4-99f0-cb3c40a19931": {
					"uId": "942e0a3f-e47a-48f4-99f0-cb3c40a19931",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocAppealStatus"
							},
							"rightExpression": {
								"type": 0,
								"value": "16d688eb-3d1e-456a-88b7-b2ced1e426b9",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocAppealStatus": {
				"623ef5a0-528f-405c-833f-1df363138ac6": {
					"uId": "623ef5a0-528f-405c-833f-1df363138ac6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "CreatedBy"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocName7d48c803-b529-4f6f-ad50-c292be3d000c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocAppealNumber",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRINGd2d5c5eb-5aaa-4e7e-8f9a-621f450b65c0",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocAppealTopic",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CreatedBy2dd8a295-6066-4e82-9388-c87eaf183275",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CreatedBy",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CreatedOn3e78bc9b-cc91-42d4-9647-9b1be8c7c45b",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CreatedOn",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUPe3a6caaf-a5d2-482e-abd9-f1f845fda2fc",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocAppealStatus",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "BaseInfoTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.BaseInfoTabLabelTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BaseInfoTabLabelGroupccca4698",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.BaseInfoTabLabelGroupccca4698GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "BaseInfoTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BaseInfoTabLabelGridLayout15f3cc28",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "BaseInfoTabLabelGroupccca4698",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocAppealContentefcefdd3-8de8-405a-ab7a-36805167d2c8",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 5,
						"column": 0,
						"row": 0,
						"layoutName": "BaseInfoTabLabelGridLayout15f3cc28"
					},
					"bindTo": "CrocAppealContent",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "BaseInfoTabLabelGridLayout15f3cc28",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocAppealSolution3020737e-46e2-4286-bb42-08b0878d47e9",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 5,
						"column": 0,
						"row": 5,
						"layoutName": "BaseInfoTabLabelGridLayout15f3cc28"
					},
					"bindTo": "CrocAppealSolution",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "BaseInfoTabLabelGridLayout15f3cc28",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "CrocNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "remove",
				"name": "ESNTab"
			},
			{
				"operation": "remove",
				"name": "ESNFeedContainer"
			},
			{
				"operation": "remove",
				"name": "ESNFeed"
			}
		]/**SCHEMA_DIFF*/
	};
});
