define("CrocDeliverySchedule1Page", ["CrocConstantsJS"], function(CrocConstantsJS) {
	return {
		entitySchemaName: "CrocDeliverySchedule",
		attributes: {
			"CrocChartExecutionStatus": {
				dependencies: [
					{
						columns:["CrocAllocatedQuotaTon", "CrocDeliveryTonFact"],
						methodName: "changeCrocChartExecutionStatus"
					}
				]
			}
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			
			positiveTonsValidator: function() {
				var invalidMessage = "";
				if (this.get("CrocAllocatedQuotaTon") < 0 || this.get("CrocDeliveryTonFact") < 0) {
					invalidMessage = this.get("Resources.Strings.LessThanZero");
				}
				return {
						invalidMessage:invalidMessage,
						fullInvalidMessage:invalidMessage
				};
			},
			
			setValidationConfig: function() {
				this.callParent(arguments);
				this.addColumnValidator("CrocAllocatedQuotaTon", this.positiveTonsValidator);
				this.addColumnValidator("CrocDeliveryTonFact", this.positiveTonsValidator);
			},
			
			changeCrocChartExecutionStatus: function() {
				if (this.get("CrocAllocatedQuotaTon") > 0 && this.get("CrocDeliveryTonFact") === 0) {
					this.set("CrocChartExecutionStatus", CrocConstantsJS.CrocChartExecutionStatus.CompletionWaiting);
				}
				if (this.get("CrocDeliveryTonFact") > 0) {
					this.set("CrocChartExecutionStatus", CrocConstantsJS.CrocChartExecutionStatus.Closed);
				}
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocDeliveryDate6158f5d3-035d-4bc8-a544-30b0b840d816",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocDeliveryDate",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocChartExecutionStatus5570501d-1b28-42eb-bd7b-3be66046c9f9",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocChartExecutionStatus",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryTonPlan698760ae-7725-48d1-86dc-cb60f5938733",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocDeliveryTonPlan"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocIsQuoteRequest02c8ef7b-af1d-4866-91e3-6b1fd4098098",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocIsQuoteRequest",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryTransportPlanb3767ae9-2722-4ccb-85e5-6b4facb1fa21",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocDeliveryTransportPlan"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocIsTermShift19f03b3d-2014-4f36-8173-48eccffc6bd4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocIsTermShift",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocAllocatedQuotaTon2976aad8-cfa8-41a8-a6b7-71f234d83eb6",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocAllocatedQuotaTon",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryTonFact416e8248-6e7d-464e-bb40-c987acf797ac",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocDeliveryTonFact",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocAllocatedQuotaTransporta9e15271-9937-4f27-8cc0-e68077ec0a3a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocAllocatedQuotaTransport",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "CrocDeliveryTransportFact552b2ef1-0598-4c3a-8d79-51897c3499ee",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocDeliveryTransportFact",
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			}
		]/**SCHEMA_DIFF*/
	};
});
