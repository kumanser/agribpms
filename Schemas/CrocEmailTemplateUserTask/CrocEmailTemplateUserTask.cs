namespace Terrasoft.Core.Process.Configuration
{
	using Newtonsoft.Json;
	using Newtonsoft.Json.Linq;
	using ProcessDesigner = Terrasoft.Configuration.ProcessDesigner;
	using System;
  	using System.Collections;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Data;
	using System.Globalization;
	using System.Linq;
	using System.Runtime.Serialization.Formatters;
	using System.Text;
  	using System.Text.RegularExpressions;
	using Terrasoft.Common;
	using Terrasoft.Common.Json;
	using Terrasoft.Configuration;
	using Terrasoft.Core;
	using Terrasoft.Core.Configuration;
	using Terrasoft.Core.DB;
	using Terrasoft.Core.Entities;
	using Terrasoft.Core.Factories;
	using Terrasoft.Core.Process;
	using Terrasoft.Mail.Sender;
	using Terrasoft.UI.WebControls;
	using Terrasoft.UI.WebControls.Controls;
	using Terrasoft.UI.WebControls.Utilities;
	using Terrasoft.UI.WebControls.Utilities.Json.Converters;

	#region Class: CrocEmailTemplateUserTask
	/// <exclude/>
	public partial class CrocEmailTemplateUserTask
	{
      
      #region Methods: Protected

		protected override bool InternalExecute(ProcessExecutingContext context) {
        	try
            {
              	if(AutoSend && ActivityId != Guid.Empty)
                {
                  	SendActivity(ActivityId);
                }
              	else
                {
                  	if(MailboxSysSettingId == Guid.Empty) { throw new Exception(SenderError); }
                  	if(EmailTemplateId == Guid.Empty) { throw new Exception(TemplateError); }
                  	var helper = Terrasoft.Core.Factories.ClassFactory.Get<Terrasoft.Configuration.ICrocUserTaskHelper>(
						new Terrasoft.Core.Factories.ConstructorArgument("userConnection", UserConnection)
					);
					ActivityId = helper.CreateEmailActivityOnTemplate(EmailTemplateId, MailboxSysSettingId, RecordId, SubjectParam, To);
              		if(AutoSend)
                	{
                  		SendActivity(ActivityId);
                	}
                }	
              	HasError = false;
            }
        	catch(Exception ex)
            {
              	HasError = true;
              	ErrorMessage = ex.Message;
            }
			
        	return true;
		}

		#endregion

		#region Methods: Public

		public override bool CompleteExecuting(params object[] parameters) {
			return base.CompleteExecuting(parameters);
		}

		public override void CancelExecuting(params object[] parameters) {
			base.CancelExecuting(parameters);
		}

		public override string GetExecutionData() {
			return string.Empty;
		}

		public override ProcessElementNotification GetNotificationData() {
			return base.GetNotificationData();
		}
      
      	public virtual void SendActivity(Guid activityId)
        {
          	//if (UserConnection.GetIsFeatureEnabled("UseAsyncEmailSender")) {
			//	AsyncEmailSender emailSender = new AsyncEmailSender(UserConnection);
			//	emailSender.SendAsync(activityId);
			//} else {
			var emailClientFactory = ClassFactory.Get<EmailClientFactory>(new ConstructorArgument("userConnection", UserConnection));
			var activityEmailSender = new ActivityEmailSender(emailClientFactory, UserConnection);
			if(CrocSendFromUser) { activityEmailSender.Send(activityId); }
			else {
				var entitySchema = UserConnection.EntitySchemaManager.GetInstanceByName("Activity");
				var entity = entitySchema.CreateEntity(UserConnection);
				if (entity.FetchFromDB(activityId))
				{
					activityEmailSender.Send(entity, null, true);
				}
			}
			//}
        }

		#endregion
	}
	
	#endregion
}

