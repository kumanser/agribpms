define("CrocNotificationEmployee1Page", [], function() {
	return {
		entitySchemaName: "CrocNotificationEmployee",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocContact": {
				"cd32ff72-308f-4ccb-8480-7b7b0f20a900": {
					"uId": "cd32ff72-308f-4ccb-8480-7b7b0f20a900",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "Account",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "CrocAccount"
				}
			},
			"CrocEvent": {
				"9ae5de68-b7fa-4587-8592-12eabc54ba93": {
					"uId": "9ae5de68-b7fa-4587-8592-12eabc54ba93",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "CrocAccount",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "CrocAccount"
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "LOOKUPff7b49e9-f6f3-41e9-b6d6-7f4a74f4a166",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPba602887-4d88-4abe-aab5-52a2ab72509d",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocEvent",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocAccount8e2e9fc5-0546-44f1-b036-f7c09b520c8d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocAccount",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "BOOLEANbedd6ab5-3bc5-45a6-9193-ce6b2a3f7813",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocIsEmail",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "BOOLEANc98f7557-7b4c-46d8-8f4a-c6b6a4b2cb66",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocIsTelegram",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "BOOLEAN908788fb-5b29-44b5-b660-979d7a8f7e9a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "Header"
					},
					"bindTo": "CrocIsPortal",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			}
		]/**SCHEMA_DIFF*/
	};
});
