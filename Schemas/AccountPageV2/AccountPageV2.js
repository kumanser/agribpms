define("AccountPageV2", ["RightUtilities", "ProcessModuleUtilities", "CrocConstantsJS"], function(RightUtilities, ProcessModuleUtilities, CrocConstantsJS) {
	return {
		entitySchemaName: "Account",
		attributes: {
			"isContactsEOVisible": { //CrocEOPortContact1Detail1f857acf
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				value: false
			},
			"AccreditationRequestActionVisible": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				value: false
			},
			"AddNewUserRequestActionVisible": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				value: false
			}
		},
		messages: {
			"AccountTypificationDataUpdated": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"ShowDocumentKitNotFoundNotification": {
				mode: this.Terrasoft.MessageMode.BROADCAST,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"AccreditationRequestVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			},
			"AddNewUserRequestVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"CrocAccountStock1Detail": {
				"schemaName": "CrocAccountStock1Detail",
				"entitySchemaName": "CrocAccountStock",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocAccountTypification1Detail": {
				"schemaName": "CrocAccountTypification1Detail",
				"entitySchemaName": "CrocAccountTypification",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocDocument1DetailAccreditation": {
				"schemaName": "CrocDocument1Detail",
				"entitySchemaName": "CrocDocument",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				},
				"filterMethod": "crocDocumentFilter"
			},
			"CrocExporterEmployee1Detail": {
				"schemaName": "CrocExporterEmployee1Detail",
				"entitySchemaName": "CrocExporterEmployee",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocNotificationSetting1Detail": {
				"schemaName": "CrocNotificationSetting1Detail",
				"entitySchemaName": "CrocNotificationSetting",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocNotificationEmployee1Detail": {
				"schemaName": "CrocNotificationEmployee1Detail",
				"entitySchemaName": "CrocNotificationEmployee",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocEOPortContact1Detail1f857acf": {
				"schemaName": "CrocEOPortContact1Detail",
				"entitySchemaName": "CrocEOPortContact",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocRule1Detail": {
				"schemaName": "CrocRule1Detail",
				"entitySchemaName": "CrocRule",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocContactInformation1Detail": {
				"schemaName": "CrocContactInformation1Detail",
				"entitySchemaName": "CrocContactInformation",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocDocument2Detail": {
				"schemaName": "CrocDocument2Detail",
				"entitySchemaName": "CrocDocument",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocRequest1Detail": {
				"schemaName": "CrocRequest1Detail",
				"entitySchemaName": "CrocRequest",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocErrand1Detail": {
				"schemaName": "CrocErrand1Detail",
				"entitySchemaName": "CrocErrand",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocDocument2Detailabb20942": {
				"schemaName": "CrocDocument2Detail",
				"entitySchemaName": "CrocDocument",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			},
			"CrocContract2Detail": {
				"schemaName": "CrocContract2Detail",
				"entitySchemaName": "CrocContract",
				"filter": {
					"detailColumn": "CrocAccount",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocReplaceManager": {
				"2fe3447b-2f6f-465d-9631-3191239964d1": {
					"uId": "2fe3447b-2f6f-465d-9631-3191239964d1",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocReplaceEndDate"
							}
						}
					]
				}
			},
			"CrocReplaceEndDate": {
				"099fac17-85ca-4d9c-878e-86748410b4f2": {
					"uId": "099fac17-85ca-4d9c-878e-86748410b4f2",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocReplaceManager"
							}
						}
					]
				}
			},
			"CrocExporterEmployee1Detail": {
				"0da5b06e-96a0-4bd8-8bc0-b1d58b17f343": {
					"uId": "0da5b06e-96a0-4bd8-8bc0-b1d58b17f343",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "57412fad-53e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNotificationSetting1Detail": {
				"69883f87-9949-4675-934f-4019ea5ea6c6": {
					"uId": "69883f87-9949-4675-934f-4019ea5ea6c6",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "57412fad-53e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocNotificationEmployee1Detail": {
				"5c07d467-5886-4c81-907c-a331ccacd666": {
					"uId": "5c07d467-5886-4c81-907c-a331ccacd666",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "57412fad-53e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocEOPortContact1Detail1f857acf": {
				"1668f4ea-283d-4090-af25-f59896e00b86": {
					"uId": "1668f4ea-283d-4090-af25-f59896e00b86",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "isContactsEOVisible"
							},
							"rightExpression": {
								"type": 0,
								"value": true,
								"dataValueType": 12
							}
						}
					]
				}
			},
			"CrocReplaceStartDate": {
				"56274132-5613-4c92-90af-03b9ccbfeabb": {
					"uId": "56274132-5613-4c92-90af-03b9ccbfeabb",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocReplaceStartDate"
							}
						}
					]
				}
			},
			"Type": {
				"4a693d68-a0ca-4af1-a55d-b3f94d9c72cd": {
					"uId": "4a693d68-a0ca-4af1-a55d-b3f94d9c72cd",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "57412fad-53e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"CrocAccountTypification1Detail": {
				"ee7a59de-d453-42dc-8e63-5c615d1a3c28": {
					"uId": "ee7a59de-d453-42dc-8e63-5c615d1a3c28",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 4,
							"leftExpression": {
								"type": 1,
								"attribute": "Type"
							},
							"rightExpression": {
								"type": 0,
								"value": "57412fad-53e6-df11-971b-001d60e938c6",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("AccountTypificationDataUpdated", this.onAccountTypificationDataUpdated, this, []);
				this.sandbox.subscribe("ShowDocumentKitNotFoundNotification", this.ShowDocumentKitNotFoundNotification, this);
			},

			ShowDocumentKitNotFoundNotification: function(argument) {
				message = this.Ext.String.format(this.get("Resources.Strings.DocumentKitNotFoundNotification"), argument.accreditationType);
				this.showInformationDialog(message);
			},
			
			onAccountTypificationDataUpdated: function(argument) {
				if(this.get("Id") === argument) {
					this.fillVisibleElements();
				}
			},

			crocDocumentFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("AccounFilter",
					this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
						"CrocAccount", this.$Id, Terrasoft.DataValueType.GUID));
				filterGroup.add("DocumentType", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "CrocDocumentType",
					CrocConstantsJS.CrocDocumentType.Accreditation.value,
					Terrasoft.DataValueType.GUID));
				var selectedStatesIds = [];
				selectedStatesIds.push(this.setGuidToParameter(CrocConstantsJS.CrocDocumentStatus.Actual.value));
				selectedStatesIds.push(this.setGuidToParameter(CrocConstantsJS.CrocDocumentStatus.Prepared.value));
				var documentInStatusFilter = this.Terrasoft.createInFilter(
					Ext.create("Terrasoft.ColumnExpression", { columnPath: "CrocDocumentStatus" }), selectedStatesIds);
				documentInStatusFilter.comparisonType = this.Terrasoft.ComparisonType.EQUAL;
				filterGroup.add("DocumentInStatus", documentInStatusFilter);
				return filterGroup;
			},

			setGuidToParameter: function(id) {
				return Ext.create("Terrasoft.ParameterExpression", {
					parameterDataType: Terrasoft.DataValueType.GUID,
					parameterValue: id
				});
			},
			
			isAccreditationRequestActionVisible: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanAccreditationRequest"},
													   function(result){
					this.$AccreditationRequestActionVisible = result && !this.isNewMode();
					this.sandbox.publish("AccreditationRequestVisibleChanged", this.$AccreditationRequestActionVisible, [this.sandbox.id]);
				},this);
			},
			isAddNewUserRequestActionVisible: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanAddNewUserRequest"},
													   function(result){
					this.$AddNewUserRequestActionVisible = result && !this.isNewMode();
					this.sandbox.publish("AddNewUserRequestVisibleChanged", this.$AddNewUserRequestActionVisible, [this.sandbox.id]);
				},this);
			},
			
			
			getActions: function() {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					Type: "Terrasoft.MenuSeparator",
                    Caption: ""	
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": { bindTo: "Resources.Strings.AccreditationRequestActionCaption" },
					"Tag": "callProcessAccreditationRequest",
					"Visible": {bindTo: "AccreditationRequestActionVisible"}
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": { bindTo: "Resources.Strings.AddNewUserRequestActionCaption" },
					"Tag": "callProcessAddNewUserRequest",
					"Visible": {bindTo: "AddNewUserRequestActionVisible"}
				}));
				return actionMenuItems;
			},
			callProcessAccreditationRequest: function() {
				var accountId = this.get("Id");
				
				var args = {
					sysProcessName: "CrocProcessAccreditationRequest",
					parameters: {
						AccountId: accountId
					}
				};
				ProcessModuleUtilities.executeProcess(args);
			},
			callProcessAddNewUserRequest: function () {
				var accountId = this.get("Id");
				
				var args = {
					sysProcessName: "CrocProcessAddNewUserRequest",
					parameters: {
						AccountId: accountId
					}
				};
				ProcessModuleUtilities.executeProcess(args);
			},
			loadContactProfileModule: function() {
				this.sandbox.loadModule("BaseSchemaModuleV2", {
					id: this.sandbox.id + "_module_ContactProfile",
					renderTo: "ContactProfile",
					instanceConfig: {
						schemaName: "ContactProfileSchema",
						isSchemaConfigInitialized: true,
						useHistoryState: false,
						parameters: {
							viewModelConfig: {
								masterColumnName: "PrimaryContact",
								profileColumnName: "Account",
								MasterAccountValue: this.$Id
							}
						}
					}
				});
			},

			onEntityInitialized: function () {
				this.callParent(arguments);
				this.fillVisibleElements();
				this.isAccreditationRequestActionVisible();
				this.isAddNewUserRequestActionVisible();
			},

			fillVisibleElements: function() {
				const workTab = this.$TabsCollection.get("Tab6c271a14TabLabel");
				workTab.set("Visible", false);
				this.$isContactsEOVisible = false;
				var esq = this.Ext.create(Terrasoft.EntitySchemaQuery, {
					rootSchemaName: "CrocAccountTypification"
				});
				esq.addColumn("CrocType");
				var filters = this.Ext.create("Terrasoft.FilterGroup");
				filters.addItem(esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "CrocAccount",
					this.$Id, Terrasoft.DataValueType.GUID));
				esq.filters = filters;
				esq.execute(function(response) {
					if(response.success) {
						var valCollection = response.collection;
						var hasPort = false;
						var hasPortEO = false;
						Terrasoft.each(valCollection, function(item) {
							if(!hasPort) {
								hasPort = item.get("CrocType").value === CrocConstantsJS.CrocAccountType.Port.value;
							}
							if(!hasPortEO) {
								hasPortEO = item.get("CrocType").value === CrocConstantsJS.CrocAccountType.Elevator.value ||
									item.get("CrocType").value === CrocConstantsJS.CrocAccountType.Port.value;
							}
						}, this);
						workTab.set("Visible", hasPortEO);
						this.set("isContactsEOVisible", hasPort);
					}
				}, this);
			},

			loadESNFeed: function() {
				var moduleId = this.getSocialFeedSandboxId();
				var rendered = this.sandbox.publish("RerenderModule", {
					renderTo: "ESNFeed"
				}, [moduleId]);
				if (!rendered) {
					var esnFeedModuleConfig = {
						renderTo: "ESNFeed",
						id: moduleId
					};
					var activeSocialMessageId = this.get("ActiveSocialMessageId") ||
						this.getDefaultValueByName("ActiveSocialMessageId");
					if (!Ext.isEmpty(activeSocialMessageId)) {
						esnFeedModuleConfig.parameters = {activeSocialMessageId: activeSocialMessageId};
					}
					this.sandbox.loadModule("CrocFeedModule", esnFeedModuleConfig);
				}
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "AccountPhotoContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountName",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountType",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "insert",
				"name": "STRINGe2529d7f-d0e6-4af1-8188-7ca5214a9d91",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocEmail",
					"enabled": true
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "AccountPhone",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "insert",
				"name": "CrocEntityStatus06bc1e83-62af-42f3-9b4d-561c1159560c",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocEntityStatus",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "Address72148efc-d18e-4869-9b31-86c88ac0cc25",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "Address"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocPinnedManagere321b275-82b7-4a22-bd83-f86d21a315b5",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocPinnedManager",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "CrocReplaceManager31f08ac7-b36e-437a-96c4-71212377c72a",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 8,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocReplaceManager",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "DATETIME03b8e904-1f41-46f8-9a52-611ce0e5c319",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 9,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocReplaceStartDate",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "insert",
				"name": "CrocReplaceEndDateecb75230-4667-4c20-9608-10389071463f",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 10,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "CrocReplaceEndDate",
					"enabled": false
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "merge",
				"name": "ContactProfile",
				"values": {
					"afterrender": {
						"bindTo": "loadContactProfileModule"
					},
					"afterrerender": {
						"bindTo": "loadContactProfileModule"
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountPageGeneralTabContainer",
				"values": {
					"order": 0
				}
			},
			{
				"operation": "merge",
				"name": "AlternativeName",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Code",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "STRINGe586fac8-49fb-42f5-b0e4-15d5940f482c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "AccountPageGeneralInfoBlock"
					},
					"bindTo": "CrocINN",
					"enabled": true
				},
				"parentName": "AccountPageGeneralInfoBlock",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING767192b0-a2f9-4242-9d0a-9e3fb05db9cf",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "AccountPageGeneralInfoBlock"
					},
					"bindTo": "CrocKPP",
					"enabled": true
				},
				"parentName": "AccountPageGeneralInfoBlock",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocAccountTypification1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocDocument1DetailAccreditation",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocExporterEmployee1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocNotificationSetting1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "CrocNotificationEmployee1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CrocContactInformation1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "CrocAccountStock1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "Tab2e4ac8adTabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab2e4ac8adTabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "CrocContract2Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab2e4ac8adTabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab533cddc8TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab533cddc8TabLabelTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocDocument2Detailabb20942",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab533cddc8TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "Tab6c271a14TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tab6c271a14TabLabelTabCaption"
					},
					"items": [],
					"order": 3
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocRule1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab6c271a14TabLabel",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocEOPortContact1Detail1f857acf",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail",
					"visible": false
				},
				"parentName": "Tab6c271a14TabLabel",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "ContactsAndStructureTabContainer",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "merge",
				"name": "HistoryTabContainer",
				"values": {
					"order": 5
				}
			},
			{
				"operation": "insert",
				"name": "CrocDocument2Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTabContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "CrocRequest1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTabContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "CrocErrand1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTabContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "merge",
				"name": "NotesTabContainer",
				"values": {
					"order": 7
				}
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 8
				}
			},
			{
				"operation": "merge",
				"name": "TimelineTab",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "remove",
				"name": "AccountOwner"
			},
			{
				"operation": "remove",
				"name": "AccountWeb"
			},
			{
				"operation": "remove",
				"name": "NewAccountCategory"
			},
			{
				"operation": "remove",
				"name": "AccountIndustry"
			},
			{
				"operation": "remove",
				"name": "CategoriesControlGroup"
			},
			{
				"operation": "remove",
				"name": "CategoriesControlGroupContainer"
			},
			{
				"operation": "remove",
				"name": "EmployeesNumber"
			},
			{
				"operation": "remove",
				"name": "Ownership"
			},
			{
				"operation": "remove",
				"name": "AnnualRevenue"
			},
			{
				"operation": "remove",
				"name": "AccountAddress"
			},
			{
				"operation": "remove",
				"name": "AccountOrganizationChart"
			},
			{
				"operation": "remove",
				"name": "RelationshipTabContainer"
			},
			{
				"operation": "remove",
				"name": "Relationships"
			},
			{
				"operation": "remove",
				"name": "RelationshipContainer"
			}
		]/**SCHEMA_DIFF*/
	};
});
