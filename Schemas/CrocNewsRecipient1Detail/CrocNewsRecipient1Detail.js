define("CrocNewsRecipient1Detail", ["LookupMultiAddMixin"], function() {
	return {
		entitySchemaName: "CrocNewsRecipient",
		mixins: {
			LookupMultiAddMixin: "Terrasoft.LookupMultiAddMixin"
		},
		attributes: {
			"IsGroup": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			},
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "AddTypedRecordButton",
				"values": {
					"controlConfig": {
						"menu": {
							"items": {"bindTo": "addMenuItems"}
						}
					},
					"visible": true,
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {

			getAddRecordButtonVisible: this.Terrasoft.emptyFn,

			init: function() {
				this.callParent(arguments);
				this.mixins.LookupMultiAddMixin.init.call(this);
				this.initAddMenuItems();
			},

			initAddMenuItems: function() {
				var addMenuItems = Ext.create("Terrasoft.BaseViewModelCollection");
				addMenuItems.add("addAccountItem", this.Ext.create("Terrasoft.BaseViewModel", {
					values: {
						"Caption": {"bindTo": "Resources.Strings.AddAccountCaption"},
						"Click": {"bindTo": "addAccount"},
					}
				}));
				addMenuItems.add("addAccountGroupItem", this.Ext.create("Terrasoft.BaseViewModel", {
					values: {
						"Caption": {"bindTo": "Resources.Strings.AddAccountGroupCaption"},
						"Click": {"bindTo": "addAccountGroup"},
					}
				}));
				this.set("addMenuItems", addMenuItems);
			},

			addAccount: function() {
				this.$IsGroup = false;
				this.initMultiSelectLookupConfig();
				const config = this.getLookupConfig();
				const filtersConfig = this.createAlreadyAddedRecordsFilter();
				config.filters = this.getAllLookupFilters(filtersConfig);
				this.set("LookupFilters", config.filters);
				this.openLookup(config, this.addSelectedItems, this);
			},

			addAccountGroup: function() {
				this.$IsGroup = true;
				this.initMultiSelectLookupConfig();
				const config = this.getLookupConfig();
				this.openLookup(config, this.addSelectedItems, this);
			},

			createBatchQueryForAddNewSelectedItems: function() {
				if (this.$IsGroup) {
					this.addAccountsByGroup();
				} else {
					var batchQuery = this.Ext.create("Terrasoft.BatchQuery");
					var rootSchemaId = this.getMasterRecordId();
					this.selectedRows.forEach(function (item) {
						var id = item.value;
						var config = {
							rootId: rootSchemaId,
							relatedId: id
						};
						var insertQuery = this.getInsertQueryForSelectedItems(config);
						batchQuery.add(insertQuery);
					}, this);
					return batchQuery;
				}
			},

			addAccountsByGroup: function() {
				this.selectedRows.forEach(function (item) {
					var accounts = [];
					var addedAccounts = [];
					this.Terrasoft.chain(function(next) {
						this.getInserts(item, function(result) {
							accounts = result;
							next();
						}, this);
					},
					function(next) {
						this.getAddedAccounts(function(result) {
							addedAccounts = result;
							next();
						}, this);
					},
					function() {
						accounts.forEach(function (accountId) {
							if (!addedAccounts.includes(accountId)) this.insertNewsRecepient(accountId);
						}, this);
						this.updateDetail({reloadAll: true});
					}, this);
				}, this);
			},

			getAddedAccounts: function(callback, scope) {
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "CrocNewsRecipient"});
				esq.addColumn("CrocAccount");
				esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "CrocNews", this.getMasterRecordId()));
				esq.getEntityCollection(function(response) {
					const entities = response.collection;
					if (response.success && !entities.isEmpty()) {
						var addedAccounts = [];
						entities.each(function(entity){
							addedAccounts.push(entity.get("CrocAccount").value);
						}, this);
						callback.call(scope, addedAccounts);
					} else callback.call(scope, []);
				}, this);
			},

			insertNewsRecepient: function(accountId) {
				var rootSchemaId = this.getMasterRecordId();
				var insert = Ext.create("Terrasoft.InsertQuery", {rootSchemaName: "CrocNewsRecipient"});
				var id = Terrasoft.generateGUID();
				insert.setParameterValue("Id", id, Terrasoft.DataValueType.GUID);
				insert.setParameterValue("CrocNews", rootSchemaId, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("CrocAccount", accountId, this.Terrasoft.DataValueType.GUID);
				insert.execute();
			},

			getInserts: function(item, callback, scope) {
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Account"});
				esq.addColumn("Id");
				esq.filters = Terrasoft.deserialize(item.SearchData);
				esq.getEntityCollection(function(response) {
					const entities = response.collection;
					if (response.success && !entities.isEmpty()) {
						var accounts = [];
						entities.each(function(entity){
							accounts.push(entity.get("Id"));
						}, this);
						callback.call(scope, accounts);
					}
				}, this);
			},

			getMultiSelectLookupConfig: function() {
				return {
					rootEntitySchemaName: "CrocNews",
					rootColumnName: "CrocNews",
					relatedEntitySchemaName: (this.$IsGroup) ? "AccountFolder" : "Account",
					relatedColumnName: "CrocAccount"
				};
			},

			getLookupColumns: function() {
				return (!this.$IsGroup) ? ["Name"] : ["Name", "SearchData"];
			},

			onCardSaved: function() {
				this.openLookupWithMultiSelect();
			},
		}
	};
});
