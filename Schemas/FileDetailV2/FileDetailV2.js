define("FileDetailV2", ["ModalBox", "MaskHelper", "FileDetailV2Resources", "CrocImageListViewModel", "CrocFileDetailMixin"],
	function (ModalBox, MaskHelper, resources) {
		return {
			mixins: {
				CrocFileDetailMixin: "Terrasoft.CrocFileDetailMixin"
			},
			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
			messages: {
				"UpdateFileDetail": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},
			},
			attributes: {
				"IsChooseTypeButtonVisible": {
					dataValueType: Terrasoft.DataValueType.BOOLEAN,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				"IsVersionButtonVisible": {
					dataValueType: Terrasoft.DataValueType.BOOLEAN,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				"SingleFileTypeId": {
					dataValueType: Terrasoft.DataValueType.GUID,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				"DocumentTypeId": {
					dataValueType: Terrasoft.DataValueType.GUID,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				"FileTypeName": {
					dataValueType: Terrasoft.DataValueType.TEXT,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				"AccountName": {
					dataValueType: Terrasoft.DataValueType.TEXT,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				"IsFileVersion": {
					dataValueType: Terrasoft.DataValueType.BOOLEAN,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
			},
			modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "merge",
					"name": "AddRecordButton",
					"values": {
						"visible": { "bindTo": "getAddRecordButtonVisible" }
					}
				},
				{
					"operation": "insert",
					"name": "AddChooseTypeRecordButton",
					"parentName": "Detail",
					"propertyName": "tools",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"click": {"bindTo": "openChooseTypeModalBox"},
						// "visible": {"bindTo": "getAddRecordButtonVisible"},
						"imageConfig": {"bindTo": "Resources.Images.AddButtonImage"},
						"visible": { "bindTo": "IsChooseTypeButtonVisible" }
					},
					"index": 1
				},
				{
					"operation": "insert",
					"name": "AddFileVersionButton",
					"parentName": "Detail",
					"propertyName": "tools",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"fileUpload": true,
						"fileUploadMultiSelect": false,
						"filesSelected": {"bindTo": "onFileVersionSelect"},
						"click": {"bindTo": "onAddFileClick"},
						"visible": {"bindTo": "IsVersionButtonVisible"},
						"imageConfig": {"bindTo": "Resources.Images.AddVersionImage"}
					},
					"index": 2
				},
			], /**SCHEMA_DIFF*/
			methods: {

				getGridRowViewModelClassName: function() {
					return "Terrasoft.CrocImageListViewModel";
				},

				onGridLoaded: function() {
					this.$IsVersionButtonVisible = this.isAnySelected() && this.getAllowVersionS3Sections().includes(this.parentEntity.EntityName);
				},

				onActiveRowChange: function() {
					this.callParent(arguments);
					this.onGridLoaded();
				},

				openChooseTypeModalBox: function() {
					this.saveCard(function() {
						let cardValues = this.getCardValues(this.parentEntity.EntityName);
						this.$DocumentTypeId = (cardValues.CrocDocumentType) ? cardValues.CrocDocumentType.value : null;
						this.$AccountName = (cardValues.CrocAccount) ? cardValues.CrocAccount.displayValue : null;
						this.getFileTypeByDocumentType(cardValues.CrocDocumentType, function (fileType) {
							if (fileType) {
								this.$SingleFileTypeId = fileType.value;
								this.$FileTypeName = fileType.displayValue;
							}
							this.sandbox.loadModule("ModalBoxSchemaModule", {
								id: this.sandbox.id + "_" + "CrocChooseTypeModalBox",
								instanceConfig: {
									moduleInfo: {
										schemaName: "CrocChooseTypeModalBox",
										documentTypeId: this.$DocumentTypeId,
										masterRecordId: this.$MasterRecordId,
										fileTypeId: this.$SingleFileTypeId,
										fileTypeName: this.$FileTypeName,
										account: this.$AccountName,
										rootSchemaName: this.parentEntity.EntityName
									}
								}
							});
						}, this);
					}, this);
				},

				getParentMethod: function() {
					var method,
						superMethod = (method = this.getParentMethod.caller) && (method.$previous ||
							((method = method.$owner ? method : method.caller) &&
								method.$owner.superclass[method.$name]));
					return superMethod;
				},

				init: function() {
					this.callParent(arguments);
					this.$AllowS3Sections = this.getAllowS3Sections();
					this.$IsChooseTypeButtonVisible = this.getAllowChooseType().includes(this.parentEntity.EntityName);
					const gridData = this.getGridData();
					gridData.on("dataLoaded", this.onGridLoaded, this);
					this.sandbox.subscribe("UpdateFileDetail", function(arg) {
						this.updateDetail({reloadAll: true});
					}, this, [this.sandbox.id + "_CrocChooseTypeModalBox"]);
				},

				getUploadConfig: function(files) {
					var config = this.callParent(arguments);
					if (this.$AllowS3Sections.includes(this.parentEntity.EntityName)) {
						Ext.apply(config, {
							uploadWebServicePath: "FilesS3Service/ImportFile",
							onFileProgress: this.onFileProgress
						});
					}
					return config;
				},

				initLoadFilesQueryColumns: function(esq) {
					this.callParent(arguments);
					if (this.$AllowS3Sections.includes(this.parentEntity.EntityName) && !esq.columns.contains("CrocS3Link")) {esq.addColumn("CrocS3Link");}
				},

				getS3FileLink: function(entitySchema, fileId) {
					var urlPattern = "{0}/rest/FilesS3Service/GetS3File/{1}/{2}/{3}/{4}";
					var workspaceBaseUrl = Terrasoft.utils.uri.getConfigurationWebServiceBaseUrl();
					var url = Ext.String.format(urlPattern, workspaceBaseUrl, entitySchema, fileId, "Data", "CrocS3Link");
					return url;
				},

				// реализация скачивания
				getColumnLinkConfig: function(item) {
					var config = this.callParent(arguments);
					this.defineEntityType(item);
					const id = item.get("Id");
					if (this.$AllowS3Sections.includes(this.parentEntity.EntityName) && this.isFile && item.$CrocS3Link) {
						config.link = this.getS3FileLink(this.entitySchema.name, id);
					}
					return config;
				},

				onFileSelect: function(files) {
					this.callParent(arguments);
					this.$IsFileVersion = false;
				},

				onFileVersionSelect: function(files) {
					this.onFileSelect(files);
					this.$IsFileVersion = true;
				},

				saveCard: function(callback, scope) {
					this.sandbox.publish("SaveRecord", {
						isSilent: true,
						callback: callback,
						callBaseSilentSavedActions: true,
						scope: scope
					}, [this.sandbox.id]);
				},

				onAddFileClick: function() {
					var parentSave = this.getParentMethod();
					var parentArguments = arguments;
					this.saveCard(function() {
						parentSave.apply(this, parentArguments);
					}, this);
				},

				upload: function(config, callback) {
					var parentSave = this.getParentMethod();
					var parentArguments = arguments;
					if (this.$AllowS3Sections.includes(this.parentEntity.EntityName)) {
						this.validateEmptyFilesSize(config.files, function (files) {
							parentSave.apply(this, parentArguments);
						}, this);
					}
					else parentSave.apply(this, parentArguments);
				},

				getEmptyFiles: function(files) {
					const filesOversize = [];
					const length = files.length;
					for (let i = length - 1; i >= 0; i--) {
						if (files[i].size === 0) {
							filesOversize.push(files[i]);
							files.splice(i, 1);
						}
					}
					return filesOversize;
				},

				generateDefaultErrorMessage: function(excludedFiles) {
					let errorMessage = "";
					const length = excludedFiles.length;
					for (let i = 0; i < length; i++) {
						errorMessage += (errorMessage === "") ? excludedFiles[i].name
							: (", " + excludedFiles[i].name);
					}
					return errorMessage;
				},

				validateEmptyFilesSize: function(files, callback, scope) {
					const filesEmpty = this.getEmptyFiles(files);
					if (filesEmpty.length > 0) {
						Terrasoft.utils.showInformation(
							Ext.String.format("{0}\n{1}", resources.localizableStrings.EmptyFilesErrorMessage,
								this.generateDefaultErrorMessage(filesEmpty)
							)
						);
					}
					Ext.callback(callback, scope || this, [files]);
				},

				onUpload: function(file, xhr, options) {
					if (this.$AllowS3Sections.includes(this.parentEntity.EntityName)) {
						this.$FileMaskId = Terrasoft.MaskHelper.showBodyMask({
							caption: "Загрузка"
						});
					}
					else this.callParent(arguments);
				},

				onFileProgress: function(event, file, xhr, options) {
					var parentSave = this.getParentMethod();
					var parentArguments = arguments;
					if (this.$AllowS3Sections.includes(this.parentEntity.EntityName)) {
						this.$FileMaskCaption = "Сохранение " + file.name;
						var caption = Terrasoft.getFormattedString("{0} ({1}%)", this.$FileMaskCaption, Math.floor(event.loaded * 100 / event.total));
						Terrasoft.MaskHelper.updateBodyMaskCaption(this.$FileMaskId, caption);
					}
					else this.callParent(arguments);
				},

				onFileComplete: function(error, xhr, file, options) {
					if (this.$AllowS3Sections.includes(this.parentEntity.EntityName)) {
						if (!error) {
							let fileId = (this.$IsFileVersion && this.getSelectedItems().length > 0) ? this.getSelectedItems()[0] : null;
							let cardValues = this.getCardValues(this.parentEntity.EntityName);
							this.executeSavingEntity(this.parentEntity.EntityName, options.data, cardValues, fileId, function(result) {
								this.updateDetail({reloadAll: true});
								if (result) {
									if (result.SaveFileResult) {
										this.showInformationDialog(Terrasoft.getFormattedString("{0}", result.SaveFileResult));
										Terrasoft.MaskHelper.hideBodyMask(this.$FileMaskId);
									} else {
										if (options.data.fileName === options.files.files[options.files.files.length - 1].name) {
											Terrasoft.MaskHelper.hideBodyMask(this.$FileMaskId);
										}
									}
								}
							}, this);
						}
						else this.showInformationDialog(error);
					}
					else this.callParent(arguments);
				},

				onComplete: function() {
					if (this.$AllowS3Sections.includes(this.parentEntity.EntityName)) {
						this.hideBodyMask();
						const errorLog = this.get("FilesUploadErrorLog");
						if (errorLog.getCount()) {
							this.onCompleteError(errorLog);
							errorLog.clear();
						}
					}
					else this.callParent(arguments);
				},

				addGridDataColumns: function (esq) {
					this.callParent(arguments);
				}


			},
			rules: {}
		};
	});
