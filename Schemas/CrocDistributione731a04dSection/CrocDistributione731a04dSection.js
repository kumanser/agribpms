define("CrocDistributione731a04dSection", [], function() {
	return {
		entitySchemaName: "CrocDistribution",
		messages: {
			"UnPublishDistributionButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"PublishDistributionButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attributes: {
			"UnPublishButtonVisible": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"PublishButtonVisible": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
		},
		methods: {

			init: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("UnPublishDistributionButtonVisibleChanged",  function(isVisible){
					this.$UnPublishButtonVisible = isVisible
				}, this, ["SectionModuleV2_InvoiceSectionV2"]);
				this.sandbox.subscribe("PublishDistributionButtonVisibleChanged",  function(isVisible){
					this.$PublishButtonVisible = isVisible
				}, this, ["SectionModuleV2_InvoiceSectionV2"]);
			},

		},
		diff: /**SCHEMA_DIFF*/[
			{
			"operation": "insert",
			"parentName": "CombinedModeActionButtonsCardLeftContainer",
			"propertyName": "items",
			"name": "PublishButton",
			"index": 0,
			"values": {
				"tag": "onPublishButtonClick",
				"visible": {
					"bindTo": "PublishButtonVisible"
				},
				"itemType": Terrasoft.ViewItemType.BUTTON,
				"style": Terrasoft.controls.ButtonEnums.style.GREEN,
				"caption": { "bindTo": "Resources.Strings.PublishButtonCaption" },
				"click": { "bindTo": "onCardAction" },
				"classes": {
					"textClass": ["actions-button-margin-right"],
					"wrapperClass": ["actions-button-margin-right"]
				},
			}
		},
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "UnPublishButton",
				"index": 1,
				"values": {
					"tag": "onUnPublishButtonClick",
					"visible": {"bindTo": "UnPublishButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.RED,
					"caption": { "bindTo": "Resources.Strings.UnPublishButtonCaption" },
					"click": { "bindTo": "onCardAction" },
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			}]/**SCHEMA_DIFF*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
	};
});
