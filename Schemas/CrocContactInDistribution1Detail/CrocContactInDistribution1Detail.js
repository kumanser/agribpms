define("CrocContactInDistribution1Detail", ["LookupMultiAddMixin"], function() {
	return {
		entitySchemaName: "CrocContactInDistribution",
		mixins: {
			LookupMultiAddMixin: "Terrasoft.LookupMultiAddMixin"
		},
		attributes: {
			"IsGroup": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			},
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "AddTypedRecordButton",
				"values": {
					"controlConfig": {
						"menu": {
							"items": {"bindTo": "addMenuItems"}
						}
					},
					"visible": true,
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {

			getAddRecordButtonVisible: this.Terrasoft.emptyFn,

			init: function() {
				this.callParent(arguments);
				this.mixins.LookupMultiAddMixin.init.call(this);
				this.initAddMenuItems();
			},

			initAddMenuItems: function() {
				var addMenuItems = Ext.create("Terrasoft.BaseViewModelCollection");
				addMenuItems.add("addItem", this.Ext.create("Terrasoft.BaseViewModel", {
					values: {
						"Caption": {"bindTo": "Resources.Strings.AddCaption"},
						"Click": {"bindTo": "addItem"},
					}
				}));
				addMenuItems.add("addItemGroup", this.Ext.create("Terrasoft.BaseViewModel", {
					values: {
						"Caption": {"bindTo": "Resources.Strings.AddGroupCaption"},
						"Click": {"bindTo": "addItemGroup"},
					}
				}));
				this.set("addMenuItems", addMenuItems);
			},

			addItem: function() {
				this.$IsGroup = false;
				this.initMultiSelectLookupConfig();
				const config = this.getLookupConfig();
				const filtersConfig = this.createAlreadyAddedRecordsFilter();
				config.filters = this.getAllLookupFilters(filtersConfig);
				this.set("LookupFilters", config.filters);
				this.openLookup(config, this.addSelectedItems, this);
			},

			addItemGroup: function() {
				this.$IsGroup = true;
				this.initMultiSelectLookupConfig();
				const config = this.getLookupConfig();
				this.openLookup(config, this.addSelectedItems, this);
			},

			createBatchQueryForAddNewSelectedItems: function() {
				if (this.$IsGroup) {
					this.addItemsByGroup();
				} else {
					var batchQuery = this.Ext.create("Terrasoft.BatchQuery");
					var rootSchemaId = this.getMasterRecordId();
					this.selectedRows.forEach(function (item) {
						var id = item.value;
						var config = {
							rootId: rootSchemaId,
							relatedId: id
						};
						var insertQuery = this.getInsertQueryForSelectedItems(config);
						batchQuery.add(insertQuery);
					}, this);
					return batchQuery;
				}
			},

			addItemsByGroup: function() {
				this.selectedRows.forEach(function (item) {
					var items = [];
					var addedItems = [];
					this.Terrasoft.chain(function(next) {
							this.getInserts(item, function(result) {
								items = result;
								next();
							}, this);
						},
						function(next) {
							this.getAddedItems(function(result) {
								addedItems = result;
								next();
							}, this);
						},
						function() {
							items.forEach(function (itemId) {
								if (!addedItems.includes(itemId)) this.insertItem(itemId);
							}, this);
							this.updateDetail({reloadAll: true});
						}, this);
				}, this);
			},

			getAddedItems: function(callback, scope) {
				var schemaName = "CrocContactInDistribution";
				var columnName = "CrocContact";
				var masterColumnName = "CrocDistribution";

				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: schemaName});
				esq.addColumn(columnName);
				esq.filters.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, masterColumnName, this.getMasterRecordId()));
				esq.getEntityCollection(function(response) {
					const entities = response.collection;
					if (response.success && !entities.isEmpty()) {
						var addedItems = [];
						entities.each(function(entity){
							addedItems.push(entity.get(columnName).value);
						}, this);
						callback.call(scope, addedItems);
					} else callback.call(scope, []);
				}, this);
			},

			insertItem: function(itemId) {
				var schemaName = "CrocContactInDistribution";
				var columnName = "CrocContact";
				var masterColumnName = "CrocDistribution";

				var rootSchemaId = this.getMasterRecordId();
				var insert = Ext.create("Terrasoft.InsertQuery", {rootSchemaName: schemaName});
				var id = Terrasoft.generateGUID();
				insert.setParameterValue("Id", id, Terrasoft.DataValueType.GUID);
				insert.setParameterValue(masterColumnName, rootSchemaId, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue(columnName, itemId, this.Terrasoft.DataValueType.GUID);
				insert.execute();
			},

			getInserts: function(item, callback, scope) {
				var schemaName = "Contact";
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: schemaName});
				esq.addColumn("Id");
				esq.filters = Terrasoft.deserialize(item.SearchData);
				esq.getEntityCollection(function(response) {
					const entities = response.collection;
					if (response.success && !entities.isEmpty()) {
						var items = [];
						entities.each(function(entity){
							items.push(entity.get("Id"));
						}, this);
						callback.call(scope, items);
					}
				}, this);
			},

			getMultiSelectLookupConfig: function() {
				return {
					rootEntitySchemaName: "CrocDistribution",
					rootColumnName: "CrocDistribution",
					relatedEntitySchemaName: (this.$IsGroup) ? "ContactFolder" : "Contact",
					relatedColumnName: "CrocContact"
				};
			},

			getLookupColumns: function() {
				return (!this.$IsGroup) ? ["Name"] : ["Name", "SearchData"];
			},

			onCardSaved: function() {
				this.openLookupWithMultiSelect();
			},
		}
	};
});
