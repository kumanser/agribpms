define("CrocAccountTypification1Detail", ["LookupMultiAddMixin"], function() {
	return {
		mixins: {
          LookupMultiAddMixin: "Terrasoft.LookupMultiAddMixin"
        },
        messages: {
            "AccountTypificationDataUpdated": {
                mode: Terrasoft.MessageMode.PTP,
                direction: Terrasoft.MessageDirectionType.PUBLISH
            }
        },
		entitySchemaName: "CrocAccountTypification",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			init: function() {
                this.callParent(arguments);
                this.mixins.LookupMultiAddMixin.init.call(this);
            },

            getAddRecordButtonVisible: function() {
                return this.getToolsVisible();
            },

            onCardSaved: function() {
                this.openLookupWithMultiSelect();
            },

            addRecord: function() {
                this.openLookupWithMultiSelect(true);
            },

            getMultiSelectLookupConfig: function() {
                return {
                    rootEntitySchemaName: "Account",
                    rootColumnName: "CrocAccount",
                    relatedEntitySchemaName: "AccountType",
                    relatedColumnName: "CrocType"
                };
            },

            onDataChanged: function() {
                this.callParent(arguments);
                var masterRecId = this.get("MasterRecordId");
                this.sandbox.publish("AccountTypificationDataUpdated", masterRecId, []);
            }
	   }
	};
});
