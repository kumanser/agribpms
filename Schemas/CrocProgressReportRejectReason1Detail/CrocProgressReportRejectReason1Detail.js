define("CrocProgressReportRejectReason1Detail", [], function() {
	return {
		entitySchemaName: "CrocProgressReportRejectReason",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/,
		methods: {
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,
			getEditRecordMenuItem: this.Terrasoft.emptyFn,
			getAddRecordButtonVisible: this.Terrasoft.emptyFn,
		}
	};
});
