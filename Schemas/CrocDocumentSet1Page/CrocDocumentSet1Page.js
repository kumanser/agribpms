define("CrocDocumentSet1Page", [], function() {
	return {
		entitySchemaName: "CrocDocumentSet",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "CrocDocumentSetFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "CrocDocumentSet"
				}
			},
			"CrocDocumentSetList1Detail": {
				"schemaName": "CrocDocumentSetList1Detail",
				"entitySchemaName": "CrocDocumentSetList",
				"filter": {
					"detailColumn": "CrocDocumentSet",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"CrocAccreditationType": {
				"39e8bdd5-1134-4d77-91c6-4d859e9b729c": {
					"uId": "39e8bdd5-1134-4d77-91c6-4d859e9b729c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "4e319a75-850d-46de-bfeb-4e0f3ae1197c",
								"dataValueType": 10
							}
						}
					]
				},
				"abfdb465-d2dc-43ed-b924-7323ea21b266": {
					"uId": "abfdb465-d2dc-43ed-b924-7323ea21b266",
					"enabled": true,
					"removed": true,
					"ruleType": 1,
					"baseAttributePatch": "CrocCompanyType",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": false,
					"type": 1,
					"attribute": "CrocCompanyType",
					"attributePath": "Name"
				},
				"373ff4f0-a5a5-4a8f-82d6-b0d2fff0591d": {
					"uId": "373ff4f0-a5a5-4a8f-82d6-b0d2fff0591d",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "CrocCompanyType",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "CrocCompanyType"
				}
			},
			"CrocDeliveryCondition": {
				"f763d51d-a4a0-4e55-a67f-17f46e299772": {
					"uId": "f763d51d-a4a0-4e55-a67f-17f46e299772",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 0,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "CrocRequestType"
							},
							"rightExpression": {
								"type": 0,
								"value": "ba5c23aa-93f0-46e8-bfc8-533d3c15c93c",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "CrocNamee9df13b8-2c47-4d53-b158-045331af4d08",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "CrocName"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP31ec9a01-4e26-435d-b549-3772066689ec",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CrocDeliveryCondition",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUP849a4684-7540-4217-a2e6-a41b6280377e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocCompanyType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "LOOKUP18bce6ec-e1de-4616-b7c2-8938e7b22412",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "CrocRequestType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUPc9f2edc2-be75-44f4-999b-28bb003eb461",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "CrocAccreditationType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "BaseInfoTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.BaseInfoTabTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BaseInfoTabGroup772befce",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.BaseInfoTabGroup772befceGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "BaseInfoTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "BaseInfoTabGridLayout8c61a55a",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "BaseInfoTabGroup772befce",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING703d3f17-74f5-4293-a9fc-f4df0f65d1e1",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "BaseInfoTabGridLayout8c61a55a"
					},
					"bindTo": "CrocDescription",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "BaseInfoTabGridLayout8c61a55a",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "CrocDocumentSetList1Detail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "BaseInfoTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "remove",
				"name": "ESNTab"
			},
			{
				"operation": "remove",
				"name": "ESNFeedContainer"
			},
			{
				"operation": "remove",
				"name": "ESNFeed"
			}
		]/**SCHEMA_DIFF*/
	};
});
