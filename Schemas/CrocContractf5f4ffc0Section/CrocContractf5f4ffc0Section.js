define("CrocContractf5f4ffc0Section", ["ProcessModuleUtilities", "RightUtilities",
		"CrocExcelHelperJS", "CrocExcelTemplateConst", "DataUtilities", "ServiceHelper"],
	function(ProcessModuleUtilities, RightUtilities,
			 CrocExcelHelperJS, CrocExcelTemplateConst, DataUtilities, ServiceHelper) {
	return {
		entitySchemaName: "CrocContract",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"name": "SendToExporterButton",
				"index": 0,
				"values": {
					"tag": "onSendToExporterButtonClick",
					"visible": {"bindTo": "SendToExporterButtonVisible"},
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					"caption": {"bindTo": "Resources.Strings.SendToExporterButtonCaption"},
					"click": {"bindTo": "onCardAction"},
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
				}
			}
        ]/**SCHEMA_DIFF*/,
		messages: {
			"SendToExporterButtonVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"RequestDocumentActionVisibleChanged": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attributes: {
			"SendToExporterButtonVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"CrocIsRequestVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"IsAssignedActionVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"CanExportFile": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			"AllSelectedRowsCount": {
				"dataValueType": Terrasoft.DataValueType.INTEGER,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": 0
			},
		},
		methods: {

			init: function() {
				this.callParent(arguments);
				this.setChangeAssignedActionVisible();
				this.setCanExportExcel();
			},

			setCanExportExcel: function() {
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanExportToExcel"},
					function(result) {
						this.$CanExportFile = result;
					}, this);
			},

			exportToExcelCPT: function() {
				var filter = this.getFilters();
				var config = {
					serviceName: "CrocExcelGeneratorService",
					methodName: "GenerateExcel",
					data: {
						model: JSON.stringify(CrocExcelTemplateConst.CrocExcelTemplates.ExcelCPT),
						filter: filter.serialize()
					}
				};

				ServiceHelper.callService(config.serviceName, config.methodName, function(response) {
					var cacheKey = response.GenerateExcelResult;
					CrocExcelHelperJS.CrocExcelHelper.downloadFile(CrocExcelTemplateConst.CrocExcelTemplates.ExcelCPT.excelReportName, cacheKey);
				}, config.data, this);
			},

			exportToExcelFCA: function() {
				var filter = this.getFilters();
				var config = {
					serviceName: "CrocExcelGeneratorService",
					methodName: "GenerateExcel",
					data: {
						model: JSON.stringify(CrocExcelTemplateConst.CrocExcelTemplates.ExcelFCA),
						filter: filter.serialize()
					}
				};

				ServiceHelper.callService(config.serviceName, config.methodName,function(response) {
					var cacheKey = response.GenerateExcelResult;
					CrocExcelHelperJS.CrocExcelHelper.downloadFile(CrocExcelTemplateConst.CrocExcelTemplates.ExcelFCA.excelReportName, cacheKey);
				}, config.data, this);
			},

			getSectionActions: function () {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					Type: "Terrasoft.MenuSeparator",
					Caption: ""
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Enabled": true,
					"Visible": {bindTo: "IsAssignedActionVisible"},
					"Caption" : {bindTo: "Resources.Strings.ChangeAssignedEmployeeActionCaption"},
					"Click": {bindTo: "callProcessChangeAssignedEmployee"},
					"IsEnabledForSelectedAll": true
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Enabled": {bindTo: "CanExportFile"},
					"Visible": {bindTo: "CanExportFile"},
					"Caption" : {bindTo: "Resources.Strings.CrocExcelCPTButtonCaption"},
					"Click": {bindTo: "exportToExcelCPT"},
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Enabled": {bindTo: "CanExportFile"},
					"Visible": {bindTo: "CanExportFile"},
					"Caption" : {bindTo: "Resources.Strings.CrocExcelFCAButtonCaption"},
					"Click": {bindTo: "exportToExcelFCA"},
				}));
				return actionMenuItems;
			},

			callProcessChangeAssignedEmployee: function() {
				var activeRowId = this.get("ActiveRow");
				var selectedRows = this.get("SelectedRows");
				var contractsId = "";
					contractsId = (selectedRows.length) ? selectedRows.join(';') : (activeRowId);
				var args = {
					sysProcessName: "CrocProcessChangeContractAssignedEmployee",
					parameters: {
						StringContractId: contractsId
					}
				};
				ProcessModuleUtilities.executeProcess(args);
				this.unSetMultiSelect();
			},

			setChangeAssignedActionVisible: function(){
				RightUtilities.checkCanExecuteOperation({operation: "CrocCanChangeContractAssignedExportRole"}, function(result) {
					this.$IsAssignedActionVisible = result;
				},this);
			},

			createSelectAllButton: function() {
				return this.getButtonMenuItem({
					"Caption": {"bindTo": "Resources.Strings.SelectAllButtonCaption"},
					"Click": {"bindTo": "selectAllItems"},
					"Visible": {"bindTo": "isSelectAllModeVisible"},
					"IsEnabledForSelectedAll": true
				});
			},

			onSelectedRowsChange: function() {
				if (this.get("MultiSelect")) {
					if (this.get("SelectAll")) {
						var selectedRows = this.get("SelectedRows");
						if (this.$AllSelectedRowsCount < selectedRows.length) this.$AllSelectedRowsCount = selectedRows.length
						else this.set("SelectAll", false);
					}
				} else this.set("SelectAll", false);
			},

			unSelectRecords: function() {
				this.callParent(arguments);
				this.set("SelectAll", false);
			},

			unSetMultiSelect: function() {
				this.callParent(arguments);
				this.set("MultiSelect", false);
			},

			selectAllItems: function () {
				this.set("IsPageable", false);
				this.set("RowCount", -1);
				this.set("SelectAll", true);
				this.getGridData().clear();
				this.reloadGridData();
			},

			onGridDataLoaded: function(response) {
				this.callParent(arguments);
				if (this.get("SelectAll")) {
					if (!this.get("MultiSelect")) {
						this.setMultiSelect();
					}
					this.set('SelectedRows', response.collection.collection.keys);
				}
			},

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("RequestDocumentActionVisibleChanged", function(isVisible) {
					this.$CrocIsRequestVisible = isVisible;
				}, this, [this.sandbox.id + "_CardModuleV2"]);
				this.sandbox.subscribe("SendToExporterButtonVisibleChanged", function(isVisible) {
					this.$SendToExporterButtonVisible = isVisible;
				}, this, ["SectionModuleV2_InvoiceSectionV2"]);
			}
		}
	};
});
