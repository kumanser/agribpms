@echo off 
if not exist build.config (
	echo.HOST=http://localhost:80>>build.config
	echo.LOGIN=Supervisor>>build.config
	echo.PASSWORD=Supervisor>>build.config
	notepad build.config
) else (
	Setlocal EnableDelayedExpansion
	set pHOST=
	set pLOGIN=
	set pPASSWORD=
	set pGITUSER=
	set pGITTOKEN=
	for /f "tokens=1,2 delims==" %%a in (build.config) do ( 
		if "%%a"=="HOST" set pHOST=!pHOST!%%b
		if "%%a"=="LOGIN" set pLOGIN=!pLOGIN!%%b
		if "%%a"=="PASSWORD" set pPASSWORD=!pPASSWORD!%%b
	)
	echo =================BUILD=================
	cd ..
	dotnet restore
	dotnet build -c Release
	dotnet test -c Release
	rem cls
	echo =================RESTART=================
	clio restart -u "!pHOST!" -l "!pLOGIN!" -p "!pPASSWORD!"
)
pause& exit