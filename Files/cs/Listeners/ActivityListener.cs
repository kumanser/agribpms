﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Linq;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Configuration.OneCIntegration;
using Terrasoft.Core.DB;
using Terrasoft.Common;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "Activity")]
    public class ActivityListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isAllocateChanged = false;
        private bool _isWorkStatus = false;
        private bool _isRefusalDateChanged = false;
        private bool _isNameChange = false;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnAllocatedChanged()
        {
            int newCarAllocate = _entity.GetTypedColumnValue<int>("CrocCarAllocate");
            int oldCarAllocate = _entity.GetTypedOldColumnValue<int>("CrocCarAllocate");
            decimal newTonAllocate = _entity.GetTypedColumnValue<decimal>("CrocTonAllocate");
            decimal oldTonAllocate = _entity.GetTypedOldColumnValue<decimal>("CrocTonAllocate");
            _isAllocateChanged = newCarAllocate != oldCarAllocate || newTonAllocate != oldTonAllocate;
        }

        private void OnStatusChanged()
        {
            Guid newStatusId = _entity.GetTypedColumnValue<Guid>("StatusId");
            Guid oldStatusId = _entity.GetTypedOldColumnValue<Guid>("StatusId");
            _isWorkStatus = newStatusId != oldStatusId && newStatusId == ConstCs.Activity.Status.InProgress;
        }

        private void OnRefusalDateChanged()
        {
            DateTime newRefusalDate = _entity.GetTypedColumnValue<DateTime>("CrocRefusalDate");
            DateTime oldRefusalDate = _entity.GetTypedOldColumnValue<DateTime>("CrocRefusalDate");
            Guid newStatusId = _entity.GetTypedColumnValue<Guid>("StatusId");
            Guid oldStatusId = _entity.GetTypedOldColumnValue<Guid>("StatusId");
            _isRefusalDateChanged = (newRefusalDate != oldRefusalDate || newStatusId != oldStatusId)
                && newStatusId == ConstCs.Activity.Status.Done
                && newRefusalDate != DateTime.MinValue;
            if (_isRefusalDateChanged) { _isAllocateChanged = true; }
        }

        private void CalculateAllocated()
        {
            Guid deliveryScheduleId = _entity.GetTypedColumnValue<Guid>("CrocDeliveryScheduleId");
            if (deliveryScheduleId != Guid.Empty && _isAllocateChanged)
            {
                int carSum = 0;
                decimal quotaSum = 0;
                List<Entity> collectionTonFact = _entityHelper.FindEntities("Activity", new Dictionary<string, object>()
                {
                    {"CrocDeliverySchedule.Id", deliveryScheduleId },
                    { "CrocRefusalDate", null}
                }).ToList();
                if (collectionTonFact?.Count > 0)
                {
                    collectionTonFact.ForEach(entity =>
                    {
                        carSum += entity.GetTypedColumnValue<int>("CrocCarAllocate");
                        quotaSum += entity.GetTypedColumnValue<decimal>("CrocTonAllocate");
                    });
                }
                _entityHelper.UpdateEntity("CrocDeliverySchedule", deliveryScheduleId, new Dictionary<string, object>()
                {
                    { "CrocAllocatedQuotaTon", quotaSum},
                    { "CrocAllocatedQuotaTransport", carSum},
                });
            }
        }

        private void SendQuote()
        {
            if (_isWorkStatus)
            {
                try
                {
                    OneCIntegrationHelper oneHelper = new OneCIntegrationHelper(_userConnection);
                    oneHelper.SendRequestQuota(_entity.PrimaryColumnValue);
                }
                catch { }
            }
        }

        private void SendRefusalQuote()
        {
            if (_isRefusalDateChanged)
            {
                try
                {
                    Guid statusId = _entity.GetTypedColumnValue<Guid>("StatusId");
                    if (statusId == ConstCs.Activity.Status.Done)
                    {
                        int carAllocate = _entity.GetTypedColumnValue<int>("CrocCarAllocate");
                        decimal tonAllocate = _entity.GetTypedColumnValue<decimal>("CrocTonAllocate");
                        if (carAllocate != 0 && tonAllocate != 0)
                        {
                            OneCIntegrationHelper oneHelper = new OneCIntegrationHelper(_userConnection);
                            oneHelper.SendRefusalQuotaI(_entity.PrimaryColumnValue);
                        }
                    }

                }
                catch { }
            }
        }

        private void UpdateIsSynchronize(string entityName, Guid entityId)
        {
            var update = new Update(_userConnection, entityName) as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(entityId));
            update.Execute();
        }

        private void OnColumnsChanged()
        {
            bool isActivityCategoryChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "ActivityCategoryId");
            bool isAccountChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "AccountId");
            bool isCrocDatetChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocDatet");
            _isNameChange = (isActivityCategoryChanged || isAccountChanged || isCrocDatetChanged);
        }

        private void SetVisibleName()
        {
            Guid activityCategoryId = _entity.GetTypedColumnValue<Guid>("ActivityCategoryId");
            string activityCategoryName = GetVisibleValue("ActivityCategory", activityCategoryId, "Name");
            Guid accountId = _entity.GetTypedColumnValue<Guid>("AccountId");
            string accountName = GetVisibleValue("Account", accountId, "Name");
            DateTime date = _entity.GetTypedColumnValue<DateTime>("CrocDatet");
            string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
            string newName = $"{activityCategoryName} / {accountName}{dateStr}";
            _entityHelper.UpdateRecord("Activity", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "Title", newName } });
        }

        private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
        {
            Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
            return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
        }

        public override void OnInserting(object sender, EntityBeforeEventArgs e)
        {
            base.OnInserting(sender, e);
            InitSettings(sender);
            OnAllocatedChanged();
        }

        public override void OnInserted(object sender, EntityAfterEventArgs e)
        {
            base.OnInserted(sender, e);
            InitSettings(sender);
            CalculateAllocated();
        }

        public override void OnUpdating(object sender, EntityBeforeEventArgs e)
        {
            base.OnUpdating(sender, e);
            InitSettings(sender);
            OnAllocatedChanged();
            OnStatusChanged();
            OnRefusalDateChanged();
        }

        public override void OnUpdated(object sender, EntityAfterEventArgs e)
        {
            base.OnUpdated(sender, e);
            InitSettings(sender);
            CalculateAllocated();
            SendQuote();
            SendRefusalQuote();
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnColumnsChanged();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            Guid accountId = _entity.GetTypedColumnValue<Guid>("AccountId");
            if (accountId != Guid.Empty) UpdateIsSynchronize("Account", accountId);
            bool isEmptyTitle = _entity.GetTypedColumnValue<string>("Title").IsNullOrEmpty();
            if (_isNameChange && isEmptyTitle) { SetVisibleName(); }
        }
    }
}
