﻿using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;
using System;
using Common.Logging;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocDocumentRegistry")]
    public class CrocDocumentRegistryListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private static ILog _log = LogManager.GetLogger("Error");
        private bool _isExportMarkChange = false;
        private bool _isExternChange = false;
        private Guid _oldExternId = Guid.Empty;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnColumnChanged()
        {
            bool exportMarkNew = _entity.GetTypedColumnValue<bool>("CrocSendDeleteMark");
            bool exportMarkOld = _entity.GetTypedOldColumnValue<bool>("CrocSendDeleteMark");
            _isExportMarkChange = exportMarkNew != exportMarkOld && exportMarkNew != false;
            Guid externIdNew = _entity.GetTypedColumnValue<Guid>("CrocGuid");
            Guid externIdOld = _entity.GetTypedOldColumnValue<Guid>("CrocGuid");
            _isExternChange = externIdOld != Guid.Empty && externIdNew != externIdOld;
            if (_isExternChange) { _oldExternId = externIdOld; }
        }

        private void ExportDataTo1C()
        {
            if (_isExportMarkChange) 
            {
                Task.StartNewWithUserConnection<SendRegisterLoadingBackground, Dictionary<string, object>>(
                            new Dictionary<string, object> { { "DocumentId", _entity.PrimaryColumnValue } }
                     );
            }
        }

        private void DeleteUnactualRegisterLoading()
        {
            try
            {
                if (_isExternChange && _oldExternId != Guid.Empty)
                {
                    _entityHelper.DeleteEntity("CrocRegisterLoading", new Dictionary<string, object> { { "CrocGuid", _oldExternId } });
                }
            }
            catch (Exception ex) { _log.Error($"{ex.Message}\n{ex.StackTrace}"); }
        }


        public override void OnUpdating(object sender, EntityBeforeEventArgs e)
        {
            base.OnUpdating(sender, e);
            InitSettings(sender);
            OnColumnChanged();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            DeleteUnactualRegisterLoading();
            ExportDataTo1C();
        }
    }
}
