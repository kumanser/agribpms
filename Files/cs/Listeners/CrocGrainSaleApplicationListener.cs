﻿using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;
using Terrasoft.Configuration.Base;
using Terrasoft.Core.Tasks;
using System.Collections.Generic;
using Terrasoft.Core.DB;
using Terrasoft.Configuration.RightsService;

namespace Terrasoft.Configuration 
{
    [EntityEventListener(SchemaName = "CrocGrainSaleApplication")]
    public class CrocGrainSaleApplication : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isAccountChange = false;
        private static ILog _log = LogManager.GetLogger("Error");

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            Guid newAccount = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
            Guid oldAccount = _entity.GetTypedOldColumnValue<Guid>("CrocAccountId");
            _isAccountChange = newAccount != Guid.Empty && newAccount != oldAccount;
        }

        private void GetAutonumerationNumber()
        {
            CrocGenerateNumberHelper generateNumberHelper = new CrocGenerateNumberHelper(_userConnection);
            _entity.SetColumnValue("CrocName", generateNumberHelper.GetAutonumerationNumber(_entity.Schema));
        }

        private void UpdateRecordRights()
        {
            try
            {
                Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
                if (_isAccountChange)
                {
                    Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid> {accountId } },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                     );
                }
                else
                {
                    RightsHelper rightsHelper = new RightsHelper(_userConnection);
                    string rightSchemaName = rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
                    var rolesCurrent = rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
                    if (accountId == Guid.Empty && (rolesCurrent == null || rolesCurrent.Count < 1))
                    {
                        Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid>() },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                        );
                    }
                }
            }
            catch { }
        }

        private void UpdateIsSynchronize()
        {
            var update = new Update(_userConnection, "CrocGrainSaleApplication") as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(_entity.PrimaryColumnValue));
            update.Execute();
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }

        public override void OnInserting(object sender, EntityBeforeEventArgs e)
        {
            base.OnInserting(sender, e);
            InitSettings(sender);
            try { GetAutonumerationNumber(); }
            catch (Exception ex) { _log.Error($"{ex.Message}\n{ex.StackTrace}"); }
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateRecordRights();
            UpdateIsSynchronize();
        }
    }
}