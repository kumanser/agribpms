﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;
using System.Linq;
using Terrasoft.Configuration.RightsService;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocPortQuota")]
    public class CrocPortQuotaListner : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private RightsHelper _rightsHelper;
        private bool _isNameChange = false;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
            _rightsHelper = new RightsHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            bool isCrocTerminalChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocTerminalId");
            bool isCrocQuotaOwnerChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocQuotaOwnerId");
            bool isCrocDateChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocDate");
            _isNameChange = (isCrocTerminalChanged || isCrocQuotaOwnerChanged || isCrocDateChanged);
        }

        private void SetVisibleName()
        {
            Guid terminalId = _entity.GetTypedColumnValue<Guid>("CrocTerminalId");
            string terminalName = GetVisibleValue("CrocTerminal", terminalId, "Name");
            Guid quotaOwnerId = _entity.GetTypedColumnValue<Guid>("CrocQuotaOwnerId");
            string quotaOwnerName = GetVisibleValue("Account", quotaOwnerId, "Name");
            DateTime date = _entity.GetTypedColumnValue<DateTime>("CrocDate");
            string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
            string newName = $"Квота / {terminalName} / {quotaOwnerName}{dateStr}";
            _entityHelper.UpdateRecord("CrocPortQuota", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocName", newName } });
        }

        private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
        {
            Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
            return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
        }

        private void UpdateRecordRights()
        {
            try
            {
                Entity currentEntity = _entityHelper.ReadEntity(_entity.SchemaName, _entity.PrimaryColumnValue);
                bool hideRecord = currentEntity.GetTypedColumnValue<bool>("CrocHideRecord");
                if (hideRecord)
                {
                    ClearAllRights();
                }
                else
                {
                    string rightSchemaName = _rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
                    var rolesCurrent = _rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
                    if (rolesCurrent == null || rolesCurrent.Count < 1)
                    {
                        Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                                { "AccountId", new List<Guid>() },
                                { "SchemaName", _entity.SchemaName },
                                { "RecordId", _entity.PrimaryColumnValue }
                            }
                        );
                    }
                }
            }
            catch { }
        }

        private void ClearAllRights()
        {
            string rightSchemaName = _rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
            var rolesToDel = _rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
            rolesToDel.ForEach(oldRole => { oldRole.isDeleted = true; });
            _rightsHelper.DeleteRecordRights(rolesToDel.ToArray(), new Record { entitySchemaName = _entity.SchemaName, primaryColumnValue = _entity.PrimaryColumnValue.ToString() });
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }


        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            if (_isNameChange) { SetVisibleName(); }
            UpdateRecordRights();
        }
    }
}
