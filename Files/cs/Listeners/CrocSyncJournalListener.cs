﻿//CrocSyncJournalListener
using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.DB;
using Terrasoft.Core.Factories;
using Terrasoft.Core.Process;
using System.Linq;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocSyncJournal")]
    public class CrocSyncJournalListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void UpdateSynchronizedObjects()
        {
            Guid eventId = _entity.GetTypedColumnValue<Guid>("CrocEventId");
            Guid objectId = _entity.GetTypedColumnValue<Guid>("CrocObjectId");
            if(objectId != Guid.Empty && eventId == ConstCs.SyncTaskEvent.Ended)
            {
                Entity syncObj = _entityHelper.ReadEntity("CrocSyncObject", objectId);
                string schemaName = syncObj.GetTypedColumnValue<string>("CrocObjectName");
                string filter = syncObj.GetTypedColumnValue<string>("CrocFilterConfig");
                EntitySchema schema = _userConnection.EntitySchemaManager.GetInstanceByName(schemaName);
                var esq = new EntitySchemaQuery(schema);
                esq.PrimaryQueryColumn.IsAlwaysSelect = true;
                var primaryColumn = esq.AddColumn(schema.PrimaryColumn.Name).Name;
                AddFilterByJson(ref esq, filter);
                var entityCollection = esq.GetEntityCollection(_userConnection);
                if(entityCollection?.Count > 0)
                {
                    List<Guid> recordIds = entityCollection.Select(x => x.GetTypedColumnValue<Guid>(primaryColumn)).ToList();
                    QueryParameter[] param = new QueryParameter[recordIds.Count];
                    for (int i = 0; i < recordIds.Count; i++)
                    {
                        param[i] = new QueryParameter(recordIds.ElementAt(i));
                    }
                    try
                    {
                        var update = new Update(_userConnection, schemaName) as Update;
                        update.Set("CrocIsSynchronize", Column.Parameter(false));
                        update.Where("Id").In(param);
                        update.Execute();
                    }
                    catch { }
                }
            }    
        }

        private void AddFilterByJson(ref EntitySchemaQuery esq, string filterData)
        {
            if (string.IsNullOrEmpty(filterData)) { return; }
            var userConnectionArgument = new ConstructorArgument("userConnection", _userConnection);
            var processDataContractFilterConverter =
                ClassFactory.Get<IProcessDataContractFilterConverter>(userConnectionArgument);
            IEntitySchemaQueryFilterItem filterCollection =
                processDataContractFilterConverter.ConvertToEntitySchemaQueryFilterItem(esq, filterData);
            if (filterCollection != null) { esq.Filters.Add(filterCollection); }
        }

        public override void OnInserted(object sender, EntityAfterEventArgs e)
        {
            base.OnInserted(sender, e);
            InitSettings(sender);
            UpdateSynchronizedObjects();
        }
    }
}
