﻿using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;
using Terrasoft.Configuration.Base;
using Terrasoft.Core.Tasks;
using System.Collections.Generic;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocIntegrationLog")]
    public class CrocIntegrationLogListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isStatusChange = false;
        private static ILog _log = LogManager.GetLogger("Error");

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            Guid newStatusId = _entity.GetTypedColumnValue<Guid>("CrocStatusId");
            Guid oldStatusId = _entity.GetTypedOldColumnValue<Guid>("CrocStatusId");
            _isStatusChange = oldStatusId != newStatusId && newStatusId == ConstCs.ConstructorIntegrationLogStatus.SuccessId;
        }

        private void PostUpdatingOnSuccessIntegration()
        {
            if(_isStatusChange)
            {
                Entity currentEntity = _entityHelper.FindEntity(_entity.SchemaName, "Id", _entity.PrimaryColumnValue);
                string methodName = currentEntity.GetTypedColumnValue<string>("CrocIntegrationMethodCrocName");
                string schemaName = string.Empty;
                switch(methodName)
                {
                    case "SendLoan":
                        schemaName = "CrocLoan";
                        break;
                    case "SendPositionPlan":
                        schemaName = "CrocPositionPlan";
                        break;
                    case "SendPortQuota":
                        schemaName = "CrocPortQuota";
                        break;
                    case "SendRegisterLoading":
                        schemaName = "CrocRegisterLoading";
                        break;
                    case "SendRegisterUnloading":
                        schemaName = "CrocRegisterUnloading";
                        break;
                }
                if(!string.IsNullOrEmpty(schemaName))
                {
                    Task.StartNewWithUserConnection<PostUpdateDataOnIntegrationBackground, Dictionary<string, object>>( new Dictionary<string, object> { { "SchemaName", schemaName } } );
                }
            }
        }

        public override void OnUpdating(object sender, EntityBeforeEventArgs e)
        {
            base.OnUpdating(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            PostUpdatingOnSuccessIntegration();
        }
    }
}


