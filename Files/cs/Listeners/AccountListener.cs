﻿using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;
using Terrasoft.Configuration.Base;
using System.Collections.Generic;
using Terrasoft.Core.DB;
using Terrasoft.Core.Configuration;
using Terrasoft.Configuration.RightsService;
using Terrasoft.Core.Tasks;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "Account")]
    public class AccountListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private static ILog _log = LogManager.GetLogger("Error");

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void AddAccountRole()
        {
            try
            {
                _entityHelper.InsertEntity("CrocEnterpriseRightRecord", new Dictionary<string, object>
                { 
                    { "CrocAccountId", _entity.PrimaryColumnValue }, 
                    { "Name", _entity.PrimaryDisplayColumnValue } 
                });
            }
            catch(Exception ex) { _log.Error($"{ex.Message}\n{ex.StackTrace}"); }
        }

        private void UpdateIsSynchronize()
        {
            var update = new Update(_userConnection, "Account") as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(_entity.PrimaryColumnValue));
            update.Execute();
        }

        private void UpdateRecordRights()
        {
            try
            {
                RightsHelper rightsHelper = new RightsHelper(_userConnection);
                string rightSchemaName = rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
                var rolesCurrent = rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
                if (rolesCurrent == null || rolesCurrent.Count < 1)
                {
                    Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                        new Dictionary<string, object>
                        {
                            { "AccountId", new List<Guid>() },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                        }
                    );
                }
            }
            catch { }
        }

        public override void OnInserted(object sender, EntityAfterEventArgs e)
        {
            base.OnInserted(sender, e);
            InitSettings(sender);
            //AddAccountRole();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateRecordRights();
            UpdateIsSynchronize();
        }
    }
}

