﻿using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;
using Terrasoft.Configuration.Base;
using Terrasoft.Core.Tasks;
using System.Collections.Generic;
using Terrasoft.Core.DB;
using Terrasoft.Configuration.RightsService;
using System.Linq;
using Terrasoft.Core.Configuration;
using Terrasoft.Configuration.OneCIntegration;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocDocument")]
    public class CrocDocumentListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isAccountChange = false;
        private static ILog _log = LogManager.GetLogger("Error");
        private bool _isNameChange = false;
        private bool _isAcceptedChange = false;
        private bool _isGU12StatusChange = false;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            Guid newAccount = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
            Guid oldAccount = _entity.GetTypedOldColumnValue<Guid>("CrocAccountId");
            Guid newPort = _entity.GetTypedColumnValue<Guid>("CrocPortId");
            Guid oldPort = _entity.GetTypedOldColumnValue<Guid>("CrocPortId");
            _isAccountChange = (newAccount != Guid.Empty && newAccount != oldAccount) ||
                (newPort != Guid.Empty && newPort != oldPort);
            bool isCrocAccountChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocAccountId");
            bool isCrocDocumentTypeChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocDocumentTypeId");
            bool isCrocNumberChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocNumber");
            bool isCrocGU12NumberChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocGU12Number");
            bool isCrocTerminalChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocTerminalId");
            _isNameChange = (isCrocAccountChanged || isCrocDocumentTypeChanged || isCrocNumberChanged || isCrocTerminalChanged || isCrocGU12NumberChanged);
            _isAcceptedChange = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocIsAccepted") && _entity.GetTypedColumnValue<bool>("CrocIsAccepted") == true;
            Guid guStatusNewId = _entity.GetTypedColumnValue<Guid>("CrocGU12StatusId");
            Guid guStatusOldId = _entity.GetTypedOldColumnValue<Guid>("CrocGU12StatusId");
            _isGU12StatusChange = guStatusNewId != guStatusOldId && guStatusNewId != Guid.Empty;
        }

        private void SetVisibleName()
        {
            string newName;
            Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
            string accountName = GetVisibleValue("Account", accountId, "Name");
            Guid documentTypeId = _entity.GetTypedColumnValue<Guid>("CrocDocumentTypeId");
            string documentTypeName = GetVisibleValue("CrocDocumentType", documentTypeId, "Name");
            if (documentTypeId == ConstCs.CrocDocument.Type.GU12)
            {
                Guid terminalId = _entity.GetTypedColumnValue<Guid>("CrocTerminalId");
                string terminalName = GetVisibleValue("CrocTerminal", terminalId, "Name");
                string numberGU12 = _entity.GetTypedColumnValue<string>("CrocGU12Number");
                newName = $"{documentTypeName} / {accountName} / {terminalName} / {numberGU12}";
            }
            else
            {
                string number = _entity.GetTypedColumnValue<string>("CrocNumber");
                newName = $"{documentTypeName} / {accountName} / {number}";
            }
            _entityHelper.UpdateRecord("CrocDocument", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocVisibleName", newName } });
        }

        private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
        {
            Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
            return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
        }

        private void GetAutonumerationNumber()
        {
            CrocGenerateNumberHelper generateNumberHelper = new CrocGenerateNumberHelper(_userConnection);
            _entity.SetColumnValue("CrocNumber", generateNumberHelper.GetAutonumerationNumber(_entity.Schema));
        }

        private void UpdateRecordRights()
        {
            try
            {
                List<Guid> accounts = new List<Guid>();
                Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
                Guid portId = _entity.GetTypedColumnValue<Guid>("CrocPortId");
                if (accountId != Guid.Empty) { accounts.Add(accountId); }
                if (portId != Guid.Empty && !accounts.Contains(portId)) { accounts.Add(portId); }
                if (_isAccountChange)
                {
                    Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", accounts },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                     );
                }
                else
                {
                    RightsHelper rightsHelper = new RightsHelper(_userConnection);
                    string rightSchemaName = rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
                    var rolesCurrent = rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
                    if (accounts.Count < 1 && (rolesCurrent == null || rolesCurrent.Count < 1))
                    {
                        Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid>() },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                        );
                    }
                }
            }
            catch { }
        }

        private void UpdateIsSynchronize(string entityName, Guid entityId)
        {
            var update = new Update(_userConnection, entityName) as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(entityId));
            update.Execute();
        }

        private void UpdateSynchronize()
        {
            UpdateIsSynchronize("CrocDocument", _entity.PrimaryColumnValue);
            Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
            if (accountId != Guid.Empty) UpdateIsSynchronize("Account", accountId);
        }

        private void SendAccreditationOnDocument()
        {
            try
            {
                if(_isAcceptedChange)
                {
                    Task.StartNewWithUserConnection<SendAccreditationOnDocumentBackground, Dictionary<string, object>>(
                            new Dictionary<string, object> { { "DocumentId", _entity.PrimaryColumnValue } }
                     );
                }
            }
            catch { }
        }

        private void SendGu12()
        {
            try 
            {
                if(_isGU12StatusChange) 
                {
                    Entity currentEntity = _entityHelper.ReadEntity(_entity.SchemaName, _entity.PrimaryColumnValue);
                    Guid documentTypeId = currentEntity.GetTypedColumnValue<Guid>("CrocDocumentTypeId");
                    Guid guStatusId = currentEntity.GetTypedColumnValue<Guid>("CrocGU12StatusId");
                    if(documentTypeId == ConstCs.CrocDocument.Type.GU12 && 
                        (guStatusId == ConstCs.CrocDocument.ApplicationGU12Status.Cofirmed || guStatusId == ConstCs.CrocDocument.ApplicationGU12Status.Canceled))
                    {
                        OneCIntegrationHelper cHelper = new OneCIntegrationHelper(_userConnection.AppConnection.SystemUserConnection);
                        cHelper.SendGU12(_entity.PrimaryColumnValue);
                    }
                }
            }
            catch { }
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }

        public override void OnInserting(object sender, EntityBeforeEventArgs e)
        {
            base.OnInserting(sender, e);
            InitSettings(sender);
            try { GetAutonumerationNumber(); }
            catch (Exception ex) { _log.Error($"{ex.Message}\n{ex.StackTrace}"); }
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateRecordRights();
            SendAccreditationOnDocument();
            UpdateSynchronize();
            if (_isNameChange) { SetVisibleName(); }
            SendGu12();
        }
    }
}