﻿using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Common;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;

namespace Terrasoft.Configuration
{

	[EntityEventListener(SchemaName = "CrocAccountInDistribution")]
	public class CrocAccountInDistributionListner : BaseEntityEventListener
	{
		private UserConnection _userConnection;
		private Entity _entity;
		private CrocEntityHelper _entityHelper;

		private void InitSettings(object sender)
		{
			_entity = (Entity)sender;
			_userConnection = _entity.UserConnection;
			_entityHelper = new CrocEntityHelper(_userConnection);
		}

		private void DeleteContactByAccount()
		{
			Guid distributionId = _entity.GetTypedColumnValue<Guid>("CrocDistributionId");
			Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
			Entity entity = _entityHelper.FindEntity("Account", "Id", accountId);
			Guid contactId = entity.GetTypedColumnValue<Guid>("PrimaryContactId");
			_entityHelper.DeleteEntity("CrocContactInDistribution", new Dictionary<string, object>()
				{
					{"CrocContact.Id", contactId },
					{"CrocDistribution.Id", distributionId },
				});
			string messageText = "{\"distributionId\": \"" + distributionId.ToString() + "\"}";
			string sender = "ReloadContactIndistributionDetail";
			CrocMsgChannelUtilities.PostMessageToAll(sender, messageText);
		}

		public override void OnDeleting(object sender, EntityBeforeEventArgs e)
		{
			base.OnDeleting(sender, e);
			InitSettings(sender);
			DeleteContactByAccount();
		}
	}
}
