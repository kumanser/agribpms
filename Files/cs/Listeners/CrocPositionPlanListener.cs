﻿using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;
using Terrasoft.Configuration.Base;
using Terrasoft.Core.Tasks;
using System.Collections.Generic;
using Terrasoft.Configuration.RightsService;
using System.Linq;
using Terrasoft.Core.DB;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocPositionPlan")]
    public class CrocPositionPlanListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isAccountChange = false;
        private bool _isNameChange = false;
        private static ILog _log = LogManager.GetLogger("Error");

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            Guid newAccount = _entity.GetTypedColumnValue<Guid>("CrocPortId");
            Guid oldAccount = _entity.GetTypedOldColumnValue<Guid>("CrocPortId");
            _isAccountChange = newAccount != Guid.Empty && newAccount != oldAccount;
            bool isCrocExporterChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocExporterId");
            bool isCrocStartPlanDateChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocStartPlanDate");
            bool isCrocEndPlanDateChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocEndPlanDate");
            _isNameChange = (isCrocExporterChanged || isCrocStartPlanDateChanged || isCrocEndPlanDateChanged);
        }

        private void SetVisibleName()
        {
            Guid exporterId = _entity.GetTypedColumnValue<Guid>("CrocExporterId");
            string exporterName = GetVisibleValue("Account", exporterId, "Name");
            DateTime startDate = _entity.GetTypedColumnValue<DateTime>("CrocStartPlanDate");
            string startDateStr = (startDate == new DateTime()) ? "" : $" / {startDate:dd.MM.yyyy}";
            DateTime endDate = _entity.GetTypedColumnValue<DateTime>("CrocEndPlanDate");
            string endDateStr = (endDate == new DateTime()) ? "" : $" / {endDate:dd.MM.yyyy}";
            string newName = $"План позиций / {exporterName}{startDateStr}{endDateStr}";
            _entityHelper.UpdateRecord("CrocPositionPlan", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocName", newName } });
        }

        private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
        {
            Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
            return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
        }

        private void UpdateRecordRights()
        {
            try
            {
                Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocPortId");
                if (_isAccountChange)
                {
                    Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid> { accountId } },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                     );
                }
                else
                {
                    RightsHelper rightsHelper = new RightsHelper(_userConnection);
                    string rightSchemaName = rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
                    var rolesCurrent = rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
                    if (accountId == Guid.Empty && (rolesCurrent == null || rolesCurrent.Count < 1))
                    {
                        Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid>() },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                        );
                    }
                }
            }
            catch { }
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateRecordRights();
            if (_isNameChange) { SetVisibleName(); }
        }
    }
}
