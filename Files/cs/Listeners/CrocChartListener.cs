﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Linq;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Configuration.OneCIntegration;
using Terrasoft.Core.Tasks;
using Terrasoft.Core.DB;
using Terrasoft.Configuration.RightsService;
using Amazon.Runtime.Internal.Transform;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocChart")]
    public class CrocChartListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isRequestChanged = false;
        private bool _isStatusChangedForExport = false;
        private bool _isAccountChange = false;
        private bool _isStatusChangedForCalc = false;
        private bool _isNameChange = false;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            Guid newAccount = _entity.GetTypedColumnValue<Guid>("CrocWarehouseLoadingId");
            Guid oldAccount = _entity.GetTypedOldColumnValue<Guid>("CrocWarehouseLoadingId");
            _isAccountChange = newAccount != Guid.Empty && newAccount != oldAccount;
            bool isCrocTransportTypeChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocTransportTypeId");
            bool isCrocContractChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocContractId");
            bool isCrocDeliveryDateFromChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CreatedOn");
            bool isCrocNumberChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocNumber");
            _isNameChange = (isCrocTransportTypeChanged || isCrocContractChanged || isCrocDeliveryDateFromChanged || isCrocNumberChanged);
        }

        private void OnStatusChange()
        {
            List<Guid> exportStatuses = new List<Guid>
            { 
                { ConstCs.CrocChart.Status.ApprovedId }, 
                { ConstCs.CrocChart.Status.WaitingExporterApprovementId },
                { ConstCs.CrocChart.Status.ProviderApprovementId },
                { ConstCs.CrocChart.Status.ProviderRejectedId }
            };
            Guid newStatusId = _entity.GetTypedColumnValue<Guid>("CrocChartStatusId");
            Guid oldStatusId = _entity.GetTypedOldColumnValue<Guid>("CrocChartStatusId");
            _isStatusChangedForExport = newStatusId != oldStatusId && exportStatuses.Contains(newStatusId);
            _isStatusChangedForCalc = newStatusId != oldStatusId && newStatusId == ConstCs.CrocChart.Status.ApprovedId;
        }

        private void SetIsRequestChanged() 
        {
            Guid chartRequest = _entity.GetTypedColumnValue<Guid>("CrocRequestId");
            Guid chartRequestOld = _entity.GetTypedOldColumnValue<Guid>("CrocRequestId");
            _isRequestChanged = chartRequest != chartRequestOld && chartRequest != Guid.Empty;
        }

        private void SetTotalTransportNumber(Guid chartId) 
        {
            if (_isStatusChangedForCalc) 
            {
                int sumPlanTransport = 0;
                List<Entity> deliverySchedules = _entityHelper.FindEntities("CrocDeliverySchedule", new Dictionary<string, object>()
                { { "CrocChart.Id", chartId } }).ToList();
                if (deliverySchedules?.Count > 0) 
                {
                    deliverySchedules.ForEach(entity => {
                        int deliveryTransportPlan = entity.GetTypedColumnValue<int>("CrocDeliveryTransportPlan");
                        sumPlanTransport += deliveryTransportPlan;
                    });
                }
                if(sumPlanTransport > 0)
                {
                    decimal totalWeight = _entity.GetTypedColumnValue<decimal>("CrocWeight");
                    decimal averageWeight = totalWeight / sumPlanTransport;
                    _entity.SetColumnValue("CrocTotalTransportNumber", sumPlanTransport);
                    _entity.SetColumnValue("CrocAverageWeightTransport", averageWeight);
                    _entity.Save();
                }
            }
        }

        private void SetLoadingScheduleRequest(Guid chartId)
        {
            if (_isRequestChanged) 
            {
                Guid requestId = _entity.GetTypedColumnValue<Guid>("CrocRequestId");
                if (requestId != Guid.Empty)
                {
                    List<Entity> loadingSchedules = _entityHelper.FindEntities("CrocLoadingSchedule", new Dictionary<string, object>()
                    { { "CrocChart.Id", chartId } }).ToList();
                    if (loadingSchedules?.Count > 0)
                    {
                        loadingSchedules.ForEach(entity =>
                        {
                            entity.SetColumnValue("CrocRequestId", requestId);
                            entity.Save();
                        });
                    }
                }
            }
        }

        private void ExportChart()
        {
            if (_isStatusChangedForExport)
            {
                try
                {
                    Guid recordId = _entity.PrimaryColumnValue;
                    Guid deliveryConditionId = _entity.GetTypedColumnValue<Guid>("CrocDeliveryConditionId");
                    Guid transportTypeId = _entity.GetTypedColumnValue<Guid>("CrocTransportTypeId");
                    int tableType = (deliveryConditionId == ConstCs.CrocDeliveryCondition.FCA || deliveryConditionId == ConstCs.CrocDeliveryCondition.EXW)
                        || transportTypeId == ConstCs.CrocTransporType.Railway ? 1 : 2;

                    OneCIntegrationHelper oneHelper = new OneCIntegrationHelper(_userConnection);
                    List<Entity> loadings = _entityHelper.FindEntities("CrocLoadingSchedule", new Dictionary<string, object> { { "CrocChart.Id", recordId } }).ToList();
                    if (loadings?.Count > 0 && tableType == 1) 
                    {
                        Task.StartNewWithUserConnection<SendChartExportBackground, Dictionary<string, object>>(
                            new Dictionary<string, object> { { "DocumentId", _entity.PrimaryColumnValue }, { "TypeInd", 1 } }
                        ); 
                    } //График погрузки
                    List<Entity> schedules = _entityHelper.FindEntities("CrocDeliverySchedule", new Dictionary<string, object> { { "CrocChart.Id", recordId } }).ToList();
                    if (schedules?.Count > 0 && tableType == 2) 
                    {
                        Task.StartNewWithUserConnection<SendChartExportBackground, Dictionary<string, object>>(
                            new Dictionary<string, object> { { "DocumentId", _entity.PrimaryColumnValue }, { "TypeInd", 2 } }
                        );
                    } //График поставки
                }
                catch { }
            }
        }

        private Guid GetAccount()
        {
            Guid accountId = Guid.Empty;
            Guid warehouseId = _entity.GetTypedColumnValue<Guid>("CrocWarehouseLoadingId");
            if(warehouseId != Guid.Empty) 
            {
                Entity terminalEntity = _entityHelper.ReadEntity("CrocTerminal", warehouseId);
                accountId = terminalEntity.GetTypedColumnValue<Guid>("CrocAccountId"); 
            }
            return accountId;
        }

        private void UpdateRecordRights()
        {
            try
            {
                Guid accountId = GetAccount();
                if (_isAccountChange)
                {
                    if (accountId != Guid.Empty)
                    {
                        Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid> { accountId } },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                        );
                    }
                }
                else
                {
                    RightsHelper rightsHelper = new RightsHelper(_userConnection);
                    string rightSchemaName = rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
                    var rolesCurrent = rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
                    if (accountId == Guid.Empty && (rolesCurrent == null || rolesCurrent.Count < 1))
                    {
                        Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid>() },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                        );
                    }
                }
            }
            catch { }
        }

        private void UpdateIsSynchronize()
        {
            var update = new Update(_userConnection, "CrocChart") as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(_entity.PrimaryColumnValue));
            update.Execute();
        }

        private void SetVisibleName()
        {
            Guid transportTypeId = _entity.GetTypedColumnValue<Guid>("CrocTransportTypeId");
            string transportTypeName = GetVisibleValue("CrocTransportType", transportTypeId, "Name");
            Guid contractId = _entity.GetTypedColumnValue<Guid>("CrocContractId");
            Entity contractEntity = _entityHelper.ReadEntity("CrocContract", contractId);
            Guid accountId = (contractEntity != null) ? contractEntity.GetTypedColumnValue<Guid>("CrocAccountId") : new Guid();
            string accountName = GetVisibleValue("Account", accountId, "Name");
            DateTime date = _entity.GetTypedColumnValue<DateTime>("CreatedOn");
            string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
            string number = _entity.GetTypedColumnValue<string>("CrocNumber");
            string newName = $"График / {transportTypeName} / {accountName}{dateStr} / {number}";
            _entityHelper.UpdateRecord("CrocChart", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocVisibleName", newName } });
        }

        private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
        {
            Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
            return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
        }

        private void SetProvider()
        {
            Guid contractId = _entity.GetTypedColumnValue<Guid>("CrocContractId");
            Entity entity = _entityHelper.ReadEntity("CrocContract", contractId);
            Guid providerId = entity.GetTypedColumnValue<Guid>("CrocAccountId");
            _entityHelper.UpdateRecord("CrocChart", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocProviderId", providerId } });
        }

        public override void OnInserted(object sender, EntityAfterEventArgs e)
        {
            base.OnInserted(sender, e);
            InitSettings(sender);
            SetProvider();
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }

        public override void OnUpdating(object sender, EntityBeforeEventArgs e)
        {
            base.OnUpdating(sender, e);
            InitSettings(sender);
            SetIsRequestChanged();
            OnStatusChange();
        }

        public override void OnUpdated(object sender, EntityAfterEventArgs e)
        {
            base.OnUpdated(sender, e);
            InitSettings(sender);
            SetLoadingScheduleRequest(e.PrimaryColumnValue);
            SetTotalTransportNumber(e.PrimaryColumnValue);
            ExportChart();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateRecordRights();
            UpdateIsSynchronize();
            if (_isNameChange) { SetVisibleName(); }
        }
    }
}
