﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;
using Terrasoft.Core.DB;
using System.Linq;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocAcceptanceStop")]
    public class CrocAcceptanceStopListner : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isNameChange = false;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            bool isCrocTerminalChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocTerminalId");
            bool isCrocDateChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocStopDate");
            bool isCrocNumberChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocNumber");
            _isNameChange = (isCrocTerminalChanged || isCrocDateChanged || isCrocNumberChanged);
        }

        private void SetVisibleName()
        {
            Guid terminalId = _entity.GetTypedColumnValue<Guid>("CrocTerminalId");
            string terminalName = GetVisibleValue("CrocTerminal", terminalId, "Name");
            DateTime date = _entity.GetTypedColumnValue<DateTime>("CrocStopDate");
            string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
            string number = _entity.GetTypedColumnValue<string>("CrocNumber");
            string newName = $"Остановка приемки / {terminalName}{dateStr} / {number}";
            _entityHelper.UpdateRecord("CrocAcceptanceStop", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocName", newName } });
        }

        private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
        {
            Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
            return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }


        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            if (_isNameChange) { SetVisibleName(); }
        }
    }
}

