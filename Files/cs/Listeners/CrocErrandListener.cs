﻿using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Common;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;

namespace Terrasoft.Configuration
{

	[EntityEventListener(SchemaName = "CrocErrand")]
	public class CrocErrandListener : BaseEntityEventListener
	{
		private UserConnection _userConnection;
		private Entity _entity;
		private CrocEntityHelper _entityHelper;
		private bool _isNameChange = false;
		private bool _isStatusChange = false;

		private void InitSettings(object sender)
		{
			_entity = (Entity)sender;
			_userConnection = _entity.UserConnection;
			_entityHelper = new CrocEntityHelper(_userConnection);
		}

		private void CalculateTotalVolumeTon(bool isDelete = false)
		{
			var isCrocLoadingFactTonChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocLoadingFactTon");
			var isCrocUnloadingTonChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocUnloadingTon");
			if (isCrocLoadingFactTonChanged || isDelete)
			{
				decimal sumTon = GetTonSum("CrocLoadingFactTon", isDelete);
				SetTotalVolumeLoadingTon("CrocTotalVolumeLoadingTon", sumTon);
			}
			if (isCrocUnloadingTonChanged || isDelete)
			{
				decimal sumTon = GetTonSum("CrocUnloadingTon", isDelete);
				SetTotalVolumeLoadingTon("CrocTotalVolumeUnloadingTon", sumTon);
			}
		}

		private decimal GetTonSum(string column, bool isDelete)
		{
			decimal sumFactTon = 0;
			Guid requestId = _entity.GetTypedColumnValue<Guid>("CrocRequestId");
			if (requestId == Guid.Empty) return sumFactTon;
			EntityCollection collectionFactTon = _entityHelper.FindEntities("CrocErrand", new Dictionary<string, object>()
			{
				{"CrocRequest.Id", requestId }
			});
			collectionFactTon.ForEach(entity =>
			{
				Guid errandId = entity.GetTypedColumnValue<Guid>("Id");
				Guid id = _entity.GetTypedColumnValue<Guid>("Id");
				if (!isDelete || (errandId != id)) sumFactTon += entity.GetTypedColumnValue<decimal>(column);
			});
			return sumFactTon;
		}

		private void SetTotalVolumeLoadingTon(string column, decimal sum)
		{
			Guid requestId = _entity.GetTypedColumnValue<Guid>("CrocRequestId");
			if (requestId == Guid.Empty) return;
			_entityHelper.UpdateEntity("CrocRequest", requestId, new Dictionary<string, object>() {
				{ column,  sum}
			});
		}

		private void OnColumnsChanged()
		{
			var isCrocLoadingFactTonChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocLoadingFactTon");
			var isCrocUnloadingTonChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocUnloadingTon");
			var isCrocLoadingPlanTonChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocLoadingPlanTon");
			if (isCrocUnloadingTonChanged || isCrocLoadingFactTonChanged) { CalculateDeviation(); }
			if (isCrocUnloadingTonChanged || isCrocLoadingFactTonChanged || isCrocLoadingPlanTonChanged) { CalculateExecution(); }
			bool isCrocAccountChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocAccountId");
			bool isCrocDateChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocDate");
			bool isCrocNumberChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocNumber");
			_isNameChange = (isCrocAccountChanged || isCrocDateChanged || isCrocNumberChanged);
			bool isCrocStatusChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocStatusId");
			_isStatusChange = isCrocStatusChanged;
		}

		private void AddVehicleRegister()
		{
			Guid statusId = _entity.GetTypedColumnValue<Guid>("CrocStatusId");
			if (statusId != ConstCs.CrocOrderStatus.Completed) return;
			EntityCollection collectionRegisterLoading = _entityHelper.FindEntities("CrocRegisterLoading",
				new Dictionary<string, object>() { { "CrocAssignment.Id", _entity.PrimaryColumnValue } });
			EntityCollection collectionRegisterUnloading = _entityHelper.FindEntities("CrocRegisterUnloading",
				new Dictionary<string, object>() { { "CrocAssignment.Id", _entity.PrimaryColumnValue } });
			collectionRegisterLoading.ForEach(entity =>
			{
				string ttn = entity.GetTypedColumnValue<string>("CrocTTN");
				string transportNumber = entity.GetTypedColumnValue<string>("CrocTransportNumber");
				DateTime shippingDate = entity.GetTypedColumnValue<DateTime>("CrocShippingDate");
				decimal grossWeightTon = entity.GetTypedColumnValue<decimal>("CrocGrossWeightTon");
				DateTime uploadDate = collectionRegisterUnloading.Where(x => x.GetTypedColumnValue<string>("CrocNumberTransport") == transportNumber
					&& x.GetTypedColumnValue<string>("CrocNumberConsignment") == ttn)
					.Select(x => x.GetTypedColumnValue<DateTime>("CrocUploadDate")).FirstOrDefault();
				decimal netWeightAcceptanceTon = collectionRegisterUnloading.Where(x => x.GetTypedColumnValue<string>("CrocNumberTransport") == transportNumber
					&& x.GetTypedColumnValue<string>("CrocNumberConsignment") == ttn)
					.Select(x => x.GetTypedColumnValue<decimal>("CrocNetWeightAcceptanceTon")).FirstOrDefault();
				decimal deviationTon = grossWeightTon - netWeightAcceptanceTon;
				_entityHelper.InsertEntity("CrocVehicleRegister", new Dictionary<string, object> {
					{ "CrocErrandId", _entity.PrimaryColumnValue },
					{ "CrocVehicle", transportNumber },
					{ "CrocNumberTTN", ttn },
					{ "CrocShipmentDate", shippingDate },
					{ "CrocUploadDate", uploadDate },
					{ "CrocLoadingVolumeTon", grossWeightTon },
					{ "CrocUnloadingVolumeTon", netWeightAcceptanceTon },
					{ "CrocDeviationTon", deviationTon },
				});
			});
		}

		private void CalculateExecution()
		{
			float result = 0;
			var loadingFact = _entity.GetTypedColumnValue<float>("CrocLoadingFactTon");
			var loadingPlan = _entity.GetTypedColumnValue<float>("CrocLoadingPlanTon");
			if (loadingPlan != 0)
			{
				result = (loadingFact / loadingPlan) * 100;
			}
			_entity.SetColumnValue("CrocExecution", result);
		}

		private void CalculateDeviation()
		{
			float result = 0;
			var loadingFact = _entity.GetTypedColumnValue<float>("CrocLoadingFactTon");
			var unloadingFact = _entity.GetTypedColumnValue<float>("CrocUnloadingTon");
			if (loadingFact != 0)
			{
				result = (loadingFact - unloadingFact);
			}
			_entity.SetColumnValue("CrocDeviation", result);
		}

		private void UpdateIsSynchronize()
		{
			var update = new Update(_userConnection, "CrocErrand") as Update;
			update.Set("CrocIsSynchronize", Column.Parameter(true));
			update.Where("Id").IsEqual(Column.Parameter(_entity.PrimaryColumnValue));
			update.Execute();
		}

		private void SetVisibleName()
		{
			Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
			string accountName = GetVisibleValue("Account", accountId, "Name");
			DateTime date = _entity.GetTypedColumnValue<DateTime>("CrocDate");
			string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
			string number = _entity.GetTypedColumnValue<string>("CrocNumber");
			string newName = $"Поручение на перевозку / {accountName}{dateStr} / {number}";
			_entityHelper.UpdateRecord("CrocErrand", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocVisibleName", newName } });
		}

		private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
		{
			Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
			return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
		}

		public override void OnSaved(object sender, EntityAfterEventArgs e)
		{
			base.OnSaved(sender, e);
			InitSettings(sender);
			UpdateIsSynchronize();
			if (_isNameChange) { SetVisibleName(); }
			if (_isStatusChange) AddVehicleRegister();
		}

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
		{
			base.OnSaving(sender, e);
			InitSettings(sender);
			OnColumnsChanged();
		}

		public override void OnDeleting(object sender, EntityBeforeEventArgs e)
		{
			base.OnDeleting(sender, e);
			InitSettings(sender);
			CalculateTotalVolumeTon(true);
		}

		public override void OnUpdated(object sender, EntityAfterEventArgs e)
		{
			base.OnUpdated(sender, e);
			InitSettings(sender);
			CalculateTotalVolumeTon();
		}

		public override void OnInserted(object sender, EntityAfterEventArgs e)
		{
			base.OnInserted(sender, e);
			InitSettings(sender);
			CalculateTotalVolumeTon();
		}
	}
}