﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Linq;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;
using Terrasoft.Core.DB;
using Terrasoft.Configuration.RightsService;
using NPOI.HSSF.Record;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocContract")]
    public class CrocContractListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isManagerChange = false;
        private bool _isSTOChange = false;
        private bool _isAccountChange = false;
        private bool _isNameChange = false;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeManagers()
        {
            Guid newAssignedManager = _entity.GetTypedColumnValue<Guid>("CrocAssignedManagerId");
            Guid oldAssignedManager = _entity.GetTypedOldColumnValue<Guid>("CrocAssignedManagerId");
            Guid newSTOManager = _entity.GetTypedColumnValue<Guid>("CrocAssignedSTOId");
            Guid oldSTOManager = _entity.GetTypedOldColumnValue<Guid>("CrocAssignedSTOId");
            _isManagerChange = newAssignedManager != Guid.Empty && newAssignedManager != oldAssignedManager;
            _isSTOChange = newSTOManager != Guid.Empty && oldSTOManager != newSTOManager;
            Guid newAccount = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
            Guid oldAccount = _entity.GetTypedOldColumnValue<Guid>("CrocAccountId");
            _isAccountChange = newAccount != Guid.Empty && newAccount != oldAccount;
        }

        private void AfterChangeManagers()
        {
            if (_isAccountChange || _isManagerChange || _isSTOChange)
            {
                try
                {
                    if (_isManagerChange)
                    {
                        Guid assignedManager = _entity.GetTypedColumnValue<Guid>("CrocAssignedManagerId");
                        List<Entity> managers = _entityHelper.FindEntities("CrocContractExporterEmployee", new Dictionary<string, object>
                        {
                            { "CrocSA.Id", _entity.PrimaryColumnValue },
                            { "CrocRole.Id", ConstCs.CrocExporterEmployeeRoles.ManagerPurchaseId }
                        }).ToList();
                        if (managers?.Count > 0)
                        {
                            if (!managers.Any(x => x.GetTypedColumnValue<Guid>("CrocContactId") == assignedManager))
                            {
                                Entity managerEntity = managers.FirstOrDefault();
                                managerEntity.SetColumnValue("CrocContactId", assignedManager);
                                managerEntity.Save(true);
                            }
                        }
                        else
                        {
                            _entityHelper.InsertEntity("CrocContractExporterEmployee", new Dictionary<string, object>
                            {
                                { "CrocSAId", _entity.PrimaryColumnValue },
                                { "CrocRoleId", ConstCs.CrocExporterEmployeeRoles.ManagerPurchaseId },
                                { "CrocContactId", assignedManager}
                            });
                        }
                    }
                }
                catch { }
                try
                {
                    if (_isSTOChange)
                    {
                        Guid stoManager = _entity.GetTypedColumnValue<Guid>("CrocAssignedSTOId");
                        List<Entity> managers = _entityHelper.FindEntities("CrocContractExporterEmployee", new Dictionary<string, object>
                        {
                            { "CrocSA.Id", _entity.PrimaryColumnValue },
                            { "CrocRole.Id", ConstCs.CrocExporterEmployeeRoles.STOId }
                        }).ToList();
                        if (managers?.Count > 0)
                        {
                            if (!managers.Any(x => x.GetTypedColumnValue<Guid>("CrocContactId") == stoManager))
                            {
                                Entity managerEntity = managers.FirstOrDefault();
                                managerEntity.SetColumnValue("CrocContactId", stoManager);
                                managerEntity.Save(true);
                            }
                        }
                        else
                        {
                            _entityHelper.InsertEntity("CrocContractExporterEmployee", new Dictionary<string, object>
                            {
                                { "CrocSAId", _entity.PrimaryColumnValue },
                                { "CrocRoleId", ConstCs.CrocExporterEmployeeRoles.STOId },
                                { "CrocContactId", stoManager}
                            });
                        }
                    }
                }
                catch { }
                try
                {
                    if (_isAccountChange)
                    {
                        Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
                        List<Guid> roles = new List<Guid>
                        {
                            ConstCs.CrocExporterEmployeeRoles.LogisticAvtoId,
                            ConstCs.CrocExporterEmployeeRoles.LogisticRailId,
                            ConstCs.CrocExporterEmployeeRoles.ManagerVEDId,
                            ConstCs.CrocExporterEmployeeRoles.SurvayId,
                            ConstCs.CrocExporterEmployeeRoles.ElevatorId
                        };
                        List<Entity> accountManagers = _entityHelper.FindEntities("CrocExporterEmployee", new Dictionary<string, object>
                        {
                            { "CrocAccount.Id", accountId }
                        }).ToList();
                        if (accountManagers?.Count > 0)
                        {
                            foreach (Guid roleId in roles)
                            {
                                List<Entity> managerInRole = accountManagers.Where(x => x.GetTypedColumnValue<Guid>("CrocRoleId") == roleId).ToList();
                                if (managerInRole?.Count > 0)
                                {
                                    List<Guid> contacts = managerInRole.Select(x => x.GetTypedColumnValue<Guid>("CrocContactId")).Distinct().ToList();
                                    foreach (Guid contactId in contacts)
                                    {
                                        if (contactId != Guid.Empty)
                                        {
                                            List<Entity> managers = _entityHelper.FindEntities("CrocContractExporterEmployee", new Dictionary<string, object>
                                            {
                                                { "CrocSA.Id", _entity.PrimaryColumnValue },
                                                { "CrocRole.Id", roleId }
                                            }).ToList();
                                            if (managers?.Count > 0)
                                            {
                                                if (!managers.Any(x => x.GetTypedColumnValue<Guid>("CrocContactId") == contactId))
                                                {
                                                    Entity managerEntity = managers.FirstOrDefault();
                                                    managerEntity.SetColumnValue("CrocContactId", contactId);
                                                    managerEntity.Save(true);
                                                }
                                            }
                                            else
                                            {
                                                _entityHelper.InsertEntity("CrocContractExporterEmployee", new Dictionary<string, object>
                                                {
                                                    { "CrocSAId", _entity.PrimaryColumnValue },
                                                    { "CrocRoleId", roleId },
                                                    { "CrocContactId", contactId }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                catch { }
            }
        }

        private void UpdateRecordRights()
        {
            try
            {
                Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
                if (_isAccountChange)
                {
                    Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid> { accountId } },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                     );
                }
                else
                {
                    RightsHelper rightsHelper = new RightsHelper(_userConnection);
                    string rightSchemaName = rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
                    var rolesCurrent = rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
                    if (accountId == Guid.Empty && (rolesCurrent == null || rolesCurrent.Count < 1))
                    {
                        Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid>() },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                        );
                    }
                }
            }
            catch { }
        }

        private void UpdateIsSynchronize()
        {
            var update = new Update(_userConnection, "CrocContract") as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(_entity.PrimaryColumnValue));
            update.Execute();
        }

        private void UpdateHierarchiPosition()
        {
            try
            {
                Guid oldParent = _entity.GetTypedOldColumnValue<Guid>("CrocParentContractId");
                Guid newParent = _entity.GetTypedColumnValue<Guid>("CrocParentContractId");
                if (newParent != oldParent)
                {
                    if (newParent == Guid.Empty) { _entity.SetColumnValue("CrocHierarchiPosition", 1); }
                    else
                    {
                        Entity parenContractEntity = _entityHelper.ReadEntity("CrocContract", newParent);
                        int parentPosition = parenContractEntity.GetTypedColumnValue<int>("CrocHierarchiPosition");
                        _entity.SetColumnValue("CrocHierarchiPosition", parentPosition + 1);
                    }
                }
            }
            catch { }
        }

        private void OnChangeColumns()
        {
            bool isCrocAccountChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocAccountId");
            bool isCrocContractTypeChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocContractTypeId");
            bool isCrocContractDateChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocContractDate");
            bool isCrocContractNumberChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocContractNumber");
            _isNameChange = (isCrocAccountChanged || isCrocContractTypeChanged || isCrocContractDateChanged || isCrocContractNumberChanged);
        }

        private void SetVisibleName()
        {
            Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
            string accountName = GetVisibleValue("Account", accountId, "Name");
            Guid contractTypeId = _entity.GetTypedColumnValue<Guid>("CrocContractTypeId");
            string contractTypeName = GetVisibleValue("CrocContractType", contractTypeId, "Name");
            DateTime date = _entity.GetTypedColumnValue<DateTime>("CrocContractDate");
            string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
            string number = _entity.GetTypedColumnValue<string>("CrocContractNumber");
            string newName = $"{contractTypeName} / {accountName}{dateStr} / {number}";
            _entityHelper.UpdateRecord("CrocContract", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocVisibleName", newName } });
        }

        private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
        {
            Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
            return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
        }

        private void SetProviderInChart()
        {
            Guid providerId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
            List<Entity> charts = _entityHelper.FindEntities("CrocChart", new Dictionary<string, object>
                {
                    { "CrocContract.Id", _entity.PrimaryColumnValue }
                }).ToList();
            if (charts?.Count > 0)
            {
                charts.ForEach(x => {
                    Guid chartId = x.GetTypedColumnValue<Guid>("Id");
                    _entityHelper.UpdateRecord("CrocChart", chartId, new Dictionary<string, object> { { "CrocProviderId", providerId } });
                });
            }
        }

        public override void OnInserting(object sender, EntityBeforeEventArgs e)
        {
            base.OnInserting(sender, e);
            InitSettings(sender);
            OnChangeManagers();
            UpdateHierarchiPosition();
        }

        public override void OnInserted(object sender, EntityAfterEventArgs e)
        {
            base.OnInserted(sender, e);
            InitSettings(sender);
            AfterChangeManagers();
        }

        public override void OnUpdating(object sender, EntityBeforeEventArgs e)
        {
            base.OnUpdating(sender, e);
            InitSettings(sender);
            OnChangeManagers();
            UpdateHierarchiPosition();
        }

        public override void OnUpdated(object sender, EntityAfterEventArgs e)
        {
            base.OnUpdated(sender, e);
            InitSettings(sender);
            AfterChangeManagers();
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateRecordRights();
            UpdateIsSynchronize();
            if (_isNameChange) { SetVisibleName(); }
            if (_isAccountChange) { SetProviderInChart(); }
        }
    }
}
