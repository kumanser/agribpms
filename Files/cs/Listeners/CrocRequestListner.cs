﻿using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.OneCIntegration;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;
using Common.Logging;
using Terrasoft.Core.DB;
using Terrasoft.Configuration.RightsService;

namespace Terrasoft.Configuration
{

	[EntityEventListener(SchemaName = "CrocRequest")]
	public class CrocRequestListner : BaseEntityEventListener
	{
		private UserConnection _userConnection;
		private Entity _entity;
		private CrocEntityHelper _entityHelper;
		private static ILog _log = LogManager.GetLogger("Error");
		private bool _isIntegrationStatus = false;
		private bool _isProviderChanged = false;
		private bool _isNameChange = false;

		private void InitSettings(object sender)
		{
			_entity = (Entity)sender;
			_userConnection = _entity.UserConnection;
			_entityHelper = new CrocEntityHelper(_userConnection);
		}

		private void OnColumnsChanged()
		{
			var isCrocLoadingChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocTotalVolumeLoadingTon");
			var isCrocUnloadingChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocTotalVolumeUnloadingTon");
			if (isCrocLoadingChanged || isCrocUnloadingChanged) CalculateDeviation();
			Guid newProviderId = _entity.GetTypedColumnValue<Guid>("CrocProviderId");
			Guid oldProviderId = _entity.GetTypedOldColumnValue<Guid>("CrocProviderId");
			_isProviderChanged = newProviderId != Guid.Empty && newProviderId != oldProviderId;
			bool isCrocProviderChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocProviderId");
			bool isCrocApplicationTypeChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocApplicationTypeId");
			bool isCreatedOnChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CreatedOn");
			bool isCrocNumberChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocNumber");
			_isNameChange = (isCrocProviderChanged || isCrocApplicationTypeChanged || isCrocNumberChanged || isCreatedOnChanged);
		}

		private void SetVisibleName()
		{
			Guid providerId = _entity.GetTypedColumnValue<Guid>("CrocProviderId");
			string providerName = GetVisibleValue("Account", providerId, "Name");
			Guid applicationTypeId = _entity.GetTypedColumnValue<Guid>("CrocApplicationTypeId");
			string applicationTypeName = GetVisibleValue("CrocApplicationType", applicationTypeId, "Name");
			string number = _entity.GetTypedColumnValue<string>("CrocNumber");
			DateTime date = _entity.GetTypedColumnValue<DateTime>("CreatedOn");
			string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
			string newName = $"{applicationTypeName} / {providerName}{dateStr} / {number}";
			_entityHelper.UpdateRecord("CrocRequest", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocVisibleName", newName } });
		}

		private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
		{
			Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
			return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
		}

		private void CalculateDeviation()
		{
			float result = 0;
			float loading = _entity.GetTypedColumnValue<float>("CrocTotalVolumeLoadingTon");
			float unloading = _entity.GetTypedColumnValue<float>("CrocTotalVolumeUnloadingTon");
			if (loading != 0)
			{
				result = (Math.Abs(unloading - loading) / loading) * 100;
			}
			_entity.SetColumnValue("CrocDeviation", result);
		}

		private void OnStatusChangeBefore()
		{
			Guid newStatusId = _entity.GetTypedColumnValue<Guid>("CrocApplicationStatusId");
			Guid oldStatusId = _entity.GetTypedOldColumnValue<Guid>("CrocApplicationStatusId");
			if (newStatusId != Guid.Empty && newStatusId != oldStatusId)
			{
				_isIntegrationStatus = newStatusId == ConstCs.CrocApplicationStatus.Cofirmed || newStatusId == ConstCs.CrocApplicationStatus.Canceled;
			}
		}

		private void OnStatusChangeAfter()
		{
			Entity requestEntity = _entityHelper.ReadEntity("CrocRequest", _entity.PrimaryColumnValue);
			Guid typeId = requestEntity.GetTypedColumnValue<Guid>("CrocApplicationTypeId");
            if (_isIntegrationStatus 
				&& (typeId == ConstCs.CrocApplicationType.Transporter || typeId == ConstCs.CrocApplicationType.Surveer))
			{
				try
				{
					OneCIntegrationHelper oneHelper = new OneCIntegrationHelper(_userConnection);
					oneHelper.SendServiceRequest(_entity.PrimaryColumnValue);
				}
				catch (Exception ex) { _log.Error($"{ex.Message}\n{ex.StackTrace}"); }
			}
		}

		private void UpdateRecordRights()
		{
			try
			{
				Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocProviderId");
				if (_isProviderChanged)
				{
					Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
							new Dictionary<string, object>
							{
							{ "AccountId", new List<Guid> { accountId } },
							{ "SchemaName", _entity.SchemaName },
							{ "RecordId", _entity.PrimaryColumnValue }
							}
					);
				}
				else
				{
					RightsHelper rightsHelper = new RightsHelper(_userConnection);
					string rightSchemaName = rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
					var rolesCurrent = rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
					if (accountId == Guid.Empty && (rolesCurrent == null || rolesCurrent.Count < 1))
					{
						Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
							new Dictionary<string, object>
							{
							{ "AccountId", new List<Guid>() },
							{ "SchemaName", _entity.SchemaName },
							{ "RecordId", _entity.PrimaryColumnValue }
							}
						);
					}
				}
			}
			catch { }
		}

		private void UpdateIsSynchronize()
		{
			var update = new Update(_userConnection, "CrocRequest") as Update;
			update.Set("CrocIsSynchronize", Column.Parameter(true));
			update.Where("Id").IsEqual(Column.Parameter(_entity.PrimaryColumnValue));
			update.Execute();
		}

		public override void OnSaving(object sender, EntityBeforeEventArgs e)
		{
			base.OnSaving(sender, e);
			InitSettings(sender);
			OnColumnsChanged();
		}

		public override void OnUpdating(object sender, EntityBeforeEventArgs e)
		{
			base.OnUpdating(sender, e);
			InitSettings(sender);
			OnStatusChangeBefore();
		}

		public override void OnUpdated(object sender, EntityAfterEventArgs e)
		{
			base.OnUpdated(sender, e);
			InitSettings(sender);
			OnStatusChangeAfter();
		}

		public override void OnSaved(object sender, EntityAfterEventArgs e)
		{
			base.OnSaved(sender, e);
			UpdateRecordRights();
			UpdateIsSynchronize();
			if (_isNameChange) { SetVisibleName(); }
        }
    }
}