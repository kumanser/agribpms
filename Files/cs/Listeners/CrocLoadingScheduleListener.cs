﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Linq;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Common;
using Common.Logging;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocLoadingSchedule")]
    public class CrocLoadingScheduleListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private Guid _chartId;
        private static ILog _log;
        private static ILog Logger
        {
            get { return _log ?? (_log = LogManager.GetLogger("Error")); }
        }

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _chartId = _entity.GetTypedColumnValue<Guid>("CrocChartId");
        }

        public override void OnInserted(object sender, EntityAfterEventArgs e)
        {
            base.OnInserted(sender, e);
            InitSettings(sender);
            SaveChartChanges();
            try { SetIsLoadingRegister();  }
            catch (Exception ex) { Logger.Error($"{ex.Message}\n{ex.StackTrace}"); }
        }

        public override void OnUpdated(object sender, EntityAfterEventArgs e)
        {
            base.OnUpdated(sender, e);
            InitSettings(sender);
            SaveChartChanges();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            SetRequest(e.PrimaryColumnValue);
        }

        private void SetRequest(Guid loadingScheduleId) 
        {
            Guid requestId;
            CrocEntityHelper helper = new CrocEntityHelper(_userConnection);
            Entity chartEntity = helper.ReadEntity("CrocChart", _chartId);
            if (chartEntity != null && chartEntity.GetTypedColumnValue<Guid>("CrocRequestId") != Guid.Empty) 
            {
                requestId = chartEntity.GetTypedColumnValue<Guid>("CrocRequestId");
                helper.UpdateEntity("CrocLoadingSchedule", loadingScheduleId, new Dictionary<string, object>() 
                {
                    { "CrocRequestId", requestId }
                });
          }
        } 

        private void SetIsLoadingRegister() {
            Guid registerId = _entity.GetTypedColumnValue<Guid>("Id"); 
            DateTime shipmentDate = _entity.GetTypedColumnValue<DateTime>("CrocShipmentDate");
            CrocEntityHelper helper = new CrocEntityHelper(_userConnection);
            List<Entity> scheduleCollection = helper.FindEntities("CrocRegisterLoading", new Dictionary<string, object>()
            {
                { "CrocShippingDate", shipmentDate}
            }).ToList();
            if (scheduleCollection?.Count > 0) 
            {
                helper.UpdateEntity("CrocLoadingSchedule", registerId, new Dictionary<string, object>() {
                    { "CrocIsLoadingRegister", true}
                });
            }
        }

        private void SaveChartChanges()
        {
            var isCrocLoadingTransporFactChanged = _entity.GetChangedColumnValues()
               .Any(column => column.Name == "CrocLoadingTransporFact");
            var isCrocLoadingTonFactChanged = _entity.GetChangedColumnValues()
                .Any(column => column.Name == "CrocLoadingTonFact");

            Guid deliveryCondition;
            Guid transportType;
            CrocEntityHelper helper = new CrocEntityHelper(_userConnection);
            Entity entity = helper.ReadEntity("CrocChart", _chartId);
            if (entity != null && entity.GetTypedColumnValue<Guid>("CrocDeliveryConditionId") != Guid.Empty)
            {
                deliveryCondition = entity.GetTypedColumnValue<Guid>("CrocDeliveryConditionId");
                transportType = entity.GetTypedColumnValue<Guid>("CrocTransportTypeId");
                if ((isCrocLoadingTransporFactChanged || isCrocLoadingTonFactChanged) && (deliveryCondition == ConstCs.CrocDeliveryCondition.FCA || (deliveryCondition == ConstCs.CrocDeliveryCondition.CPT && transportType == ConstCs.CrocTransporType.Railway)))
                {
                    CalculateCPTRailwayOrFCAFix();
                }
            }
        }

        private decimal getSumLoadingTonFact(Guid chartId) 
        {
            decimal sumTonFact = 0;
            CrocEntityHelper helper = new CrocEntityHelper(_userConnection);
            EntityCollection collectionTonFact = helper.FindEntities("CrocLoadingSchedule", new Dictionary<string, object>()
            {
                {"CrocChart.Id", chartId }
            });
            collectionTonFact.ForEach(entity =>
            {
                sumTonFact += entity.GetTypedColumnValue<decimal>("CrocLoadingTonFact");
            });
            return sumTonFact;
        }

        private decimal getWeight(Guid chartId)
        {
            decimal weight;
            CrocEntityHelper helper = new CrocEntityHelper(_userConnection);
            Entity entity = helper.ReadEntity("CrocChart", chartId);
            weight = entity.GetTypedColumnValue<decimal>("CrocWeight");
            return weight;
        }

        private void CalculateCPTRailwayOrFCAFix() 
        {
            decimal sumTonFact = getSumLoadingTonFact(_chartId);
            decimal remainingExportedTon = getWeight(_chartId) - sumTonFact;
            CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
            entityHelper.UpdateEntity("CrocChart", _chartId, new Dictionary<string, object>()
            {
                { "CrocFactLoadingTon", sumTonFact},
                { "CrocRemainingExportedTon", remainingExportedTon},
            });
        }
    }
}
