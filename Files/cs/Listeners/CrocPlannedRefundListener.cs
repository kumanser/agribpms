﻿using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;
using Terrasoft.Configuration.OneCIntegration;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocPlannedRefund")]
    public class CrocPlannedRefundListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private static ILog _log = LogManager.GetLogger("Error");

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
        }

        private void SendPlannedRefund()
        {
            try
            {
                OneCIntegrationHelper oneHelper = new OneCIntegrationHelper(_userConnection);
                oneHelper.SendPlanRefundI(_entity.PrimaryColumnValue);
            }
            catch (Exception ex) { _log.Error($"{ex.Message}\n{ex.StackTrace}"); }
        }


        public override void OnInserted(object sender, EntityAfterEventArgs e)
        {
            base.OnInserted(sender, e);
            InitSettings(sender);
            SendPlannedRefund();
        }
    }
}

