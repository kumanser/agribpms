﻿using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;
using Terrasoft.Configuration.Base;
using Terrasoft.Core.Tasks;
using System.Collections.Generic;
using Terrasoft.Configuration.RightsService;
using System.Linq;
using Terrasoft.Core.DB;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocLoan")]
    public class CrocLoanListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isAccountChange = false;
        private bool _isNameChange = false;
        private static ILog _log = LogManager.GetLogger("Error");

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            Guid newAccount = _entity.GetTypedColumnValue<Guid>("CrocPortId");
            Guid oldAccount = _entity.GetTypedOldColumnValue<Guid>("CrocPortId");
            _isAccountChange = newAccount != Guid.Empty && newAccount != oldAccount;
            bool isCrocTerminalChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocTerminalId");
            bool isCrocIssueDateChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocIssueDate");
            bool isCrocNameChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocName");
            _isNameChange = (isCrocTerminalChanged || isCrocIssueDateChanged || isCrocNameChanged);
        }

        private void UpdateRecordRights()
        {
            try
            {
                Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocPortId");
                if (_isAccountChange)
                {
                    Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid> {accountId } },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                     );
                }
                else
                {
                    RightsHelper rightsHelper = new RightsHelper(_userConnection);
                    string rightSchemaName = rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
                    var rolesCurrent = rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
                    if (accountId == Guid.Empty && (rolesCurrent == null || rolesCurrent.Count < 1))
                    {
                        Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid>() },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                        );
                    }
                }
            }
            catch { }
        }

        private void SetVisibleName()
        {
            Guid terminalId = _entity.GetTypedColumnValue<Guid>("CrocTerminalId");
            string terminalName = GetVisibleValue("CrocTerminal", terminalId, "Name");
            DateTime date = _entity.GetTypedColumnValue<DateTime>("CrocIssueDate");
            string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
            string number = _entity.GetTypedColumnValue<string>("CrocName");
            string newName = $"Товарный займ / {terminalName}{dateStr} / {number}";
            _entityHelper.UpdateRecord("CrocLoan", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocVisibleName", newName } });
        }

        private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
        {
            Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
            return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateRecordRights();
            if (_isNameChange) { SetVisibleName(); }
        }
    }
}

