﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocEnterpriseRightRecord")]
    public class CrocEnterpriseRightRecordListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void UpdateAccountRecordRights()
        {
            Guid accountId = _entity.GetTypedColumnValue<Guid>("CrocAccountId");
            Guid roleId = _entity.GetTypedColumnValue<Guid>("CrocRoleId");
            if (accountId != Guid.Empty && roleId != Guid.Empty)
            {
                Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                        new Dictionary<string, object>
                        {
                            { "AccountId", new List<Guid> { accountId } },
                            { "SchemaName", "Account" },
                            { "RecordId", accountId }
                        }
                 );
                Task.StartNewWithUserConnection<RightsOnExistObjectUpdater, Dictionary<string, object>>(
                        new Dictionary<string, object>
                        {
                            { "AccountId", accountId }
                        }
                 );
            }
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateAccountRecordRights();
        }
    }
}


