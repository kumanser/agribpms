﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;
using Terrasoft.Core.DB;
using System.Linq;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocNotificationFeed")]
    public class CrocNotificationFeedListner : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isNameChange = false;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void UpdateIsSynchronize()
        {
            var update = new Update(_userConnection, "CrocNotificationFeed") as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(_entity.PrimaryColumnValue));
            update.Execute();
        }

        private void OnColumnsChanged()
        {
            bool isCrocNotificationTypeChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocNotificationTypeId");
            bool isCrocSendDateChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocSendDate");
            bool isCrocNameChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocName");
            _isNameChange = (isCrocNotificationTypeChanged || isCrocSendDateChanged || isCrocNameChanged);
        }

        private void SetVisibleName()
        {
            Guid notificationTypeId = _entity.GetTypedColumnValue<Guid>("CrocNotificationTypeId");
            string notificationTypeName = GetVisibleValue("CrocNotificationType", notificationTypeId, "Name");
            DateTime date = _entity.GetTypedColumnValue<DateTime>("CrocSendDate");
            string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
            string fullName = _entity.GetTypedColumnValue<string>("CrocName");
            string[] parts = fullName.Split(new char[] { '/' });
            string name = (parts[0]).Trim();
            string newName = $"{name} / {notificationTypeName}{dateStr}";
            _entityHelper.UpdateRecord("CrocNotificationFeed", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocName", newName } });
        }

        private string GetVisibleValue(string shemaName, Guid entityId, string columnName)
        {
            Entity entity = _entityHelper.ReadEntity(shemaName, entityId);
            return (entity != null) ? entity.GetTypedColumnValue<string>(columnName) : "";
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnColumnsChanged();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateIsSynchronize();
            if (_isNameChange) { SetVisibleName(); }
        }
    }
}