﻿using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;
using Terrasoft.Configuration.Base;
using System.Collections.Generic;
using System.Linq;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocRegisterEmptyCar")]
    public class CrocRegisterEmptyCarEventListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private ICrocEntityHelper _entityHelper;
        private static ILog _log;
        private static ILog Logger
        {
            get { return _log ?? (_log = LogManager.GetLogger("Error")); }
        }

        private void FillUpdateCrocLoadingSchedule()
        {
            Guid errandId = _entity.GetTypedColumnValue<Guid>("CrocErrandId");
            Guid errandIdOld = _entity.GetTypedOldColumnValue<Guid>("CrocErrandId");
            if (errandId != errandIdOld) FillCrocLoadingSchedule();
        }

        private void FillCrocLoadingSchedule()
        {
            _entityHelper = new CrocEntityHelper(_userConnection);
            Guid errandId = _entity.GetTypedColumnValue<Guid>("CrocErrandId");
            if (errandId != Guid.Empty)
            {
                Entity errandEntity = _entityHelper.ReadEntity("CrocErrand", errandId);
                DateTime controlDate = errandEntity.GetTypedColumnValue<DateTime>("CrocDate").Date;
                Guid requestId = errandEntity.GetTypedColumnValue<Guid>("CrocRequestId");
                if(requestId != Guid.Empty)
                {
                    List<Entity> loadingSchedules = _entityHelper.FindEntities("CrocLoadingSchedule", new Dictionary<string, object>
                    { { "CrocRequest.Id", requestId } }).ToList();
                    if (loadingSchedules?.Count > 0)
                    {
                        List<Guid> sheduleIds = loadingSchedules.Where(x => x.GetTypedColumnValue<DateTime>("CrocShipmentDate").Date == controlDate).Select(x => x.PrimaryColumnValue).ToList();
                        if (sheduleIds?.Count > 0)
                        {
                            _entity.SetColumnValue("CrocLoadingScheduleId", sheduleIds.FirstOrDefault());
                        }
                    }
                }
            }
        }

        public override void OnUpdating(object sender, EntityBeforeEventArgs e)
        {
            base.OnUpdating(sender, e);
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            try
            {
                FillUpdateCrocLoadingSchedule();
            }
            catch (Exception ex) { Logger.Error($"{ex.Message}\n{ex.StackTrace}"); }
        }

        public override void OnInserting(object sender, EntityBeforeEventArgs e)
        {
            base.OnInserting(sender, e);
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            try
            {
                FillCrocLoadingSchedule();
            }
            catch (Exception ex) { Logger.Error($"{ex.Message}\n{ex.StackTrace}"); }
        }
    }
}