﻿using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;
using Terrasoft.Configuration.Base;
using Terrasoft.Core.DB;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "AccountCommunication")]
    public class AccountCommunicationListner : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private static ILog _log = LogManager.GetLogger("Error");

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void UpdateIsSynchronize(string entityName, Guid entityId)
        {
            var update = new Update(_userConnection, entityName) as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(entityId));
            update.Execute();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            Guid accountId = _entity.GetTypedColumnValue<Guid>("AccountId");
            if (accountId != Guid.Empty) UpdateIsSynchronize("Account", accountId);
        }
    }
}

