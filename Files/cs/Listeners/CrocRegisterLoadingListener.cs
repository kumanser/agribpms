﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Linq;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Common.Logging;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocRegisterLoading")]
    public class CrocRegisterLoadingListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private static ILog _log = LogManager.GetLogger("Error");

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
        }

        private void SetIsLoadingRegister() 
        {
            try
            {
                Guid registerId = _entity.GetTypedColumnValue<Guid>("Id");
                DateTime shippingDate = _entity.GetTypedColumnValue<DateTime>("CrocShippingDate");
                CrocEntityHelper helper = new CrocEntityHelper(_userConnection);
                List<Entity> scheduleCollection = helper.FindEntities("CrocLoadingSchedule", new Dictionary<string, object>()
                { { "CrocShipmentDate", shippingDate} }).ToList();
                if (scheduleCollection?.Count > 0)
                {
                    List<Guid> scheduleIds = scheduleCollection.Select(x => x.PrimaryColumnValue).ToList();
                    if (scheduleIds?.Count() > 0)
                    {
                        scheduleIds.ForEach(id => {
                            helper.UpdateEntity("CrocLoadingSchedule", id, new Dictionary<string, object>() 
                            { { "CrocIsLoadingRegister", true} });
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error($"{ex.Message}\n{ex.StackTrace}");
            }
        }

        public override void OnInserting(object sender, EntityBeforeEventArgs e)
        {
            base.OnInserting(sender, e);
            InitSettings(sender);
            SetIsLoadingRegister();
        }
    }
}
