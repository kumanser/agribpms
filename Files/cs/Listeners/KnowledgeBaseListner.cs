﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;
using Terrasoft.Core.DB;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "KnowledgeBase")]
    public class KnowledgeBaseListner : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void UpdateIsSynchronize()
        {
            var update = new Update(_userConnection, "KnowledgeBase") as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(_entity.PrimaryColumnValue));
            update.Execute();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateIsSynchronize();
        }
    }
}