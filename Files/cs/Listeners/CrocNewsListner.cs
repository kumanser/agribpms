﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;
using Terrasoft.Core.DB;
using System.Linq;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocNews")]
    public class CrocNewsListner : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isNameChange = false;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            bool isCrocPublicatedOnChanged = _entity.GetChangedColumnValues().Any(column => column.Name == "CrocPublicatedOn");
            _isNameChange = (isCrocPublicatedOnChanged);
        }

        private void SetVisibleName()
        {
            DateTime date = _entity.GetTypedColumnValue<DateTime>("CrocPublicatedOn");
            string dateStr = (date == new DateTime()) ? "" : $" / {date:dd.MM.yyyy}";
            string fullName = _entity.GetTypedColumnValue<string>("CrocName");
            string[] parts = fullName.Split(new char[] { '/' });
            string name = (parts[0]).Trim();
            string newName = $"{name}{dateStr}";
            _entityHelper.UpdateRecord("CrocNews", _entity.PrimaryColumnValue, new Dictionary<string, object> { { "CrocName", newName } });
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }


        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            if (_isNameChange) { SetVisibleName(); }
        }
    }
}