﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Common;
using Terrasoft.Core.Tasks;
using Common.Logging;
using Terrasoft.Core.DB;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "CrocDeliverySchedule")]
    public class CrocDeliveryScheduleListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private ICrocEntityHelper _entityHelper;
        private Guid _chartId;
        private bool _isPlanChangeged = false;
        private bool _isStatusChanged = false;
        private bool _isTermShift = false;
        private Guid _intChartId = Guid.Empty;
        private static readonly ILog _logger = LogManager.GetLogger("Error");

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
            _chartId = new Guid(_entity.GetColumnValue("CrocChartId").ToString());
        }

        private void OnDeliveryScheduleUpdated() 
        {
            int deliveryTSPlan = _entity.GetTypedColumnValue<int>("CrocDeliveryTransportPlan");
            int deliveryTSFact = _entity.GetTypedColumnValue<int>("CrocDeliveryTransportFact");
            int sumFactTS = GetSumTransportFact();
            int totalTS = GetTotalTransportNumber();
            if (totalTS > sumFactTS || deliveryTSPlan > deliveryTSFact || deliveryTSFact > deliveryTSPlan) 
            {
                CalcDeliverySchedule();
            }
        }

        private void CalcDeliverySchedule() 
        {
            Entity chart = _entityHelper.FindEntity("CrocChart", "Id", _chartId);
            string chartName = chart.GetTypedColumnValue<string>("CrocNumber");
            decimal weight = GetWeight();
            decimal sumFactTon = GetFactSum();
            decimal balanceDeliveredTon = weight - sumFactTon;
            decimal averageWeight = GetAverageWeightTransport();           
            decimal norm = GetNorm();
            if ( norm == 0 || averageWeight == 0 ) return;
            _entityHelper.UpdateEntity("CrocChart", _chartId, new Dictionary<string, object>()
                {
                    { "CrocBalanceDeliveredTon", balanceDeliveredTon }
                });
            ClearOldRecords();
            var select = new Select(_userConnection)
                        .Top(1)
                            .Column("CrocDeliveryDate")
                        .From("CrocDeliverySchedule")
                        .Where("CrocChartId").IsEqual(Column.Parameter(_chartId))
                        .OrderByDesc("CrocDeliveryDate")
                         as Select;
            DateTime lastDate = DateTime.Now;
            using (DBExecutor executor = _userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = select.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        lastDate = reader.GetColumnValue<DateTime>("CrocDeliveryDate");
                    }
                    reader.Close();
                }
            }

            int deviation = GetDeviation();
            int days = Convert.ToInt32(Math.Ceiling(deviation * averageWeight / norm));
            decimal totalTon = deviation * averageWeight;
            int totalTransport = deviation;
            int normTS = Convert.ToInt32(Math.Ceiling(norm / averageWeight));
            for (int i = 0; i < days; i++)
            {
                _isPlanChangeged = false;
                if (totalTon < norm) norm = totalTon;
                if (totalTransport < normTS) normTS = totalTransport; 
                _entityHelper.InsertEntity("CrocDeliverySchedule", new Dictionary<string, object>()
                    {
                        { "CrocChartId", _chartId },
                        { "CrocDeliveryTonPlan" , norm},
                        { "CrocDeliveryTransportPlan",  normTS},
                        { "CrocDeliveryDate" , lastDate.AddDays(1)},
                        { "CrocChartExecutionStatusId", ConstCs.ChartExecutionStatus.Plan },
                        { "CrocIsTermShift" , true},
                        { "CrocName", chartName + "/" + lastDate.AddDays(1).ToString("dd.MM.yyyy")}
                    });
                _isTermShift = true;
                _intChartId = _chartId;
                totalTon -= norm;
                totalTransport -= normTS;
                lastDate = lastDate.AddDays(1);
            }
        }

        private int GetDeviation()
        {
            List<Guid> chainedSchedules = GetSchedulesFromActivity();
            int deviation = 0;
            var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, "CrocDeliverySchedule");
            var PlanTSCol = esq.AddColumn("CrocDeliveryTransportPlan");
            var allocatedQoutaCol = esq.AddColumn("CrocAllocatedQuotaTransport");
            var FactTSCol = esq.AddColumn("CrocDeliveryTransportFact");
            esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal, "CrocChart", _chartId));
            var entities = esq.GetEntityCollection(_userConnection);
            entities.ForEach(entity => {
                int PlanTS = entity.GetTypedColumnValue<int>(PlanTSCol.Name);
                int FactTS = entity.GetTypedColumnValue<int>(FactTSCol.Name);
                int allocatedQouta = entity.GetTypedColumnValue<int>(allocatedQoutaCol.Name);
                if (FactTS > 0 || allocatedQouta > 0) 
                { 
                    deviation += PlanTS - ((FactTS > 0) ? FactTS : allocatedQouta); 
                }
            });
            return deviation;
        }

        private decimal GetAverageWeightTransport()
        {
            decimal averageWeight;
            Entity entity = _entityHelper.ReadEntity("CrocChart", _chartId);
            averageWeight = entity.GetTypedColumnValue<decimal>("CrocAverageWeightTransport");
            return averageWeight;
        }

        private int GetTotalTransportNumber()
        {
            int totalTransport;
            Entity entity = _entityHelper.ReadEntity("CrocChart", _chartId);
            totalTransport = entity.GetTypedColumnValue<int>("CrocTotalTransportNumber");
            return totalTransport;
        }

        private void ClearOldRecords()
        {
            Select subselect = new Select(_userConnection)
                .Column("CrocDeliveryScheduleId")
                .From("Activity")
                .Where("CrocDeliveryScheduleId").Not().IsNull()
            as Select;
            
            Delete delete = new Delete(_userConnection)
                .From("CrocDeliverySchedule")
                .Where("CrocChartId").IsEqual(Column.Parameter(_chartId))
                .And("CrocChartExecutionStatusId").IsEqual(Column.Parameter(ConstCs.ChartExecutionStatus.Plan))
                .And("CrocIsTermShift").IsEqual(Column.Const(true))
                .And("Id").Not().In(subselect)
            as Delete;
            if(delete.Execute() > 0)
            {
                _isTermShift = true;
                _intChartId = _chartId;
            };
        }

        private decimal GetFactSum()
        {
            decimal sumFactTon = 0;
            List<Entity> collectionFactTon = _entityHelper.FindEntities("CrocDeliverySchedule", new Dictionary<string, object>()
            {
                {"CrocChart.Id", _chartId }
            }).ToList();
            if (collectionFactTon?.Count > 0)
            {
                collectionFactTon.ForEach(entity => { sumFactTon += entity.GetTypedColumnValue<decimal>("CrocDeliveryTonFact"); });
            }
            return sumFactTon;
        }

        private List<Guid> GetSchedulesFromActivity() 
        {
            List<Guid> result = new List<Guid>();
            Select subselect = new Select(_userConnection)
                .Column("CrocDeliveryScheduleId")
                .From("Activity")
                .Where("CrocDeliveryScheduleId").Not().IsNull()
            as Select;

            using (DBExecutor executor = _userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = subselect.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        result.Add(reader.GetGuid(reader.GetOrdinal("CrocDeliveryScheduleId")));
                    }
                    reader.Close();
                }
            }
            return result;
        }

        private void SetExecutionStatus()
        {
            var isAllocatedQuotaChanged = _entity.GetChangedColumnValues()
               .Any(column => column.Name == "CrocAllocatedQuotaTon");
            var isDeliveryTonFactChanged = _entity.GetChangedColumnValues()
               .Any(column => column.Name == "CrocDeliveryTonFact");
            decimal AllocatedQuota = _entity.GetTypedColumnValue<decimal>("CrocAllocatedQuotaTon");
            decimal DeliveryTonFact = _entity.GetTypedColumnValue<decimal>("CrocDeliveryTonFact");

            if (isAllocatedQuotaChanged && AllocatedQuota > 0 && DeliveryTonFact == 0)
            {
                _entity.SetColumnValue("CrocChartExecutionStatusId", ConstCs.ChartExecutionStatus.CompletionWaiting);
            }
            if (isDeliveryTonFactChanged && DeliveryTonFact > 0)
            {
                _entity.SetColumnValue("CrocChartExecutionStatusId", ConstCs.ChartExecutionStatus.Closed);
            }

        }
       
        private void CalcFactChartChanges() 
        {
            var isCrocDeliveryTransportFactChanged = _entity.GetChangedColumnValues()
               .Any(column => column.Name == "CrocDeliveryTransportFact");
            var isCrocDeliveryTonFactChanged = _entity.GetChangedColumnValues()
                .Any(column => column.Name == "CrocDeliveryTonFact");
            Guid deliveryCondition;
            Entity entity = _entityHelper.ReadEntity("CrocChart", _chartId);
            if (entity != null && entity.GetTypedColumnValue<Guid>("CrocDeliveryConditionId") != Guid.Empty)
            {
                deliveryCondition = entity.GetTypedColumnValue<Guid>("CrocDeliveryConditionId");
                
                if ((isCrocDeliveryTonFactChanged || isCrocDeliveryTransportFactChanged) && deliveryCondition == ConstCs.CrocDeliveryCondition.CPT)
                {
                    CalculateCPTAfterFix();
                }
            }
        }

        private int GetSumTransportFact()
        {
            int sumTransportFact = 0;
            List<Entity> collectionTonFact = _entityHelper.FindEntities("CrocDeliverySchedule", new Dictionary<string, object>()
            {
                {"CrocChart.Id", _chartId }
            }).ToList();
            if (collectionTonFact?.Count > 0)
            {
                collectionTonFact.ForEach(entity =>
                {
                    sumTransportFact += entity.GetTypedColumnValue<int>("CrocDeliveryTransportFact");
                });
            }
            return sumTransportFact;
        }

        private decimal GetSumQuota()
        {
            decimal sumQuota = 0;
            List<Entity> collectionTonFact = _entityHelper.FindEntities("CrocDeliverySchedule", new Dictionary<string, object>()
            {
                {"CrocChart.Id", _chartId }
            }).ToList();
            if (collectionTonFact?.Count > 0)
            {
                collectionTonFact.ForEach(entity =>
                {
                    sumQuota += entity.GetTypedColumnValue<decimal>("CrocAllocatedQuotaTon");
                });
            }
            return sumQuota;
        }

        private decimal GetWeight() 
        {
            decimal weight;
            Entity entity = _entityHelper.ReadEntity("CrocChart", _chartId);
            weight = entity.GetTypedColumnValue<decimal>("CrocWeight");
            return weight;
        }

        private decimal GetNorm()
        {
            decimal norm;
            Entity entity = _entityHelper.ReadEntity("CrocChart", _chartId);
            norm = entity.GetTypedColumnValue<decimal>("CrocNormDeliveryOneDayTon");
            return norm;
        }

        private void CalculateCPTAfterFix() 
        {
            decimal sumTonFact = GetFactSum();
            decimal sumTransportFact = GetSumTransportFact();
            decimal sumQuota = GetSumQuota();
            decimal balanceDeliveredTon = GetWeight() - sumTonFact;
            decimal chartDeviationTon = sumQuota - sumTonFact;
            _entityHelper.UpdateEntity("CrocChart", _chartId, new Dictionary<string, object>() 
            {
                { "CrocQuantityFactTon", sumTonFact},
                { "CrocFactDeliveryTransport", sumTransportFact},
                { "CrocBalanceDeliveredTon", balanceDeliveredTon},
                { "CrocChartDeviationTon", chartDeviationTon}
            });
        }

        private void PlanDataChanging()
        {
            decimal tonPlan = _entity.GetTypedColumnValue<decimal>("CrocDeliveryTonPlan");
            decimal tonPlanOld = _entity.GetTypedOldColumnValue<decimal>("CrocDeliveryTonPlan");
            int transporPlan = _entity.GetTypedColumnValue<int>("CrocDeliveryTransportPlan");
            int transporPlanOld = _entity.GetTypedOldColumnValue<int>("CrocDeliveryTransportPlan");
            _isPlanChangeged = tonPlan != tonPlanOld || transporPlan != transporPlanOld;
        }

        private void UpdataDataOnPlanChange()
        {
            if (_isPlanChangeged)
            {
                try
                {
                    Guid chartId = _entity.GetTypedColumnValue<Guid>("CrocChartId");
                    DateTime deliveryDate = _entity.GetTypedColumnValue<DateTime>("CrocDeliveryDate");
                    Task.StartNewWithUserConnection<UpdateMonthlyWeeklyPlansBackground, Dictionary<string, object>>(
                        new Dictionary<string, object> { { "ChartId", chartId }, { "DeliveryDate", deliveryDate } });
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                    _logger.Error(ex.StackTrace);
                }
            }
        }

        private void SendIntegration()
        {
            if (_isTermShift && _intChartId != Guid.Empty)
            {
                try
                {
                    Task.StartNewWithUserConnection<SendChartExportBackground, Dictionary<string, object>>(
                            new Dictionary<string, object> { { "DocumentId", _intChartId }, { "TypeInd", 2 } }
                        );
                }
                catch(Exception ex)
                {
                    _logger.Error($"{ex.Message}\n{ex.StackTrace}");
                }
                _isTermShift = false;
            }
        }

        private void CheckShiftDeleting()
        {
            Entity currentEntity = _entityHelper.ReadEntity(_entity.SchemaName, _entity.PrimaryColumnValue);
            bool isTermShift = currentEntity.GetTypedColumnValue<bool>("CrocIsTermShift");
            if(isTermShift)
            {
                _isTermShift = true;
                _intChartId = currentEntity.GetTypedColumnValue<Guid>("CrocChartId");
            }
        }

        public override void OnInserting(object sender, EntityBeforeEventArgs e)
        {
            base.OnInserting(sender, e);
            InitSettings(sender);
            SetExecutionStatus();
            PlanDataChanging();
        }

        public override void OnInserted(object sender, EntityAfterEventArgs e)
        {
            base.OnInserted(sender, e);
            InitSettings(sender);
            CalcFactChartChanges();
            UpdataDataOnPlanChange();
        }

        public override void OnUpdating(object sender, EntityBeforeEventArgs e)
        {
            base.OnUpdating(sender, e);
            InitSettings(sender);
            SetExecutionStatus();
            PlanDataChanging();
        }

        public override void OnUpdated(object sender, EntityAfterEventArgs e)
        {
            base.OnUpdated(sender, e);
            InitSettings(sender);
            CalcFactChartChanges();
            OnDeliveryScheduleUpdated();
            UpdataDataOnPlanChange();
        }

        public override void OnDeleting(object sender, EntityBeforeEventArgs e)
        {
            base.OnDeleting(sender, e);
            InitSettings(sender);
            CheckShiftDeleting();
        }
        public override void OnDeleted(object sender, EntityAfterEventArgs e)
        {
            base.OnDeleted(sender, e);
            InitSettings(sender);
            SendIntegration();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            SendIntegration();
        }
    }

}