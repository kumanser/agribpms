﻿using System;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using System.Collections.Generic;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using Terrasoft.Core.Tasks;
using Terrasoft.Core.DB;
using Terrasoft.Configuration.RightsService;

namespace Terrasoft.Configuration
{
    [EntityEventListener(SchemaName = "Contact")]
    public class ContactListener : BaseEntityEventListener
    {
        private UserConnection _userConnection;
        private Entity _entity;
        private CrocEntityHelper _entityHelper;
        private bool _isAccountChange = false;

        private void InitSettings(object sender)
        {
            _entity = (Entity)sender;
            _userConnection = _entity.UserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private void OnChangeColumns()
        {
            Guid newAccount = _entity.GetTypedColumnValue<Guid>("AccountId");
            Guid oldAccount = _entity.GetTypedOldColumnValue<Guid>("AccountId");
            _isAccountChange = newAccount != Guid.Empty && newAccount != oldAccount;
        }

        private void UpdateRecordRights()
        {
            try
            {
                Guid accountId = _entity.GetTypedColumnValue<Guid>("AccountId");
                if (_isAccountChange)
                {
                    Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                            { "AccountId", new List<Guid> {accountId } },
                            { "SchemaName", _entity.SchemaName },
                            { "RecordId", _entity.PrimaryColumnValue }
                            }
                     );
                }
                else
                {
                    RightsHelper rightsHelper = new RightsHelper(_userConnection);
                    string rightSchemaName = rightsHelper.GetRecordRightsSchemaDefName(_entity.SchemaName);
                    var rolesCurrent = rightsHelper.GetRecordRights(rightSchemaName, _entity.PrimaryColumnValue.ToString());
                    if (accountId == Guid.Empty && (rolesCurrent == null || rolesCurrent.Count < 1))
                    {
                        Task.StartNewWithUserConnection<RightsUpdaterBackground, Dictionary<string, object>>(
                            new Dictionary<string, object>
                            {
                                { "AccountId", new List<Guid>() },
                                { "SchemaName", _entity.SchemaName },
                                { "RecordId", _entity.PrimaryColumnValue }
                            }
                        );
                    }
                }
            }
            catch { }
        }

        private void UpdateIsSynchronize(string entityName, Guid entityId)
        {
            var update = new Update(_userConnection, entityName) as Update;
            update.Set("CrocIsSynchronize", Column.Parameter(true));
            update.Where("Id").IsEqual(Column.Parameter(entityId));
            update.Execute();
        }

        public override void OnSaving(object sender, EntityBeforeEventArgs e)
        {
            base.OnSaving(sender, e);
            InitSettings(sender);
            OnChangeColumns();
        }

        public override void OnSaved(object sender, EntityAfterEventArgs e)
        {
            base.OnSaved(sender, e);
            InitSettings(sender);
            UpdateRecordRights();
            UpdateIsSynchronize("Contact", _entity.PrimaryColumnValue);
            Guid accountId = _entity.GetTypedColumnValue<Guid>("AccountId");
            if (accountId != Guid.Empty) UpdateIsSynchronize("Account", accountId);
        }
    }
}

