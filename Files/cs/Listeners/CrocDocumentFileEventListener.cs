﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.S3;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;
using static Terrasoft.Configuration.S3.FilesS3Service;

namespace Terrasoft.Configuration
{
	[EntityEventListener(SchemaName = "CrocDocumentFile")]
	public class CrocDocumentFileEventListener : BaseEntityEventListener
	{
		private UserConnection _userConnection;
		private Entity _entity;
		private CrocEntityHelper _entityHelper;
		private Guid fileId;

		private List<string> GetVersionFiles(Guid fileId)
		{
			return _entityHelper.FindEntities("CrocDocumentVersion", new Dictionary<string, object>() {
				{ "CrocDocumentFile", fileId }
			}, 10000, new string[] { "CrocS3Link" })
			.Where(w=> !string.IsNullOrWhiteSpace(w.GetTypedColumnValue<string>("CrocS3Link")))
			.Select(x => x.GetTypedColumnValue<string>("CrocS3Link")).ToList();
		}

		private void BeforeDeleteS3File()
		{
			fileId = _entity.PrimaryColumnValue;
			FilesS3Helper s3Helper = new FilesS3Helper(_userConnection);
			List<string> list = GetVersionFiles(fileId);
			if (s3Helper.IsSuccessConnected())
			{
				foreach (var link in list)
				{
					(string bucket, string shortFileLink) s3Link = s3Helper.GetShortS3Link(System.Web.HttpUtility.UrlDecode(link));
					s3Helper.DeleteS3Object(s3Link.shortFileLink);
				}
			}
		}

		public override void OnDeleting(object sender, EntityBeforeEventArgs e)
		{
			base.OnDeleting(sender, e);
			_entity = (Entity)sender;
			_userConnection = _entity.UserConnection;
			_entityHelper = new CrocEntityHelper(_userConnection);
			BeforeDeleteS3File();
		}

	}
}
