﻿namespace Terrasoft.Configuration
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using Newtonsoft.Json;
	using Quartz.Util;
	using Terrasoft.Common;
    using Terrasoft.Configuration.Base;
    using Terrasoft.Core;
	using Terrasoft.Core.DB;
    using Terrasoft.Core.Entities;
    using Terrasoft.Core.Factories;

	#region Class: ${Name}

	[DefaultBinding(typeof(ICrocContractAssignedHelper))]
	public class CrocContractAssignedHelper : ICrocContractAssignedHelper
	{
		public class ContractItem
		{
			public Guid Id { get; set; }
			public Guid SubstitutionId { get; set; }
		}

		public class ColumnNames
		{
			public string AssignName { get; set; }
			public string SubstitutionName { get; set; }
			public string ReplacementDateName { get; set; }
		}

		#region Constructors: Public

		public CrocContractAssignedHelper(UserConnection userConnection)
		{
			_userConnection = userConnection;
		}

		#endregion

		#region Properties: Private

		private UserConnection _userConnection { get; set; }

        #endregion

        #region Methods: Private

        private Guid GetSubstitutionEmployee(Guid contractId, ColumnNames colNames)
        {
            CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
            Entity entity = entityHelper.ReadEntity("CrocContract", contractId);
            Guid employeeId = Guid.Empty;
            if (entity != null) { 
				if (entity.GetTypedColumnValue<Guid>(colNames.SubstitutionName) == Guid.Empty)
					employeeId = entity.GetTypedColumnValue<Guid>(colNames.AssignName); 
				else employeeId = entity.GetTypedColumnValue<Guid>(colNames.SubstitutionName);
			}
            return employeeId;
        } 
		
		private List<ContractItem> GetContractsWithSubstitutions(string substitutionName, string replacementName)
        {
			var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, "CrocContract");
			var idCol = esq.AddColumn("Id");
			var substitutionCol = esq.AddColumn(substitutionName);
			var today = DateTime.Now;
			esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Less, replacementName, today));
			var entities = esq.GetEntityCollection(_userConnection);
			List<ContractItem> contracts = new List<ContractItem>();
			entities.ForEach(entity => {
                ContractItem item = new ContractItem
                {
                    Id = entity.GetTypedColumnValue<Guid>(idCol.Name),
                    SubstitutionId = entity.GetTypedColumnValue<Guid>(substitutionCol.Name)
                };
				contracts.Add(item);
			});
			return contracts;
        }

        private void SetNewContractData(Guid contractId, Guid contactId, ColumnNames colNames, Guid? substitutionId = null, DateTime? replacementDate = null)
        {
			CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
			entityHelper.UpdateEntity("CrocContract", contractId, new Dictionary<string, object>() {
				{ colNames.AssignName,  contactId},
				{ colNames.SubstitutionName,  substitutionId},
				{ colNames.ReplacementDateName,  replacementDate}
			});
		}

		private List<Guid> GetCurrentEmployeeContracts(Guid currentEmployeeId, ColumnNames colNames)
		{
			var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, "CrocContract");
			var idCol = esq.AddColumn("Id");
			string assignName = colNames.AssignName.Remove(colNames.AssignName.Length - 2) + ".Id";
			esq.AddColumn(assignName);
			esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal, assignName, currentEmployeeId));
			var entities = esq.GetEntityCollection(_userConnection);
			List<Guid> contractsId = new List<Guid>();
			entities.ForEach(entity => {
				Guid contractId = entity.GetTypedColumnValue<Guid>(idCol.Name);
				contractsId.Add(contractId);
			});
			return contractsId;
		}

		#endregion

		#region Methods: Public

		public void ChangeAssignedEmployee(string contractsStr, Guid contactId, Guid roleId, DateTime replacementDate)
		{
			ColumnNames colNames = new ColumnNames();
			if (roleId == ConstCs.CrocEmployeeRole.PurchasingManager)
			{
				colNames.AssignName = "CrocAssignedManagerId";
				colNames.SubstitutionName = "CrocSubstitutionManagerId";
				colNames.ReplacementDateName = "CrocReplacementPeriodManager";

			}
			else if (roleId == ConstCs.CrocEmployeeRole.ManagerSTO)
			{
				colNames.AssignName = "CrocAssignedSTOId";
				colNames.SubstitutionName = "CrocSubstitutionSTOId";
				colNames.ReplacementDateName = "CrocReplacementPeriodSTO";
			}
			else return;
			string[] contractsArr = contractsStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
			contractsArr.ForEach(idStr => {
				Guid contractId = new Guid(idStr);
				Guid substitutionId = GetSubstitutionEmployee(contractId, colNames);
                SetNewContractData(contractId, contactId, colNames, substitutionId, replacementDate);
            });
		}

		public void ChangeAssignedEmployeeInAllContract(Guid currentEmployeeId, Guid contactId, Guid roleId, DateTime replacementDate)
		{
			ColumnNames colNames = new ColumnNames();
			if (roleId == ConstCs.CrocEmployeeRole.PurchasingManager)
			{
				colNames.AssignName = "CrocAssignedManagerId";
				colNames.SubstitutionName = "CrocSubstitutionManagerId";
				colNames.ReplacementDateName = "CrocReplacementPeriodManager";

			}
			else if (roleId == ConstCs.CrocEmployeeRole.ManagerSTO)
			{
				colNames.AssignName = "CrocAssignedSTOId";
				colNames.SubstitutionName = "CrocSubstitutionSTOId";
				colNames.ReplacementDateName = "CrocReplacementPeriodSTO";
			}
			else return;
			List<Guid> contractsId = GetCurrentEmployeeContracts(currentEmployeeId, colNames);
			contractsId.ForEach(contractId => {
				SetNewContractData(contractId, contactId, colNames, currentEmployeeId, replacementDate);
			});
		}

		public void ReturnAssignedEmployee()
		{
            ColumnNames colNames = new ColumnNames
            {
                AssignName = "CrocAssignedManagerId",
                SubstitutionName = "CrocSubstitutionManagerId",
                ReplacementDateName = "CrocReplacementPeriodManager"
            };
            List<ContractItem> contracts = GetContractsWithSubstitutions("CrocSubstitutionManager.Id", colNames.ReplacementDateName);
			contracts.ForEach(contract => {
				SetNewContractData(contract.Id, contract.SubstitutionId, colNames);
			});
			ColumnNames colNamesSTO = new ColumnNames
			{
				AssignName = "CrocAssignedSTOId",
				SubstitutionName = "CrocSubstitutionSTOId",
				ReplacementDateName = "CrocReplacementPeriodSTO"
			};
			List<ContractItem> contractsSTO = GetContractsWithSubstitutions("CrocSubstitutionSTO.Id", colNamesSTO.ReplacementDateName);
			contractsSTO.ForEach(contract => {
				SetNewContractData(contract.Id, contract.SubstitutionId, colNamesSTO);
			});
		}

		#endregion

	}

	#endregion

}