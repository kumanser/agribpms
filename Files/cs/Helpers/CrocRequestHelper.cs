﻿namespace Terrasoft.Configuration
{
	using System;
	using System.Collections.Generic;
	using Terrasoft.Common;
	using Terrasoft.Configuration.Base;
	using Terrasoft.Core;
	using Terrasoft.Core.Entities;
	using Terrasoft.Core.Factories;

	#region Class: ${Name}

	[DefaultBinding(typeof(ICrocRequestHelper))]
	public class CrocRequestHelper : ICrocRequestHelper
	{
		#region Constructors: Public
		public CrocRequestHelper(UserConnection userConnection)
		{
			_userConnection = userConnection;
		}
		#endregion

		#region Properties: Private
		private UserConnection _userConnection { get; set; }
		#endregion

		#region Methods: Private
		private List<Guid> GetProcessingRequests()
		{
			var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, "CrocRequest");
			var idCol = esq.AddColumn("Id");
			esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal, "CrocApplicationStatus", ConstCs.CrocApplicationStatus.Processing));
			var entities = esq.GetEntityCollection(_userConnection);
			List<Guid> requestIds = new List<Guid>();
			entities.ForEach(entity => {
				Guid id = entity.GetTypedColumnValue<Guid>(idCol.Name);
				requestIds.Add(id);
			});
			return requestIds;
		}

		private void SetIsCheckAct(Guid requestId)
		{
			CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
			entityHelper.UpdateEntity("CrocRequest", requestId, new Dictionary<string, object>() {
				{ "CrocIsCheckAct",  true},
			});
		}
		#endregion

		#region Methods: Public
		public void SetRequestIsCheckAct()
		{
			List<Guid> requestIds = GetProcessingRequests();
			requestIds.ForEach(requestId => {
				SetIsCheckAct(requestId);
			});
		}
		#endregion
	}
	#endregion
}
