﻿using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Core.Factories;
using HtmlAgilityPack;
using System.IO;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using System.Data;
using Terrasoft.Common;
using Terrasoft.Core.Entities;
using System.Text.RegularExpressions;
using Terrasoft.Web.Common;
using Terrasoft.Core.Configuration;
using Terrasoft.Web.Http.Abstractions;
using Common.Logging;
using Terrasoft.Configuration.Base;

namespace Terrasoft.Configuration
{
    #region Class: CrocUserTaskHelper
    [DefaultBinding(typeof(ICrocUserTaskHelper))]
    class CrocUserTaskHelper : ICrocUserTaskHelper
    {
        #region Fields
        private readonly UserConnection _userConnection;
        private readonly UserConnection _systemUserConnection;
        private const string _urlMacroName = "BaseCardURL";
        private const string _defaultTitle = "Сообщение";
        private IHttpContextAccessor _httpContextAccessor;
        private IHttpContextAccessor HttpContextAccessor
        {
            get
            {
                if (_httpContextAccessor == null)
                {
                    _httpContextAccessor = HttpContext.HttpContextAccessor;
                }
                return _httpContextAccessor;
            }
        }
        private static ILog _log;
        private static ILog Logger
        {
            get { return _log ?? (_log = LogManager.GetLogger("Error")); }
        }
        #endregion

        #region Constructor
        public CrocUserTaskHelper(UserConnection userConnection)
        {
            _userConnection = userConnection;
            _systemUserConnection = userConnection.AppConnection.SystemUserConnection;
        }
        #endregion

        #region Methods: Private

        private static void ConvertTo(HtmlNode node, TextWriter outText)
        {
            string html;
            switch (node.NodeType)
            {
                case HtmlNodeType.Comment:
                    // don't output comments
                    break;

                case HtmlNodeType.Document:
                    ConvertContentTo(node, outText);
                    break;

                case HtmlNodeType.Text:
                    // script and style must not be output
                    string parentName = node.ParentNode.Name;
                    if ((parentName == "script") || (parentName == "style"))
                        break;

                    // get text
                    html = ((HtmlTextNode)node).Text;

                    // is it in fact a special closing node output as text?
                    if (HtmlNode.IsOverlappedClosingElement(html))
                        break;

                    // check the text is meaningful and not a bunch of whitespaces
                    if (html.Trim().Length > 0)
                    {
                        outText.Write(HtmlEntity.DeEntitize(html));
                    }
                    break;

                case HtmlNodeType.Element:
                    switch (node.Name)
                    {
                        case "p":
                            // treat paragraphs as crlf
                            outText.Write("\r\n");
                            break;
                        case "br":
                            outText.Write("\r\n");
                            break;
                    }

                    if (node.HasChildNodes)
                    {
                        ConvertContentTo(node, outText);
                    }
                    break;
            }
        }

        private static void ConvertContentTo(HtmlNode node, TextWriter outText)
        {
            foreach (HtmlNode subnode in node.ChildNodes)
            {
                ConvertTo(subnode, outText);
            }
        }

        private string GetEmails(Guid roleId)
        {
            var select = new Select(_userConnection)
                    .Column("Contact", "Email")
                    .From("SysUserInRole")
                    .LeftOuterJoin("SysAdminUnit")
                    .On("SysAdminUnit", "Id").IsEqual("SysUserInRole", "SysUserId")
                    .LeftOuterJoin("Contact")
                    .On("Contact", "Id").IsEqual("SysAdminUnit", "ContactId")
                    .Where("SysUserInRole", "SysRoleId").IsEqual(Column.Const(roleId))
                    .And("SysAdminUnit", "Active").IsEqual(Column.Const(true))
                    .And("Contact", "Email").IsNotEqual(Column.Const(string.Empty))
                as Select;
            string result = string.Empty;
            using (DBExecutor dbExecutor = _userConnection.EnsureDBConnection())
            {
                using (IDataReader dataReader = select.ExecuteReader(dbExecutor))
                {
                    while (dataReader.Read())
                    {
                        result += dataReader.GetColumnValue<string>("Email") + ";";
                    }
                }
            }
            if (result.IsNullOrEmpty())
            {
                throw new Exception($"Для указанной роли не найден ни один заполненный email, " +
                    $"проверьте Роль или наличие email у соответствующих контактов");
            }
            return result;
        }

        private List<string> FindMacro(string source)
        {
            var rez = new List<string>();
            foreach (Match match in Regex.Matches(source, @"\[#(.+?)#\]"))
            {
                rez.Add(match.Groups[1].Value);
            }
            return rez;
        }

        private string GetTextWOMacroses(List<string> Macroses, string Text, Entity entity, Dictionary<string, string> columnNames)
        {
            string dataEmpty = string.Empty;
            foreach (string columnNamePass in Macroses)
            {
                if (columnNamePass == _urlMacroName)
                {
                    string baseUrl = GetCardLink(entity);
                    Text = Text.Replace("[#" + columnNamePass + "#]", baseUrl);
                }
                else
                {
                    string columnName = string.Empty;
                    string value = string.Empty;
                    try
                    {
                        columnName = columnNames[columnNamePass];
                        value = (entity.GetColumnValue(columnName) ?? dataEmpty).ToString();
                    }
                    catch
                    {
                        value = dataEmpty;
                    }
                    Text = Text.Replace("[#" + columnNamePass + "#]", value);
                }
            }
            return Text;
        }

        private string GetCardLink(Entity entity)
        {
            var schemaColumns = entity.Schema.Columns;
            var referenceSchemaName = entity.SchemaName;
            var referenceSchemaUId = entity.Schema.UId;
            var mainEntityId = entity.PrimaryColumnValue;
            var typeColumnUId = GetSchemaTypeColumnUId(referenceSchemaUId, out Guid moduleEntityId);
            var cardSchemaName = string.Concat(referenceSchemaName, "Page");
            var cardSchemaUId = Guid.Empty;

            if (!typeColumnUId.Equals(Guid.Empty))
            {
                try
                {
                    string typeColumnName = schemaColumns.GetByUId(typeColumnUId).Name;
                    Guid typeColumnValue = entity.GetTypedColumnValue<Guid>(typeColumnName + "Id");
                    cardSchemaUId = GetCardSchemaUId(typeColumnValue, moduleEntityId);
                    cardSchemaName = GetCardSchemaName(cardSchemaUId);
                }
                catch
                {
                    cardSchemaUId = GetCardSchemaUId(Guid.Empty, moduleEntityId);
                    cardSchemaName = GetCardSchemaName(cardSchemaUId);
                }
            }
            else
            {
                cardSchemaUId = GetCardSchemaUId(Guid.Empty, moduleEntityId);
                cardSchemaName = GetCardSchemaName(cardSchemaUId);
            }
            var sectionNameUId = GetSectionName(moduleEntityId);
            var sectionName = GetCardSchemaName(sectionNameUId);
            var template = string.Empty;
            var formatStr = string.Empty;
            /*string baseUrl = Web.Common.WebUtilities
                .GetBaseApplicationUrl(Web.Http.Abstractions.HttpContext.Current.Request);
            */
            HttpContext httpContext = HttpContextAccessor.GetInstance();
            string baseUrl = httpContext != null
                ? WebUtilities.GetBaseApplicationUrl(httpContext.Request)
                : (string)SysSettings.GetValue(_userConnection, "SiteUrl");
            if (sectionName.Contains("V2"))
            {
                template = "{2}/Nui/ViewModule.aspx#SectionModuleV2/{1}/{0}/edit/";
                formatStr = string.Format(template, cardSchemaName, sectionName, baseUrl);
            }
            else
            {
                template = "{1}/Nui/ViewModule.aspx#CardModule/{0}/view/";
                formatStr = string.Format(template, cardSchemaName, baseUrl);
            };
            return formatStr;
        }

        private Guid GetSchemaTypeColumnUId(Guid schemaUId, out Guid moduleEntityId)
        {
            moduleEntityId = Guid.Empty;
            Guid typeColumnUId = Guid.Empty;
            CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
            Dictionary<string, object> condition = new Dictionary<string, object>
            {
                { "SysEntitySchemaUId", schemaUId}
            };
            EntityCollection entityCollection = entityHelper.FindEntities("SysModuleEntity", condition);
            if (entityCollection.Count > 0)
            {
                var entity = entityCollection[0];
                typeColumnUId = entity.GetTypedColumnValue<Guid>("TypeColumnUId");
                moduleEntityId = entity.PrimaryColumnValue;
            }
            return typeColumnUId;
        }

        private Guid GetCardSchemaUId(Guid typeColumnValue, Guid sysModuleEntityId)
        {
            var cardSchemaUId = Guid.Empty;
            CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
            Dictionary<string, object> condition = new Dictionary<string, object>
            {
                { "SysModuleEntity.Id", sysModuleEntityId}
            };
            if (!typeColumnValue.Equals(Guid.Empty))
            {
                condition.Add("TypeColumnValue", typeColumnValue);
            };
            EntityCollection entityCollection = entityHelper.FindEntities("SysModuleEdit", condition);
            if (entityCollection.Count > 0)
            {
                Entity entity = entityCollection[0];
                cardSchemaUId = entity.GetTypedColumnValue<Guid>("CardSchemaUId");
            }
            return cardSchemaUId;
        }

        private string GetCardSchemaName(Guid cardSchemaUId)
        {
            var cardSchemaName = string.Empty;
            CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
            Dictionary<string, object> condition = new Dictionary<string, object>
            {
                { "UId", cardSchemaUId}
            };
            EntityCollection entityCollection = entityHelper.FindEntities("SysSchema", condition);
            if (entityCollection.Count > 0)
            {
                Entity entity = entityCollection[0];
                cardSchemaName = entity.GetTypedColumnValue<string>("Name");
            }
            return cardSchemaName;
        }

        private Guid GetSectionName(Guid Id)
        {
            var cardSchemaUId = Guid.Empty;
            CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
            Dictionary<string, object> condition = new Dictionary<string, object>
            {
                { "SysModuleEntity.Id", Id}
            };
            EntityCollection entityCollection = entityHelper.FindEntities("SysModule", condition);
            if (entityCollection.Count > 0)
            {
                Entity entity = entityCollection[0];
                cardSchemaUId = entity.GetTypedColumnValue<Guid>("SectionSchemaUId");
            }
            return cardSchemaUId;
        }

        private Guid GetSchemaTypeColumnValue(string referenceSchemaName, string typeColumnName, Guid primaryColumnValue)
        {
            Guid typeColumnValue = Guid.Empty;
            CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
            Entity entity = entityHelper.ReadEntity(referenceSchemaName, primaryColumnValue);
            if (entity != null) { typeColumnValue = entity.GetTypedColumnValue<Guid>(typeColumnName + "Id"); }
            return typeColumnValue;
        }

        private Guid CreateActivity(Dictionary<string, object> values)
        {
            CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
            return entityHelper.InsertEntity("Activity", values);
        }
        #endregion

        #region Public Virtual

        public virtual string GetSenderEmailFromMailbox(Guid mailboxSysSettingId)
        {
            CrocEntityHelper entityHelper = new CrocEntityHelper(_systemUserConnection);
            Entity emailEntity = entityHelper.ReadEntity("MailboxSyncSettings", mailboxSysSettingId);
            return emailEntity.GetTypedColumnValue<string>("SenderEmailAddress");
        }

        public virtual Entity GetFormattedEmailTemplateEntity(Guid emailTemplateId, Guid recordId)
        {
            var manager = _userConnection.EntitySchemaManager;
            CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
            Dictionary<string, object> templateCondition = new Dictionary<string, object>
            {
                { "Object.SysWorkspace.Id", _userConnection.Workspace.Id },
                { "Id", emailTemplateId}
            };
            EntityCollection entityCollection = entityHelper.FindEntities("EmailTemplate", templateCondition);
            if (entityCollection == null || entityCollection.Count < 1)
            {
                throw new Exception($"Шаблон с идентификатором {emailTemplateId} не найден");
            }
            Entity templateEntity = entityCollection.FirstOrDefault();
            string subject = templateEntity.GetTypedColumnValue<string>("Subject");
            string body = templateEntity.GetTypedColumnValue<string>("Body");
            var macrosBytes = templateEntity.GetBytesValue("Macros");
            List<string> macrosList = FindMacro(subject);
            macrosList.AddRange(FindMacro(body));
            macrosList = macrosList.Distinct().ToList();
            Guid objectId = templateEntity.GetTypedColumnValue<Guid>("ObjectId");
            Entity objectEntity = entityHelper.ReadEntity("VwSysSchemaInfo", objectId);
            string objectSchemaName = objectEntity.GetTypedColumnValue<string>("Name");
            EntitySchema schema = manager.GetInstanceByName(objectSchemaName);
            Guid referenceSchemaUId = schema.UId;
            Guid typeColumnUId = GetSchemaTypeColumnUId(referenceSchemaUId, out Guid moduleEntityId);
            EntitySchemaQuery mainESQ = new EntitySchemaQuery(manager, objectSchemaName);
            mainESQ.PrimaryQueryColumn.IsAlwaysSelect = true;
            if (!typeColumnUId.Equals(Guid.Empty))
            {
                EntitySchemaColumn column = schema.Columns.FindByUId(typeColumnUId);
                if (column != null)
                {
                    mainESQ.AddColumn(column.Name);
                }
            }
            Dictionary<string, string> columnNames = new Dictionary<string, string>();
            foreach (var columnNamePass in macrosList)
            {
                if (!columnNames.ContainsKey(columnNamePass))
                {
                    if (columnNamePass != _urlMacroName)
                    {
                        var column = schema.FindSchemaColumnByPath(columnNamePass);
                        if (column != null)
                        {
                            string esqColumnName = mainESQ.AddColumn(columnNamePass).Name;
                            columnNames.Add(columnNamePass, esqColumnName);
                        }
                    }
                }
            }
            var entity = mainESQ.GetEntity(_userConnection, (object)recordId);
            if (!string.IsNullOrEmpty(subject)) { subject = GetTextWOMacroses(macrosList, subject, entity, columnNames); }
            templateEntity.SetColumnValue("Subject", subject);
            body = GetTextWOMacroses(macrosList, body, entity, columnNames);
            templateEntity.SetColumnValue("Body", body);
            return templateEntity;
        }

        #endregion

        #region Methods: Public

        public string CollectContactsEmails(Guid roleId)
        {
            string emails;
            if (roleId.IsNotEmpty())
            {
                emails = GetEmails(roleId);
            }
            else
            {
                throw new Exception($"Параметр не может быть пустым - Роль для отбора: {roleId}");
            }
            return emails;
        }

        public Guid CreateEmailActivityOnTemplate(Guid templateId, Guid senderId, Guid macroRecordId, string subjectParam, string toParam)
        {
            try
            {
                string senderEmail = GetSenderEmailFromMailbox(senderId);
                Entity template = GetFormattedEmailTemplateEntity(templateId, macroRecordId);
                string bodyText = template.GetTypedColumnValue<string>("Body");
                string subjectText = template.GetTypedColumnValue<string>("Subject");
                if (string.IsNullOrEmpty(subjectText))
                {
                    if (string.IsNullOrEmpty(subjectParam)) { subjectText = _defaultTitle; }
                    else { subjectText = subjectParam; }
                }
                Dictionary<string, object> values = new Dictionary<string, object>
            {
                { "TypeId", ConstCs.Activity.Type.Email},
                { "Sender", senderEmail},
                { "Recepient", toParam},
                { "Title", subjectText},
                { "Body", bodyText},
                { "IsHtmlBody", true},
                { "StartDate", DateTime.UtcNow},
                { "DueDate", DateTime.UtcNow},
                { "OwnerId", _userConnection.CurrentUser.ContactId},
                { "AuthorId",  _userConnection.CurrentUser.ContactId},
                { "StatusId", ConstCs.Activity.Status.NotStarted},
                { "MessageTypeId", ConstCs.Activity.MessageType.Outgoing}
            };
                return CreateActivity(values);
            }
            catch (Exception ex)
            {
                Logger.Error($"{ex.Message}\n{ex.StackTrace}");
                throw ex;
            }
        }

        #endregion
    }
    #endregion
}
