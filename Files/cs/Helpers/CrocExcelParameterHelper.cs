﻿using System;
using System.Collections.Generic;
using System.Text;
using Terrasoft.Core;
using Terrasoft.Configuration.Base;
using System.Runtime.Serialization;
using System.Diagnostics;
using Newtonsoft.Json;
using CoreDB = Terrasoft.Core.DB;
using System.Data;
using Terrasoft.Common;
using System.Threading;
using Common.Logging;
using Terrasoft.Core.Entities;
using System.IO;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using System.Text.RegularExpressions;
using System.Linq;

namespace Terrasoft.Configuration.CrocDemetra
{
    public class CrocExcelParameterHelper
    {
        private readonly UserConnection _userConnection;
        private CrocEntityHelper _entityHelper;
        private CommonHelper _commonHelper;
        private ILog _loger = LogManager.GetLogger("Common");
        private bool _loadedData = true;
        private string _importErrorMessage = string.Empty;
        private readonly Regex _cellColumnIndexRegex = new Regex("[A-Za-z]+");
        private readonly Regex _cellRowIndexRegex = new Regex("[0-9]+");
        private bool _sharedStringItemsValuesInitialized;
        private List<string> _sharedStringItemsValues;
        private Guid _defaultSchemaUId = Guid.Parse("1bab9dcf-17d5-49f8-9536-8e0064f1dce0");
        private WorkbookPart _workbookPart;
        /// <summary>
        /// Workbook part.
        /// </summary>
        protected WorkbookPart WorkbookPart
        {
            get => _workbookPart;
            set
            {
                _sharedStringItemsValuesInitialized = false;
                _workbookPart = value;
            }
        }

        protected readonly string replaceableSymbol = "_x000D_";

        private enum FileImportStagesEnum
        {
            PrepareFileImportStage = 1,
            ProcessColumnsFileImportStage = 2,
            ProcessEntitiesFileImportStage = 3,
            CompleteFileImportStage = 4
        }
        private readonly string _importParametersSchemaName = "FileImportParameters";
        private readonly string _templateParametersSchemaName = "FileImportTemplate";
        private const string _fileImportProcessName = "FileImportProcess";

        public CrocExcelParameterHelper(UserConnection userConnection)
        {
            _userConnection = userConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
            _commonHelper = new CommonHelper(_userConnection);
        }

        public Stream GetFileStream(Guid sessionId)
        {
            var queryParameter = new CoreDB.QueryParameter("importSessionId", sessionId);
            CoreDB.Select select = new CoreDB.Select(_userConnection);
            select.Column("FileData")
                .From(_importParametersSchemaName)
                .Where("Id")
                .IsEqual(queryParameter);
            using (var dbExecuter = _userConnection.EnsureDBConnection())
            {
                using (var dataReader = select.ExecuteReader(dbExecuter))
                {
                    if (dataReader.Read()) { return dataReader.GetStreamValue("FileData"); }
                }
            }
            return null;
        }
        #region Private Methods Excel

        private IEnumerable<Row> ReadAllRows(Guid importParametersId)
        {
            using (Stream stream = GetFileStream(importParametersId))
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(stream, false))
                {
                    using (var reader = OpenXmlReader.Create(ProcessSpreadsheetDocument(spreadsheetDocument)))
                    {
                        while (reader.Read())
                        {
                            if (reader.ElementType != typeof(Row)) { continue; }
                            do
                            {
                                var row = (Row)reader.LoadCurrentElement();
                                var celValues = ProcessRow(row);
                                if (celValues.Any())
                                {
                                    yield return row;
                                }
                            } while (reader.ReadNextSibling());
                        }
                    }
                }
            }
        }

        private WorksheetPart ProcessSpreadsheetDocument(SpreadsheetDocument spreadsheetDocument)
        {
            WorkbookPart = spreadsheetDocument.WorkbookPart;
            return GetWorksheetPart(WorkbookPart);
        }

        private WorksheetPart GetWorksheetPart(WorkbookPart workbookPart)
        {
            IEnumerable<Sheet> sheets = workbookPart.Workbook.Descendants<Sheet>();
            Sheet firstSheet = sheets.First();
            WorksheetPart worksheetPart = (WorksheetPart)WorkbookPart.GetPartById(firstSheet.Id);
            return worksheetPart;
        }

        public IEnumerable<ImportColumnValue> ProcessRow(Row row)
        {
            IEnumerable<Cell> cells = row.Elements<Cell>();
            return ProcessCells(cells);
        }

        public IEnumerable<ImportColumnValue> ProcessCells(IEnumerable<Cell> cells)
        {
            List<ImportColumnValue> columnValues = new List<ImportColumnValue>();
            foreach (Cell cell in cells)
            {
                var columnValue = ProcessCell(cell);
                if (columnValue.Value.IsNotNullOrEmpty())
                {
                    columnValue.Value = columnValue.Value.Replace(replaceableSymbol, String.Empty);
                    columnValues.Add(columnValue);
                }
            }
            return columnValues;
        }

        public ImportColumnValue ProcessCell(Cell cell)
        {
            string cellValue = GetCellValue(cell);
            string cellColumnIndex = GetCellIndex(_cellColumnIndexRegex, cell);
            string rowIndex = GetCellIndex(_cellRowIndexRegex, cell);
            return new ImportColumnValue(cellValue, cellColumnIndex, rowIndex);
        }

        private string GetCellValue(Cell cell)
        {
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return GetSharedCellValue(cell);
            }
            return (cell.CellValue != null) ? ProcessCellValue(cell.CellValue.Text) : null;
        }

        private string ProcessCellValue(string value)
        {
            bool success = double.TryParse(value, System.Globalization.NumberStyles.AllowDecimalPoint
                | System.Globalization.NumberStyles.AllowExponent,
                System.Globalization.CultureInfo.InvariantCulture, out double doubleValue);
            return (success) ? doubleValue.ToString() : value;
        }

        private string GetCellIndex(Regex regexExpression, Cell cell)
        {
            Match match = regexExpression.Match(cell.CellReference.Value);
            return match.Value;
        }

        private string GetSharedCellValue(Cell cell)
        {
            int itemIndex = int.Parse(cell.CellValue.Text);
            return SharedStringItemsValues[itemIndex];
        }

        protected List<string> SharedStringItemsValues
        {
            get
            {
                if (!_sharedStringItemsValuesInitialized)
                {
                    IEnumerable<SharedStringItem> sharedStringItems = GetSharedStringItems(WorkbookPart);
                    _sharedStringItemsValues = GetSharedStringItemsValues(sharedStringItems);
                    _sharedStringItemsValuesInitialized = true;
                }
                return _sharedStringItemsValues;
            }
        }

        private List<string> GetSharedStringItemsValues(IEnumerable<SharedStringItem> sharedStringItems)
        {
            List<string> sharedStringItemValues = new List<string>();
            foreach (SharedStringItem item in sharedStringItems)
            {
                sharedStringItemValues.Add(item.InnerText);
            }
            return sharedStringItemValues;
        }

        private IEnumerable<SharedStringItem> GetSharedStringItems(WorkbookPart workbookPart)
        {
            SharedStringTablePart sharedStringTablePart = workbookPart.SharedStringTablePart;
            SharedStringTable sharedStringTable = sharedStringTablePart.SharedStringTable;
            return sharedStringTable.Elements<SharedStringItem>();
        }

        private IEnumerable<Cell> GetRowCels(Row firstRow)
        {
            string exceptionMessage = "Пустая строка, загрузка отменена";
            if (firstRow == null)
            {
                throw new ArgumentNullOrEmptyException(exceptionMessage);
            }
            IEnumerable<Cell> dataCells = firstRow.Elements<Cell>();
            if (!dataCells.Any())
            {
                throw new ArgumentNullOrEmptyException(exceptionMessage);
            }
            return dataCells;
        }

        #endregion

        private string GetStringFrom(byte[] value)
        {
            return value == null
                ? null
                : Encoding.UTF8.GetString(value);
        }

        private byte[] GetBytesFrom<T>(string value)
        {
            object val = JsonConvert.DeserializeObject<T>(value);
            byte[] arraySama = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(val));
            return arraySama;
        }

        

        private bool CheckIsFileValid(Guid importParametersId, string schemaCaption)
        {
            Thread.Sleep(3000);
            bool isFileValid = true;
            var rows = ReadAllRows(importParametersId).ToList();
            if (rows.Any() && (rows.FirstOrDefault(r => r.RowIndex != 1) != null))
            {
                int rowNumb = 1;
                try
                {
                    var headerRow = rows.FirstOrDefault(r => r.RowIndex == 1);
                    CheckExcelHeaders(importParametersId, GetRowCels(headerRow));
                    rows = rows.Where(r => r.RowIndex > 1).ToList();
                    foreach (var row in rows)
                    {
                        rowNumb++;
                        if (isFileValid)
                        {
                            var dataCells = GetRowCels(row);
                            try
                            {
                                isFileValid = CheckDataCellsValueTypes(dataCells, importParametersId);
                                if (!isFileValid)
                                {
                                    _importErrorMessage = $"{schemaCaption}: Некорректные данные в строке {rowNumb}, загрузка отменена";
                                    return isFileValid;
                                }
                            }
                            catch(Exception ex)
                            {
                                isFileValid = false;
                                _importErrorMessage = $"{schemaCaption}: {ex.Message}, загрузка отменена";
                            }
                            
                        }
                    }
                }
                catch(Exception ex)
                {
                    isFileValid = false;
                    _importErrorMessage = $"{schemaCaption}: Некорректный файл, загрузка отменена. Просьба скачать шаблон из базы знаний";
                    _loger.Error(ex.Message);
                    _loger.Error(ex.StackTrace);
                }
            }
            else
            {
                isFileValid = false;
                _importErrorMessage = $"{schemaCaption}: Некорректный файл, загрузка отменена. Просьба скачать шаблон из базы знаний";
            }
            return isFileValid;
        }

        private void CheckExcelHeaders(Guid importParametersId, IEnumerable<Cell> headerCell)
        {
            List<ImportColumn> columns = GetColumnsConfig(importParametersId);
            foreach (ImportColumn column in columns)
            {
                string indexCode = column.Index;
                string value = string.Empty;
                List<Cell> cells = headerCell.Where(x => x.CellReference.Value.Contains(indexCode)).ToList();
                if (cells?.Count > 0)
                {
                    value = GetCellValue(cells.FirstOrDefault());
                }
                if (string.IsNullOrEmpty(value)) { throw new Exception("Пустой заголовок"); }
                if (value.Trim().ToLower() != column.Source.Trim().ToLower()) { throw new Exception($"Заголовок {value} не соответствует настройке импорта {column.Source}"); }
            }
        }

        private bool CheckDataCellsValueTypes(IEnumerable<Cell> dataCell, Guid importParametersId)
        {
            bool result = true;
            List<ImportColumn> columns = GetColumnsConfig(importParametersId);
            foreach (ImportColumn column in columns)
            {
                if(result)
                {
                    string indexCode = column.Index;
                    string typeUId = column.Destinations.FirstOrDefault().Properties.GetValue("TypeUId", string.Empty);
                    string referSchemaName = column.Destinations.FirstOrDefault().Properties.GetValue("ReferenceSchemaName", string.Empty);
                    DataValueType dataType = new DataValueTypeManager().FindInstanceByUId(Guid.Parse(typeUId));
                    Type valueType = dataType.ValueType;
                    string value = string.Empty;
                    List<Cell> cells = dataCell.Where(x => x.CellReference.Value.Contains(indexCode)).ToList();
                    if(cells?.Count > 0)
                    {
                        value = GetCellValue(cells.FirstOrDefault());
                    }
                    if(!string.IsNullOrEmpty(value))
                    {
                        if(!string.IsNullOrEmpty(referSchemaName))
                        {
                            EntitySchema entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(referSchemaName);
                            string displayColumn = entitySchema.PrimaryDisplayColumn.Name;
                            List<Entity> refEntities = _entityHelper.FindEntities(referSchemaName, new Dictionary<string, object> { { displayColumn, value } }).ToList();
                            if (refEntities == null || refEntities.Count < 1) 
                            {
                                string refCaprion = _commonHelper.GetSchemaCaption(referSchemaName);
                                throw new Exception($"В справочнике {refCaprion} отсутствует значение {value}");
                            }
                        }
                        else
                        {
                            if (valueType == typeof(DateTime))
                            {
                                try 
                                {
                                    if (!DateTime.TryParse(value, out DateTime date))
                                    {
                                        double d = double.Parse(value);
                                        DateTime conv = DateTime.FromOADate(d);
                                    }
                                }
                                catch { result = false; }
                            }
                            if (valueType == typeof(int))
                            {
                                try
                                { Convert.ToInt32(value); }
                                catch { result = false; }
                            }
                            if (valueType == typeof(decimal) || valueType == typeof(float))
                            {
                                try
                                { Convert.ToDecimal(value); }
                                catch { result = false; }
                            }
                            if (valueType == typeof(bool))
                            {
                                try
                                { Convert.ToBoolean(value); }
                                catch { result = false; }
                            }
                        }
                    }
                }
            }
            return result;
        }

        private void StartFileImportProcess(Guid importSessionId)
        {
            var manager = _userConnection.ProcessSchemaManager;
            var processSchema = manager.GetInstanceByName(_fileImportProcessName);
            var process = processSchema.CreateProcess(_userConnection);
            process.TrySetPropertyValue("ImportSessionId", importSessionId);
            process.Execute(_userConnection);
            int waitCounter = 5;
            while (!process.IsExecuted || waitCounter >= 0)
            {
                waitCounter -= 1;
                Thread.Sleep(1500);
                _loger.Info($"StartFileImportProcess - {importSessionId} Counter: {waitCounter}");
            }
            _loger.Info($"StartFileImportProcess - {importSessionId} CounterOut: {waitCounter}");
            //_loadedData = waitCounter > 0;
        }

        /*private void UpdateImportedData(string schemaName, string detailColumn, Guid detailValue)
        {
            string dbColumnName = detailColumn + "Id";
            DateTime controlDate = DateTime.UtcNow.Date;
            Select subSelect = new Select(_userConnection)
                .Column("Id")
                .From(schemaName)
                .Where(dbColumnName).IsNull()
                .And("CreatedById").IsEqual(Column.Parameter(_userConnection.CurrentUser.ContactId))
                .And("CreatedOn").IsGreaterOrEqual(Column.Parameter(controlDate))
            as Select;
            Update update = new Update(_userConnection, schemaName)
                .Set(dbColumnName, Column.Parameter(detailValue))
                .Where("Id").In(subSelect)
            as Update;
            int updateCounter = update.Execute();
            _loger.Info($"UpdateImportedData - {schemaName} UpdateCounter - {updateCounter}");
        }*/

        private void UpdateImportedDataEsq(string schemaName, string detailColumn, Guid detailValue) 
        {
            
            string dbColumnName = detailColumn + "Id";
            DateTime controlDate = DateTime.UtcNow.Date;
            var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, schemaName);
            esq.PrimaryQueryColumn.IsAlwaysSelect = true;
            esq.AddColumn("CreatedBy");
            esq.AddColumn("CreatedOn");
            esq.AddColumn(detailColumn);
            esq.Filters.Add(esq.CreateIsNullFilter(detailColumn));
            esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal, "CreatedBy", _userConnection.CurrentUser.ContactId));
            esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.GreaterOrEqual, "CreatedOn", controlDate));
            EntityCollection entities = esq.GetEntityCollection(_userConnection);
            entities.ForEach(entity => {
                entity.SetColumnValue(dbColumnName, detailValue);
                entity.Save();
            });
            _loger.Info($"UpdateImportedData - {schemaName}");
        }

        private void ClearOldRecords(string schemaName, string detailColumn, Guid detailValue)
        {
            string dbColumnName = detailColumn + "Id";
            var subSelect = new CoreDB.Select(_userConnection)
                .Column("Id")
                .From(schemaName)
                .Where(dbColumnName).IsEqual(CoreDB.Column.Parameter(detailValue))
            as CoreDB.Select;
            var delete = new CoreDB.Delete(_userConnection)
                .From(schemaName)
                .Where("Id").In(subSelect)
            as CoreDB.Delete;
            int deleteCounter = delete.Execute();
            _loger.Info($"ClearOldRecords - {schemaName} DeleteCounter - {deleteCounter}");
        }

        private void DeleteUploadedFile(Guid sessionId)
        {
            Entity importParameters = _entityHelper.ReadEntity(_importParametersSchemaName, sessionId);
            int stage = importParameters.GetTypedColumnValue<int>("Stage");
            if(stage == 4)
            {
                importParameters.SetColumnValue("FileData", null);
                importParameters.Save(false);
            }
        }

        private List<ImportColumn> GetColumnsConfig(Guid sessionId)
        {
            var select = new CoreDB.Select(_userConnection)
                .Column(_importParametersSchemaName, "ImportParameters")
                .From(_importParametersSchemaName).As(_importParametersSchemaName)
                .Where(_importParametersSchemaName, "Id").IsEqual(CoreDB.Column.Parameter(sessionId))
            as CoreDB.Select;
            string importText = GetStringFrom(select.ExecuteScalar<byte[]>());
            ImportParameters parameters = JsonConvert.DeserializeObject<ImportParameters>(importText);
            return parameters.Columns.ToList();
        }

        private void PostUpdateAdditional(string schemaName, string detailColumn, Guid detailValue)
        {
            if(string.IsNullOrEmpty(schemaName) || string.IsNullOrEmpty(detailColumn) || detailValue == Guid.Empty)
            { return; }
            switch(schemaName)
            {
                case "CrocRegisterLoading":
                    _commonHelper.UpdateDocumentRegisterLoading(detailValue);
                    break;
            }
        }


        public Guid AddNewExcelImport(string config)
        {
            Guid newImportId = Guid.Empty;
            newImportId = _entityHelper.InsertEntity(_importParametersSchemaName, new Dictionary<string, object>
            {
                { "Stage", 0},
                { "ImportParameters", GetBytesFrom<ImportParameters>(config)}
            });
            return newImportId;
        }

        public void RunImport(Guid sessionId, string schemaName, string detailColumn, Guid detailValue, bool clearOldRows)
        {
            if(CheckIsFileValid(sessionId, _commonHelper.GetSchemaCaption(schemaName)))
            {
                if (clearOldRows)
                {
                    ClearOldRecords(schemaName, detailColumn, detailValue);
                }
                StartFileImportProcess(sessionId);
                if (_loadedData)
                {
                    //UpdateImportedData(schemaName, detailColumn, detailValue);
                    UpdateImportedDataEsq(schemaName, detailColumn, detailValue);
                    DeleteUploadedFile(sessionId);
                    PostUpdateAdditional(schemaName, detailColumn, detailValue);
                    _commonHelper.SendMessageToClient(schemaName, detailValue);
                }
            }
            else
            {
                if(!string.IsNullOrEmpty(_importErrorMessage)) { _commonHelper.SendRemindMessageToUser(_importErrorMessage, _defaultSchemaUId); }
            }
        }

        public string TestImportParameters(Guid recordId)
        {
            string result = string.Empty;
            Dictionary<Guid, string> parameters = new Dictionary<Guid, string>();
            var select = new CoreDB.Select(_userConnection)
                .Top(1)
                    .Column(_importParametersSchemaName, "Id")
                    .Column(_importParametersSchemaName, "ImportParameters")
                    .Column(_importParametersSchemaName, "ImportEntities")
                    .Column(_importParametersSchemaName, "Stage")
                .From(_importParametersSchemaName).As(_importParametersSchemaName)
                .Where("Id").IsEqual(CoreDB.Column.Parameter(recordId))
                 as CoreDB.Select;
            using (CoreDB.DBExecutor dbExecutor = _userConnection.EnsureDBConnection())
            {
                using (IDataReader record = select.ExecuteReader(dbExecutor))
                {
                    while (record.Read())
                    {
                        var parametersT = GetStringFrom(record.GetColumnValue<byte[]>("ImportParameters"));
                        var entities = GetStringFrom(record.GetColumnValue<byte[]>("ImportEntities"));
                        var processId = record.GetColumnValue<Guid>("Id");
                        if (!parameters.ContainsKey(processId)) parameters.Add(processId, parametersT);
                    }
                }
            }
            if (parameters?.Count > 0)
            {
                if (parameters.ContainsKey(recordId)) {
                    if(parameters.TryGetValue(recordId, out result))
                    {
                        var obj = JsonConvert.DeserializeObject<ImportParameters>(result);
                        result = JsonConvert.SerializeObject(obj);
                    }
                }
            }
            return result;
        }

        public void TestTemplateImportParameters(Guid recordId)
        {
            Dictionary<Guid, string> parameters = new Dictionary<Guid, string>();
            var select = new CoreDB.Select(_userConnection)
                .Top(1)
                    .Column(_templateParametersSchemaName, "Id")
                    .Column(_templateParametersSchemaName, "TemplateData")
                .From(_templateParametersSchemaName).As(_templateParametersSchemaName)
                .OrderByDesc(_templateParametersSchemaName, "CreatedOn")
                 as CoreDB.Select;
            using (CoreDB.DBExecutor dbExecutor = _userConnection.EnsureDBConnection())
            {
                using (IDataReader record = select.ExecuteReader(dbExecutor))
                {
                    while (record.Read())
                    {
                        var parametersT = GetStringFrom(record.GetColumnValue<byte[]>("TemplateData"));
                        var processId = record.GetColumnValue<Guid>("Id");
                        if (!parameters.ContainsKey(processId)) parameters.Add(processId, parametersT);
                    }
                }
            }
            if (parameters?.Count > 0)
            {
                parameters.ContainsKey(recordId);
            }
        }
    }

    [DataContract]
    [Serializable]
    public class ImportParameters
    {

        /// <summary>
        /// Flag for send notify.
        /// </summary>
        [DataMember(Name = "NeedSendNotify")]
        public bool NeedSendNotify;

        /// <summary>
        /// Root schema unique identifier.
        /// </summary>
        [DataMember(Name = "rootSchemaUId")]
        public Guid RootSchemaUId;

        /// <summary>
        /// Not imported rows count.
        /// </summary>
        [DataMember(Name = "notImportedRowsCount")]
        public uint NotImportedRowsCount;

        /// <summary>
        /// Imported rows count.
        /// </summary>
        [DataMember(Name = "importedRowsCount")]
        public uint ImportedRowsCount;

        /// <summary>
        /// Processed rows count.
        /// </summary>
        public uint ProcessedRowsCount;

        /// <summary>
        /// Total rows count.
        /// </summary>
        [DataMember(Name = "totalRowsCount")]
        public uint TotalRowsCount;

        /// <summary>
        /// Import chunk size.
        /// </summary>
        [DataMember(Name = "chunkSize")]
        public int ChunkSize;

        /// <summary>
        /// Id of contact who started import
        /// </summary>.
        [DataMember(Name = "authorId")]
        public Guid AuthorId;

        /// <summary>
        /// Time zone of contact who started import, need for Newtonsoft.Json serialization
        /// </summary>
        [DataMember(Name = "authorTimeZoneInfo")]
        public string AuthorTimeZoneInfo;

        /// <summary>
		/// Import columns.
		/// </summary>
		[DataMember(Name = "columns")]
        public IEnumerable<ImportColumn> Columns { get; set; }


        [DataMember(Name = "importObject")]
        public ImportObject ImportObject { get; set; }
    }

    [DataContract]
    [DebuggerDisplay("Index={Index}; Source={Source}")]
    [Serializable]
    public class ImportColumn
    {
        [DataMember(Name = "index")]
        public string Index;

        [DataMember(Name = "source")]
        public string Source;

        [DataMember(Name = "destinations")]
        public List<ImportColumnDestination> Destinations { get; set; }
    }

    [DataContract]
    [DebuggerDisplay("SchemaUId={SchemaUId}; ColumnName={ColumnName}")]
    [Serializable]
    public class ImportColumnDestination
    { 
        /// <summary>
        /// Schema unique identifier.
        /// </summary>
        [DataMember(Name = "schemaUId")]
        public Guid SchemaUId;

        /// <summary>
        /// Column name.
        /// </summary>
        [DataMember(Name = "columnName")]
        public string ColumnName;

        /// <summary>
        /// Column value name.
        /// </summary>
        [DataMember(Name = "columnValueName")]
        public string ColumnValueName;

        /// <summary>
        /// True if key for duplication search.
        /// </summary>
        [DataMember(Name = "isKey")]
        public bool IsKey;

        /// <summary>
        /// Destination properties.
        /// </summary>
        [DataMember(Name = "properties")]
        public Dictionary<string, object> Properties { get; set; }

        /// <summary>
        /// Destination properties.
        /// </summary>
        [DataMember(Name = "attributes")]
        public Dictionary<string, object> Attributes { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ImportObject
    {

        [DataMember(Name = "uId")]
        public Guid UId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "caption")]
        public string Caption { get; set; }

        [DataMember(Name = "isOtherObject")]
        public bool IsOtherObject { get; set; }

    }

    #region Class: ImportColumnValue

    /// <summary>
    /// An instance of this class represents import column value.
    /// </summary>
    [DebuggerDisplay("Value={" + nameof(Value) + "}")]
    [Serializable]
    public class ImportColumnValue
    {

        #region Fields: Public

        /// <summary>
        /// Column index.
        /// </summary>
        [DataMember(Name = "columnIndex")]
        public string ColumnIndex;

        /// <summary>
        /// Row index.
        /// </summary>
        [DataMember(Name = "rowIndex")]
        public string RowIndex;

        #endregion

        #region Properties: Public

        /// <summary>
        /// Value.
        /// </summary>
        [DataMember(Name = "value")]
        public string Value { get; set; }

        #endregion

        #region Constructors: Public

        /// <summary>
        /// Creates instance of type <see cref="ImportColumnValue"/>.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="columnIndex">Column index.</param>
        /// <param name="rowIndex">Row index.</param>
        public ImportColumnValue(string value, string columnIndex, string rowIndex = "")
        {
            ColumnIndex = columnIndex;
            Value = value;
            RowIndex = rowIndex;
        }

        #endregion
    }

    #endregion
}
