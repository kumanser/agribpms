﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.OneCIntegration;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Tasks;

namespace Terrasoft.Configuration
{
    public class SendAccreditationOnDocumentBackground : IBackgroundTask<Dictionary<string, object>>, IUserConnectionRequired
    {
        private UserConnection _userConnection;
        private UserConnection _currentUserConnection;
        private CrocEntityHelper _entityHelper;

        public void SetUserConnection(UserConnection userConnection)
        {
            _currentUserConnection = userConnection;
            _userConnection = userConnection.AppConnection.SystemUserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }
        public void Run(Dictionary<string, object> parameters)
        {
            Guid documentId = (Guid)parameters.FirstOrDefault(x => x.Key == "DocumentId").Value;
            List<Entity> setDocuments = _entityHelper.FindEntities("CrocSetDocument", new Dictionary<string, object>
            { { "CrocDocument.Id", documentId } }).ToList();
            if (setDocuments?.Count > 0) 
            {
                Guid masterDocumentId = setDocuments.Select(x => x.GetTypedColumnValue<Guid>("CrocMainDocumentId")).FirstOrDefault();
                if (masterDocumentId != Guid.Empty) 
                {
                    Entity accreditation = _entityHelper.ReadEntity("CrocDocument", masterDocumentId);
                    DateTime endDate = accreditation.GetTypedColumnValue<DateTime>("CrocEndAccreditation");
                    Guid checkStatusId = accreditation.GetTypedColumnValue<Guid>("CrocCheckStatusId");
                    Guid documentTypeId = accreditation.GetTypedColumnValue<Guid>("CrocDocumentTypeId");
                    if (checkStatusId == ConstCs.CrocDocument.CheckStatus.PassedId 
                        && endDate > DateTime.Now
                        && documentTypeId == ConstCs.CrocDocument.Type.AccreditationId)
                    {
                        OneCIntegrationHelper cHelper = new OneCIntegrationHelper(_currentUserConnection);
                        cHelper.SendAccreditation(masterDocumentId);
                    }
                }
            }

        }
    }
}