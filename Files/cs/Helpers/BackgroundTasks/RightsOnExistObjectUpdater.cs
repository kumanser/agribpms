﻿//BackgroundRightsOnExistObjectUpdater
using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.RightsService;
using Terrasoft.Core;
using Terrasoft.Core.Configuration;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Tasks;
using Common.Logging;

namespace Terrasoft.Configuration
{
    public class RightsOnExistObjectUpdater : IBackgroundTask<Dictionary<string, object>>, IUserConnectionRequired
    {
        private UserConnection _userConnection;
        private RightsHelper _rightsHelper;
        private CrocEntityHelper _entityHelper;
        private static ILog _log = LogManager.GetLogger("Error");
        private static Dictionary<string, List<string>> _directSchemaNames = new Dictionary<string, List<string>>
        {
            { "Contact", new List<string> { "Account" } },
            { "CrocChart", new List<string> { "CrocWarehouseLoading" } },
            { "CrocContract", new List<string> { "CrocAccount" } },
            { "CrocDocument", new List<string> { "CrocAccount", "CrocPort" } },
            { "CrocGrainSaleApplication", new List<string> { "CrocAccount" } },
            { "CrocHelp", new List<string> { "CreatedBy" } },
            { "CrocLoan", new List<string> { "CrocPort" } },
            { "CrocPositionPlan", new List<string> { "CrocPort" } },
            { "CrocRejectedTransport", new List<string> { "CrocProvider" } },
            { "CrocRequest", new List<string> { "CrocProvider" } },
            { "CrocSupplyPlan", new List<string> { "CrocPort" } }
        };
        private static List<string> _additionalSchemaNames = new List<string>
        { "CrocChart", "CrocHelp"};

        public void SetUserConnection(UserConnection userConnection)
        {
            _userConnection = userConnection.AppConnection.SystemUserConnection;
            _rightsHelper = new RightsHelper(_userConnection);
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private List<Guid> GetAccountRoles(Guid accountId)
        {
            List<Guid> roles = null;
            List<Entity> entities = _entityHelper.FindEntities("CrocEnterpriseRightRecord", new Dictionary<string, object>
            { { "CrocAccount.Id", accountId } }).ToList();
            if (entities?.Count > 0)
            {
                roles = entities
                    .Where(x => x.GetTypedColumnValue<Guid>("CrocRoleId") != Guid.Empty)
                    .Select(x => x.GetTypedColumnValue<Guid>("CrocRoleId"))
                    .Distinct()
                    .ToList();
            }
            return roles;
        }

        private void SetDefaultRights(string schemaName, Guid recordId)
        {
            List<DefRight> rights = _rightsHelper.GetSchemaDefRights(schemaName);
            if (rights?.Count > 0)
            {
                foreach (DefRight right in rights)
                {
                    _rightsHelper.SetRecordRight(right.SysAdminUnitId, schemaName, recordId.ToString(), right.Operation, right.RightLevel);
                }
            }
        }

        private List<Guid> GetObjectRecords(string schemaName, Guid accountId)
        {
            List<Guid> result = new List<Guid>();
            if(_directSchemaNames.TryGetValue(schemaName, out List<string> fields))
            {
                if(_additionalSchemaNames.Contains(schemaName))
                {
                    string field = fields.FirstOrDefault();
                    List<EntitySchemaColumn> schemaColums = _userConnection.EntitySchemaManager.GetInstanceByName(schemaName).Columns.ToList();
                    schemaColums = schemaColums.Where(x => x.Name == field).ToList();
                    if(schemaColums?.Count > 0)
                    {
                        EntitySchemaColumn column = schemaColums.FirstOrDefault();
                        string refSchemaName = column.ReferenceSchema.Name;
                        List<Guid> recordIds = new List<Guid>();
                        switch (refSchemaName)
                        {
                            case "CrocTerminal":
                                List<Entity> entitiesTerminal = _entityHelper.FindEntities(refSchemaName, new Dictionary<string, object> { { "CrocAccount.Id", accountId } }).ToList();
                                recordIds = entitiesTerminal.Select(x => x.PrimaryColumnValue).Distinct().ToList();
                                break;
                            case "Contact":
                                List<Entity> entitiesContact = _entityHelper.FindEntities(refSchemaName, new Dictionary<string, object> { { "CrocAccount.Id", accountId } }).ToList();
                                recordIds = entitiesContact.Select(x => x.PrimaryColumnValue).Distinct().ToList();
                                break;
                        }
                        if(recordIds.Count > 0)
                        {
                            foreach(Guid refRecordId in recordIds)
                            {
                                List<Entity> entities = _entityHelper.FindEntities(schemaName, new Dictionary<string, object> { { field + ".Id", refRecordId } }).ToList();
                                if (entities?.Count > 0)
                                {
                                    List<Guid> recordOnRefIds = entities.Select(x => x.PrimaryColumnValue).Distinct().ToList();
                                    result.AddRange(recordIds);
                                }
                            }
                            if (result.Count > 0) { result = result.Distinct().ToList(); }
                        }
                    }

                }
                else
                {
                    foreach (string field in fields)
                    {
                        List<Entity> entities = _entityHelper.FindEntities(schemaName, new Dictionary<string, object> { { field + ".Id", accountId } } ).ToList();
                        if (entities?.Count > 0)
                        {
                            List<Guid> recordIds = entities.Select(x => x.PrimaryColumnValue).Distinct().ToList();
                            result.AddRange(recordIds);
                        }
                    }
                    if (result.Count > 0) { result = result.Distinct().ToList(); }
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters">AccountId (Guid)</param>
        public void Run(Dictionary<string, object> parameters)
        {
            Guid accountId = (Guid)parameters.FirstOrDefault(x => x.Key == "AccountId").Value;
            try
            {
                if (accountId == null || accountId == Guid.Empty) return;
                foreach(KeyValuePair<string, List<string>> directSchema in _directSchemaNames)
                {
                    string schemaName = directSchema.Key;
                    List<Guid> records = GetObjectRecords(schemaName, accountId);
                    try
                    {
                        if(records?.Count > 0)
                        {
                            foreach(Guid recordId in records)
                            {
                                List<Guid> roles = new List<Guid>();
                                string rightSchemaName = _rightsHelper.GetRecordRightsSchemaDefName(schemaName);
                                var rolesToDel = _rightsHelper.GetRecordRights(rightSchemaName, recordId.ToString());
                                rolesToDel.ForEach(oldRole => { oldRole.isDeleted = true; });
                                _rightsHelper.DeleteRecordRights(rolesToDel.ToArray(), new Record { entitySchemaName = schemaName, primaryColumnValue = recordId.ToString() });
                                SetDefaultRights(schemaName, recordId);
                                List<Guid> tmpRoles = GetAccountRoles(accountId);
                                if (tmpRoles?.Count > 0) roles.AddRange(tmpRoles);
                                if (roles?.Count > 0)
                                {
                                    roles = roles.Distinct().ToList();
                                    foreach (Guid roleId in roles)
                                    {
                                        _rightsHelper.SetRecordRight(roleId, schemaName, recordId.ToString(), (int)EntitySchemaRecordRightOperation.Read, 1);
                                        _rightsHelper.SetRecordRight(roleId, schemaName, recordId.ToString(), (int)EntitySchemaRecordRightOperation.Edit, 1);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error($"{schemaName} rights: {ex.Message}\n{ex.StackTrace}");
                    }
                }
                
            }
            catch (Exception ex)
            {
                _log.Error($"rights update: {ex.Message}\n{ex.StackTrace}");
            }
        }
    }
}
