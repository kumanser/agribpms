﻿using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Configuration.CrocDemetra;
using Terrasoft.Core;
using Terrasoft.Core.Tasks;

namespace Terrasoft.Configuration
{
    public class UpdateMonthlyWeeklyPlansBackground : IBackgroundTask<Dictionary<string, object>>, IUserConnectionRequired
    {
        private UserConnection _userConnection;
        private CommonHelper _commonHelper;

		public void SetUserConnection(UserConnection userConnection)
		{
			_userConnection = userConnection.AppConnection.SystemUserConnection;
			_commonHelper = new CommonHelper(_userConnection);
		}
		public void Run(Dictionary<string, object> parameters)
		{
			Guid chartId = (Guid)parameters.FirstOrDefault(x => x.Key == "ChartId").Value;
			DateTime deliveryDate = (DateTime)parameters.FirstOrDefault(x => x.Key == "DeliveryDate").Value;
			if (chartId != Guid.Empty)
			{
				_commonHelper.UpdataDataOnPlanChange(chartId, deliveryDate);
			}
		}
	}
}
