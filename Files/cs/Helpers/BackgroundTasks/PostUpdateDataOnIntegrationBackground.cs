﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.CrocDemetra;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Tasks;
using Terrasoft.Configuration.RightsService;

namespace Terrasoft.Configuration
{
    public class PostUpdateDataOnIntegrationBackground : IBackgroundTask<Dictionary<string, object>>, IUserConnectionRequired
    {
        private UserConnection _userConnection;
        private UserConnection _currentUserConnection;
        private CrocEntityHelper _entityHelper;
        private CommonHelper _commonHelper;
        private RightsHelper _rightsHelper;
        private static ILog _log = LogManager.GetLogger("Error");
        private string _schemaName = string.Empty;

        public void SetUserConnection(UserConnection userConnection)
        {
            _currentUserConnection = userConnection;
            _userConnection = userConnection.AppConnection.SystemUserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
            _commonHelper = new CommonHelper(_userConnection);
            _rightsHelper = new RightsHelper(_userConnection);
        }

        #region Loan
        private void UpdateCrocLoan()
        {
            List<Guid> loanToUpdate = GetPKToUpdate(_schemaName, "CrocIsRefresh");
            if(loanToUpdate?.Count > 0)
            {
                foreach(Guid loanId in loanToUpdate)
                {
                    Entity loan = _entityHelper.ReadEntity(_schemaName, loanId);
                    decimal loanVolume = loan.GetTypedColumnValue<decimal>("CrocVolume");
                    List<Entity> repayments = _entityHelper.FindEntities("CrocRepayment", new Dictionary<string, object> { { "CrocLoan.Id", loanId } }).ToList();
                    decimal sumRepaymentVolume = 0;
                    if(repayments?.Count > 0)
                    {
                        try
                        {
                            List<Entity> repaymentsToCount = repayments.Where(x => x.GetTypedColumnValue<bool>("CrocIsLeaveOnUpdate") == true).ToList();
                            if(repaymentsToCount?.Count > 0)
                            {
                                sumRepaymentVolume = repaymentsToCount.Sum(x => x.GetTypedColumnValue<decimal>("CrocRepaymentVolume"));
                                QueryParameter[] queryParameters = _commonHelper.GetPametersGuidList(repaymentsToCount.Select(x => x.PrimaryColumnValue).ToList());
                                Update update = new Update(_userConnection, "CrocRepayment")
                                    .Set("CrocIsLeaveOnUpdate", Column.Parameter(false))
                                    .Where("Id").In(queryParameters)
                                as Update;
                                update.Execute();
                            }
                            List<Guid> repaymentsToDelete = repayments
                                .Where(x => x.GetTypedColumnValue<bool>("CrocIsLeaveOnUpdate") == false)
                                .Select(x => x.PrimaryColumnValue).ToList();
                            if(repaymentsToDelete?.Count > 0)
                            {
                                QueryParameter[] queryParameters = _commonHelper.GetPametersGuidList(repaymentsToDelete);
                                Delete delete = new Delete(_userConnection)
                                    .From("CrocRepayment")
                                    .Where("Id").In(queryParameters)
                                as Delete;
                                delete.Execute();
                            }
                        }
                        catch { }
                    }
                    loan.SetColumnValue("CrocRepaymentVolumeRemainder", loanVolume - sumRepaymentVolume);
                    loan.SetColumnValue("CrocIsRefresh", false);
                    loan.Save(true);
                }
            }
        }
        #endregion

        private List<Guid> GetPKToUpdate(string schemaName, string controlField)
        {
            List<Guid> result = new List<Guid>();
            List<Entity> entities = _entityHelper.FindEntities(schemaName, new Dictionary<string, object> { { controlField, true } }).ToList();
            if(entities?.Count > 0)
            {
                result = entities.Select(x => x.PrimaryColumnValue).ToList();
            }
            return result;
        }

        #region PositionPlan
        private void UpdateCrocPositionPlan()
        {
            List<Guid> positionPlanToUpdate = GetPKToUpdate(_schemaName, "CrocRefresh");
            if (positionPlanToUpdate?.Count > 0)
            {
                foreach (Guid positionPlanId in positionPlanToUpdate)
                {
                    Entity positionPlan = _entityHelper.ReadEntity(_schemaName, positionPlanId);
                    List<Entity> positions = _entityHelper.FindEntities("CrocPosition", new Dictionary<string, object> { 
                        { "CrocPositionPlan.Id", positionPlanId },
                    }).ToList();
                    if (positions?.Count > 0)
                    {
                        try
                        {
                            List<Guid> positionsToDelete = positions
                                .Where(x => x.GetTypedColumnValue<bool>("CrocLeaveOnUpdate") == false)
                                .Select(x => x.PrimaryColumnValue).ToList();
                            if (positionsToDelete?.Count > 0)
                            {
                                QueryParameter[] queryParameters = _commonHelper.GetPametersGuidList(positionsToDelete);
                                Delete delete = new Delete(_userConnection)
                                    .From("CrocPosition")
                                    .Where("Id").In(queryParameters)
                                as Delete;
                                delete.Execute();
                            }
                            List<Guid> positionsToUpdate = positions
                                .Where(x => x.GetTypedColumnValue<bool>("CrocLeaveOnUpdate") == true)
                                .Select(x => x.PrimaryColumnValue).ToList();
                            if (positionsToUpdate?.Count > 0)
                            {
                                QueryParameter[] queryParameters = _commonHelper.GetPametersGuidList(positionsToUpdate);
                                Update update = new Update(_userConnection, "CrocPosition")
                                    .Set("CrocLeaveOnUpdate", Column.Parameter(false))
                                    .Where("Id").In(queryParameters)
                                as Update;
                                update.Execute();
                            }
                        }
                        catch { }
                    }
                    positionPlan.SetColumnValue("CrocRefresh", false);
                    positionPlan.Save(true);
                }
            }
        }
        #endregion

        #region PortQuota
        private void UpdateCrocPortQuota()
        {
            List<Guid> quotaToUpdate = GetPKToUpdate(_schemaName, "CrocHideRecord");
            if(quotaToUpdate?.Count > 0)
            {
                List<Guid> terminalIds = GetTerminalsOnQuota(quotaToUpdate);
                if(terminalIds?.Count > 0)
                {
                    DateTime controlDate = DateTime.UtcNow.Date;
                    foreach(Guid terminalId in terminalIds)
                    {
                        List<Guid> quotaToDelete = GetQuotaToDelete(terminalId, controlDate);
                        if(quotaToDelete?.Count > 0)
                        {
                            QueryParameter[] queryParameters = _commonHelper.GetPametersGuidList(quotaToDelete);
                            string rightSchemaName = _rightsHelper.GetRecordRightsSchemaDefName(_schemaName);
                            Delete deleteRight = new Delete(_userConnection)
                                .From(rightSchemaName)
                                .Where("RecordId").In(queryParameters)
                            as Delete;
                            deleteRight.Execute();
                            Delete delete = new Delete(_userConnection)
                                .From(_schemaName)
                                .Where("Id").In(queryParameters)
                            as Delete;
                            delete.Execute();
                        }
                    }
                }
                foreach (Guid quotaId in quotaToUpdate)
                {
                    Entity quota = _entityHelper.ReadEntity(_schemaName, quotaId);
                    quota.SetColumnValue("CrocHideRecord", false);
                    quota.Save(true);
                }
            }
        }

        private List<Guid> GetTerminalsOnQuota(List<Guid> quotaIds)
        {
            List<Guid> terminalsOnQuota = new List<Guid>();
            QueryParameter[] queryParameters = _commonHelper.GetPametersGuidList(quotaIds);
            Select select = new Select(_userConnection)
                .Distinct()
                .Column("CrocTerminalId")
                .From(_schemaName)
                .Where("Id").In(queryParameters)
            as Select;
            using (DBExecutor executor = _userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = select.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        terminalsOnQuota.Add(reader.GetGuid(reader.GetOrdinal("CrocTerminalId")));
                    }
                    reader.Close();
                }
            }
            return terminalsOnQuota;
        }

        private List<Guid> GetQuotaToDelete(Guid terminalId, DateTime controlDate)
        {
            List<Guid> quotaToDelete = new List<Guid>();
            Select select = new Select(_userConnection)
                .Column("Id")
                .From(_schemaName)
                .Where("CrocHideRecord").IsEqual(Column.Parameter(false))
                    .And("CrocTerminalId").IsEqual(Column.Parameter(terminalId))
                    .And("CrocDate").IsGreaterOrEqual(Column.Parameter(controlDate))
            as Select;
            using (DBExecutor executor = _userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = select.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        quotaToDelete.Add(reader.GetGuid(reader.GetOrdinal("Id")));
                    }
                    reader.Close();
                }
            }
            return quotaToDelete;
        }
        #endregion

        #region RegisterLoading
        private void UpdateRegisterLoading()
        {
            List<Entity> documentsLoading = _entityHelper.FindEntities("CrocDocumentRegistry", new Dictionary<string, object>
            {
                { "CrocRenew", true }, { "CrocToDelete", false}
            }).ToList();
            List<Entity> documentsLoadingDelete = _entityHelper.FindEntities("CrocDocumentRegistry", new Dictionary<string, object>
            {
                { "CrocToDelete", true}
            }).ToList();
            if (documentsLoading?.Count > 0)
            {
                foreach(Entity document in documentsLoading)
                {
                    Guid externId = document.GetTypedColumnValue<Guid>("CrocGuid");
                    if(externId != Guid.Empty)
                    {
                        List<Entity> loadings = _entityHelper.FindEntities(_schemaName, new Dictionary<string, object>
                        {
                            { "CrocGuid", externId }
                        }).ToList();
                        if(loadings?.Count > 0)
                        {
                            List<Guid> toDeleteLoadingIds = loadings
                                .Where(x => x.GetTypedColumnValue<bool>("CrocIsLeaveOnUpdate") == false)
                                .Select(x => x.PrimaryColumnValue)
                                .ToList();
                            List<Guid> toUpdateLoadingIds = loadings
                                .Where(x => x.GetTypedColumnValue<bool>("CrocIsLeaveOnUpdate") == true)
                                .Select(x => x.PrimaryColumnValue)
                                .ToList();
                            if(toDeleteLoadingIds?.Count > 0)
                            {
                                QueryParameter[] queryParametersDelete = _commonHelper.GetPametersGuidList(toDeleteLoadingIds);
                                Delete deleteLoadings = new Delete(_userConnection)
                                    .From(_schemaName)
                                    .Where("Id").In(queryParametersDelete)
                                as Delete;
                                deleteLoadings.Execute();
                            }
                            if(toUpdateLoadingIds?.Count > 0)
                            {
                                QueryParameter[] queryParametersUpdate = _commonHelper.GetPametersGuidList(toUpdateLoadingIds);
                                Update update = new Update(_userConnection, _schemaName)
                                    .Set("CrocIsLeaveOnUpdate", Column.Parameter(false))
                                    .Where("Id").In(queryParametersUpdate)
                                as Update;
                                update.Execute();
                            }
                        }
                    }
                }
                QueryParameter[] queryParameters = _commonHelper.GetPametersGuidList(documentsLoading.Select(x => x.PrimaryColumnValue).ToList());
                Update updateDocument = new Update(_userConnection, "CrocDocumentRegistry")
                    .Set("CrocRenew", Column.Parameter(false))
                    .Where("Id").In(queryParameters)
                as Update;
                updateDocument.Execute();
            }
            if(documentsLoadingDelete?.Count > 0)
            {
                foreach(Entity document in documentsLoadingDelete)
                {
                    Guid externId = document.GetTypedColumnValue<Guid>("CrocGuid");
                    if(externId != Guid.Empty)
                    {
                        List<Entity> loadings = _entityHelper.FindEntities(_schemaName, new Dictionary<string, object>
                        {
                            { "CrocGuid", externId }
                        }).ToList();
                        if(loadings?.Count > 0)
                        {
                            QueryParameter[] queryParametersDelete = _commonHelper.GetPametersGuidList(loadings.Select(x => x.PrimaryColumnValue).ToList());
                            Delete deleteLoadings = new Delete(_userConnection)
                                .From(_schemaName)
                                .Where("Id").In(queryParametersDelete)
                            as Delete;
                            deleteLoadings.Execute();
                        }
                    }
                }
                QueryParameter[] queryParametersDocument = _commonHelper.GetPametersGuidList(documentsLoadingDelete.Select(x => x.PrimaryColumnValue).ToList());
                Delete deleteDocuments = new Delete(_userConnection)
                    .From("CrocDocumentRegistry")
                    .Where("Id").In(queryParametersDocument)
                as Delete;
                deleteDocuments.Execute();
            }
        }
        #endregion

        #region RegisterUnloading
        private void UpdateRegisterUnloading()
        {
            List<Entity> documentsUnloading = _entityHelper.FindEntities("CrocDocumentUnloadings", new Dictionary<string, object>
            {
                { "CrocRenew", true }, { "CrocToDelete", false}
            }).ToList();
            List<Entity> documentsUnloadingDelete = _entityHelper.FindEntities("CrocDocumentUnloadings", new Dictionary<string, object>
            {
                { "CrocToDelete", true}
            }).ToList();
            if (documentsUnloading?.Count > 0)
            {
                foreach (Entity document in documentsUnloading)
                {
                    Guid externId = document.GetTypedColumnValue<Guid>("CrocGuid");
                    if (externId != Guid.Empty)
                    {
                        List<Entity> loadings = _entityHelper.FindEntities(_schemaName, new Dictionary<string, object>
                        {
                            { "CrocGuid", externId }
                        }).ToList();
                        if (loadings?.Count > 0)
                        {
                            List<Guid> toDeleteLoadingIds = loadings
                                .Where(x => x.GetTypedColumnValue<bool>("CrocIsLeaveOnUpdate") == false)
                                .Select(x => x.PrimaryColumnValue)
                                .ToList();
                            List<Guid> toUpdateLoadingIds = loadings
                                .Where(x => x.GetTypedColumnValue<bool>("CrocIsLeaveOnUpdate") == true)
                                .Select(x => x.PrimaryColumnValue)
                                .ToList();
                            if (toDeleteLoadingIds?.Count > 0)
                            {
                                QueryParameter[] queryParametersDelete = _commonHelper.GetPametersGuidList(toDeleteLoadingIds);
                                Delete deleteLoadings = new Delete(_userConnection)
                                    .From(_schemaName)
                                    .Where("Id").In(queryParametersDelete)
                                as Delete;
                                deleteLoadings.Execute();
                            }
                            if (toUpdateLoadingIds?.Count > 0)
                            {
                                QueryParameter[] queryParametersUpdate = _commonHelper.GetPametersGuidList(toUpdateLoadingIds);
                                Update update = new Update(_userConnection, _schemaName)
                                    .Set("CrocIsLeaveOnUpdate", Column.Parameter(false))
                                    .Where("Id").In(queryParametersUpdate)
                                as Update;
                                update.Execute();
                            }
                        }
                    }
                }
                QueryParameter[] queryParameters = _commonHelper.GetPametersGuidList(documentsUnloading.Select(x => x.PrimaryColumnValue).ToList());
                Update updateDocument = new Update(_userConnection, "CrocDocumentUnloadings")
                    .Set("CrocRenew", Column.Parameter(false))
                    .Where("Id").In(queryParameters)
                as Update;
                updateDocument.Execute();
            }
            if (documentsUnloadingDelete?.Count > 0)
            {
                foreach (Entity document in documentsUnloadingDelete)
                {
                    Guid externId = document.GetTypedColumnValue<Guid>("CrocGuid");
                    if (externId != Guid.Empty)
                    {
                        List<Entity> loadings = _entityHelper.FindEntities(_schemaName, new Dictionary<string, object>
                        {
                            { "CrocGuid", externId }
                        }).ToList();
                        if (loadings?.Count > 0)
                        {
                            QueryParameter[] queryParametersDelete = _commonHelper.GetPametersGuidList(loadings.Select(x => x.PrimaryColumnValue).ToList());
                            Delete deleteLoadings = new Delete(_userConnection)
                                .From(_schemaName)
                                .Where("Id").In(queryParametersDelete)
                            as Delete;
                            deleteLoadings.Execute();
                        }
                    }
                }
                QueryParameter[] queryParametersDocument = _commonHelper.GetPametersGuidList(documentsUnloadingDelete.Select(x => x.PrimaryColumnValue).ToList());
                Delete deleteDocuments = new Delete(_userConnection)
                    .From("CrocDocumentUnloadings")
                    .Where("Id").In(queryParametersDocument)
                as Delete;
                deleteDocuments.Execute();
            }
        }
        #endregion
        public void Run(Dictionary<string, object> parameters)
        {
            try
            {
                _schemaName = (string)parameters.FirstOrDefault(x => x.Key == "SchemaName").Value;
                if (!string.IsNullOrEmpty(_schemaName))
                {
                    switch (_schemaName)
                    {
                        case "CrocLoan":
                            UpdateCrocLoan();
                            break;
                        case "CrocPositionPlan":
                            UpdateCrocPositionPlan();
                            break;
                        case "CrocPortQuota":
                            UpdateCrocPortQuota();
                            break;
                        case "CrocRegisterLoading":
                            UpdateRegisterLoading();
                            break;
                        case "CrocRegisterUnloading":
                            UpdateRegisterUnloading();
                            break;
                    }
                }
            }
            catch(Exception ex) {  _log.Error($"{ex.Message}\n{ex.StackTrace}"); }
        }
    }
}