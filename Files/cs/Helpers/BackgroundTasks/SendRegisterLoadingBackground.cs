﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.OneCIntegration;
using Terrasoft.Core;
using Terrasoft.Core.Tasks;

namespace Terrasoft.Configuration
{
    public class SendRegisterLoadingBackground : IBackgroundTask<Dictionary<string, object>>, IUserConnectionRequired
    {
        private UserConnection _userConnection;
        private UserConnection _currentUserConnection;
        private CrocEntityHelper _entityHelper;
        private static ILog _log = LogManager.GetLogger("Error");

        public void SetUserConnection(UserConnection userConnection)
        {
            _currentUserConnection = userConnection;
            _userConnection = userConnection.AppConnection.SystemUserConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }
        public void Run(Dictionary<string, object> parameters)
        {
            Guid documentId = (Guid)parameters.FirstOrDefault(x => x.Key == "DocumentId").Value;
            try
            {
                OneCIntegrationHelper integrationHelper = new OneCIntegrationHelper(_currentUserConnection);
                integrationHelper.SendRegisterLoadingOnDocument(documentId);
                _entityHelper.UpdateEntity("CrocDocumentRegistry", documentId,
                    new Dictionary<string, object> { { "CrocSendDeleteMark", false } });
            }
            catch (Exception ex) { _log.Error($"{ex.Message}\n{ex.StackTrace}"); }

        }
    }
}
