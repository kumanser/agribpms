﻿//BackgroundRightsUpdater
using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.RightsService;
using Terrasoft.Core;
using Terrasoft.Core.Configuration;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Tasks;
using Common.Logging;

namespace Terrasoft.Configuration
{
	public class RightsUpdaterBackground : IBackgroundTask<Dictionary<string, object>>, IUserConnectionRequired
	{
		private UserConnection _userConnection;
		private RightsHelper _rightsHelper;
		private CrocEntityHelper _entityHelper;
		private static ILog _log = LogManager.GetLogger("Error");

		public void SetUserConnection(UserConnection userConnection)
		{
			_userConnection = userConnection.AppConnection.SystemUserConnection;
			_rightsHelper = new RightsHelper(_userConnection);
			_entityHelper = new CrocEntityHelper(_userConnection);
		}

		private List<Guid> GetAccountRoles(Guid accountId)
        {
			List<Guid> roles = null;
			List<Entity> entities = _entityHelper.FindEntities("CrocEnterpriseRightRecord", new Dictionary<string, object>
			{ { "CrocAccount.Id", accountId } }).ToList();
			if (entities?.Count > 0) 
			{ 
				roles = entities
					.Where(x => x.GetTypedColumnValue<Guid>("CrocRoleId") != Guid.Empty)
					.Select(x => x.GetTypedColumnValue<Guid>("CrocRoleId"))
					.Distinct()
					.ToList(); 
			}
			return roles;
        }

		private void SetDefaultRights(string schemaName, Guid recordId)
        {
			List<DefRight> rights = _rightsHelper.GetSchemaDefRights(schemaName);
			if(rights?.Count > 0)
            {
				foreach (DefRight right in rights)
                {
					_rightsHelper.SetRecordRight(right.SysAdminUnitId, schemaName, recordId.ToString(), right.Operation, right.RightLevel);
				}
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parameters">AccountId (List_Guid), SchemaName (string), RecordId (Guid)</param>
		public void Run(Dictionary<string, object> parameters)
		{
			List<Guid> accounts = (List<Guid>)parameters.FirstOrDefault(x => x.Key == "AccountId").Value;
			Guid recordId = (Guid)parameters.FirstOrDefault(x => x.Key == "RecordId").Value;
			string schemaName = (string)parameters.FirstOrDefault(x => x.Key == "SchemaName").Value;
			try
			{
				if (recordId == Guid.Empty || string.IsNullOrEmpty(schemaName)) return;
				//var schemaRights = _rightsHelper.GetSchemaOperationRightLevel(schemaName);
				List<Guid> roles = new List<Guid>();
                string rightSchemaName = _rightsHelper.GetRecordRightsSchemaDefName(schemaName);
                var rolesToDel = _rightsHelper.GetRecordRights(rightSchemaName, recordId.ToString());
                rolesToDel.ForEach(oldRole => { oldRole.isDeleted = true; });
                _rightsHelper.DeleteRecordRights(rolesToDel.ToArray(), new Record { entitySchemaName = schemaName, primaryColumnValue = recordId.ToString() });
                SetDefaultRights(schemaName, recordId);
                if (accounts?.Count > 0)
				{
                    foreach (Guid accountId in accounts)
                    {
                        List<Guid> tmpRoles = GetAccountRoles(accountId);
                        if (tmpRoles?.Count > 0) roles.AddRange(tmpRoles);
                    }
                }
				if (roles?.Count > 0)
				{
					roles = roles.Distinct().ToList();
					foreach (Guid roleId in roles)
					{
						_rightsHelper.SetRecordRight(roleId, schemaName, recordId.ToString(), (int)EntitySchemaRecordRightOperation.Read, 1);
						_rightsHelper.SetRecordRight(roleId, schemaName, recordId.ToString(), (int)EntitySchemaRecordRightOperation.Edit, 1);
					}
				}
			}
			catch (Exception ex)
			{
				_log.Error($"{schemaName} rights: {ex.Message}\n{ex.StackTrace}");
			}
		}
	}
}
