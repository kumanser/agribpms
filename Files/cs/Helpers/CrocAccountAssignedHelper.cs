﻿namespace Terrasoft.Configuration
{
	using System;
	using System.Collections.Generic;
	using Terrasoft.Common;
	using Terrasoft.Configuration.Base;
	using Terrasoft.Core;
	using Terrasoft.Core.Entities;
	using Terrasoft.Core.Factories;

	#region Class: ${Name}

	[DefaultBinding(typeof(ICrocAccountAssignedHelper))]
	public class CrocAccountAssignedHelper : ICrocAccountAssignedHelper
	{
		public class AccountItem
		{
			public Guid Id { get; set; }
			public Guid SubstitutionId { get; set; }
			public Guid PinnedId { get; set; }
		}

		public class ColumnNames
		{
			public string AssignName { get; set; }
			public string SubstitutionName { get; set; }
			public string ReplacementDateName { get; set; }
			public string ReplacementToday { get; set; }
		}

		#region Constructors: Public

		public CrocAccountAssignedHelper(UserConnection userConnection)
		{
			_userConnection = userConnection;
		}

		#endregion

		#region Properties: Private

		private UserConnection _userConnection { get; set; }

		#endregion

		#region Methods: Private

		private Guid GetSubstitutionEmployee(Guid accountId, ColumnNames colNames)
		{
			CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
			Entity entity = entityHelper.ReadEntity("Account", accountId);
			Guid employeeId = Guid.Empty;
			if (entity != null)
			{
				if (entity.GetTypedColumnValue<Guid>(colNames.SubstitutionName) == Guid.Empty)
				{
					employeeId = entity.GetTypedColumnValue<Guid>(colNames.AssignName);
				}
				else employeeId = entity.GetTypedColumnValue<Guid>(colNames.SubstitutionName);
			}
			return employeeId;
		}

		private List<AccountItem> GetAccountsWithSubstitutions(string substitutionName, string replacementName)
		{
			var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, "Account");
			var idCol = esq.AddColumn("Id");
			var substitutionCol = esq.AddColumn(substitutionName);
			var pinnedCol = esq.AddColumn("CrocPinnedManager.Id");
			var today = DateTime.Now; 
			esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Less, replacementName, today));
			var entities = esq.GetEntityCollection(_userConnection);
			List<AccountItem> accounts = new List<AccountItem>();
			entities.ForEach(entity => {
				AccountItem item = new AccountItem
				{
					Id = entity.GetTypedColumnValue<Guid>(idCol.Name),
					SubstitutionId = entity.GetTypedColumnValue<Guid>(substitutionCol.Name),
					PinnedId = entity.GetTypedColumnValue<Guid>(pinnedCol.Name)

				};
				accounts.Add(item);
			});
			return accounts;
		}

		private void DeleteExporterEmployee(Guid accountId, Guid contactId) 
		{
			CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
			entityHelper.DeleteRecord("CrocExporterEmployee", new Dictionary<string, object>()
			{
				{ "CrocContactId", contactId},
				{ "CrocAccountId", accountId}
			});
		}

		private void SetNewAccountData(Guid accountId, Guid contactId, ColumnNames colNames, Guid? substitutionId = null, DateTime? replacementStartDate = null, DateTime? replacementEndDate = null)
		{
			CrocEntityHelper entityHelper = new CrocEntityHelper(_userConnection);
			entityHelper.UpdateEntity("Account", accountId, new Dictionary<string, object>() {
				{ colNames.AssignName,  contactId},
				{ colNames.SubstitutionName,  substitutionId},
				{ colNames.ReplacementDateName,  replacementEndDate},
				{ colNames.ReplacementToday, replacementStartDate}
			});
		}

		#endregion

		#region Methods: Public

		public void ChangeAssignedEmployee(string accountsStr, Guid contactId, DateTime replacementEndDate)
		{
			ColumnNames colNames = new ColumnNames();

			colNames.AssignName = "CrocPinnedManagerId"; 
			colNames.SubstitutionName = "CrocReplaceManagerId"; 
			colNames.ReplacementDateName = "CrocReplaceEndDate";
			colNames.ReplacementToday = "CrocReplaceStartDate";

			string[] accountArr = accountsStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
			accountArr.ForEach(idStr => {
				Guid accountId = new Guid(idStr);
				Guid substitutionId = GetSubstitutionEmployee(accountId, colNames);
				DateTime replacementStartDate = DateTime.Today;
				SetNewAccountData(accountId, contactId, colNames, substitutionId, replacementStartDate, replacementEndDate);
			});
		}

		public void ReturnAssignedEmployee()
		{
			ColumnNames colNames = new ColumnNames
			{
				AssignName = "CrocPinnedManagerId",
				SubstitutionName = "CrocReplaceManagerId",
				ReplacementDateName = "CrocReplaceEndDate",
				ReplacementToday = "CrocReplaceStartDate"
			};
			List<AccountItem> accounts = GetAccountsWithSubstitutions("CrocReplaceManager.Id", colNames.ReplacementDateName);

			accounts.ForEach(account => {
				DeleteExporterEmployee(account.Id, account.PinnedId);
				SetNewAccountData(account.Id, account.SubstitutionId, colNames);
			});
		}

		#endregion

	}

	#endregion

}