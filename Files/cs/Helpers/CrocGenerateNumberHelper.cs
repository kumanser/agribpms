﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terrasoft.Core;
using Common.Logging;
using Terrasoft.Core.Entities;
using CoreSysSettings = Terrasoft.Core.Configuration.SysSettings;


namespace Terrasoft.Configuration
{
    public class CrocGenerateNumberHelper
    {
        #region Private Fields
        private readonly UserConnection _userConnection;
        #endregion


        #region Constructor
        public CrocGenerateNumberHelper(UserConnection userConnection) 
        {
            _userConnection = userConnection;
        }
        #endregion

        #region Public

        public virtual string GetAutonumerationNumber(EntitySchema entitySchema) 
        {
            string entitySchemaName = entitySchema.Name;
            string codeMaskSettingName = entitySchemaName + "CodeMask";
            string lastNumberSettingName = entitySchemaName + "LastNumber";
            string sysSettingsCodeMask = string.Empty;
            var sysSettingsMaskItem = new CoreSysSettings(_userConnection)
            {
                UseAdminRights = false,
                Code = codeMaskSettingName
            };
            if (!sysSettingsMaskItem.FetchFromDB("Code", codeMaskSettingName))
            {
                throw new Exception($"No setting {codeMaskSettingName} in system");
            }
            if (sysSettingsMaskItem.IsPersonal)
            {
                sysSettingsCodeMask = (string)CoreSysSettings.GetValue(_userConnection, codeMaskSettingName);
            }
            else
            {
                sysSettingsCodeMask = (string)CoreSysSettings.GetDefValue(_userConnection, codeMaskSettingName);
            }
            int sysSettingsLastNumber = Convert.ToInt32(CoreSysSettings.GetDefValue(_userConnection, lastNumberSettingName));
            ++sysSettingsLastNumber;
            CoreSysSettings.SetDefValue(_userConnection, lastNumberSettingName, sysSettingsLastNumber);
            return string.Format(sysSettingsCodeMask, sysSettingsLastNumber);
        }
        #endregion
    }
}
