﻿using Amazon.Runtime.Internal.Transform;
using Common.Logging;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.ExtendedProperties;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using DocumentFormat.OpenXml.Office2010.Word;
using Newtonsoft.Json;
using NPOI.OpenXmlFormats.Spreadsheet;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.BaseIntegration;
using Terrasoft.Configuration.CrocDemetra;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Factories;
using static Amazon.S3.Util.S3EventNotification;
using CoreConfiguration = Terrasoft.Core.Configuration;

namespace Terrasoft.Configuration.OneCIntegration
{
    [DefaultBinding(typeof(IOneCIntegrationHelper))]
    public class OneCIntegrationHelper: IOneCIntegrationHelper
    {
        #region Fields
        private const string _dateFormat = "yyyy-MM-dd";
        private const string _dateFormatSS = "yyyy-MM-ddTHH:mm:ss.000Z";
        private readonly UserConnection _userConnection;
        private readonly UserConnection _currentUserConnection;
        private string _login;
        private string _password;
        private string _url;
        private string _server;
        private int _timeout;
        private CrocEntityHelper _entityHelper;
        private CommonHelper _commonHelper;
        private readonly ILog _logger = LogManager.GetLogger("Error");
        private string _defaultMessage = "Выгружено успешно";
        private Guid _defaultSchemaUId = Guid.Parse("1bab9dcf-17d5-49f8-9536-8e0064f1dce0");

        #endregion

        #region Constructor
        public OneCIntegrationHelper(UserConnection userConnection)
        {
            _userConnection = userConnection.AppConnection.SystemUserConnection;
            _currentUserConnection = userConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
            _commonHelper = new CommonHelper(_userConnection);
            GetAuthData();
        }
        #endregion

        #region Methods: Private
        private void GetAuthData()
        {
            _login = CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCLogin", "");
            _password = CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCPassword").ToString();
            _server = CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCServer", "");
        }

        private void GetFullUrl(string serviceName)
        {
            _url = _server + serviceName;
        }

        private Dictionary<string, string> MakeRequestHeaders()
        {
            string authHash = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{_login}:{_password}"));
            return new Dictionary<string, string>
            {
                { "cache-control", "no-cache" },
                { "content-type", "application/json"},
                { "authorization", $"Basic {authHash}"}
            };
        }

        private BaseResponse SendData(object body)
        {
            RestHelper restHelper = new RestHelper();
            restHelper.Url = _url;
            restHelper.Method = Method.POST;
            restHelper.DataFormat = DataFormat.Json;
            restHelper.Headers = MakeRequestHeaders();
            //restHelper.Body = body;
            restHelper.Parameter = new Parameter { Name = "application/json", Value = body, Type = ParameterType.RequestBody };
            return restHelper.SendRequest();
        }

        private string GetOneCObjectCode(string schemaName, string columnName, Guid recordId)
        {
            string value = string.Empty;
            if(recordId == Guid.Empty) return value;
            Entity entity = _entityHelper.ReadEntity(schemaName, recordId);
            var valueTemp = entity.GetColumnValue(columnName);
            if (valueTemp != null)
            {
                if (valueTemp.GetType() == typeof(string)) { value = (string)valueTemp; }
                else if (valueTemp.GetType() == typeof(Guid)) { value = valueTemp.ToString(); }
            }
            else { _logger.Error($"Для {schemaName} по id {recordId} не найдено значение в поле {columnName}"); }
            return value;
        }

        private PlanRefund PrepareRefundData(Guid refundId)
        {
            PlanRefund planRefund = new PlanRefund();
            Entity refundEntity = _entityHelper.ReadEntity("CrocPlannedRefund", refundId);
            string refundExternId = GetOneCObjectCode("CrocPlannedRefund", "CrocId", refundId);
            planRefund.Id = refundId.ToString(); // refundExternId - id 1C
            Guid accountId = refundEntity.GetTypedColumnValue<Guid>("CrocAccountId");
            if (accountId != Guid.Empty) { planRefund.Account = GetOneCObjectCode("Account", "CrocGuid", accountId);  }
            Guid contractId = refundEntity.GetTypedColumnValue<Guid>("CrocContractId");
            if (contractId != Guid.Empty) { planRefund.AdditionalAgreement = contractId.ToString(); }
            Guid managerId = refundEntity.GetTypedColumnValue<Guid>("CrocManagerId");
            if (managerId != Guid.Empty) { planRefund.Manager = GetOneCObjectCode("Contact", "CrocGuid", managerId); }
            Guid productId = refundEntity.GetTypedColumnValue<Guid>("CrocProductId");
            if (productId != Guid.Empty) { planRefund.Nomenclature = GetOneCObjectCode("CrocProduct", "CrocGuid", productId); } 
            Guid reasonId = refundEntity.GetTypedColumnValue<Guid>("CrocRefundReasonId");
            if (reasonId != Guid.Empty) { planRefund.RefundReason = GetOneCObjectCode("CrocRefundReason", "CrocId", reasonId); }
            planRefund.Ammount = refundEntity.GetTypedColumnValue<decimal>("CrocRefundAmount");
            planRefund.Comment = refundEntity.GetTypedColumnValue<string>("CrocComment");
            planRefund.RefundDate = refundEntity.GetTypedColumnValue<DateTime>("CrocReturnDate").ToString(_dateFormat);
            return planRefund;
        }

        private EmptyVehicle PrepareEmptyVehiclesData(Guid recordId, string masterColumnName)
        {
            EmptyVehicle emptyVehicle = new EmptyVehicle();
            emptyVehicle.Id = recordId.ToString();
            if(masterColumnName == "CrocErrand") { emptyVehicle.Id = GetOneCObjectCode("CrocErrand", "CrocGuid", recordId); }
            emptyVehicle.Vehicles = new List<Vehicle>();
            List<Entity> registerEmptyCars = _entityHelper.FindEntities("CrocRegisterEmptyCar", new Dictionary<string, object>
            { { $"{masterColumnName}.Id", recordId } }).ToList();
            if (registerEmptyCars?.Count > 0)
            {
                int badVehicleCounter = 0;
                int totalVehicleCounter = registerEmptyCars.Count;
                registerEmptyCars.ForEach(car =>
                {
                    try
                    {
                        Vehicle vehicle = new Vehicle()
                        {
                            LicensePlate = car.GetTypedColumnValue<string>("CrocVehicle"),
                            TrailerNumber = car.GetTypedColumnValue<string>("CrocTrailerNumber"),
                            DriverFullName = car.GetTypedColumnValue<string>("CrocDriverFullName"),
                            DriverInn = car.GetTypedColumnValue<string>("CrocDriverINN"),
                            CarrierOwner = car.GetTypedColumnValue<string>("CrocCarrierOwner"),
                            CarrierOwnerINN = car.GetTypedColumnValue<string>("CrocCarrierOwnerINN"),
                            Carrier = car.GetTypedColumnValue<string>("CrocCarrier"),
                            CarrierINN = car.GetTypedColumnValue<string>("CrocCarrierINN"),
                            BrandVehicle = car.GetTypedColumnValue<string>("CrocBrandVehicle"),
                            DriverPhone = car.GetTypedColumnValue<string>("CrocDriverPhone"),
                            NumberForNotification = car.GetTypedColumnValue<string>("CrocContractCode")
                        };
                        /* on OLD field CrocOwnershipType
                        string ownershipType = car.GetTypedColumnValue<string>("CrocOwnershipType");
                        if (!string.IsNullOrEmpty(ownershipType))
                        {
                            Regex regexObj = new Regex(@"[^\d]");
                            ownershipType = regexObj.Replace(ownershipType, "").Trim();
                        }
                        vehicle.OwnershipType = string.IsNullOrEmpty(ownershipType) ? 0 : Convert.ToInt32(ownershipType);
                        */
                        vehicle.OwnershipType = car.GetTypedColumnValue<int>("CrocOwnershipTypeInt");
                        string pricePerTon = car.GetTypedColumnValue<string>("CrocPriceNetPerTon");
                        vehicle.PriceNetPerTon = string.IsNullOrEmpty(pricePerTon) ? 0 : Convert.ToDecimal(pricePerTon);
                        string commissionPerTon = car.GetTypedColumnValue<string>("CrocCommissionPerTon");
                        vehicle.CommissionPerTon = string.IsNullOrEmpty(commissionPerTon) ? 0 : Convert.ToDecimal(commissionPerTon); //не обязательно
                        if (ValidateVehicleToExport(vehicle))
                        { emptyVehicle.Vehicles.Add(vehicle); }
                        else { badVehicleCounter++; }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex.Message);
                        _logger.Error(ex.StackTrace);
                        badVehicleCounter++;
                    }
                });
                if(badVehicleCounter > 0)
                {
                    string errorVehicleMessage = $"Присутствуют не полные записи. Выгружено {totalVehicleCounter - badVehicleCounter} из {totalVehicleCounter}";
                    _commonHelper.SendRemindMessageToUser(GetShoterMessage($"Реестр порожних ТС: {errorVehicleMessage}"), _defaultSchemaUId);
                }
            }
            else { throw new InvalidOperationException("No data for upload"); }
            return emptyVehicle;
        }

        private bool ValidateVehicleToExport(Vehicle vehicle)
        {
            bool result = true;
            if(string.IsNullOrEmpty(vehicle.LicensePlate)
                || string.IsNullOrEmpty(vehicle.TrailerNumber)
                || string.IsNullOrEmpty(vehicle.DriverFullName)
                || string.IsNullOrEmpty(vehicle.DriverInn)
                || string.IsNullOrEmpty(vehicle.CarrierOwner)
                || string.IsNullOrEmpty(vehicle.CarrierOwnerINN)
                || string.IsNullOrEmpty(vehicle.Carrier)
                || string.IsNullOrEmpty(vehicle.CarrierINN)
                || string.IsNullOrEmpty(vehicle.BrandVehicle)
                || string.IsNullOrEmpty(vehicle.DriverPhone)
                || string.IsNullOrEmpty(vehicle.NumberForNotification)
                || vehicle.OwnershipType == 0
                || vehicle.PriceNetPerTon == 0
            ) { result = false; }
            return result;
        }

        private RequestGU PrepareRequestGUData(Guid recordId)
        {
            RequestGU requestGU = new RequestGU();
            Entity guEntity = _entityHelper.ReadEntity("CrocDocument", recordId);
            requestGU.Id = recordId.ToString();
            Guid statusId = guEntity.GetTypedColumnValue<Guid>("CrocGU12StatusId");
            if (statusId != Guid.Empty) { requestGU.Status = GetOneCObjectCode("CrocApplicationGU12Status", "CrocGuid", statusId); }
            Guid planId = guEntity.GetTypedColumnValue<Guid>("CrocChartId");
            if (planId != Guid.Empty) { requestGU.Plan = GetOneCObjectCode("CrocChart", "CrocId", planId); }
            return requestGU;
        }

        private RegisterLoadings PrepareRegisterLoadingData(Guid recordId)
        {
            RegisterLoadings registrLoading = new RegisterLoadings();
            registrLoading.GoodsRegisters = new List<GoodsRegister>();  
            List<Entity> registerEntities = _entityHelper.FindEntities("CrocRegisterLoading", new Dictionary<string, object> { { "CrocChart.Id", recordId } }).ToList();
            DateTime shipmentDate = new DateTime();
            if (registerEntities?.Count > 0) 
            {
                foreach (Entity registerEntity in registerEntities)
                {
                    if (shipmentDate == DateTime.MinValue) { shipmentDate = registerEntity.GetTypedColumnValue<DateTime>("CrocShippingDate"); }
                    GoodsRegister register = new GoodsRegister
                    {
                        WeightBrutto = registerEntity.GetTypedColumnValue<decimal>("CrocGrossWeightTTNTon"),
                        WeightNetto = registerEntity.GetTypedColumnValue<decimal>("CrocGrossWeightTon"),
                        WeightPackage = registerEntity.GetTypedColumnValue<decimal>("CrocWeightContainerTTNTon"),
                        BrokenGrains = registerEntity.GetTypedColumnValue<decimal>("CrocBroken"),
                        Gluten = registerEntity.GetTypedColumnValue<decimal>("CrocGluten"),
                        BugEated = registerEntity.GetTypedColumnValue<decimal>("CrocBugEated"),
                        DamagedGrain = registerEntity.GetTypedColumnValue<decimal>("CrocDamaged"),
                        FallingNumber = registerEntity.GetTypedColumnValue<decimal>("CrocChP"),
                        ForeignGrain = registerEntity.GetTypedColumnValue<decimal>("CrocForeignGrain"),
                        ForeignMaterial = registerEntity.GetTypedColumnValue<decimal>("CrocWeedImpurity"),
                        GlutenDeformationIndex = registerEntity.GetTypedColumnValue<decimal>("CrocIDK"),
                        GrainsDifferentColor = registerEntity.GetTypedColumnValue<decimal>("CrocGrainsDifferentColor"),
                        Moisture = registerEntity.GetTypedColumnValue<decimal>("CrocHumidity"),
                        Nature = registerEntity.GetTypedColumnValue<int>("CrocNatur"),
                        Protein = registerEntity.GetTypedColumnValue<decimal>("CrocProtein")
                    };
                    register.VehicleId = registerEntity.GetTypedColumnValue<string>("CrocTransportNumber");
                    register.InvoiceNumber = registerEntity.GetTypedColumnValue<string>("CrocTTN");
                    Guid productClassId = registerEntity.GetTypedColumnValue<Guid>("CrocProductClassId");
                    if (productClassId != Guid.Empty) 
                    {
                        register.GrainsClass = registerEntity.GetTypedColumnValue<string>("CrocProductClassName");
                    }
                    registrLoading.GoodsRegisters.Add(register);
                }
            }
            else { throw new InvalidOperationException("No data for upload"); }
            Entity planEntity = _entityHelper.ReadEntity("CrocChart", recordId);
            Guid productId = planEntity.GetTypedColumnValue<Guid>("CrocProductId");
            Guid shipmentPointId = planEntity.GetTypedColumnValue<Guid>("CrocWarehouseLoadingId");
            Guid shipperLocationId = planEntity.GetTypedColumnValue<Guid>("CrocWarehouseUnloadingId");
            Guid shipperId = planEntity.GetTypedColumnValue<Guid>("CrocShipperId");
            registrLoading.ShipmentDate = shipmentDate.ToString(_dateFormat);
            registrLoading.Nomenclature = GetOneCObjectCode("CrocProduct", "CrocGuid", productId);
            registrLoading.ShipmentPoint = GetOneCObjectCode("CrocTerminal", "CrocId", shipmentPointId);
            registrLoading.ShipperLocation = GetOneCObjectCode("CrocTerminal", "CrocId", shipperLocationId);
            registrLoading.ShipperId = GetOneCObjectCode("Account", "CrocGuid", shipperId);
            registrLoading.Plan = GetOneCObjectCode("CrocChart", "CrocId", recordId);
            registrLoading.Id = recordId.ToString();
            return registrLoading;
        }

        private RegisterLoadings PrepareRegisterLoadingDataOnDocument(Guid documentId)
        {
            RegisterLoadings registrLoading = new RegisterLoadings();
            registrLoading.GoodsRegisters = new List<GoodsRegister>();
            Entity documentRegister = _entityHelper.ReadEntity("CrocDocumentRegistry", documentId);
            Guid extId = documentRegister.GetTypedColumnValue<Guid>("CrocGuid");
            Guid chartId = documentRegister.GetTypedColumnValue<Guid>("CrocChartId");
            List<Entity> registerEntities = _entityHelper.FindEntities("CrocRegisterLoading", new Dictionary<string, object> 
            { 
                { "CrocChart.Id", chartId }, { "CrocGuid", extId }

            }).ToList();
            DateTime shipmentDate = new DateTime();
            if (registerEntities?.Count > 0)
            {
                foreach (Entity registerEntity in registerEntities)
                {
                    if (shipmentDate == DateTime.MinValue) { shipmentDate = registerEntity.GetTypedColumnValue<DateTime>("CrocShippingDate"); }
                    GoodsRegister register = new GoodsRegister
                    {
                        WeightBrutto = registerEntity.GetTypedColumnValue<decimal>("CrocGrossWeightTTNTon"),
                        WeightNetto = registerEntity.GetTypedColumnValue<decimal>("CrocGrossWeightTon"),
                        WeightPackage = registerEntity.GetTypedColumnValue<decimal>("CrocWeightContainerTTNTon"),
                        BrokenGrains = registerEntity.GetTypedColumnValue<decimal>("CrocBroken"),
                        Gluten = registerEntity.GetTypedColumnValue<decimal>("CrocGluten"),
                        BugEated = registerEntity.GetTypedColumnValue<decimal>("CrocBugEated"),
                        DamagedGrain = registerEntity.GetTypedColumnValue<decimal>("CrocDamaged"),
                        FallingNumber = registerEntity.GetTypedColumnValue<decimal>("CrocChP"),
                        ForeignGrain = registerEntity.GetTypedColumnValue<decimal>("CrocForeignGrain"),
                        ForeignMaterial = registerEntity.GetTypedColumnValue<decimal>("CrocWeedImpurity"),
                        GlutenDeformationIndex = registerEntity.GetTypedColumnValue<decimal>("CrocIDK"),
                        GrainsDifferentColor = registerEntity.GetTypedColumnValue<decimal>("CrocGrainsDifferentColor"),
                        Moisture = registerEntity.GetTypedColumnValue<decimal>("CrocHumidity"),
                        Nature = registerEntity.GetTypedColumnValue<int>("CrocNatur"),
                        Protein = registerEntity.GetTypedColumnValue<decimal>("CrocProtein")
                    };
                    register.VehicleId = registerEntity.GetTypedColumnValue<string>("CrocTransportNumber");
                    register.InvoiceNumber = registerEntity.GetTypedColumnValue<string>("CrocTTN");
                    Guid productClassId = registerEntity.GetTypedColumnValue<Guid>("CrocProductClassId");
                    if (productClassId != Guid.Empty)
                    {
                        register.GrainsClass = GetOneCObjectCode("CrocProductClass", "Name", productClassId);
                    }
                    registrLoading.GoodsRegisters.Add(register);
                }
            }
            else { throw new InvalidOperationException("No data for upload"); }
            Entity planEntity = _entityHelper.ReadEntity("CrocChart", chartId);
            Guid productId = planEntity.GetTypedColumnValue<Guid>("CrocProductId");
            Guid shipmentPointId = planEntity.GetTypedColumnValue<Guid>("CrocWarehouseLoadingId");
            Guid shipperLocationId = planEntity.GetTypedColumnValue<Guid>("CrocWarehouseUnloadingId");
            Guid shipperId = planEntity.GetTypedColumnValue<Guid>("CrocShipperId");
            registrLoading.ShipmentDate = shipmentDate.ToString(_dateFormat);
            registrLoading.Nomenclature = GetOneCObjectCode("CrocProduct", "CrocGuid", productId);
            registrLoading.ShipmentPoint = GetOneCObjectCode("CrocTerminal", "CrocId", shipmentPointId);
            registrLoading.ShipperLocation = GetOneCObjectCode("CrocTerminal", "CrocId", shipperLocationId);
            registrLoading.ShipperId = GetOneCObjectCode("Account", "CrocGuid", shipperId);
            registrLoading.Plan = GetOneCObjectCode("CrocChart", "CrocId", chartId);
            registrLoading.Id = extId.ToString();
            return registrLoading;
        }

        private ShipmentPlan PrepareShipmentPlanData(Guid recordId, int typePlan)
        {
            Entity planEntity = _entityHelper.ReadEntity("CrocChart", recordId);
            Guid contractId = planEntity.GetTypedColumnValue<Guid>("CrocContractId");
            Guid statusId = planEntity.GetTypedColumnValue<Guid>("CrocChartStatusId");
            string gu12 = planEntity.GetTypedColumnValue<string>("CrocNumberGU12");
            string rat = planEntity.GetTypedColumnValue<string>("CrocNumberRequestRAT");
            string consignorGU = planEntity.GetTypedColumnValue<string>("CrocConsignorGU12");
            ShipmentPlan shipmentPlan = new ShipmentPlan
            {
                PlanType = typePlan,
                Id = recordId.ToString(),
                TaskId = planEntity.GetTypedColumnValue<Guid>("CrocId").ToString(),
                AdditionalAgreement = contractId.ToString(),
                Status = GetOneCObjectCode("CrocChartStatus", "CrocGuid", statusId),
                NumberGU12 = gu12,
                NumberRAT = rat,
                ConsignerGU = consignorGU
            };
            string region = planEntity.GetTypedColumnValue<string>("CrocRegion");
            if (!string.IsNullOrEmpty(region)) { shipmentPlan.Region = region; }
            Guid transportTypeId = planEntity.GetTypedColumnValue<Guid>("CrocTransportTypeId");
            if(transportTypeId == ConstCs.CrocTransporType.Railway)
            {
                string consigneeGU = planEntity.GetTypedColumnValue<string>("CrocConsigneeGU12");
                shipmentPlan.ConsigneeGU = consigneeGU;
                Guid destinationStation = planEntity.GetTypedColumnValue<Guid>("CrocDestinationStationId");
                if (destinationStation != Guid.Empty)
                {
                    shipmentPlan.DestinationStation = GetOneCObjectCode("CrocRailwayStation", "CrocCode", destinationStation);
                }
                Guid departureStation = planEntity.GetTypedColumnValue<Guid>("CrocDepartureStationId");
                if (departureStation != Guid.Empty)
                {
                    shipmentPlan.DepartureStation = GetOneCObjectCode("CrocRailwayStation", "CrocCode", departureStation);
                }
            }
            
            switch (typePlan)
            {
                case 1: //"CrocLoadingSchedule"
                    List<Entity> loadSchedule = _entityHelper.FindEntities("CrocLoadingSchedule", new Dictionary<string, object> { { "CrocChart.Id", recordId } }).ToList();
                    if (loadSchedule?.Count > 0)
                    {
                        shipmentPlan.Loadings = new List<Loading>();
                        foreach (Entity loading in loadSchedule)
                        {
                            shipmentPlan.Loadings.Add(new Loading
                            {
                                LoadingDate = loading.GetTypedColumnValue<DateTime>("CrocShipmentDate").ToString(_dateFormat),
                                WeightPlan = loading.GetTypedColumnValue<decimal>("CrocLoadingTonPlan"),
                                VehiclePlan = loading.GetTypedColumnValue<int>("CrocLoadingTransporPlan")
                            });
                        }
                    }
                    break;
                case 2: //"CrocDeliverySchedule"
                    List<Entity> deliverySchedule = _entityHelper.FindEntities("CrocDeliverySchedule", new Dictionary<string, object> { { "CrocChart.Id", recordId } }).ToList();
                    if (deliverySchedule?.Count > 0)
                    {
                        shipmentPlan.Deliveries = new List<Delivery>();
                        foreach (Entity delivery in deliverySchedule)
                        {
                            shipmentPlan.Deliveries.Add(new Delivery
                            {
                                DeliveryDate = delivery.GetTypedColumnValue<DateTime>("CrocDeliveryDate").ToString(_dateFormat),
                                WeightPlan = delivery.GetTypedColumnValue<decimal>("CrocDeliveryTonPlan"),
                                VehiclePlan = delivery.GetTypedColumnValue<int>("CrocDeliveryTransportPlan")
                            });
                        }
                    }
                    break;
            } 
            return shipmentPlan;
        }

        private QuotaRequest PrepareRequestQuotaData(Guid recordId)
        {
            QuotaRequest requestQuota = new QuotaRequest();
            Entity activityEntity = _entityHelper.ReadEntity("Activity", recordId);
            requestQuota.Id = recordId.ToString();
            Guid productId = activityEntity.GetTypedColumnValue<Guid>("CrocNomenclatureId");
            Guid terminalId = activityEntity.GetTypedColumnValue<Guid>("CrocTerminalId");
            Guid deliveryId = activityEntity.GetTypedColumnValue<Guid>("CrocDeliveryScheduleId");
            Entity deliveryEntity = _entityHelper.ReadEntity("CrocDeliverySchedule", deliveryId);
            Guid chartId = deliveryEntity.GetTypedColumnValue<Guid>("CrocChartId");
            Guid orderId = activityEntity.GetTypedColumnValue<Guid>("CrocOrderId");
            Guid accountId = activityEntity.GetTypedColumnValue<Guid>("AccountId");
            requestQuota.Nomenclature = GetOneCObjectCode("CrocProduct", "CrocGuid", productId);
            requestQuota.Counterparty = GetOneCObjectCode("Account", "CrocGuid", accountId);
            requestQuota.Terminal = GetOneCObjectCode("CrocTerminal", "CrocId", terminalId);
            if (orderId != Guid.Empty) { requestQuota.Assignment = GetOneCObjectCode("CrocErrand", "CrocGuid", orderId); }
            requestQuota.Shipment = GetOneCObjectCode("CrocChart", "CrocId", chartId);//deliveryId.ToString();
            requestQuota.WeightQuote = activityEntity.GetTypedColumnValue<decimal>("CrocTonRequest");
            requestQuota.VehicleQuote = activityEntity.GetTypedColumnValue<int>("CrocCarRequest");
            requestQuota.Period = activityEntity.GetTypedColumnValue<DateTime>("CrocDatet").ToString(_dateFormat);
            return requestQuota;
        }

        private QuotaRefusal PrepareRefusalQuotaData(Guid recordId)
        {
            QuotaRefusal refusalQuota = new QuotaRefusal();
            Entity activityEntity = _entityHelper.ReadEntity("Activity", recordId);
            refusalQuota.Id = recordId.ToString();
            Guid deliveryId = activityEntity.GetTypedColumnValue<Guid>("CrocDeliveryScheduleId");
            Entity deliveryEntity = _entityHelper.ReadEntity("CrocDeliverySchedule", deliveryId);
            Guid chartId = deliveryEntity.GetTypedColumnValue<Guid>("CrocChartId");
            refusalQuota.Shipment = GetOneCObjectCode("CrocChart", "CrocId", chartId);
            refusalQuota.WeightQuote = activityEntity.GetTypedColumnValue<decimal>("CrocTonAllocate");
            refusalQuota.VehicleQuote = activityEntity.GetTypedColumnValue<int>("CrocCarAllocate");
            refusalQuota.Period = activityEntity.GetTypedColumnValue<DateTime>("CrocDatet").ToString(_dateFormat);
            return refusalQuota;
        }

        private Accreditation PrepareAccreditationData(Guid recordId)
        {
            Accreditation accreditation = new Accreditation();
            Entity documentEntity = _entityHelper.ReadEntity("CrocDocument", recordId);
            //Guid accreditationExtId = documentEntity.GetTypedColumnValue<Guid>("CrocGuid");
            List<Entity> complectDocuments = _entityHelper.FindEntities("CrocSetDocument", new Dictionary<string, object> 
            { { "CrocMainDocument.Id", recordId } }).ToList();
            if (complectDocuments?.Count > 0)
            {
                if(complectDocuments.Any(x => x.GetTypedColumnValue<bool>("CrocIsRequired") == true))
                {
                    if(!CheckRequiredFiles(complectDocuments
                        .Where(x => x.GetTypedColumnValue<bool>("CrocIsRequired") == true)
                        .Select(x => x.GetTypedColumnValue<Guid>("CrocDocumentId")).ToList()))
                    throw new InvalidOperationException("TCM: Необходимо добавить файлы в документы из комплекта с признаком Обязательный"); 
                }
                accreditation.Documents = new List<Document>();
                foreach (Entity setDocument in complectDocuments)
                {
                    Guid subDocumentId = setDocument.GetTypedColumnValue<Guid>("CrocDocumentId");
                    Entity subDocumentEntity = _entityHelper.ReadEntity("CrocDocument", subDocumentId);
                    Guid typeId = subDocumentEntity.GetTypedColumnValue<Guid>("CrocDocumentTypeId");
                    Dictionary<DateTime, string> files = GetDocumentFiles(subDocumentId);
                    if (files.Count > 0)
                    {
                        foreach (KeyValuePair<DateTime, string> file in files)
                        {
                            accreditation.Documents.Add(new Document
                            {
                                Type = GetOneCObjectCode("CrocDocumentType", "CrocGuid", typeId),
                                ValidOnDate = subDocumentEntity.GetTypedColumnValue<DateTime>("CrocRelevanceDate").ToString(_dateFormat),
                                AddOnDate = file.Key.ToString(_dateFormatSS),
                                Comment = setDocument.GetTypedColumnValue<string>("CrocComment"),
                                Link = file.Value
                            });
                        }
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("TCM: Необходимо добавить файлы в документы из комплекта");
            }
            accreditation.Id = recordId.ToString();
            Guid accountId = documentEntity.GetTypedColumnValue<Guid>("CrocAccountId");
            if(accountId != Guid.Empty) { accreditation.Account = GetOneCObjectCode("Account", "CrocGuid", accountId); }
            Guid accreditationTypeId = documentEntity.GetTypedColumnValue<Guid>("CrocAccreditationTypeId");
            if(accreditationTypeId != Guid.Empty)
            {
                Entity accreditationType = _entityHelper.ReadEntity("CrocAccreditationType", accreditationTypeId);
                accreditation.AccreditationType = accreditationTypeId.ToString();
                accreditation.AccreditationTypeName = accreditationType.GetTypedColumnValue<string>("CrocCompanyTypeName");
            }
            return accreditation;
        }

        private bool CheckRequiredFiles(List<Guid> documentIds)
        {
            bool result = true;
            foreach(Guid documentId in documentIds)
            {
                List<Entity> files = _entityHelper.FindEntities("CrocDocumentFile", new Dictionary<string, object>
                    { { "CrocDocument.Id", documentId } }).ToList();
                result = files?.Count > 0;
                if (!result) return result;
            }
            return result;
        }

        private Dictionary<DateTime, string> GetDocumentFiles(Guid documentId)
        {
            Dictionary<DateTime, string> result = new Dictionary<DateTime, string>();
            List<Entity> files = _entityHelper.FindEntities("CrocDocumentFile", 
                new Dictionary<string, object> { { "CrocDocument.Id", documentId } }).ToList();
            if (files?.Count > 0)
            {
                List<Guid> fileIds = files.Select(x => x.PrimaryColumnValue).ToList();
                foreach (var fileId in fileIds)
                {
                    List<Entity> versionFiles = _entityHelper.FindEntities("CrocDocumentVersion", 
                        new Dictionary<string, object> { { "CrocDocumentFile.Id", fileId } }).ToList();
                    if(versionFiles?.Count > 1)
                    {
                        Entity versionFile = versionFiles.OrderByDescending(x => x.GetTypedColumnValue<int>("CrocVersion")).FirstOrDefault();
                        string linkS3 = versionFile.GetTypedColumnValue<string>("CrocS3Link");
                        DateTime createOnFile = versionFile.GetTypedColumnValue<DateTime>("CreatedOn");
                        if (!string.IsNullOrEmpty(linkS3) && !result.ContainsKey(createOnFile))
                        {
                            result.Add(createOnFile, linkS3);
                        }
                    }
                    else
                    {
                        Entity file = files.Where(x => x.PrimaryColumnValue == fileId).FirstOrDefault();
                        string linkS3 = file.GetTypedColumnValue<string>("CrocS3Link");
                        DateTime createOnFile = file.GetTypedColumnValue<DateTime>("CreatedOn");
                        if (!string.IsNullOrEmpty(linkS3) && !result.ContainsKey(createOnFile))
                        {
                            result.Add(createOnFile, linkS3);
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// выгрузка из Договора
        /// </summary>
        /// <param name="recordId"></param>
        /// <returns></returns>
        private ShipmentDocument PrepareShipmentDocumentData(Guid recordId)
        {
            ShipmentDocument shipmentDocuments = new ShipmentDocument
            {
                AdditionalAgreement = recordId.ToString(),
                Documents = new List<ShipmentDocumentTS>()
            };
            Entity contractEntity = _entityHelper.ReadEntity("CrocContract", recordId);
            List<Entity> documentSetList = _entityHelper.FindEntities("CrocSetDocument", new Dictionary<string, object>
            { { "CrocContract.Id", recordId} }).ToList();
            if (documentSetList?.Count > 0)
            {
                List<Guid> documentIds = documentSetList.Select(x => x.GetTypedColumnValue<Guid>("CrocDocumentId")).Distinct().ToList();
                documentIds.ForEach(documentId =>
                    {
                        Entity documentEntity = _entityHelper.ReadEntity("CrocDocument", documentId);
                        string documentTypeName = documentEntity.GetTypedColumnValue<string>("CrocDocumentTypeName");
                        Dictionary<DateTime, string> files = GetDocumentFiles(documentId);
                        if (files.Count > 0)
                        {
                            foreach (KeyValuePair<DateTime, string> file in files)
                            {
                                shipmentDocuments.Documents.Add(new ShipmentDocumentTS()
                                {
                                    Type = documentTypeName,
                                    AddOnDate = file.Key.ToString(_dateFormatSS),
                                    Link = file.Value
                                });
                            }
                        }
                    }
                );
            }
            return shipmentDocuments;
        }

        private ServiceRequest PrepareServiceRequestData(Guid recordId)
        {
            Entity requestEntity = _entityHelper.ReadEntity("CrocRequest", recordId);
            string extId = GetOneCObjectCode("CrocRequest", "CrocId", recordId);
            Guid statusId = requestEntity.GetTypedColumnValue<Guid>("CrocApplicationStatusId");
            string applicationTypeName = requestEntity.GetTypedColumnValue<string>("CrocApplicationTypeName");
            Guid providerId = requestEntity.GetTypedColumnValue<Guid>("CrocProviderId");
            Guid typeId = requestEntity.GetTypedColumnValue<Guid>("CrocApplicationTypeId");
            string idFieldName = typeId == ConstCs.CrocApplicationType.Transporter ? "CrocGuidTr" : "CrocGuid";
            ServiceRequest serviceRequest = new ServiceRequest()
            {
                Id = extId,
                Status = GetOneCObjectCode("CrocApplicationStatus", idFieldName, statusId),
                ApplicationTypeName = applicationTypeName
            };
            if (typeId == ConstCs.CrocApplicationType.Transporter && providerId != Guid.Empty) 
            {
                serviceRequest.Provider = GetOneCObjectCode("Account", "CrocGuid", providerId);
            }
            return serviceRequest;
        }

        private string GetShoterMessage(string text)
        {
            if(string.IsNullOrEmpty(text)) return string.Empty;
            return text.Length > 500 ? text.Substring(0, 500) : text;
        }

        private void SetIntegrationLog(string name, string schemaName, string requestText, string result, Guid recordId, string responseCode = "")
        {
            string dateText = DateTime.Now.ToString("dd.MM.yyyy") +"_"+ DateTime.Now.ToString("t");
            _entityHelper.InsertEntity("CrocOneCIntegrationLog", new Dictionary<string, object>
            {
                { "CrocName", $"{name}_{dateText}"},
                { "CrocSchemaName", schemaName},
                { "CrocResult", result },
                { "CrocUserId", _currentUserConnection.CurrentUser.ContactId },
                { "CrocRecordUId", recordId },
                { "CrocRequest",  requestText},
                { "CrocResponse", responseCode }
            });
        }

        #region SmartSeeds Private

        private Dictionary<string, string> GetBaseSmartSeedHeaders()
        {
            return new Dictionary<string, string>
            {
                { "Accept", "application/json, text/plain, */*" },
                { "X-Requested-With", "XMLHttpRequest"}
            };
        }

        private string GetSmartSeedTimespan()
        {
            DateTime current = DateTime.UtcNow;
            DateTimeOffset dto = new DateTimeOffset(current.Year, current.Month, current.Day, current.Hour, current.Minute, current.Second, TimeSpan.Zero);
            return dto.ToUnixTimeMilliseconds().ToString();
        }

        private string GetSmartSeedsAuth(string link)
        {
            string result = string.Empty;
            link = $@"{link}?_={GetSmartSeedTimespan()}";
            RestHelper restHelper = new RestHelper
            {
                Url = link,
                Method = Method.GET,
                DataFormat = DataFormat.Json,
                Headers = GetBaseSmartSeedHeaders()
            };
            BaseTypeResponse<SSToken> baseResponse = restHelper.SendRequest<SSToken>();
            if (baseResponse.Success) { result = baseResponse.Response.Token; }
            return result;
        }

        private SmartseedsCalculationRequest FillSmartSeedsParameters(SSClientParameters parameters)
        {
            Entity productEntity = _entityHelper.ReadEntity("CrocProduct", parameters.ProductId);
            int productCode = productEntity.GetTypedColumnValue<int>("CrocSmartseedsProductCode");
            Entity addressFrom = _entityHelper.ReadEntity("CrocSmartseedsAddresses", parameters.OriginId);
            string addressFromCode = addressFrom.GetTypedColumnValue<string>("CrocGuid");
            Entity addressTo = _entityHelper.ReadEntity("CrocSmartseedsAddresses", parameters.DestinationId);
            string addressToCode = addressTo.GetTypedColumnValue<string>("CrocGuid");
            SmartseedsCalculationRequest calculationRequest = new SmartseedsCalculationRequest
            {
                OriginId = addressFromCode,
                DestinationId = addressToCode,
                DeliveryDate = parameters.DeliveryDate.ToString(_dateFormatSS),
                Crops = productCode,
                Weight = parameters.Weight
            };
            return calculationRequest;
        }

        private SmartseedsCalculationRequest FillSmartSeedsChartParameters(SSClientParameters parameters)
        {
            Entity productEntity = _entityHelper.ReadEntity("CrocProduct", parameters.ProductId);
            int productCode = productEntity.GetTypedColumnValue<int>("CrocSmartseedsProductCode");
            Entity addressFrom = _entityHelper.ReadEntity("CrocTerminal", parameters.OriginId);
            string addressFromCode = addressFrom.GetTypedColumnValue<string>("CrocFIASCode");
            Entity addressTo = _entityHelper.ReadEntity("CrocTerminal", parameters.DestinationId);
            string addressToCode = addressTo.GetTypedColumnValue<string>("CrocFIASCode");
            SmartseedsCalculationRequest calculationRequest = new SmartseedsCalculationRequest
            {
                OriginId = addressFromCode,
                DestinationId = addressToCode,
                DeliveryDate = parameters.DeliveryDate.ToString(_dateFormatSS),
                Crops = productCode,
                Weight = parameters.Weight
            };
            return calculationRequest;
        }

        private BaseTypeResponse<SmartseedsCalculationResponse> SendSmartSeedsCalculation(string token, string link, object body)
        {
            Dictionary<string, string> headers = GetBaseSmartSeedHeaders();
            headers.Add("Authorization", $"Bearer {token}");
            headers.Add("Content-Type", "application/json");
            headers.Add("cache-control", "no-cache");
            RestHelper restHelper = new RestHelper
            {
                Url = link,
                Method = Method.POST,
                DataFormat = DataFormat.Json,
                Headers = headers//,
                //Body = body
            };
            restHelper.Parameter = new Parameter {  Name = "application/json", Value = body, Type = ParameterType.RequestBody};
            return restHelper.SendRequest<SmartseedsCalculationResponse>();
        }

        private BaseResponse SaveSmartSeedsCalculationResult(SSClientParameters clientParameters, BaseTypeResponse<SmartseedsCalculationResponse> response)
        {
            BaseResponse result = new BaseResponse
            {
                Success = response.Success,
                ErrorInfo = response.ErrorInfo
            };
            if (!result.Success) 
            {
                _logger.Error(response.ErrorInfo.Message);
                return result;
            }
            //"CrocDistanceSmartseeds" Расстояние, км - distance
            //"CrocNumberFlightSmartseeds" Кол-во рейсов - vehicleCount
            //"CrocTonCarSmartseeds" Тоннаж 1 машины - maxVehicleRoominess
            //"CrocTravelTimeSmartseeds" Время в часах в пути - time (округлять до целых)
            //"CrocAmountTonSmartseeds" Сумма за тонну, рубли - priceGrossPerTon.amount
            //"CrocTotalAmountSmartseeds" Итоговая сумма, рубли - priceGross[].amount
            SmartseedsCalculationResponse parameters = response.Response;
            int distance = parameters.Distance/1000;
            int vehicleCount = parameters.VehicleCount;
            int maxVehicleRoominess = parameters.MaxVehicleRoominess;
            int time = Convert.ToInt32(Math.Round(parameters.Time, 0));
            decimal priceGrossPerTon = Convert.ToDecimal(parameters.PriceGrossPerTon.Amount / 100);
            decimal priceGross = Convert.ToDecimal(parameters.PriceGross.Amount / 100);
            _entityHelper.UpdateEntity("CrocContract", clientParameters.RecordId, new Dictionary<string, object>
            {
                { "CrocDistanceSmartseeds", distance },
                { "CrocNumberFlightSmartseeds", vehicleCount },
                { "CrocTonCarSmartseeds", maxVehicleRoominess },
                { "CrocTravelTimeSmartseeds", time },
                { "CrocAmountTonSmartseeds", priceGrossPerTon },
                { "CrocTotalAmountSmartseeds", priceGross },
                { "CrocSmartseedsAddressesFromId", clientParameters.OriginId },
                { "CrocSmartseedsAddressesToId", clientParameters.DestinationId },
                { "CrocShipmentDateSmartseeds" ,clientParameters.DeliveryDate },
                { "CrocProductSmartseedsId", clientParameters.ProductId },
                { "CrocTonSmartseeds", clientParameters.Weight }
            });
            return result;
        }

        private BaseResponse SaveSmartSeedsChartCalculationResult(SSClientParameters clientParameters, BaseTypeResponse<SmartseedsCalculationResponse> response)
        {
            BaseResponse result = new BaseResponse
            {
                Success = response.Success,
                ErrorInfo = response.ErrorInfo
            };
            if (!result.Success)
            {
                _logger.Error(response.ErrorInfo.Message);
                return result;
            }
            //"CrocDistanceSmartseeds" Расстояние, км - distance
            //"CrocNumberFlightSmartseeds" Кол-во рейсов - vehicleCount
            //"CrocTonCarSmartseeds" Тоннаж 1 машины - maxVehicleRoominess
            //"CrocTravelTimeSmartseeds" Время в часах в пути - time (округлять до целых)
            //"CrocAmountTonSmartseeds" Сумма за тонну, рубли - priceGrossPerTon.amount
            //"CrocTotalAmountSmartseeds" Итоговая сумма, рубли - priceGross[].amount
            SmartseedsCalculationResponse parameters = response.Response;
            int distance = parameters.Distance/1000;
            int vehicleCount = parameters.VehicleCount;
            int maxVehicleRoominess = parameters.MaxVehicleRoominess;
            int time = Convert.ToInt32(Math.Round(parameters.Time, 0));
            decimal priceGrossPerTon = Convert.ToDecimal(parameters.PriceGrossPerTon.Amount / 100);
            decimal priceGross = Convert.ToDecimal(parameters.PriceGross.Amount / 100);
            _entityHelper.UpdateEntity("CrocChart", clientParameters.RecordId, new Dictionary<string, object>
            {
                { "CrocDistanceSmartseeds", distance },
                { "CrocNumberFlightSmartseeds", vehicleCount },
                { "CrocTonCarSmartseeds", maxVehicleRoominess },
                { "CrocTravelTimeSmartseeds", time },
                { "CrocAmountTonSmartseeds", priceGrossPerTon },
                { "CrocTotalAmountSmartseeds", priceGross }
            });
            return result;
        }

        #endregion

        #endregion

        #region Methods: Public

        #region 1C Integration
        public BaseResponse SendPlanRefund(Guid refundId)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCPlanRefundsService", ""));
                object body = PrepareRefundData(refundId);
                requestText = JsonConvert.SerializeObject(body);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog( "Планируемый возврат денежных средств", _commonHelper.GetSchemaCaption("CrocPlannedRefund"),
                requestText, GetShoterMessage(resultText), refundId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Планируемый возврат денежных средств: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendEmptyVehicles(Guid recordId, string masterColumnName)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCEmptyVehiclesService", ""));
                object body = PrepareEmptyVehiclesData(recordId, masterColumnName);
                requestText = JsonConvert.SerializeObject(body);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("Реестр порожних ТС", _commonHelper.GetSchemaCaption("CrocRegisterEmptyCar"),
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Реестр порожних ТС: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendGU12(Guid recordId)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCRequestGU12Service", ""));
                object body = PrepareRequestGUData(recordId);
                requestText = JsonConvert.SerializeObject(body);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("Заявка ГУ-12", _commonHelper.GetSchemaCaption("CrocDocument"), 
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Заявка ГУ-12: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendRegisterLoading(Guid recordId)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCRegisterLoadingService", ""));
                object body = PrepareRegisterLoadingData(recordId);
                requestText = JsonConvert.SerializeObject(body);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("Реестр погрузки Поставщика", _commonHelper.GetSchemaCaption("CrocRegisterLoading"), 
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Реестр погрузки Поставщика: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendRegisterLoadingOnDocument(Guid recordId)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCRegisterLoadingService", ""));
                object body = PrepareRegisterLoadingDataOnDocument(recordId);
                requestText = JsonConvert.SerializeObject(body);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("Реестр погрузки Поставщика", _commonHelper.GetSchemaCaption("CrocRegisterLoading"),
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Реестр погрузки Поставщика: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendShipmentPlan(Guid recordId, int typePlan)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message =  string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCShipmentPlanService", ""));
                object body = PrepareShipmentPlanData(recordId, typePlan);
                requestText = JsonConvert.SerializeObject(body);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("График отгрузок_поставки", _commonHelper.GetSchemaCaption("CrocChart"),
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"График отгрузок_поставки: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendRequestQuota(Guid recordId)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCRequestQuotaService", ""));
                object body = PrepareRequestQuotaData(recordId);
                requestText = JsonConvert.SerializeObject(body);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("Запрос квоты", _commonHelper.GetSchemaCaption("Activity"),
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Запрос квоты: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendRefusalQuota(Guid recordId)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCRefusalQuotaService", ""));
                object body = PrepareRefusalQuotaData(recordId);
                requestText = JsonConvert.SerializeObject(body);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("Отказ от квоты", _commonHelper.GetSchemaCaption("Activity"),
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Отказ от квоты: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendAccreditation(Guid recordId)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCAccreditationService", ""));
                Accreditation accreditation = PrepareAccreditationData(recordId);
                requestText = JsonConvert.SerializeObject(accreditation);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("Аккредитация", _commonHelper.GetSchemaCaption("CrocDocument"),
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Аккредитация: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendShipmentDocument(Guid recordId)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCShipmentDocumentService", ""));
                ShipmentDocument documents = PrepareShipmentDocumentData(recordId);
                if(documents.Documents.Count > 0)
                {
                    requestText = JsonConvert.SerializeObject(documents);
                    baseResponse = SendData(requestText);
                    if (baseResponse == null)
                    {
                        baseResponse = new BaseResponse { Success = true, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                    }
                }
                else { throw new InvalidOperationException("Нет корректных файлов в документах из комплекта"); }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("Документы по перевозке", _commonHelper.GetSchemaCaption("CrocContract"),
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Документы по перевозке: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        public BaseResponse SendServiceRequest(Guid recordId)
        {
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { ErrorCode = string.Empty, Message = string.Empty } };
            string requestText = string.Empty;
            try
            {
                GetFullUrl(CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocOneCServiceRequestService", ""));
                object body = PrepareServiceRequestData(recordId);
                requestText = JsonConvert.SerializeObject(body);
                baseResponse = SendData(requestText);
                if (baseResponse == null)
                {
                    baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = "No answere", ErrorCode = string.Empty } };
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                _logger.Error(ex.StackTrace);
                baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = ex.Message, StackTrace = ex.StackTrace, ErrorCode = string.Empty } };
            }
            string resultText = _defaultMessage;
            string statusCode = string.IsNullOrEmpty(baseResponse.ErrorInfo.ErrorCode) ? string.Empty : baseResponse.ErrorInfo.ErrorCode;
            if (!baseResponse.Success) { resultText = baseResponse.ErrorInfo.Message; }
            SetIntegrationLog("Заявка на оказание услуг", _commonHelper.GetSchemaCaption("CrocRequest"),
                requestText, GetShoterMessage(resultText), recordId, statusCode);
            //_commonHelper.SendRemindMessageToUser(GetShoterMessage($"Заявка на оказание услуг: {resultText}"), _defaultSchemaUId);
            return baseResponse;
        }

        #endregion

        #region SmartSeed

        public BaseResponse SendSmartSeedsCalculationRequest(string config)
        {
            string messError = string.Empty;
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo() };
            string baseLink = CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocBaseSmartSeedsUrl", string.Empty);
            string authLink = CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocSmartSeedsAuthorizationService", string.Empty);
            string calcLink = CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocSmartSeedsCalculationService", string.Empty);
            if (string.IsNullOrEmpty(baseLink) || string.IsNullOrEmpty(authLink) || string.IsNullOrEmpty(calcLink)) 
            { messError = "Не заполнены системные настройки для интеграции со SmartSeeds"; }
            else
            {
                string token = GetSmartSeedsAuth($@"{baseLink}{authLink}");
                if (string.IsNullOrEmpty(token)) { messError = "Ошибка получения Токена авторизации SmartSeeds"; }
                else
                {
                    try
                    {
                        SSClientParameters parameters = JsonConvert.DeserializeObject<SSClientParameters>(config);
                        SmartseedsCalculationRequest calculationRequest = FillSmartSeedsParameters(parameters);
                        string obj = JsonConvert.SerializeObject(calculationRequest);
                        baseResponse = SaveSmartSeedsCalculationResult(parameters, SendSmartSeedsCalculation(token, $@"{baseLink}{calcLink}", obj));
                    }
                    catch (Exception ex)
                    {
                        messError = ex.Message;
                        _logger.Error(ex.Message);
                        _logger.Error(ex.StackTrace);
                    }
                }
            }
            if (!string.IsNullOrEmpty(messError)) 
            {
                baseResponse.ErrorInfo.Message = messError;
                _logger.Error(messError);
            }
            return baseResponse;
        }

        public BaseResponse SendSmartSeedsChartRequest(string config)
        {
            string messError = string.Empty;
            BaseResponse baseResponse = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo() };
            string baseLink = CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocBaseSmartSeedsUrl", string.Empty);
            string authLink = CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocSmartSeedsAuthorizationService", string.Empty);
            string calcLink = CoreConfiguration.SysSettings.GetValue(_userConnection, "CrocSmartSeedsCalculationService", string.Empty);
            if (string.IsNullOrEmpty(baseLink) || string.IsNullOrEmpty(authLink) || string.IsNullOrEmpty(calcLink))
            { messError = "Не заполнены системные настройки для интеграции со SmartSeeds"; }
            else
            {
                string token = GetSmartSeedsAuth($@"{baseLink}{authLink}");
                if (string.IsNullOrEmpty(token)) { messError = "Ошибка получения Токена авторизации SmartSeeds"; }
                else
                {
                    try
                    {
                        SSClientParameters parameters = JsonConvert.DeserializeObject<SSClientParameters>(config);
                        SmartseedsCalculationRequest calculationRequest = FillSmartSeedsChartParameters(parameters);
                        string obj = JsonConvert.SerializeObject(calculationRequest);
                        baseResponse = SaveSmartSeedsChartCalculationResult(parameters, SendSmartSeedsCalculation(token, $@"{baseLink}{calcLink}", obj));
                        if (!baseResponse.Success) 
                        {
                            if (!string.IsNullOrEmpty(baseResponse.ErrorInfo.Message)) { _logger.Error(baseResponse.ErrorInfo.Message); }
                            baseResponse.ErrorInfo.Message = "Расчет не произведен по причине некорректности данных";
                        }
                    }
                    catch (Exception ex)
                    {
                        messError = ex.Message;
                        _logger.Error(ex.Message);
                        _logger.Error(ex.StackTrace);
                    }
                }
            }
            if (!string.IsNullOrEmpty(messError))
            {
                baseResponse.ErrorInfo.Message = messError;
                _logger.Error(messError);
            }
            return baseResponse;
        }
        #endregion

        #region Interface
        public void SendPlanRefundI(Guid refundId)
        {
            SendPlanRefund(refundId);
        }

        public void SendEmptyVehiclesI(Guid recordId, string masterColumnName)
        {
            SendEmptyVehicles(recordId, masterColumnName);
        }

        public void SendGU12I(Guid recordId)
        {
            SendGU12(recordId);
        }

        public void SendRegisterLoadingI(Guid recordId)
        {
            SendRegisterLoading(recordId);
        }

        public void SendShipmentPlanI(Guid recordId, int typePlan)
        {
            SendShipmentPlan(recordId, typePlan);
        }

        public void SendRequestQuotaI(Guid recordId)
        {
            SendRequestQuota(recordId);
        }

        public void SendRefusalQuotaI(Guid recordId)
        {
            SendRefusalQuota(recordId);
        }

        public void SendAccreditationI(Guid recordId)
        {
            SendAccreditation(recordId);
        }

        public void SendShipmentDocumentI(Guid recordId)
        {
            SendShipmentDocument(recordId);
        }

        public void SendServiceRequestI(Guid recordId)
        {
            SendServiceRequest(recordId);
        }
        #endregion

        #endregion
    }
}
