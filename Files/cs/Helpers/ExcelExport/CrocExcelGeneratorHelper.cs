﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Text.RegularExpressions;
using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using NPOI.XSSF.UserModel.Helpers;
using Terrasoft.Common;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.S3;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Nui.ServiceModel.DataContract;
using Terrasoft.Nui.ServiceModel.Extensions;
using Terrasoft.Web.Http.Abstractions;
using EntityCollection = Terrasoft.Core.Entities.EntityCollection;

namespace Terrasoft.Configuration
{
	public class CrocExcelGeneratorHelper
	{
		#region Private: Fields

		private static IWorkbook _workbook;

		private readonly UserConnection _userConnection;
		private readonly IHttpContextAccessor _httpContextAccessor;
		private HttpContext CurrentContext => _httpContextAccessor.GetInstance();

		private int _rowCount = 0;
		private int _columnCount = 0;
		private int _rowOffset = 3;
		private List<Dictionary<int, object>> _excelData = new List<Dictionary<int, object>>();

		private readonly string _columnIdCaption = "№";
		private readonly string _columnMonthCaption = "Month";

		#endregion

		#region Constructor

		public CrocExcelGeneratorHelper(UserConnection userConnection)
		{
			_userConnection = userConnection;
			_httpContextAccessor = HttpContext.HttpContextAccessor;
		}

		#endregion

		public byte[] Generate(ExcelModel model, Filters filters)
		{
			_workbook = new XSSFWorkbook();
			ISheet sheet = _workbook.CreateSheet(model.ExcelReportName);

			using (MemoryStream ms = new MemoryStream())
			{
				ConfigExcelFile(sheet, model, filters);
				_workbook.Write(ms);
				return ms.ToArray();
			}
		}

		public void GetExcelFile(CrocFileInfo fileInfo)
		{
			if (fileInfo != null) GetStreamFromFile(fileInfo);
		}

		public void GetStreamFromFile(CrocFileInfo fileInfo)
		{
			using (var memoryStream = new MemoryStream(fileInfo.Data))
			{
				CurrentContext.Response.Headers["Content-Length"] =
					fileInfo.Data.Length.ToString(CultureInfo.InvariantCulture);
				string contentDisposition = GetResponseContentDisposition(fileInfo.Name + "." + fileInfo.Format);
				CurrentContext.Response.AddHeader("Content-Disposition", contentDisposition);
				MimeTypeResult mimeTypeResult = MimeTypeDetector.GetMimeType(fileInfo.Name);
				CurrentContext.Response.ContentType = mimeTypeResult.HasError
					? "application/octet-stream"
					: mimeTypeResult.Type;
				memoryStream.Flush();
				memoryStream.WriteTo(CurrentContext.Response.OutputStream);
			}
		}
		
		protected virtual string GetResponseContentDisposition(string fileName)
		{
			string processedFileName;
			HttpRequest request = CurrentContext.Request;
			string userAgent = (request.UserAgent ?? String.Empty).ToLowerInvariant();
			if (userAgent.Contains("android"))
			{
				processedFileName = $"filename=\"{RemoveSpecialCharacters(fileName)}\"";
			}
			else if (userAgent.Contains("safari"))
			{
				processedFileName = $"filename*=UTF-8''{System.Web.HttpUtility.UrlPathEncode(fileName)}";
			}
			else
			{
				processedFileName =
					$"filename=\"{fileName}\"; filename*=UTF-8''{System.Web.HttpUtility.UrlPathEncode(fileName)}";
			}

			return $"attachment; {processedFileName}";
		}

		private string RemoveSpecialCharacters(string fileName)
		{
			return Regex.Replace(fileName, @"[^a-zA-Z\p{IsCyrillic}0-9_.,^&@£$€!½§~'=()\[\]{} «»<>~#*%+-]+",
				"", RegexOptions.Compiled);
		}

		private void ConfigExcelFile(ISheet sheet, ExcelModel model, Filters filters)
		{
			_rowOffset = model.RowOffset + 1;

			SetCreateDate(sheet, model);
			FormColumnHeader(sheet, model);

			if (model.ExcelReportName == "План позиций ВЭД")
				FormVEDHeader(sheet, model);

			SetDataExcelFile(sheet, model, filters);
		}

		private void FormColumnHeader(ISheet sheet, ExcelModel model)
		{
			if (model.Columns.Count > 0)
			{
				var row = sheet.CreateRow(model.RowOffset);
				ICell firstCol = null;
				string firstColInfo = "";

				if (model.HasIdColumn)
				{
					firstColInfo = _columnIdCaption;
				}

				if (model.HasMonthColumn)
				{
					firstColInfo = _columnMonthCaption;
				}

				var style = SetColumnHeaderStyle(model);

				if (!String.IsNullOrEmpty(firstColInfo))
				{
					firstCol = row.CreateCell(0);
					_columnCount++;

					sheet.AutoSizeColumn(0);
					firstCol.CellStyle = style;
					firstCol.SetCellValue(firstColInfo);
				}
				
				for (int i = 0; i < model.Columns.Count; i++)
				{
					if (model.Columns[i].IsVisible && !model.Columns[i].IsTechnicalColumn && !model.Columns[i].IsExcelFormula)
					{
						var cell = row.CreateCell(model.Columns[i].Position);
						cell.CellStyle = style;
						cell.SetCellValue(model.Columns[i].DisplayName);
						_columnCount++;
					}
				}
			}
		}

		private void FormVEDHeader(ISheet sheet, ExcelModel model)
		{
			var style = _workbook.CreateCellStyle();
			var font = _workbook.CreateFont();
			font.Color = IndexedColors.Red.Index;
			style.SetFont(font);
			style.Alignment = HorizontalAlignment.Center;
			style.BorderLeft = BorderStyle.Thin;
			style.BorderTop = BorderStyle.Thin;
			style.BorderRight = BorderStyle.Thin;
			style.BorderBottom = BorderStyle.Thin;

			var row = sheet.CreateRow(0);
			var cellPosition = row.CreateCell(0);
			cellPosition.SetCellValue("ПОЗИЦИЯ В ПОРТУ");
			cellPosition.CellStyle = style;
			NPOI.SS.Util.CellRangeAddress portPosition = new CellRangeAddress(0, 0, 0, 5);

			var cellSell = row.CreateCell(6);
			cellSell.SetCellValue("ПРОДАЖА");
			cellSell.CellStyle = style;
			NPOI.SS.Util.CellRangeAddress sell = new CellRangeAddress(0, 0, 6, 9);

			var cellVessel = row.CreateCell(10);
			cellVessel.SetCellValue("НОМИНАЦИЯ СУДНА");
			cellVessel.CellStyle = style;
			NPOI.SS.Util.CellRangeAddress vessel = new CellRangeAddress(0, 0, 10, 13);

			style.BorderRight = BorderStyle.None;
			style.BorderBottom = BorderStyle.None;
			style.BorderTop = BorderStyle.None;
			var techCell = row.CreateCell(14);
			techCell.CellStyle = style;

			sheet.AddMergedRegion(portPosition);
			sheet.AddMergedRegion(sell);
			sheet.AddMergedRegion(vessel);
		}

		private ICellStyle SetColumnHeaderStyle(ExcelModel model)
		{
			var style = _workbook.CreateCellStyle();
			var font = _workbook.CreateFont();

			if (model.ExcelReportName == "План позиций ВЭД")
			{
				font.Color = IndexedColors.White.Index;
				style.BorderLeft = BorderStyle.Thin;
				style.BorderTop = BorderStyle.Thin;
				style.BorderRight = BorderStyle.Thin;
				style.BorderBottom = BorderStyle.Thin;
				style.FillBackgroundColor = IndexedColors.Blue.Index;
				style.FillForegroundColor = IndexedColors.DarkBlue.Index;
				style.FillPattern = FillPattern.SolidForeground;
			}
			else
			{
				font.IsBold = true;
				style.BorderLeft = BorderStyle.Thick;
				style.BorderTop = BorderStyle.Thick;
				style.BorderRight = BorderStyle.Thick;
				style.BorderBottom = BorderStyle.Thick;
			}

			style.SetFont(font);

			return style;
		}

		private void SetDataExcelFile(ISheet sheet, ExcelModel model, Filters filters)
		{
			var entityCollection = GetData(model, filters);

			if (entityCollection != null && entityCollection.Count > 0)
			{
				for (int count = 0; count < entityCollection.Count; count++)
				{
					_excelData.Add(new Dictionary<int, object>());
					var row = sheet.CreateRow(_rowCount + _rowOffset);
					SetIdColumn(row, model.HasIdColumn);
					SetMonthColumn(row, model.HasMonthColumn);
					for (int j = 0; j < model.Columns.Count; j++)
					{
						if (!String.IsNullOrEmpty(model.Columns[j].Name) && !model.Columns[j].IsAggregate &&
						    model.Columns[j].IsVisible && !model.Columns[j].IsRailwayColumn)
						{
							var result = entityCollection[count].GetColumnValue(model.Columns[j].Name);
							_excelData[_rowCount].Add(model.Columns[j].Position, result);
						}
					}

					InsertAdditionalData(sheet, model, _rowCount + _rowOffset, entityCollection[count]);
					_rowCount++;
				}

				SeedData(sheet, model);
				FormResultExcelFormula(model, sheet);
				SetCellStyle(sheet);
			}
		}

		private void SeedData(ISheet sheet, ExcelModel model)
		{
			for (int i = 0; i < _excelData.Count; i++)
			{
				var row = sheet.GetRow(i + _rowOffset);
				foreach (var data in _excelData[i])
				{
					var isVisibleColumn = model.Columns.FirstOrDefault(column => column.Position == data.Key).IsVisible;
					if (isVisibleColumn)
					{
						var cell = row.CreateCell(data.Key);
						SetCellValue(data.Value, cell);
					}

					sheet.AutoSizeColumn(data.Key);
				}
			}
		}

		private void SetCellStyle(ISheet sheet)
		{
			for (int i = 0; i < _rowCount; i++)
			{
				var row = sheet.GetRow(i + _rowOffset);
				for (int j = 0; j < _columnCount; j++)
				{
					var cell = row.GetCell(j);

					if (cell == null)
						cell = row.CreateCell(j);

					SetDataCellStyle(i, _rowCount, j, _columnCount, cell);
				}
			}
		}

		private void SetDataCellStyle(int i, int maxRow, int j, int maxColumn, ICell cell)
		{
			var style = _workbook.CreateCellStyle();
			style.BorderTop = BorderStyle.Thin;
			style.BorderRight = BorderStyle.Thin;
			style.BorderBottom = BorderStyle.Thin;
			style.BorderLeft = BorderStyle.Thin;

			cell.CellStyle = style;
		}

		private EntityCollection GetData(ExcelModel model, Filters filters)
		{
			EntityCollection result = null;
			if (model != null && model.Columns.Count > 0)
			{
				EntitySchemaQuery esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, model.EntitySchemaName);
				model.Columns.ForEach(column =>
				{
					if(!String.IsNullOrEmpty(column.Path) && !column.IsAggregate && !column.IsOperationColumn && !column.ConcatenateValues && 
					   !column.IsTechnicalColumn && !column.IsRailwayColumn)
						column.Name = esq.AddColumn(column.Path).Name;
				});

				if (model.Filter != null && model.Filter.Count > 0)
				{
					foreach (var templateFilter in model.Filter)
					{
						esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal, templateFilter.Path, templateFilter.Value));
					}
				}

				var filter = filters.BuildEsqFilter(model.EntitySchemaName, _userConnection);
				esq.Filters.Add(filter);
				esq.PrimaryQueryColumn.IsAlwaysSelect = true;

				result = esq.GetEntityCollection(_userConnection);
			}

			return result;
		}

		private void InsertAdditionalData(ISheet sheet, ExcelModel model, int rowId, Entity entity)
		{
			InsertAggregateData(model, entity.PrimaryColumnValue, rowId);
			InsertOperationResult(model, rowId);
			InsertConcatenateExistColumns(model, sheet, entity, rowId);
			InsertConcatenateDataByColumns(model, sheet, entity.PrimaryColumnValue, rowId);

			if (model.Columns.Any(column =>
				column.Path == "[CrocRegisterLoading:CrocChart:CrocChart].CrocGrossWeightTon" && column.IsRailwayColumn))
			{
				FormResultLoadingTonFact(model.Columns.FirstOrDefault(column =>
					column.Path == "[CrocRegisterLoading:CrocChart:CrocChart].CrocGrossWeightTon" && column.IsRailwayColumn), entity.PrimaryColumnValue, model.EntitySchemaName, rowId, sheet);
			}
		}

		private void InsertOperationResult(ExcelModel model, int rowId)
		{
			var operationColumns = model.Columns.Where(column => column.OperationColumns != null && 
			                                                     column.OperationColumns.Count > 0).ToList();
			if (operationColumns.Count > 0)
			{
				for (int i = 0; i < operationColumns.Count; i++)
				{
					decimal firstVal = (decimal) _excelData[rowId - _rowOffset][operationColumns[i].OperationColumns[0]];
					decimal secondVal = (decimal) _excelData[rowId - _rowOffset][operationColumns[i].OperationColumns[1]];
					decimal result = GetOperationResult(operationColumns[i].Operator, firstVal, secondVal);
					_excelData[rowId - _rowOffset].Add(operationColumns[i].Position, result);
				}
			}
		}

		private decimal GetOperationResult(Operator oper, decimal firstValue, decimal secondValue)
		{
			decimal result = 0;
			switch (oper)
			{
				case Operator.Plus:
					result = firstValue + secondValue;
				break;

				case Operator.Minus:
					result = firstValue - secondValue;
				break;

				case Operator.Divide:
					result = firstValue / secondValue;
				break;

				case Operator.Multiply:
					result = firstValue * secondValue;
				break;

				default:
				break;
			}

			return result;
		}

		private void InsertAggregateData(ExcelModel model, Guid entityId, int rowId)
		{
			var aggregateColumn = model.Columns.Where(column => column.IsAggregate).ToList();
			if (aggregateColumn.Count > 0)
			{
				for (int i = 0; i < aggregateColumn.Count; i++)
				{
					EntitySchemaQuery esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, model.EntitySchemaName);
					aggregateColumn[i].Name = esq.AddColumn(esq.CreateAggregationFunction(aggregateColumn[i].Func, aggregateColumn[i].Path)).Name;
					esq.PrimaryQueryColumn.IsAlwaysSelect = true;
					var entity = esq.GetEntity(_userConnection, entityId);
					var result = entity.GetColumnValue(aggregateColumn[i].Name);
					_excelData[rowId - _rowOffset].Add(aggregateColumn[i].Position, result);
				}
			}
		}

		private void InsertConcatenateDataByColumns(ExcelModel model, ISheet sheet, Guid entityId, int rowId)
		{
			var columnToConcatenate = model.Columns.Where(column => column.ConcatenateValues).ToList();
			if (columnToConcatenate.Count > 0)
			{
				var row = sheet.GetRow(rowId);

				for (int i = 0; i < columnToConcatenate.Count; i++)
				{
					var result = FormResultConcatenateValueString(columnToConcatenate[i], entityId, model.EntitySchemaName);
					_excelData[rowId - _rowOffset].Add(columnToConcatenate[i].Position, result);
				}
			}
		}

		private void InsertConcatenateExistColumns(ExcelModel model, ISheet sheet, Entity entity, int rowId)
		{
			var columnToConcatenate = model.Columns.Where(column => column.ConcatenateColumns != null 
			                                                        && column.ConcatenateColumns.Count > 0).ToList();
			if (columnToConcatenate.Count > 0)
			{
				for (int j = 0; j < columnToConcatenate.Count; j++)
				{
					var result = FormResultConcatenateColumnString(columnToConcatenate[j], model, entity, sheet, rowId);
					_excelData[rowId - _rowOffset].Add(columnToConcatenate[j].Position, result);
				}
			}
		}

		private string FormResultConcatenateValueString(ExcelColumn column, Guid entityId, string entitySchema)
		{
			StringBuilder resultSB = new StringBuilder();
			EntitySchemaQuery esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, entitySchema);
			var name = esq.AddColumn(column.Path).Name;
			esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal, "Id", entityId));
			var entityCollection = esq.GetEntityCollection(_userConnection);

			if (entityCollection.Count > 0)
			{
				for (int i = 0; i < entityCollection.Count; i++)
				{
					var resultValue = entityCollection[i].GetColumnValue(name);
					string result;
					if (resultValue != null)
					{
						if (resultValue is DateTime resultDate)
						{
							result = resultDate.ToString("dd.MM.yyyy");
						}
						else
						{
							result = resultValue.ToString();
						}
					}
					else
					{
						result = "";
					}
					
					if (!resultSB.ToString().Contains(result))
					{
						if (i == 0)
						{
							resultSB.Append(result);
						}
						else
						{
							resultSB.Append($"{column.Separator} " + result);
						}
					}
				}
			}

			return resultSB.ToString();
		}

		private string FormResultConcatenateColumnString(ExcelColumn columnConcatenate, ExcelModel model, Entity entity, ISheet sheet, int rowId)
		{
			StringBuilder resultSB = new StringBuilder();
			for (int i = 0; i < columnConcatenate.ConcatenateColumns.Count; i++)
			{
				var row = sheet.GetRow(rowId);
				var columnToConcatenate =
					model.Columns.Find(columns => columns.Position == columnConcatenate.ConcatenateColumns[i]);

				var resultValue = _excelData[rowId - _rowOffset][columnToConcatenate.Position];
				string result;
				if (resultValue != null)
				{
					if (resultValue is DateTime resultDate)
					{
						result = resultDate.ToString(columnConcatenate.DateFormat);
					}
					else
					{
						result = resultValue.ToString();
					}
				}
				else
				{
					result = "";
				}

				if (!resultSB.ToString().Contains(result))
				{
					if (i == 0)
					{
						resultSB.Append(result);
					}
					else
					{
						resultSB.Append($" {columnConcatenate.Separator} " + result);
					}
				}
			}

			return resultSB.ToString();
		}

		private void SetCellValue(object value, ICell cell, string datePattern = "dd.MM.yyyy")
		{
			if (String.IsNullOrEmpty(datePattern))
				datePattern = "dd.MM.yyyy";

			if (value != null)
			{
				if (value is DateTime valueDate)
				{
					cell.SetCellValue(valueDate.ToString(datePattern));
				}
				else if (value is decimal valueDecimal)
				{
					cell.SetCellValue((double)valueDecimal);
				}
				else if (value is double valueDouble)
				{
					cell.SetCellValue(valueDouble);
				}
				else if (value is float valueFloat)
				{
					cell.SetCellValue(valueFloat);
				}
				else if (value is int valueInt)
				{
					cell.SetCellValue(valueInt);
				}
				else
				{
					cell.SetCellValue(value.ToString());
				}
			}
		}

		private void FormResultExcelFormula(ExcelModel model, ISheet sheet)
		{
			var formulaColumn = model.Columns.Where(column => column.IsExcelFormula).ToList();

			if (formulaColumn.Count > 0)
			{
				var rowIndex = _rowOffset + _rowCount;
				for (int i = 0; i < formulaColumn.Count; i++)
				{
					IRow row;
					if (sheet.GetRow(rowIndex) != null)
						row = sheet.GetRow(rowIndex);
					else
						row = sheet.CreateRow(rowIndex);

					var cell = row.CreateCell(formulaColumn[i].FormulaPosition);
					var cellAddress = cell.Address.FormatAsString();
					var symbol = Regex.Replace(cellAddress, "[0-9]", "", RegexOptions.IgnoreCase);

					var result = $"{formulaColumn[i].Formula}({symbol}{_rowOffset + 1}:{symbol}{rowIndex})";
					cell.SetCellFormula(result);
				}
			}
		}

		private void FormResultLoadingTonFact(ExcelColumn column, Guid entityId, string entitySchema, int rowId, ISheet sheet)
		{
			StringBuilder resultSB = new StringBuilder();
			EntitySchemaQuery esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, entitySchema);
			var name = esq.AddColumn(column.Path).Name;
			var currentDate = DateTime.Now.Date;
			currentDate = currentDate.AddDays(-1);
			esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal,
				"[CrocRegisterLoading:CrocChart:CrocChart].CrocShippingDate", currentDate));
			esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal, "Id", entityId));
			var entityCollection = esq.GetEntityCollection(_userConnection);

			if (entityCollection.Count > 0)
			{
				var resultValue = entityCollection[0].GetColumnValue(name);
				var result = resultValue != null ? resultValue.ToString() : "";
				var row = sheet.GetRow(rowId);
				row.CreateCell(column.Position).SetCellValue(result);
			}
		}

		private void SetCreateDate(ISheet sheet, ExcelModel model)
		{
			var column = model.Columns.FirstOrDefault(col => col.Path == "CurrentDate");
			if (column != null)
			{
				var row = sheet.CreateRow(0);
				row.CreateCell(0).SetCellValue(column.DisplayName);
				row.CreateCell(1).SetCellValue(DateTime.Now.Date.ToString("dd.MM.yyyy"));
			}
		}

		private void SetIdColumn(IRow row, bool hasIdColumn)
		{
			if (hasIdColumn)
			{
				var idNum = row.RowNum - _rowOffset + 1;
				row.CreateCell(0).SetCellValue(idNum);
			}
		}

		private void SetMonthColumn(IRow row, bool hasMonthColumn)
		{
			if (hasMonthColumn)
			{
				row.CreateCell(0).SetCellValue(DateTime.Now.Date.ToString("MMMM"));
			}
		}
	}
}
