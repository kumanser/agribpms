﻿using System;
using System.Collections.Generic;
using Terrasoft.Core;
using Terrasoft.Configuration.Base;
using System.Data;
using Terrasoft.Core.Entities;
using System.Linq;
using System.Globalization;
using Terrasoft.Core.Configuration;
using Terrasoft.Messaging.Common;
using Newtonsoft.Json;
using Terrasoft.Core.DB;
using Common.Logging;

namespace Terrasoft.Configuration.CrocDemetra
{
    public class CommonHelper
    {
        private UserConnection _userConnection;
        private CrocEntityHelper _entityHelper;
        private static ILog _log = LogManager.GetLogger("Error");
        public CommonHelper(UserConnection userConnection)
        {
            _userConnection = userConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        private Guid? CreateDocument(Guid sourceDocumentId, Guid masterRecordId, string masterColumn)
        {
            Guid? result = null;
            Entity baseDocument = _entityHelper.ReadEntity("CrocDocument", sourceDocumentId);
            if (baseDocument != null)
            {
                Guid newDocumentId = Guid.NewGuid();
                EntitySchema entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName("CrocDocument");
                Entity newDocument = entitySchema.CreateEntity(_userConnection);
                newDocument = (Entity)baseDocument.Clone();
                newDocument.SetDefColumnValues();
                newDocument.PrimaryColumnValue = newDocumentId;
                newDocument.SetColumnValue("CrocDocumentStatusId", ConstCs.CrocDocument.Status.PreparationId);
                newDocument.Save(true);
                if (newDocument.FetchFromDB(newDocumentId)) 
                {
                    result = _entityHelper.InsertEntity("CrocSetDocument", new Dictionary<string, object>
                    { 
                        { masterColumn + "Id", masterRecordId },
                        { "CrocDocumentId", newDocumentId },
                        { "CrocName", newDocument.PrimaryDisplayColumnValue } 
                    });
                }
            }
            return result;
        }

        private void UpdateSupplayPlan(Entity supplay, Guid transportTypeId)
        {
            Guid terminalId = supplay.GetTypedColumnValue<Guid>("CrocTerminalId");
            DateTime beginDate = supplay.GetTypedColumnValue<DateTime>("CrocStart");
            DateTime endDate = supplay.GetTypedColumnValue<DateTime>("CrocEnd");
            List<Entity> deliverySchedules = GetDeliverySchedules(terminalId, transportTypeId, beginDate, endDate);
            if (deliverySchedules?.Count > 0)
            {
                int controlWeek = 0;
                string monthy = beginDate.Month.ToString();
                monthy = monthy.Length == 1 ? $"0{monthy}" : monthy;
                Guid weekGuid = Guid.Empty;
                Guid week4DaysGuid = Guid.Empty;
                for (DateTime tempDate = beginDate; tempDate <= endDate.Date; tempDate = tempDate.AddDays(1))
                {
                    weekGuid = Guid.Empty;
                    int deliveryWeek = new CultureInfo("ru-RU").Calendar.GetWeekOfYear(tempDate, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                    if (controlWeek != deliveryWeek)
                    {
                        controlWeek = deliveryWeek;
                        weekGuid = GetWeekIdOnNumber(deliveryWeek);
                        week4DaysGuid = weekGuid;
                    }
                    if (weekGuid != Guid.Empty)
                    {
                        GetBorderOfTheWeek(tempDate.Date, monthy, out DateTime beginDay, out DateTime endDay);
                        decimal planTC = deliverySchedules
                            .Where(x => x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") >= beginDay
                                && x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") <= endDay)
                            .Sum(x => x.GetTypedColumnValue<decimal>("CrocDeliveryTransportPlan"));
                        decimal planVolum = deliverySchedules
                            .Where(x => x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") >= beginDay
                                && x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") <= endDay)
                            .Sum(x => x.GetTypedColumnValue<decimal>("CrocDeliveryTonPlan"));
                        List<Entity> monthEntities = _entityHelper.FindEntities("CrocDeliveryScheduleMonthly", new Dictionary<string, object>
                        {
                            { "CrocSupplyPlan.Id", supplay.PrimaryColumnValue },
                            { "CrocWeekNumber.Id", weekGuid },
                            { "CrocTransportType.Id", transportTypeId }
                        }).ToList();
                        if (monthEntities?.Count > 0)
                        {
                            Entity monthEntity = monthEntities.FirstOrDefault();
                            monthEntity.SetColumnValue("CrocQuantityTransport", planTC);
                            monthEntity.SetColumnValue("CrocVolumePlan", planVolum);
                            monthEntity.Save(true);
                        }
                        else
                        {
                            _entityHelper.InsertEntity("CrocDeliveryScheduleMonthly", new Dictionary<string, object>
                            {
                                { "CrocQuantityTransport", planTC },
                                { "CrocSupplyPlanId", supplay.PrimaryColumnValue },
                                { "CrocTransportTypeId", transportTypeId },
                                { "CrocVolumePlan", planVolum },
                                { "CrocWeekNumberId", weekGuid }
                            });
                        }
                    }
                    if (week4DaysGuid != Guid.Empty)
                    {
                        decimal planTC = deliverySchedules
                            .Where(x => x.GetTypedColumnValue<DateTime>("CrocDeliveryDate").Date == tempDate)
                            .Sum(x => x.GetTypedColumnValue<decimal>("CrocDeliveryTransportPlan"));
                        decimal planVolum = deliverySchedules
                            .Where(x => x.GetTypedColumnValue<DateTime>("CrocDeliveryDate").Date == tempDate)
                            .Sum(x => x.GetTypedColumnValue<decimal>("CrocDeliveryTonPlan"));
                        List<Entity> weekEntities = _entityHelper.FindEntities("CrocDeliveryScheduleWeek", new Dictionary<string, object>
                        {
                            { "CrocSupplyPlan.Id", supplay.PrimaryColumnValue },
                            { "CrocWeekNumber.Id", week4DaysGuid },
                            { "CrocTransportType.Id", transportTypeId },
                            { "CrocPlannedDate", tempDate}
                        }).ToList();
                        if (weekEntities?.Count > 0)
                        {
                            Entity weekEntity = weekEntities.FirstOrDefault();
                            weekEntity.SetColumnValue("CrocQuantityTransport", planTC);
                            weekEntity.SetColumnValue("CrocVolumePlan", planVolum);
                            weekEntity.Save(true);
                        }
                        else
                        {
                            _entityHelper.InsertEntity("CrocDeliveryScheduleWeek", new Dictionary<string, object>
                            {
                                { "CrocQuantityTransport", planTC },
                                { "CrocSupplyPlanId", supplay.PrimaryColumnValue },
                                { "CrocTransportTypeId", transportTypeId },
                                { "CrocVolumePlan", planVolum },
                                { "CrocWeekNumberId", week4DaysGuid },
                                { "CrocPlannedDate", tempDate}
                            });
                        }
                    }
                }
            }
        }

        private void CreateNewSupplyPlan(Entity chartEntity, DateTime deliveryDate)
        {
            Guid transportTypeId = chartEntity.GetTypedColumnValue<Guid>("CrocTransportTypeId");
            Guid terminalId = chartEntity.GetTypedColumnValue<Guid>("CrocWarehouseUnloadingId");
            Entity terminalEntity = _entityHelper.ReadEntity("CrocTerminal", terminalId);
            Guid portId = terminalEntity.GetTypedColumnValue<Guid>("CrocAccountId");
            string portName = terminalEntity.GetTypedColumnValue<string>("CrocAccountName");
            Guid productId = chartEntity.GetTypedColumnValue<Guid>("CrocProductId");
            string productName = chartEntity.GetTypedColumnValue<string>("CrocProductName");
            Guid exporterId = chartEntity.GetTypedColumnValue<Guid>("CrocExporterId");
            string monthy = deliveryDate.Month.ToString();
            monthy = monthy.Length == 1 ? $"0{monthy}" : monthy;
            DateTime beginDate = DateTime.Parse($"{deliveryDate.Year}-{monthy}-01").Date;
            string beginDateString = beginDate.ToString("dd.MM.yyyy");
            DateTime endDate = DateTime.Parse($"{deliveryDate.Year}-{monthy}-{DateTime.DaysInMonth(deliveryDate.Year, deliveryDate.Month)}")
                .Date
                .AddDays(1)
                .AddMilliseconds(-1);
            string templateName = $"План завоза для {portName} на {beginDateString} культура {productName}";
            Guid newPlanId = _entityHelper.InsertEntity("CrocSupplyPlan", new Dictionary<string, object>
            {
                { "CrocExporterId", exporterId},
                { "CrocProductId", productId},
                { "CrocTerminalId", terminalId},
                { "CrocStart", beginDate},
                { "CrocEnd", endDate},
                { "CrocPortId", portId},
                { "CrocName", templateName}
            });
            if (newPlanId != Guid.Empty)
            {
                List<Entity> deliverySchedules = GetDeliverySchedules(terminalId, transportTypeId, beginDate, endDate);
                if (deliverySchedules?.Count > 0)
                {
                    int controlWeek = 0;
                    Guid weekGuid = Guid.Empty;
                    Guid week4DaysGuid = Guid.Empty;
                    for (DateTime tempDate = beginDate; tempDate <= endDate.Date; tempDate = tempDate.AddDays(1))
                    {
                        weekGuid = Guid.Empty;
                        int deliveryWeek = new CultureInfo("ru-RU").Calendar.GetWeekOfYear(tempDate, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                        if (controlWeek != deliveryWeek)
                        {
                            controlWeek = deliveryWeek;
                            weekGuid = GetWeekIdOnNumber(deliveryWeek);
                            week4DaysGuid = weekGuid;
                        }
                        if (weekGuid != Guid.Empty)
                        {
                            GetBorderOfTheWeek(tempDate.Date, monthy, out DateTime beginDay, out DateTime endDay);
                            decimal planTC = deliverySchedules
                                .Where(x => x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") >= beginDay
                                    && x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") <= endDay)
                                .Sum(x => x.GetTypedColumnValue<decimal>("CrocDeliveryTransportPlan"));
                            decimal planVolum = deliverySchedules
                                .Where(x => x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") >= beginDay
                                    && x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") <= endDay)
                                .Sum(x => x.GetTypedColumnValue<decimal>("CrocDeliveryTonPlan"));
                            _entityHelper.InsertEntity("CrocDeliveryScheduleMonthly", new Dictionary<string, object>
                            {
                                { "CrocQuantityTransport", planTC },
                                { "CrocSupplyPlanId", newPlanId },
                                { "CrocTransportTypeId", transportTypeId },
                                { "CrocVolumePlan", planVolum },
                                { "CrocWeekNumberId", weekGuid }
                            });
                        }
                        if (week4DaysGuid != Guid.Empty)
                        {
                            decimal planTC = deliverySchedules
                                .Where(x => x.GetTypedColumnValue<DateTime>("CrocDeliveryDate").Date == tempDate)
                                .Sum(x => x.GetTypedColumnValue<decimal>("CrocDeliveryTransportPlan"));
                            decimal planVolum = deliverySchedules
                                .Where(x => x.GetTypedColumnValue<DateTime>("CrocDeliveryDate").Date == tempDate)
                                .Sum(x => x.GetTypedColumnValue<decimal>("CrocDeliveryTonPlan"));
                            _entityHelper.InsertEntity("CrocDeliveryScheduleWeek", new Dictionary<string, object>
                            {
                                { "CrocQuantityTransport", planTC },
                                { "CrocSupplyPlanId", newPlanId },
                                { "CrocTransportTypeId", transportTypeId },
                                { "CrocVolumePlan", planVolum },
                                { "CrocWeekNumberId", week4DaysGuid },
                                { "CrocPlannedDate", tempDate}
                            });
                        }
                    }
                }
            }
        }

        private List<Entity> GetDeliverySchedules(Guid terminalId, Guid transportTypeId, DateTime beginDate, DateTime endDate)
        {
            List<Entity> deliverySchedules = _entityHelper.FindEntities("CrocDeliverySchedule", new Dictionary<string, object>
            {
                { "CrocChart.CrocWarehouseUnloading.Id", terminalId },
                { "CrocChart.CrocTransportType.Id", transportTypeId }
            }).ToList();
            if (deliverySchedules?.Count > 0)
            {
                deliverySchedules = deliverySchedules
                    .Where(x => x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") >= beginDate
                        && x.GetTypedColumnValue<DateTime>("CrocDeliveryDate") <= endDate)
                    .ToList();
            }
            return deliverySchedules;
        }

        private void UpdateDocumentRegister(Guid masterRecordId)
        {
            _log.Info($"After upload RegisterLoading on Chart {masterRecordId}");
            List<Entity> loadings = _entityHelper.FindEntities("CrocRegisterLoading", new Dictionary<string, object> { { "CrocChart.Id", masterRecordId } }).ToList();
            if (loadings?.Count > 0)
            {
                List<Entity> newLoadings = loadings
                    .Where(x => x.GetTypedColumnValue<Guid>("CrocGuid") == Guid.Empty).ToList();
                if(newLoadings?.Count > 0)
                {
                    List<DateTime> dates = newLoadings
                    .Select(x => x.GetTypedColumnValue<DateTime>("CrocShippingDate"))
                    .Distinct()
                    .ToList();
                    if (dates?.Count > 0)
                    {
                        foreach (DateTime date in dates)
                        {
                            bool isNewDoc = true;
                            Guid newRecId = Guid.Empty;
                            Guid externId = Guid.Empty;
                            List<Entity> existDocuments = _entityHelper.FindEntities("CrocDocumentRegistry", new Dictionary<string, object>
                            {
                                { "CrocChart.Id", masterRecordId },
                                { "CrocLoadingDate", date }
                            }).ToList();
                            if (existDocuments?.Count > 0) 
                            { 
                                newRecId = existDocuments.OrderByDescending(x => x.GetTypedColumnValue<DateTime>("CreatedOn")).Select(x => x.PrimaryColumnValue).FirstOrDefault();
                                externId = existDocuments.Where(x => x.PrimaryColumnValue == newRecId).Select(x => x.GetTypedColumnValue<Guid>("CrocGuid")).FirstOrDefault();
                                isNewDoc = false;
                            }
                            else
                            {
                                newRecId = _entityHelper.InsertEntity("CrocDocumentRegistry", new Dictionary<string, object>
                                {
                                    { "CrocChartId", masterRecordId },
                                    { "CrocLoadingDate", date },
                                    { "CrocRenew", false },
                                    { "CrocCreateByProvider", true},
                                    { "CrocToDelete", false},
                                    { "CrocSendDeleteMark", false }
                                });
                                isNewDoc = true;
                            }
                            
                            if (newRecId != Guid.Empty)
                            {
                                Dictionary<string, object> updateFields = new Dictionary<string, object>
                                { { "CrocSendDeleteMark", true } };
                                if (isNewDoc) { 
                                    externId = newRecId;
                                    updateFields.Add("CrocGuid", externId);
                                }
                                List<Guid> loadingIds = newLoadings
                                    .Where(x => x.GetTypedColumnValue<DateTime>("CrocShippingDate") == date)
                                    .Select(x => x.PrimaryColumnValue)
                                    .ToList();
                                if (loadingIds?.Count > 0)
                                {
                                    QueryParameter[] queryParameters = GetPametersGuidList(loadingIds);
                                    Update update = new Update(_userConnection, "CrocRegisterLoading")
                                        .Set("CrocGuid", Column.Parameter(externId))
                                        .Where("Id").In(queryParameters)
                                    as Update;
                                    update.Execute();
                                }
                                _entityHelper.UpdateEntity("CrocDocumentRegistry", newRecId, updateFields);
                            }
                        }
                    }
                }
                
            }
            else { _log.Info($"No RegisterLoading on Chart {masterRecordId}"); }
        }

        #region Public Methods

        public Guid? CreateNewDocumentOnExist(Guid sourceDocumentId, Guid masterRecordId, string masterColumn)
        {
            return CreateDocument(sourceDocumentId, masterRecordId, masterColumn);
        }

        public void UpdataDataOnPlanChange(Guid chartId, DateTime deliveryDate)
        {
            if (chartId != Guid.Empty && deliveryDate != DateTime.MinValue)
            {
                Entity chartEntity = _entityHelper.ReadEntity("CrocChart", chartId);
                Guid transportTypeId = chartEntity.GetTypedColumnValue<Guid>("CrocTransportTypeId");
                Guid warehouseUnloadingId = chartEntity.GetTypedColumnValue<Guid>("CrocWarehouseUnloadingId");
                Guid productId = chartEntity.GetTypedColumnValue<Guid>("CrocProductId");
                Guid exporterId = chartEntity.GetTypedColumnValue<Guid>("CrocExporterId");
                if (exporterId == Guid.Empty || productId == Guid.Empty || warehouseUnloadingId == Guid.Empty)
                {
                    throw new Exception("No control data");
                }
                List<Entity> supplyPlans = _entityHelper.FindEntities("CrocSupplyPlan", new Dictionary<string, object>
                    {
                        { "CrocExporter.Id", exporterId},
                        { "CrocProduct.Id", productId},
                        { "CrocTerminal.Id", warehouseUnloadingId}
                    }).ToList();
                if (supplyPlans?.Count > 0)
                {
                    supplyPlans = supplyPlans
                        .Where(x => (x.GetTypedColumnValue<DateTime>("CrocStart") <= deliveryDate)
                            && (x.GetTypedColumnValue<DateTime>("CrocEnd") >= deliveryDate))
                        .ToList();
                    if (supplyPlans?.Count > 0)
                    {
                        supplyPlans.ForEach(supplay => UpdateSupplayPlan(supplay, transportTypeId));
                    }
                    else { CreateNewSupplyPlan(chartEntity, deliveryDate); }
                }
                else { CreateNewSupplyPlan(chartEntity, deliveryDate); }
            }
        }

        public Guid GetWeekIdOnNumber(int weekNum)
        {
            Guid result = Guid.Empty;
            List<Entity> weeks = _entityHelper.FindEntities("CrocWeekNumbers", new Dictionary<string, object>
            { { "Name", weekNum.ToString() } }).ToList();
            if (weeks?.Count > 0)
            {
                result = weeks.Select(x => x.PrimaryColumnValue).FirstOrDefault();
            }
            return result;
        }

        public void GetBorderOfTheWeek(DateTime controlDate, string monthy, out DateTime beginDay, out DateTime endDay)
        {
            DateTime beginMonth = DateTime.Parse($"{controlDate.Year}-{monthy}-01").Date;
            DateTime endMonth = DateTime.Parse($"{controlDate.Year}-{monthy}-{DateTime.DaysInMonth(controlDate.Year, controlDate.Month)}")
                .Date
                .AddDays(1)
                .AddMilliseconds(-1);
            CultureInfo infoCult = new CultureInfo("ru-RU");
            DayOfWeek dow = infoCult.Calendar.GetDayOfWeek(controlDate);
            int normDOW = dow == 0 ? 7 : (int)dow;
            beginDay = controlDate.AddDays(1-normDOW);
            if (beginDay < beginMonth) { beginDay = beginMonth; }
            endDay = controlDate.AddDays(8 - normDOW).AddMilliseconds(-1);
            if (endDay > endMonth) { endDay = endMonth; }
        }

        public BaseResponse UpdateDocumentRegisterLoading(Guid masterRecordId)
        {
            BaseResponse response = new BaseResponse { Success = false, ErrorInfo = new ErrorInfo { Message = string.Empty } };
            try
            {
                UpdateDocumentRegister(masterRecordId);
                response.Success = true;
            }
            catch(Exception ex)
            {
                _log.Error(ex.Message +"\n"+ ex.StackTrace);
                response.ErrorInfo.Message = ex.Message;
            }
            return response;
        }

        public void SendRemindMessageToUser(string caption, Guid schemaId)
        {
            SysUserInfo currentUser = _userConnection.CurrentUser;
            Guid contactId = currentUser.ContactId;
            DateTime dateTime = currentUser.GetCurrentDateTime();
            _entityHelper.InsertEntity("Reminding", new Dictionary<string, object>
            {
                { "AuthorId", contactId },
                { "ContactId", contactId },
                { "SourceId", ConstCs.RemindingConsts.RemindingSourceAuthorId },
                { "RemindTime", dateTime },
                { "SubjectCaption", caption },
                { "SysEntitySchemaId", schemaId }
            });
        }

        public void SendMessageToClient(string schemaName, Guid masterId)
        {
            
            string header = $"{schemaName}_{masterId}";
            Guid userId = _userConnection.CurrentUser.Id;
            var channelManager = MsgChannelManager.Instance;
            IMsgChannel channel = channelManager.FindItemByUId(userId);
            if (channel == null) { return; }
            var simpleMessage = new SimpleMessage
            {
                Body = JsonConvert.SerializeObject(new { LoadStatus = true }),
                Id = channel.Id
            };
            simpleMessage.Header.Sender = header;
            channel.PostMessage(simpleMessage);
            string messageText = "{\"masterId\": \"" + masterId.ToString() + "\"}";
            string sender = $"Update{schemaName}";
            CrocMsgChannelUtilities.PostMessageToAll(sender, messageText);
        }

        public string GetSchemaCaption(string schemaName)
        {
            EntitySchema entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(schemaName);
            return entitySchema.Caption.Value;
        }

        public QueryParameter[] GetPametersGuidList(List<Guid> list)
        {
            QueryParameter[] param = new QueryParameter[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                param[i] = new QueryParameter(list.ElementAt(i));
            }
            return param;
        }

        #endregion
    }
}
