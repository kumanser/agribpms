﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using Terrasoft.Web.Common;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.CrocDemetra;
using System.Threading.Tasks;
using Terrasoft.Core;

namespace Terrasoft.Configuration
{

	[ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CrocToolsService : BaseService
    {

		[OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public string TestImportParameters(Guid recordId)
        {
			CrocExcelParameterHelper helper = new CrocExcelParameterHelper(UserConnection);
			return helper.TestImportParameters(recordId);
		}

		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
		public void TestTemplateImportParameters(Guid recordId)
		{

			CrocExcelParameterHelper helper = new CrocExcelParameterHelper(UserConnection);
			helper.TestTemplateImportParameters(recordId);
		}

		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
		public Guid CreateNewImportParameters(string config)
		{
			CrocExcelParameterHelper helper = new CrocExcelParameterHelper(UserConnection);
			return helper.AddNewExcelImport(config);
		}

		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
		public void RunImport(Guid sessionId, string schemaName, string detailColumn, Guid detailValue, bool clearOldRows)
		{
			UserConnection userConnection = UserConnection;
			Task.Run(() =>
			{
				CrocExcelParameterHelper helper = new CrocExcelParameterHelper(userConnection);
				helper.RunImport(sessionId, schemaName, detailColumn, detailValue, clearOldRows);
			});
			
		}

		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
		public Guid? CreateNewDocumentOnExist(Guid sourceDocumentId, Guid masterRecordId, string masterColumn)
		{
			CommonHelper helper = new CommonHelper(UserConnection);
			return helper.CreateNewDocumentOnExist(sourceDocumentId, masterRecordId, masterColumn);
		}

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse UpdateDocumentRegister(Guid recordId)
        {
            CommonHelper helper = new CommonHelper(UserConnection.AppConnection.SystemUserConnection);
            return helper.UpdateDocumentRegisterLoading(recordId);
        }
    }

}

