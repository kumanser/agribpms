﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Wordprocessing;
using global::Common.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Terrasoft.Common;
using Terrasoft.Configuration.Base;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Factories;
using Terrasoft.Web.Common;
using Terrasoft.Web.Http.Abstractions;

namespace Terrasoft.Configuration.S3
{

	public class CrocWebResponseProcessor
	{

		/// <summary>
		/// Assigns file stream response.
		/// </summary>
		/// <param name="httpContext"><see cref="HttpContext"/>.</param>
		/// <param name="contentType">Response content type.</param>
		/// <param name="contentLength">Response content length.</param>
		/// <param name="contentDisposition">Response content disposition.</param>
		public virtual void AssignFileResponseContent(HttpContext httpContext, string contentType, long contentLength, string contentDisposition)
		{
#if !NETSTANDARD2_0
			WebOperationContext.Current.OutgoingResponse.ContentType = contentType;
			WebOperationContext.Current.OutgoingResponse.ContentLength = contentLength;
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition", contentDisposition);
#else
			HttpResponse response = httpContext.Response;
			response.ContentType = contentType;
			response.Headers["Content-Length"] = contentLength.ToString(CultureInfo.InvariantCulture);
			response.Headers["Content-Disposition"] = contentDisposition;
#endif
		}

	}

	[DataContract]
	public class FileImageData
	{
		public FileImageData(Guid id, string base64)
		{
			Id = id;
			Base64 = base64;
		}

		[DataMember]
		public Guid Id { get; set; }
		[DataMember]
		public string Base64 { get; set; }
	}

	[Serializable]
	[DataContract]
	public class FileRedisData
	{
		public FileRedisData(string name, string link)
		{
			Name = name;
			Link = link;
		}

		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string Link { get; set; }
	}

	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class FilesS3Service : BaseService
	{
		ILog _log = LogManager.GetLogger("FilesS3Service");

		[OperationContract]
		[WebInvoke(Method = "POST", UriTemplate = "ImportFile", ResponseFormat = WebMessageFormat.Json)]
		public string ImportFile(Stream fileContent)
		{
			string fileLink = String.Empty;
			try
			{
				var httpContext = HttpContextAccessor.GetInstance();
				var request = httpContext.Request;
				var queryString = request.QueryString;
				string entitySchemaName = queryString["entitySchemaName"];
				string parentColumnValue = queryString["parentColumnValue"];
				string fileId = queryString["fileId"];
				string range = request.Headers["Range"];
				string contentRange = request.Headers["Content-Range"];
				string key = $"{entitySchemaName}_{parentColumnValue}_{fileId}";
				FilesS3Helper helper = new FilesS3Helper(UserConnection, range, contentRange);
				string fileName = (!helper.IsChunkedUpload()) ? request.Files["files"].FileName : queryString["fileName"];
				if (!helper.IsChunkedUpload() || helper.IsFirstChunk())
				{
					MemoryStream stream = helper.GetStream(fileContent, request.TotalBytes);
					helper.SaveStream(key, fileName, stream);
				}
				else
				{
					MemoryStream stream = helper.GetStream(fileContent, request.TotalBytes);
					helper.AppendStream(key, stream);
				}
			}
			catch (Exception ex)
			{
				_log.Error($"ImportFile: Error:{ex.Message}, StackTrace:{ex.StackTrace}");
			}
			return fileLink;
		}

		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
			ResponseFormat = WebMessageFormat.Json)]
		public string SaveFile(FileS3InfoData data)
		{
			FilesS3Helper helper = new FilesS3Helper(UserConnection);
			return (data.Version?.ParentColumnValue != null) ? helper.AddFileVersion(data) : helper.SaveFile(data);
		}

		[OperationContract]
		[WebGet(UriTemplate = "GetS3File/{schemaName}/{fileId}/{dataColumnName}/{s3ColumnName}")]
		public void GetS3File(string schemaName, string fileId, string dataColumnName, string s3ColumnName)
		{
			FilesS3Helper helper = new FilesS3Helper(UserConnection);
			helper.GetS3File(schemaName, new Guid(fileId), dataColumnName, s3ColumnName);
		}

		public class FilesS3Helper
		{
			private readonly IHttpContextAccessor _httpContextAccessor;
			private HttpContext CurrentContext => _httpContextAccessor.GetInstance();
			private CrocWebResponseProcessor WebResponseProcessor => ClassFactory.Get<CrocWebResponseProcessor>();
			private readonly UserConnection _userConnection;
			private CrocEntityHelper _entityHelper;
			private AmazonS3Client _s3Client;
			private readonly string _S3_Url;
			private readonly string _S3_AccessKeyID;
			private readonly string _S3_SecretKey;
			private readonly string _S3_Bucket;
			private readonly string _S3_DefPath;
			private readonly int _S3_Timeout;
			private readonly string _headerRange;
			private readonly string _headerContentRange;
			private Regex _firstChunkRegex = new Regex(@"^\D*0");
			private readonly List<string> _imageFiles = new List<string>() { "jpg", "png", "gif", "bmp", "jpeg" };
			ILog _log = LogManager.GetLogger("FilesS3Service");

			public FilesS3Helper(UserConnection userConnection)
			{
				_userConnection = userConnection;
				_entityHelper = new CrocEntityHelper(_userConnection);
				_httpContextAccessor = HttpContext.HttpContextAccessor;
				_S3_Url = (string)Terrasoft.Core.Configuration.SysSettings.GetDefValue(_userConnection, "CrocS3Url");
				_S3_AccessKeyID = (string)Terrasoft.Core.Configuration.SysSettings.GetDefValue(_userConnection, "CrocS3AccessKeyID");
				_S3_SecretKey = (string)Terrasoft.Core.Configuration.SysSettings.GetDefValue(_userConnection, "CrocS3SecretKey");
				_S3_Bucket = (string)Terrasoft.Core.Configuration.SysSettings.GetDefValue(_userConnection, "CrocS3Bucket");
				_S3_DefPath = (string)Terrasoft.Core.Configuration.SysSettings.GetDefValue(_userConnection, "CrocS3DefaultPath");
				_S3_Timeout = 5000;
				_s3Client = CreateAmazonS3Client();
			}

			public FilesS3Helper(UserConnection userConnection, string headerRange, string headerContentRange) : this(userConnection)
			{
				_headerRange = headerRange;
				_headerContentRange = headerContentRange;
			}

			private AmazonS3Client CreateAmazonS3Client()
			{
				try
				{
					return new AmazonS3Client(
						_S3_AccessKeyID,
						_S3_SecretKey,
						new AmazonS3Config()
						{
							ServiceURL = _S3_Url,
							Timeout = TimeSpan.FromMilliseconds(_S3_Timeout),
							ReadWriteTimeout = TimeSpan.FromMilliseconds(_S3_Timeout),
							MaxErrorRetry = 1
						}
					);
				}
				catch
				{
					return null;
				}
			}

			private GetObjectResponse GetS3Object(string bucket, string link)
			{
				Task<GetObjectResponse> task = _s3Client.GetObjectAsync(bucket, link);
				task.Wait();
				return task.Result;
			}

			private byte[] DownloadFromS3(string bucket, string link)
			{
				try
				{
					GetObjectResponse s3object = GetS3Object(bucket, link);
					MemoryStream memoryStream = new MemoryStream();
					using (Stream responseStream = s3object.ResponseStream)
					{
						responseStream.CopyTo(memoryStream);
					}
					return memoryStream.ToArray();
				}
				catch
				{
					return null;
				}
			}

			private string ByteRange()
			{
				string value = _headerRange;
				if (string.IsNullOrEmpty(value))
				{
					value = _headerContentRange;
				}
				return value;
			}

			private byte[] Combine(byte[] first, byte[] second)
			{
				byte[] bytes = new byte[first.Length + second.Length];
				Buffer.BlockCopy(first, 0, bytes, 0, first.Length);
				Buffer.BlockCopy(second, 0, bytes, first.Length, second.Length);
				return bytes;
			}

			private CrocFileInfo GetFileFromDB(string schemaName, Guid fileId, string dataColumnName, string s3ColumnName)
			{
				var entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(schemaName);
				Entity fileEntity = _entityHelper.FindEntity(schemaName, "Id", fileId, new string[] {
					entitySchema.GetPrimaryDisplayColumnName(), s3ColumnName, dataColumnName
				});
				return new CrocFileInfo()
				{
					Name = fileEntity.PrimaryDisplayColumnValue,
					Link = fileEntity.GetTypedColumnValue<string>(s3ColumnName),
					Data = fileEntity.GetBytesValue(dataColumnName)
				};
			}

			private string RemoveSpecialCharacters(string fileName)
			{
				return Regex.Replace(fileName, @"[^a-zA-Z\p{IsCyrillic}0-9_.,^&@£$€!½§~'=()\[\]{} «»<>~#*%+-]+",
					"_", RegexOptions.Compiled);
			}

			private int GetLastFileVersion(Guid fileId)
			{
				Entity entity = _entityHelper.FindEntities("CrocDocumentVersion", new Dictionary<string, object>() {
					{ "CrocDocumentFile", fileId }
				}, 10000, new string[] { "CrocVersion" })
				.OrderByDescending(o => o.GetTypedColumnValue<int>("CrocVersion")).FirstOrDefault();
				return entity != default(Entity) ? entity.GetTypedColumnValue<int>("CrocVersion") : 0;
			}

			protected virtual string GetResponseContentDisposition(string fileName)
			{
				string processedFileName;
				HttpRequest request = CurrentContext.Request;
				string userAgent = (request.UserAgent ?? String.Empty).ToLowerInvariant();
				if (userAgent.Contains("android"))
				{
					processedFileName = $"filename=\"{RemoveSpecialCharacters(fileName)}\"";
				}
				else if (userAgent.Contains("safari"))
				{
					processedFileName = $"filename*=UTF-8''{System.Web.HttpUtility.UrlPathEncode(fileName)}";
				}
				else
				{
					processedFileName = $"filename=\"{fileName}\"; filename*=UTF-8''{System.Web.HttpUtility.UrlPathEncode(fileName)}";
				}
				return $"attachment; {processedFileName}";
			}

			public bool IsSuccessConnected()
			{
				return _s3Client != null;
			}

			public (string bucket, string shortPath) GetShortS3Link(string fullPath)
			{
				string shortLink = String.Empty;
				List<string> list = fullPath.Split('/').ToList();
				return (list.Count > 4) ? (list[3], String.Join("/", list.Skip(4))) : (list[3], shortLink);
			}

			public CrocFileInfo GetFileFromLink(string schemaName, Guid fileId, string dataColumnName, string s3ColumnName)
			{
				CrocFileInfo fileInfo = GetFileFromDB(schemaName, fileId, dataColumnName, s3ColumnName);
				string fileName = fileInfo.Name;
				string fullFileLink = fileInfo.Link;
				byte[] fileBytes = new byte[0];
				if (!string.IsNullOrWhiteSpace(fullFileLink))
				{
					(string bucket, string shortFileLink) fileLink = GetShortS3Link(System.Web.HttpUtility.UrlDecode(fullFileLink));
					fileBytes = (_s3Client != null) ? DownloadFromS3(fileLink.bucket, fileLink.shortFileLink) : fileInfo.Data;
				}
				else
				{
					CrocFileInfo crocFileInfo = GetFileFromDB(schemaName, fileId, dataColumnName, s3ColumnName);
					fileBytes = crocFileInfo.Data;
				}
				if (fileBytes != null)
				{
					return new CrocFileInfo()
					{
						Name = fileName,
						Data = fileBytes
					};
				}
				else return null;
			}

			public string SaveToS3(string path, CrocFileInfo fileInfo)
			{
				try
				{
					string fileName = $"{Path.GetFileNameWithoutExtension(fileInfo.Name)}_{DateTime.Now.ToString("yyyyMMdd_HHmmss")}{Path.GetExtension(fileInfo.Name)}";
					fileName = Regex.Replace(fileName, @"[^\w\.@-]", "", RegexOptions.None);
					//string filePath = String.Join("/", _S3_Path, parentColumnName, parentColumnValue, fileName);
					string filePath = String.Join("/", _S3_DefPath, path, fileName);
					Stream fileStream = new MemoryStream(fileInfo.Data);
					var fileTransferUtility = new TransferUtility(_s3Client);
					fileTransferUtility.Upload(fileStream, _S3_Bucket, filePath);
					return filePath;
				}
				catch (AmazonServiceException ex)
				{
					_log.Error($"SaveFile: Ошибка сохранения файла {fileInfo.Name}, Error: {ex.Message}, StackTrace: {ex.StackTrace}");
					return string.Empty;
				}
			}

			public void GetStreamFromFile(CrocFileInfo fileInfo)
			{
				using (var memoryStream = new MemoryStream(fileInfo.Data))
				{
					CurrentContext.Response.Headers["Content-Length"] = fileInfo.Data.Length.ToString(CultureInfo.InvariantCulture);
					string contentDisposition = GetResponseContentDisposition(fileInfo.Name);
					CurrentContext.Response.AddHeader("Content-Disposition", contentDisposition);
					MimeTypeResult mimeTypeResult = MimeTypeDetector.GetMimeType(fileInfo.Name);
					CurrentContext.Response.ContentType = mimeTypeResult.HasError
						? "application/octet-stream"
						: mimeTypeResult.Type;
					memoryStream.Flush();
					memoryStream.WriteTo(CurrentContext.Response.OutputStream);
				}
			}

			public bool IsFirstChunk()
			{
				return _firstChunkRegex.IsMatch(ByteRange());
			}

			public MemoryStream GetStream(Stream fileContent, long totalBytes)
			{
				byte[] fileContentBytes = new byte[totalBytes];
				fileContent.Read(fileContentBytes, 0, fileContentBytes.Length);
				return new MemoryStream(fileContentBytes);
			}

			public bool IsChunkedUpload()
			{
				return !string.IsNullOrEmpty(ByteRange());
			}

			public void SaveStream(string key, string fileName, MemoryStream stream)
			{
				CrocFileInfo fileInfo = new CrocFileInfo()
				{
					Name = fileName,
					Data = stream.ToArray()
				};
				_userConnection.SessionData[key] = fileInfo;
			}

			public void AppendStream(string key, MemoryStream stream)
			{
				CrocFileInfo fileInfo = (CrocFileInfo)_userConnection.SessionData[key];
				_userConnection.SessionData[key] = new CrocFileInfo()
				{
					Name = fileInfo.Name,
					Data = Combine(fileInfo.Data, stream.ToArray())
				};
			}

			public void GetS3File(string schemaName, Guid fileId, string dataColumnName, string s3ColumnName)
			{
				CrocFileInfo fileInfo = GetFileFromLink(schemaName, fileId, dataColumnName, s3ColumnName);
				if (fileInfo != null) GetStreamFromFile(fileInfo);
			}

			public void DeleteS3Object(string link)
			{
				try
				{
					DeleteObjectResponse response = _s3Client.DeleteObject(new DeleteObjectRequest() { BucketName = _S3_Bucket, Key = link });
				}
				catch (Exception ex)
				{
					_log.Error($"Ошибка удаления файла по ссылке {link}. Error: {ex.Message}, StackTrace: {ex.StackTrace}");
				}
			}

			public string SaveFile(FileS3InfoData data)
			{
				CrocFileInfo fileInfo = null;
				if (data.File.FileInfo != null)
				{
					fileInfo = data.File.FileInfo;
				}
				else
				{
					string key = $"{data.File.EntitySchemaName}_{data.File.ParentColumnValue}_{data.File.Id}";
					fileInfo = (CrocFileInfo)_userConnection.SessionData[key];
					fileInfo.Id = data.File.Id;
					_userConnection.SessionData.Remove(key);
				}
				string success = String.Empty;
				try
				{
					string link = (_s3Client != null) ? SaveToS3(data.Path, fileInfo) : String.Empty;
					Dictionary<string, object> dic = new Dictionary<string, object>() {
						{ "Id", fileInfo.Id },
						{ $"{data.File.ParentColumnName}Id",  data.File.ParentColumnValue },
						{ "Name", fileInfo.Name },
						{ "TypeId",  new Guid("529BC2F8-0EE0-DF11-971B-001D60E938C6") },
						{ "Size",  fileInfo.Data.Length },
						{ "Version",  1 }
					};
					if (data.File.Columns != null)
					{
						Dictionary<string, Guid> columns = JsonConvert.DeserializeObject<Dictionary<string, Guid>>(data.File.Columns);
						columns.ForEach(x => dic.Add(x.Key, x.Value));
					}
					if (!string.IsNullOrWhiteSpace(link)) dic.Add(data.File.S3ColumnName, $"{_S3_Url}/{_S3_Bucket}/{link}");
					else dic.Add(data.File.ColumnName, fileInfo.Data);
					Guid recordId = _entityHelper.InsertEntity(data.File.EntitySchemaName, dic);
					if (data.Version != null)
					{
						Dictionary<string, object> dicVersion = new Dictionary<string, object>() {
							{ data.Version.ParentColumnName + "Id", recordId },
							{ data.Version.DisplayColumnName, fileInfo.Name },
							{ data.Version.VersionColumnName, 1  }
						};
						if (!string.IsNullOrWhiteSpace(link)) dicVersion.Add(data.Version.S3ColumnName, $"{_S3_Url}/{_S3_Bucket}/{link}");
						else dicVersion.Add(data.Version.ColumnName, fileInfo.Data);
						_entityHelper.InsertEntity(data.Version.EntitySchemaName, dicVersion);
					}
				}
				catch (Exception ex)
				{
					success = $"Ошибка сохранения файла: {ex.Message}";
					_log.Error($"SaveFile: entitySchemaName:{data.File.EntitySchemaName}, parentColumnName:{data.File.ParentColumnName}, parentColumnValue:{data.File.ParentColumnValue}, fileId: {fileInfo.Id}. Error: {ex.Message}, StackTrace: {ex.StackTrace}");
				}
				return success;
			}

			public string AddFileVersion(FileS3InfoData data)
			{
				string result = String.Empty;
				string key = $"{data.File.EntitySchemaName}_{data.File.ParentColumnValue}_{data.File.Id}";
				CrocFileInfo fileInfo = (CrocFileInfo)_userConnection.SessionData[key];
				fileInfo.Id = data.File.Id;
				_userConnection.SessionData.Remove(key);
				try
				{
					CrocFileInfo parentFileInfo = GetFileFromDB(data.Version.ParentEntitySchemaName, data.Version.ParentColumnValue.Value, data.File.ColumnName, data.File.S3ColumnName);
					int version = GetLastFileVersion(data.Version.ParentColumnValue.Value);
					if (Path.GetExtension(parentFileInfo.Name) == Path.GetExtension(data.File.Name))
					{
						string link = (_s3Client != null) ? SaveToS3(data.Path, fileInfo) : String.Empty;
						Dictionary<string, object> dic = new Dictionary<string, object>() {
							{ data.Version.ParentColumnName + "Id", data.Version.ParentColumnValue },
							{ data.Version.DisplayColumnName, parentFileInfo.Name },
							{ data.Version.VersionColumnName, version + 1  }
						};
						if (!string.IsNullOrWhiteSpace(link)) dic.Add(data.Version.S3ColumnName, $"{_S3_Url}/{_S3_Bucket}/{link}");
						else dic.Add(data.Version.ColumnName, fileInfo.Data);
						_entityHelper.InsertEntity(data.Version.EntitySchemaName, dic);
						Dictionary<string, object> dicParent = new Dictionary<string, object>();
						if (!string.IsNullOrWhiteSpace(link)) dicParent.Add(data.File.S3ColumnName, $"{_S3_Url}/{_S3_Bucket}/{link}");
						else dicParent.Add(data.File.ColumnName, fileInfo.Data);
						_entityHelper.UpdateEntity(data.File.EntitySchemaName, data.Version.ParentColumnValue.Value, dicParent);
					}
					else result = $"Расширение версии файла ({Path.GetExtension(parentFileInfo.Name)}) не совпадает с расширением файла ({Path.GetExtension(data.File.Name)})";
				}
				catch (Exception ex)
				{
					result = $"Ошибка сохранения файла: {ex.Message}";
					_log.Error($"SaveFile: entitySchemaName:{data.File.EntitySchemaName}, parentColumnName:{data.File.ParentColumnName}, parentColumnValue:{data.Version.ParentColumnValue}, fileId: {fileInfo.Id}. Error: {ex.Message}, StackTrace: {ex.StackTrace}");
				}
				return result;
			}

		}
	}

}