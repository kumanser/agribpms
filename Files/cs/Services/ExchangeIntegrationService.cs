﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Threading.Tasks;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration.OneCIntegration;
using Terrasoft.Core;
using Terrasoft.Web.Common;

namespace Terrasoft.Configuration
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ExchangeIntegrationService : BaseService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendPlanRefund(Guid refundId) //+
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendPlanRefund(refundId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendEmptyVehicles(Guid recordId, string masterColumnName)
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendEmptyVehicles(recordId, masterColumnName);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public void SendEmptyVehiclesA(Guid recordId, string masterColumnName)
        {
            UserConnection userConnection = UserConnection;
            Task.Run(() =>
            {
                OneCIntegrationHelper helper = new OneCIntegrationHelper(userConnection);
                helper.SendEmptyVehicles(recordId, masterColumnName);
            });
            
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendGU12(Guid recordId) //+
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendGU12(recordId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public void SendGU12A(Guid recordId)
        {
            UserConnection userConnection = UserConnection;
            Task.Run(() =>
            {
                OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
                helper.SendGU12(recordId);
            });
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendRegisterLoading(Guid recordId) //+
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendRegisterLoading(recordId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public void SendRegisterLoadingA(Guid recordId) //+
        {
            UserConnection userConnection = UserConnection;
            Task.Run(() =>
            {
                OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
                helper.SendRegisterLoading(recordId);
            });
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendLoadingPlan(Guid recordId) //+
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendShipmentPlan(recordId, 1);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendDeliveryPlan(Guid recordId) //+
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendShipmentPlan(recordId, 2);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendRequestQuota(Guid recordId)
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendRequestQuota(recordId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendRefusalQuota(Guid recordId)
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendRefusalQuota(recordId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendAccreditation(Guid recordId)
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendAccreditation(recordId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendShipmentDocument(Guid recordId)
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendShipmentDocument(recordId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendServiceRequest(Guid recordId)
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendServiceRequest(recordId);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendSmartSeedsCalculationRequest(string config)
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendSmartSeedsCalculationRequest(config);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public BaseResponse SendSmartSeedsChartRequest(string config)
        {
            OneCIntegrationHelper helper = new OneCIntegrationHelper(UserConnection);
            return helper.SendSmartSeedsChartRequest(config);
        }
    }
}
