﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using Terrasoft.Configuration.Base;
using Terrasoft.Web.Common;
using Terrasoft.Common;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Process;

namespace Terrasoft.Configuration
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CrocCommentService : BaseService
    {
        private void SetStatusFixWaiting(Guid commentId)
        {
            CrocEntityHelper entityHelper = new CrocEntityHelper(UserConnection);
            entityHelper.UpdateEntity("CrocComment", commentId, new Dictionary<string, object>()
            {
                { "CrocStatusId", ConstCs.CrocCommentStatus.FixWaiting }
            });
        }

        private void SetStatusFixAccepted(Guid commentId)
        {
            CrocEntityHelper entityHelper = new CrocEntityHelper(UserConnection);
            entityHelper.UpdateEntity("CrocComment", commentId, new Dictionary<string, object>()
            {
                { "CrocStatusId", ConstCs.CrocCommentStatus.FixAccepted }
            });
        }


        private void SetStatusFixed(Guid commentId)
        {
            CrocEntityHelper entityHelper = new CrocEntityHelper(UserConnection);
            entityHelper.UpdateEntity("CrocComment", commentId, new Dictionary<string, object>()
            {
                { "CrocStatusId", ConstCs.CrocCommentStatus.Fixed }
            });
        }

        private void UnsetCommentExists(Guid commentId) 
        {
            CrocEntityHelper entityHelper = new CrocEntityHelper(UserConnection);
            Entity entity = entityHelper.ReadEntity("CrocComment", commentId);
            Guid documentSetId;
            bool isComment = false;
            if (entity != null && entity.GetTypedColumnValue<Guid>("CrocSetDocumentId") != Guid.Empty)
            {
                documentSetId = entity.GetTypedColumnValue<Guid>("CrocSetDocumentId");
                entityHelper.UpdateEntity("CrocSetDocument", documentSetId, new Dictionary<string, object>()
                {
                    { "CrocIsAvailabilityComment", isComment}               
                });
            }
        }

        private void RunNotificationProcess(Guid commentId) 
        {
            string processName = "CrocProcessCommentFixedNotification";
            var param = new Dictionary<string, string>();
            param["CommentId"] = commentId.ToString();
            IProcessExecutor processExecutor = UserConnection.ProcessEngine.ProcessExecutor;
            processExecutor.Execute(processName, param);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public void ReturnToRevision(string commentsStr)
        {
            string[] commentStr = commentsStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            commentStr.ForEach(idStr =>
            {
                Guid commentId = new Guid(idStr);
                SetStatusFixWaiting(commentId);
            });
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public void AcceptCorrection(string commentsStr) 
        {
            string[] commentStr = commentsStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            commentStr.ForEach(idStr =>
            {
                Guid commentId = new Guid(idStr);
                SetStatusFixAccepted(commentId);
                UnsetCommentExists(commentId);
            });
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public void CommentFixed(string commentsStr)
        {
            string[] commentStr = commentsStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            commentStr.ForEach(idStr => 
            {
                Guid commentId = new Guid(idStr);
                SetStatusFixed(commentId);
                RunNotificationProcess(commentId);
            });
        }
    }
}