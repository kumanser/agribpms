﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using Terrasoft.Configuration.Base;
using Terrasoft.Web.Common;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using System.IO;
using Common.Logging;
using Terrasoft.Configuration.S3;

namespace Terrasoft.Configuration
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    class CrocDocumentGU12FilesService: BaseService
    {
        ILog _log = LogManager.GetLogger("CrocDocumentGU12FilesService");

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ImportFile", ResponseFormat = WebMessageFormat.Json)]

        public Guid ImportFile() 
        {
            Guid newDocumentId = Guid.Empty;
            try
            {
                var httpContext = HttpContextAccessor.GetInstance();
                var request = httpContext.Request;
                var form = request.Form;
                Guid chartId = new Guid(form["chartId"]);
                string fileName = request.Files["files"].FileName;
                Stream fileContent = request.Files["files"].InputStream;
                fileContent.Position = 0;
                int FileSize = (int)fileContent.Length;
                byte[] fileByteData = new byte[fileContent.Length];
                fileContent.Read(fileByteData, 0, fileByteData.Length);
                CrocDocumentGU12FilesHelper docHelper = new CrocDocumentGU12FilesHelper(UserConnection);
                newDocumentId = docHelper.CreateNewGU12Document(chartId);
                FileS3InfoData fileData = new FileS3InfoData() 
                {
                    File = new FileS3Data() 
                    {
                        Id = newDocumentId,
                        EntitySchemaName = "CrocDocumentFile",
                        ParentColumnName = "CrocDocument",
                        ParentColumnValue = newDocumentId,
                        ColumnName = "Data",
                        S3ColumnName = "CrocS3Link",
                        Columns = "{\"CrocFileTypeId\": \"358ff086-0a6e-4c2b-a457-7a902c4cc49d\"}", //Заявка ГУ-12
                        FileInfo = new CrocFileInfo()
                        {
                            Name = fileName,
                            Data = fileByteData
                        }
                    },
                    Version = new VersionS3Data()
                    {
                        DisplayColumnName = "CrocName",
                        VersionColumnName = "CrocVersion",
                        ParentEntitySchemaName = "CrocDocumentFile",
                        ParentColumnName = "CrocDocumentFile",
                        ParentColumnValue = newDocumentId,
                        S3ColumnName = "CrocS3Link",
                        ColumnName = "CrocData",
                        EntitySchemaName = "CrocDocumentVersion"
                    }
                };
                FilesS3Service.FilesS3Helper S3helper = new FilesS3Service.FilesS3Helper(UserConnection);
                S3helper.SaveFile(fileData);
                docHelper.CreateSetDocument(chartId, newDocumentId);
            }
            catch (Exception ex) 
            {
                _log.Error($"ImportFile: Error:{ex.Message}, StackTrace:{ex.StackTrace}");
            }
            return newDocumentId;
        }
    }

    public class CrocDocumentGU12FilesHelper
    {
        private readonly UserConnection _userConnection;
        private CrocEntityHelper _entityHelper;

        public CrocDocumentGU12FilesHelper(UserConnection userConnection)
        {
            _userConnection = userConnection;
            _entityHelper = new CrocEntityHelper(_userConnection);
        }

        public Guid CreateNewGU12Document(Guid chartId)
        {
            var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, "CrocChart");
            string accName = esq.AddColumn("CrocContract.CrocAccount.Id").Name;
            string portelevatorName = esq.AddColumn("CrocContract.CrocPortElevator.Id").Name;
            string terminalName = esq.AddColumn("CrocContract.CrocTerminal.Id").Name;
            string numberName = esq.AddColumn("CrocNumberGU12").Name;
            var chart = esq.GetEntity(_userConnection, chartId);
            string GU12Number = chart.GetTypedColumnValue<string>(numberName);
            Guid portElevator = chart.GetTypedColumnValue<Guid>(portelevatorName);
            Guid terminal = chart.GetTypedColumnValue<Guid>(terminalName);
            Guid account = chart.GetTypedColumnValue<Guid>(accName);

            Dictionary<string, object> basedic = new Dictionary<string, object>() 
            {
                { "CrocGU12Number", GU12Number},
                { "CrocAccountId", account},
                { "CrocChartId", chartId},
                { "CrocDocumentTypeId", ConstCs.CrocDocument.Type.GU12},
                { "CrocPreparationDate", DateTime.Now},
                { "CrocDocumentStatusId", ConstCs.CrocDocument.Status.PreparationId}
            };
            if (terminal != Guid.Empty)
            {
                basedic.Add("CrocTerminalId", terminal);
            }
            if (portElevator != Guid.Empty)
            {
                basedic.Add("CrocPortId", portElevator);
            }
            Guid newDocId = _entityHelper.InsertEntity("CrocDocument", basedic);
            return newDocId;
        }

        public void CreateSetDocument(Guid chartId, Guid sourceDocumentId)
        {
            Entity chart = _entityHelper.ReadEntity("CrocChart", chartId);
            if (chart != null)
            {
                Guid additionalContract = chart.GetTypedColumnValue<Guid>("CrocContractId");
                if (additionalContract != Guid.Empty)
                {
                    _entityHelper.InsertEntity("CrocSetDocument", new Dictionary<string, object>()
                    {
                        { "CrocContractId", additionalContract},
                        { "CrocDocumentId", sourceDocumentId},
                        { "CrocIsLoaded", true}
                    });
                }
            }
        }
    }
}
