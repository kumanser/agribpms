﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using Terrasoft.Configuration.Base;
using Terrasoft.Web.Common;
using Terrasoft.Common;

namespace Terrasoft.Configuration
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CrocRequestService : BaseService
    {
        private void UnsetIsCheckAct(Guid requestId)
        {
            CrocEntityHelper entityHelper = new CrocEntityHelper(UserConnection);
            entityHelper.UpdateEntity("CrocRequest", requestId, new Dictionary<string, object>() {
                { "CrocIsCheckAct",  false},
            });
        }

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        public void UnsetRequestIsCheckAct(string requestsStr)
        {
            string[] requestsArr = requestsStr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            requestsArr.ForEach(requestStr =>
            {
                Guid requestId = new Guid(requestStr);
                UnsetIsCheckAct(requestId);
            });
        }
    }
}