﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using NPOI.POIFS.Storage;
using Terrasoft.Common;
using Terrasoft.Common.Json;
using Terrasoft.Configuration.Base;
using Terrasoft.Configuration;
using Terrasoft.Configuration.S3;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Factories;
using Terrasoft.Nui.ServiceModel.DataContract;
using Terrasoft.Reports;
using Terrasoft.Web.Common;

namespace Terrasoft.Configuration
{
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class CrocExcelGeneratorService : BaseService
	{
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
		public string GenerateExcel(string model, string filter)
		{
			var filterModel = Json.Deserialize<Filters>(filter);
			var excelModel = Json.Deserialize<ExcelModel>(model);

			CrocExcelGeneratorHelper excelHelper = new CrocExcelGeneratorHelper(UserConnection);
			var excelBytes = excelHelper.Generate(excelModel, filterModel);
			var reportData = new CrocFileInfo()
			{
				Name = excelModel.ExcelReportName,
				Data = excelBytes,
				Format = "xlsx"
			};

			string key = string.Format("ReportCacheKey_{0}", Guid.NewGuid());
			UserConnection.SessionData[key] = reportData;

			return key;
		}

		[OperationContract]
		[WebGet(UriTemplate = "GetFile/{key}")]
		public void GetFile(string key)
		{
			CrocExcelGeneratorHelper excelHelper = new CrocExcelGeneratorHelper(UserConnection);
			CrocFileInfo fileInfo = (CrocFileInfo) UserConnection.SessionData[key];
			UserConnection.SessionData.Remove(key);
			if (fileInfo != null)
			{
				excelHelper.GetExcelFile(fileInfo);
			}
		}
	}
}

