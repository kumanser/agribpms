﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Terrasoft.Configuration.OneCIntegration
{

    #region 1C model
    /// <summary>
    /// Планируемый возврат денежных средств
    /// </summary>
    [Serializable()]
    [DataContract]
    public class PlanRefund
    {
        [DataMember(Name = "id", IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// Дата возврата
        /// </summary>
        [DataMember(Name = "refundDate", IsRequired = true)]
        public string RefundDate { get; set; }

        /// <summary>
        /// Сумма возврата
        /// </summary>
        [DataMember(Name = "ammount", IsRequired = true)]
        public decimal Ammount { get; set; }

        /// <summary>
        /// Причина возврата - справочник
        /// </summary>
        [DataMember(Name = "refundReason", IsRequired = true)]
        public string RefundReason { get; set; }

        /// <summary>
        /// Дополнительное соглашение - спарвочник
        /// </summary>
        [DataMember(Name = "additionalAgreement", IsRequired = true)]
        public string AdditionalAgreement { get; set; }

        /// <summary>
        /// Менеджер - справочник
        /// </summary>
        [DataMember(Name = "manager", IsRequired = false)]
        public string Manager { get; set; }

        /// <summary>
        /// Номенклатура - справочник
        /// </summary>
        [DataMember(Name = "nomenclature", IsRequired = true)]
        public string Nomenclature { get; set; }

        /// <summary>
        /// Контрагент - справочник
        /// </summary>
        [DataMember(Name = "account", IsRequired = true)]
        public string Account { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [DataMember(Name = "comment", IsRequired = false)]
        public string Comment { get; set; }
    }

    /// <summary>
    /// Реестр порожних ТС
    /// </summary>
    [Serializable()]
    [DataContract]
    public class EmptyVehicle
    {
        /// <summary>
        /// ID Поручение
        /// </summary>
        [DataMember(Name = "id", IsRequired = true)]
        public string Id { get; set; }
        /// <summary>
        /// Реестр ТС
        /// </summary>
        [DataMember(Name = "listVehicles")]
        public List<Vehicle> Vehicles { get; set; }
    }
    [Serializable()]
    [DataContract]
    public class Vehicle
    {
        /// <summary>
        /// Номер ТС
        /// </summary>
        [DataMember(Name = "licensePlate", IsRequired = true)]
        public string LicensePlate { get; set; }

        /// <summary>
        /// Номер прицепа
        /// </summary>
        [DataMember(Name = "trailerNumber", IsRequired = true)]
        public string TrailerNumber { get; set; }

        /// <summary>
        /// ФИО водителя
        /// </summary>
        [DataMember(Name = "driverFullName", IsRequired = true)]
        public string DriverFullName { get; set; }

        /// <summary>
        /// ИНН водителя
        /// </summary>
        [DataMember(Name = "driverInn", IsRequired = true)]
        public string DriverInn { get; set; }

        /// <summary>
        /// Собственник ТС
        /// </summary>
        [DataMember(Name = "carrierOwner", IsRequired = true)]
        public string CarrierOwner { get; set; }

        /// <summary>
        /// ИНН собственника ТС
        /// </summary>
        [DataMember(Name = "carrierOwnerINN", IsRequired = true)]
        public string CarrierOwnerINN { get; set; }

        /// <summary>
        /// Фактический перевозчик
        /// </summary>
        [DataMember(Name = "carrier", IsRequired = true)]
        public string Carrier { get; set; }

        /// <summary>
        /// ИНН Факт.перевозчика
        /// </summary>
        [DataMember(Name = "carrierINN", IsRequired = true)]
        public string CarrierINN { get; set; }

        /// <summary>
        /// Договор перевозки
        /// </summary>
        [DataMember(Name = "numberForNotification", IsRequired = true)]
        public string NumberForNotification { get; set; }

        /// <summary>
        /// Марка автомобиля
        /// </summary>
        [DataMember(Name = "brandVehicle", IsRequired = true)]
        public string BrandVehicle { get; set; }

        /// <summary>
        /// Номер телефона водителя
        /// </summary>
        [DataMember(Name = "driverPhone", IsRequired = true)]
        public string DriverPhone { get; set; }

        /// <summary>
        /// Вид законного владения
        /// </summary>
        [DataMember(Name = "ownershipType", IsRequired = true)]
        public int OwnershipType { get; set; }

        /// <summary>
        /// Тариф перевозки
        /// </summary>
        [DataMember(Name = "priceNetPerTon", IsRequired = true)]
        public decimal PriceNetPerTon { get; set; }

        /// <summary>
        /// Тариф вознаграждения
        /// </summary>
        [DataMember(Name = "commissionPerTon", IsRequired = false)]
        public decimal CommissionPerTon { get; set; }
    }

    /// <summary>
    /// Реестр погрузок
    /// </summary>
    [Serializable()]
    [DataContract]
    public class RegisterLoadings
    {
        [DataMember(Name = "id", IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// График
        /// </summary>
        [DataMember(Name = "plan", IsRequired = true)]
        public string Plan { get; set; }

        /// <summary>
        /// Номенклатура (груз) - справочник
        /// </summary>
        [DataMember(Name = "nomenclature", IsRequired = true)]
        public string Nomenclature { get; set; }

        /// <summary>
        /// Дата отгрузки
        /// </summary>
        [DataMember(Name = "shipmentDate", IsRequired = true)]
        public string ShipmentDate { get; set; }

        /// <summary>
        /// Элеватор отгрузки - спарвочник
        /// </summary>
        [DataMember(Name = "shipmentPoint", IsRequired = true)]
        public string ShipmentPoint { get; set; }

        /// <summary>
        /// Станция отправления - справочник
        /// </summary>
        [DataMember(Name = "shipperLocation", IsRequired = true)]
        public string ShipperLocation { get; set; }

        /// <summary>
        /// ID Грузоотправителя
        /// </summary>
        [DataMember(Name = "shipperId", IsRequired = true)]
        public string ShipperId { get; set; }

        /// <summary>
        /// Табличная часть Товары отгруженные
        /// </summary>
        [DataMember(Name = "goodsRegister", IsRequired = true)]
        public List<GoodsRegister> GoodsRegisters { get; set; }
    }

    /// <summary>
    ///Товары отгруженные
    /// </summary>
    [Serializable()]
    [DataContract]
    public class GoodsRegister
    {
        /// <summary>
        /// ID ТС
        /// </summary>
        [DataMember(Name = "vehicleId", IsRequired = true)]
        public string VehicleId { get; set; }

        /// <summary>
        /// ТТН/ЖД накладной
        /// </summary>
        [DataMember(Name = "invoiceNumber", IsRequired = true)]
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// Вес брутто по накладной, тонны
        /// </summary>
        [DataMember(Name = "weightBrutto", IsRequired = true)]
        public decimal WeightBrutto { get; set; }

        /// <summary>
        /// Вес тара, по наклад-ной, тонны
        /// </summary>
        [DataMember(Name = "weightPackage", IsRequired = true)]
        public decimal WeightPackage { get; set; }

        /// <summary>
        /// Вес по наклад-ной, тонны
        /// </summary>
        [DataMember(Name = "weightNetto", IsRequired = true)]
        public decimal WeightNetto { get; set; }

        /// <summary>
        /// Протеин
        /// </summary>
        [DataMember(Name = "protein", IsRequired = true)]
        public decimal Protein { get; set; }

        /// <summary>
        /// Натура
        /// </summary>
        [DataMember(Name = "nature", IsRequired = true)]
        public int Nature { get; set; }

        /// <summary>
        /// ЧП (Число Падения)
        /// </summary>
        [DataMember(Name = "fallingNumber", IsRequired = true)]
        public decimal FallingNumber { get; set; }

        /// <summary>
        /// Сорная примесь %
        /// </summary>
        [DataMember(Name = "foreignMaterial", IsRequired = true)]
        public decimal ForeignMaterial { get; set; }

        /// <summary>
        /// Влажность %
        /// </summary>
        [DataMember(Name = "moisture", IsRequired = true)]
        public decimal Moisture { get; set; }

        /// <summary>
        /// Битые
        /// </summary>
        [DataMember(Name = "brokenGrains", IsRequired = true)]
        public decimal BrokenGrains { get; set; }

        /// <summary>
        /// Поврежденные
        /// </summary>
        [DataMember(Name = "damagedGrain", IsRequired = true)]
        public decimal DamagedGrain { get; set; }

        /// <summary>
        /// Клейковина, %
        /// </summary>
        [DataMember(Name = "gluten", IsRequired = true)]
        public decimal Gluten { get; set; }

        /// <summary>
        /// ИДК, ед
        /// </summary>
        [DataMember(Name = "glutenDeformationIndex", IsRequired = true)]
        public decimal GlutenDeformationIndex { get; set; }

        /// <summary>
        /// Поврежденные клопом/черепашкой
        /// </summary>
        [DataMember(Name = "bugEated", IsRequired = true)]
        public decimal BugEated { get; set; }

        /// <summary>
        /// Зерновая примесь
        /// </summary>
        [DataMember(Name = "foreignGrain", IsRequired = true)]
        public decimal ForeignGrain { get; set; }

        /// <summary>
        /// Зерна другого цвета
        /// </summary>
        [DataMember(Name = "grainsDifferentColor", IsRequired = true)]
        public decimal GrainsDifferentColor { get; set; }

        /// <summary>
        /// Класс
        /// </summary>
        [DataMember(Name = "class", IsRequired = true)]
        public string GrainsClass { get; set; }
    }

    /// <summary>
    ///Заявка ГУ-12
    /// </summary>
    [Serializable()]
    [DataContract]
    public class RequestGU
    {
        [DataMember(Name = "id", IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// Статус согласования заявки ГУ-12 - справочник
        /// </summary>
        [DataMember(Name = "status", IsRequired = true)]
        public string Status { get; set; }

        /// <summary>
        /// График - справочник
        /// </summary>
        [DataMember(Name = "plan", IsRequired = true)]
        public string Plan { get; set; }
    }

    /// <summary>
    /// График отгрузок
    /// </summary>
    [Serializable()]
    [DataContract]
    public class ShipmentPlan
    {
        /// <summary>
        /// ID графика
        /// </summary>
        [DataMember(Name = "id", IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// ID задания на логистику
        /// </summary>
        [DataMember(Name = "taskId", IsRequired = true)]
        public string TaskId { get; set; }

        /// <summary>
        /// Вид операции
        /// </summary>
        [DataMember(Name = "type", IsRequired = true)]
        public int PlanType { get; set; }

        /// <summary>
        /// ID Справочника Населенные пункты(верхний уровень край/область)
        /// </summary>
        [DataMember(Name = "region", IsRequired = false)]
        public string Region { get; set; }

        /// <summary>
        /// Дополнительное соглашение
        /// </summary>
        [DataMember(Name = "additionalAgreement", IsRequired = false)]
        public string AdditionalAgreement { get; set; }

        /// <summary>
        /// Номер заявки ГУ-12
        /// </summary>
        [DataMember(Name = "numberGU12", IsRequired = false)]
        public string NumberGU12 { get; set; }

        /// <summary>
        /// Номер заявки в РАТ
        /// </summary>
        [DataMember(Name = "numberRAT", IsRequired = false)]
        public string NumberRAT { get; set; }

        /// <summary>
        /// Статус согласования
        /// </summary>
        [DataMember(Name = "status", IsRequired = false)]
        public string Status { get; set; }

        /// <summary>
        /// Грузоотправитель по ГУ-12
        /// </summary>
        [DataMember(Name = "consignerGU12", IsRequired = false)]
        public string ConsignerGU { get; set; }

        /// <summary>
        /// Грузополучатель по ГУ-12
        /// </summary>
        [DataMember(Name = "consigneeGU12", IsRequired = false)]
        public string ConsigneeGU { get; set; }

        /// <summary>
        /// Станция назначения
        /// </summary>
        [DataMember(Name = "destinationStation", IsRequired = false)]
        public string DestinationStation { get; set; }

        /// <summary>
        /// Станция отправления
        /// </summary>
        [DataMember(Name = "departureStation", IsRequired = false)]
        public string DepartureStation { get; set; }

        /// <summary>
        /// Табличная часть График погрузок
        /// </summary>
        [DataMember(Name = "loading")]
        public List<Loading> Loadings { get; set; }

        /// <summary>
        /// Табличная часть График поставок
        /// </summary>
        [DataMember(Name = "delivery")]
        public List<Delivery> Deliveries { get; set; }
    }

    /// <summary>
    ///График погрузок
    /// </summary>
    [Serializable()]
    [DataContract]
    public class Loading
    {
        /// <summary>
        /// Дата погрузки
        /// </summary>
        [DataMember(Name = "loadingDate", IsRequired = true)]
        public string LoadingDate { get; set; }

        /// <summary>
        /// План погрузки, тонны
        /// </summary>
        [DataMember(Name = "weightPlan", IsRequired = true)]
        public decimal WeightPlan { get; set; }

        /// <summary>
        /// План погрузки, ТС
        /// </summary>
        [DataMember(Name = "vehiclePlan", IsRequired = true)]
        public int VehiclePlan { get; set; }
    }

    /// <summary>
    ///График поставок
    /// </summary>
    [Serializable()]
    [DataContract]
    public class Delivery
    {
        /// <summary>
        /// Дата поставки
        /// </summary>
        [DataMember(Name = "deliveryDate", IsRequired = true)]
        public string DeliveryDate { get; set; }

        /// <summary>
        /// План поставки, тонны
        /// </summary>
        [DataMember(Name = "weightPlan", IsRequired = true)]
        public decimal WeightPlan { get; set; }

        /// <summary>
        /// План поставки, ТС
        /// </summary>
        [DataMember(Name = "vehiclePlan", IsRequired = true)]
        public int VehiclePlan { get; set; }
    }

    /// <summary>
    ///Запрос квоты
    /// </summary>
    [Serializable()]
    [DataContract]
    public class QuotaRequest
    {

        [DataMember(Name = "id", IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// Терминал
        /// </summary>
        [DataMember(Name = "terminal", IsRequired = true)]
        public string Terminal { get; set; }

        /// <summary>
        /// ID контрагента, запрашивающего квоту
        /// </summary>
        [DataMember(Name = "counterparty", IsRequired = true)]
        public string Counterparty { get; set; }

        /// <summary>
        /// Номенклатура
        /// </summary>
        [DataMember(Name = "nomenclature", IsRequired = true)]
        public string Nomenclature { get; set; }

        /// <summary>
        /// Запрашиваемая квота, тонн
        /// </summary>
        [DataMember(Name = "weightQuote", IsRequired = true)]
        public decimal WeightQuote { get; set; }

        /// <summary>
        /// Запрашиваемая квота, машин
        /// </summary>
        [DataMember(Name = "vehicleQuote", IsRequired = true)]
        public int VehicleQuote { get; set; }

        /// <summary>
        /// График поставок
        /// </summary>
        [DataMember(Name = "shipment", IsRequired = true)]
        public string Shipment { get; set; }

        /// <summary>
        /// Поручение
        /// </summary>
        [DataMember(Name = "assignment", IsRequired = false)]
        public string Assignment { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember(Name = "period", IsRequired = true)]
        public string Period { get; set; }
    }

    /// <summary>
    ///Отказ от квоты
    /// </summary>
    [Serializable()]
    [DataContract]
    public class QuotaRefusal
    {

        [DataMember(Name = "id", IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// Отклоняемая квота, тонн
        /// </summary>
        [DataMember(Name = "weightQuote", IsRequired = true)]
        public decimal WeightQuote { get; set; }

        /// <summary>
        /// Отклоняемая квота, машин
        /// </summary>
        [DataMember(Name = "vehicleQuote", IsRequired = true)]
        public int VehicleQuote { get; set; }

        /// <summary>
        /// График поставок
        /// </summary>
        [DataMember(Name = "shipment", IsRequired = true)]
        public string Shipment { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember(Name = "period", IsRequired = true)]
        public string Period { get; set; }
    }

    /// <summary>
    ///Аккредитация
    /// </summary>
    [Serializable()]
    [DataContract]
    public class Accreditation
    {
        [DataMember(Name = "id", IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// Контрагент
        /// </summary>
        [DataMember(Name = "account", IsRequired = true)]
        public string Account { get; set; }

        /// <summary>
        /// Набор документов (Вид аккредитации)
        /// </summary>
        [DataMember(Name = "idTypeDocuments", IsRequired = false)]
        public string AccreditationType { get; set; }

        /// <summary>
        /// Набор документов Название (Вид аккредитации)
        /// </summary>
        [DataMember(Name = "nameTypeDocuments", IsRequired = false)]
        public string AccreditationTypeName { get; set; }

        /// <summary>
        /// Табличная часть Комплект документов
        /// </summary>
        [DataMember(Name = "documents", IsRequired = true)]
        public List<Document> Documents { get; set; }
    }

    /// <summary>
    ///Комплект документов
    /// </summary>
    [Serializable()]
    [DataContract]
    public class Document
    {
        /// <summary>
        /// Тип документа
        /// </summary>
        [DataMember(Name = "documentType", IsRequired = true)]
        public string Type { get; set; }

        /// <summary>
        /// Ссылка на файл
        /// </summary>
        [DataMember(Name = "link", IsRequired = true)]
        public string Link { get; set; }

        /// <summary>
        /// Срок действия
        /// </summary>
        [DataMember(Name = "validOnDate", IsRequired = false)]
        public string ValidOnDate { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [DataMember(Name = "comment", IsRequired = false)]
        public string Comment { get; set; }

        /// <summary>
        /// Дата загрузки файла
        /// </summary>
        [DataMember(Name = "addOnDate", IsRequired = false)]
        public string AddOnDate { get; set; }
    }

    /// <summary>
    ///Документы по перевозке
    /// </summary>
    [Serializable()]
    [DataContract]
    public class ShipmentDocument
    {
        /// <summary>
        /// ДС
        /// </summary>
        [DataMember(Name = "id", IsRequired = true)]
        public string AdditionalAgreement { get; set; }

        /// <summary>
        /// Табличная часть Комплект документов
        /// </summary>
        [DataMember(Name = "documents", IsRequired = true)]
        public List<ShipmentDocumentTS> Documents { get; set; }
    }

    /// <summary>
    ///Документы по перевозке табличная часть
    /// </summary>
    [Serializable()]
    [DataContract]
    public class ShipmentDocumentTS
    {
        /// <summary>
        /// Тип документа
        /// </summary>
        [DataMember(Name = "documentType", IsRequired = true)]
        public string Type { get; set; }

        /// <summary>
        /// Ссылка на файл
        /// </summary>
        [DataMember(Name = "link", IsRequired = true)]
        public string Link { get; set; }

        /// <summary>
        /// Дата загрузки файла
        /// </summary>
        [DataMember(Name = "addOnDate", IsRequired = false)]
        public string AddOnDate { get; set; }
    }

    /// <summary>
    ///Заявка на оказание услуг
    /// </summary>
    [Serializable()]
    [DataContract]
    public class ServiceRequest
    {
        /// <summary>
        /// ID заявки (1С)
        /// </summary>
        [DataMember(Name = "id", IsRequired = true)]
        public string Id { get; set; }

        /// <summary>
        /// Тип документа
        /// </summary>
        [DataMember(Name = "status", IsRequired = true)]
        public string Status { get; set; }

        /// <summary>
        /// Вид заявки
        /// </summary>
        [DataMember(Name = "applicationType", IsRequired = true)]
        public string ApplicationTypeName { get; set; }

        /// <summary>
        /// Вид заявки
        /// </summary>
        [DataMember(Name = "provider_guid", IsRequired = false)]
        public string Provider { get; set; }
    }

    #endregion

    #region SmartSeeds

    [Serializable()]
    [DataContract]
    public class SSToken
    {
        [DataMember(Name = "access_token")]
        public string Token { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class SSClientParameters
    {
        [DataMember(Name = "recordId")]
        public Guid RecordId { get; set; }

        [DataMember(Name = "originId")]
        public Guid OriginId { get; set; }

        [DataMember(Name = "destinationId")]
        public Guid DestinationId { get; set; }

        [DataMember(Name = "productId")]
        public Guid ProductId { get; set; }

        [DataMember(Name = "deliveryDate")]
        public DateTime DeliveryDate { get; set; }

        [DataMember(Name = "weight")]
        public int Weight { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class SmartseedsCalculationRequest
    {
        [DataMember(Name = "crops")]
        public int Crops { get; set; }

        [DataMember(Name = "origin")]
        public string OriginId { get; set; }

        [DataMember(Name = "destination")]
        public string DestinationId { get; set; }

        [DataMember(Name = "deliveryDate")]
        public string DeliveryDate { get; set; }

        [DataMember(Name = "weight")]
        public int Weight { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class SmartseedsCalculationResponse
    {

        [DataMember(Name = "isForwardingServicesAdded")]
        public bool isForwardingServicesAdded { get; set; }

        [DataMember(Name = "forwardingServicesPricePerTone")]
        public AmountWithCurrency ForwardingServicesPricePerTone { get; set; }

        [DataMember(Name = "forwardingServicesMinPricePerDay")]
        public AmountWithCurrency ForwardingServicesMinPricePerDay { get; set; }

        [DataMember(Name = "priceAutoIncreaseEnabled")]
        public bool PriceAutoIncreaseEnabled { get; set; }

        [DataMember(Name = "uuid")]
        public string Uuid { get; set; }

        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "origin")]
        public AddressPoint Origin { get; set; }

        [DataMember(Name = "destination")]
        public AddressPoint Destination { get; set; }

        [DataMember(Name = "deliveryDate")]
        public string DeliveryDate { get; set; }

        [DataMember(Name = "deliveryDateTz")]
        public string DeliveryDateTz { get; set; }

        [DataMember(Name = "crops")]
        public Crops Crops { get; set; }

        [DataMember(Name = "weight")]
        public int Weight { get; set; }

        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        [DataMember(Name = "useBestPlaces")]
        public bool UseBestPlaces { get; set; }

        [DataMember(Name = "maxVehicleRoominess")]
        public int MaxVehicleRoominess { get; set; }

        [DataMember(Name = "distance")]
        public int Distance { get; set; }

        [DataMember(Name = "time")]
        public double Time { get; set; }

        [DataMember(Name = "priceGross")]
        public AmountWithCurrency PriceGross { get; set; }

        [DataMember(Name = "priceGrossPerTonForCalculation")]
        public AmountWithCurrency PriceGrossPerTonForCalculation { get; set; }

        [DataMember(Name = "priceGrossPerTon")]
        public AmountWithCurrency PriceGrossPerTon { get; set; }

        [DataMember(Name = "priceNet")]
        public AmountWithCurrency PriceNet { get; set; }

        [DataMember(Name = "prepaymentPriceGross")]
        public AmountWithCurrency PrepaymentPriceGross { get; set; }

        [DataMember(Name = "prepaymentPriceNet")]
        public AmountWithCurrency PrepaymentPriceNet { get; set; }

        [DataMember(Name = "vehicleCount")]
        public int VehicleCount { get; set; }

        [DataMember(Name = "terminalMarkup")]
        public AmountWithCurrency TerminalMarkup { get; set; }

        [DataMember(Name = "terminalPremium")]
        public int TerminalPremium { get; set; }

        [DataMember(Name = "routeMeta")]
        public RouteMeta RouteMeta { get; set; }
        
        [DataMember(Name = "additionalVATStrategy")]
        public string AdditionalVATStrategy { get; set; }

        [DataMember(Name = "previewImage")]
        public string PreviewImage { get; set; }

        [DataMember(Name = "priceGrossPerTonBase")]
        public AmountWithCurrency PriceGrossPerTonBase { get; set; }

        [DataMember(Name = "forwardingServicesTotalPrice")]
        public AmountWithCurrency ForwardingServicesTotalPrice { get; set; }

        [DataMember(Name = "priceGrossPerTonBaseWithForwardingService")]
        public AmountWithCurrency PriceGrossPerTonBaseWithForwardingService { get; set; }

        [DataMember(Name = "priceGrossWithForwardingService")]
        public AmountWithCurrency PriceGrossWithForwardingService { get; set; }

        [DataMember(Name = "priceGrossPerTonForCalculationWithForwardingService")]
        public AmountWithCurrency PriceGrossPerTonForCalculationWithForwardingService { get; set; }

        [DataMember(Name = "priceGrossPerToneBase")]
        public AmountWithCurrency PriceGrossPerToneBase { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class AmountWithCurrency
    {

        [DataMember(Name = "amount")]
        public int Amount { get; set; }

        [DataMember(Name = "currency")]
        public string Currency { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class AddressPoint
    {

        [DataMember(Name = "id")]
        public int? Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class Crops
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class OverviewPolyline
    {

        [DataMember(Name = "points")]
        public string Points { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class Coordinats
    {

        [DataMember(Name = "lat")]
        public double Lat { get; set; }

        [DataMember(Name = "lng")]
        public double Lng { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class Bounds
    {

        [DataMember(Name = "northeast")]
        public Coordinats Northeast { get; set; }

        [DataMember(Name = "southwest")]
        public Coordinats Southwest { get; set; }
    }

    [Serializable()]
    [DataContract]
    public class RouteMeta
    {
        [DataMember(Name = "overview_polyline")]
        public OverviewPolyline OverviewPolyline { get; set; }

        [DataMember(Name = "bounds")]
        public Bounds Bounds { get; set; }
    }

    #endregion
}