﻿using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Terrasoft.Configuration
{

	[Serializable]
    public class CrocFileInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public byte[] Data { get; set; }
        public Guid TypeId { get; set; }
        public string Format { get; set; }
		public string Link { get; set; }
	}

	[DataContract]
	public class FileS3Data
	{
		[DataMember(Name = "fileId")]
		public Guid Id { get; set; }

		[DataMember(Name = "fileName")]
		public string Name { get; set; }

		[DataMember(Name = "entitySchemaName")]
		public string EntitySchemaName { get; set; }

		[DataMember(Name = "parentColumnName")]
		public string ParentColumnName { get; set; }

		[DataMember(Name = "parentColumnValue")]
		public Guid ParentColumnValue { get; set; }

		[DataMember(Name = "columnName")]
		public string ColumnName { get; set; }
		[DataMember(Name = "s3ColumnName")]
		public string S3ColumnName { get; set; }
		[DataMember(Name = "columns")]
		public string Columns { get; set; }
		public CrocFileInfo FileInfo { get; set; }
	}

	[DataContract]
	public class FileS3InfoData
	{
		[DataMember(Name = "file")]
		public FileS3Data File { get; set; }

		[DataMember(Name = "version")]
		public VersionS3Data Version { get; set; }

		[DataMember(Name = "path")]
		public string Path { get; set; }
	}

	[DataContract]
	public class VersionS3Data
	{
		[DataMember(Name = "entitySchemaName")]
		public string EntitySchemaName { get; set; }		
		[DataMember(Name = "parentEntitySchemaName")]
		public string ParentEntitySchemaName { get; set; }

		[DataMember(Name = "parentColumnName")]
		public string ParentColumnName { get; set; }

		[DataMember(Name = "parentColumnValue")]
		public Guid? ParentColumnValue { get; set; }

		[DataMember(Name = "columnName")]
		public string ColumnName { get; set; }
		[DataMember(Name = "s3ColumnName")]
		public string S3ColumnName { get; set; }		
		[DataMember(Name = "versionColumnName")]
		public string VersionColumnName { get; set; }		
		[DataMember(Name = "displayColumnName")]
		public string DisplayColumnName { get; set; }

	}

}
