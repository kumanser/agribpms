﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Terrasoft.Common;
using Terrasoft.Core.Entities;
using Terrasoft.Nui.ServiceModel.DataContract;

namespace Terrasoft.Configuration
{
	public enum Operator
	{
		None,
		Plus,
		Minus,
		Divide,
		Multiply
	}

	[Serializable]
	public class ReportData
	{

		#region Properties : Public

		public string Caption { get; set; }

		public byte[] Data { get; set; }

		public string Format { get; set; }

		#endregion

	}

	[DataContract]
	public class ExcelModel
	{
		[DataMember(Name = "entitySchemaName")]
		public string EntitySchemaName { get; set; }

		[DataMember(Name = "excelReportName")]
		public string ExcelReportName { get; set; }

		[DataMember(Name = "templateFilter")]
		public List<TemplateFilter> Filter { get; set; }

		[DataMember(Name = "hasIdColumn")]
		public bool HasIdColumn { get; set; }

		[DataMember(Name = "hasMonthColumn")]
		public bool HasMonthColumn { get; set; }

		[DataMember(Name = "rowOffset")]
		public int RowOffset { get; set; }

		[DataMember(Name = "columns")]
		public List<ExcelColumn> Columns { get; set; }
	}

	[DataContract]
	public class TemplateFilter
	{
		[DataMember(Name = "path")]
		public string Path { get; set; }

		[DataMember(Name = "value")]
		public string Value { get; set; }
	}

	[DataContract]
	public class ExcelColumn
	{
		[DataMember(Name = "path")]
		public string Path { get; set; }

		[DataMember(Name = "name")]
		public string Name { get; set; }

		[DataMember(Name = "displayName")]
		public string DisplayName  { get; set; }

		[DataMember(Name = "func")]
		public AggregationTypeStrict Func { get; set; }

		[DataMember(Name = "isAggregate")]
		public bool IsAggregate { get; set; }

		[DataMember(Name = "position")]
		public int Position { get; set; }

		[DataMember(Name = "concatenateColumns")]
		public List<int> ConcatenateColumns { get; set; }

		[DataMember(Name = "concatenateValues")]
		public bool ConcatenateValues { get; set; }

		[DataMember(Name = "separator")]
		public string Separator { get; set; }

		[DataMember(Name = "isTechnicalColumn")]
		public bool IsTechnicalColumn { get; set; }

		[DataMember(Name = "isVisible")]
		public bool IsVisible { get; set; }

		[DataMember(Name = "dateFormat")]
		public string DateFormat { get; set; }

		[DataMember(Name = "isExcelFormula")]
		public bool IsExcelFormula { get; set; }

		[DataMember(Name = "formula")]
		public string Formula { get; set; }

		[DataMember(Name = "formulaPosition")]
		public int FormulaPosition { get; set; }

		[DataMember(Name = "isRailWayColumn")]
		public bool IsRailwayColumn { get; set; }

		[DataMember(Name = "isOperationColumn")]
		public bool IsOperationColumn { get; set; }

		[DataMember(Name = "operator")]
		public Operator Operator { get; set; }

		[DataMember(Name = "operationColumns")]
		public List<int> OperationColumns { get; set; }
	}
}
