﻿using System;
using System.Runtime.Serialization;

namespace Terrasoft.Configuration.Base
{
	#region BaseResponse

	public class BaseTypeResponse<T> : BaseResponse
	{
		public T Response { get; set; }
	}

	[Serializable, DataContract]
	public class BaseResponse
	{
		// Methods
		public BaseResponse() { }

		// Properties
		[DataMember(Name = "success")]
		public bool Success { get; set; }
		[DataMember(Name = "errorInfo")]
		public ErrorInfo ErrorInfo { get; set; }
	}

	[DataContract]
	public class ErrorInfo
	{
		// Methods
		public ErrorInfo() { }

		// Properties
		[DataMember(Name = "errorCode")]
		public string ErrorCode { get; set; }
		[DataMember(Name = "message")]
		public string Message { get; set; }
		[DataMember(Name = "stackTrace")]
		public string StackTrace { get; set; }
	}

	#endregion
}
