﻿using System;
namespace Terrasoft.Configuration.Base
{
    public class ConstCs
    {
        public static class Example
        {
            public static readonly Guid Value = new Guid("4A4DD6E3-668F-47E3-8D91-4D1476CCF4B4"); // Text
        }

        public static class SchemaUId
        {
            public static readonly Guid Order = new Guid("80294582-06B5-4FAA-A85F-3323E5536B71"); // Order
            public static readonly Guid Contact = new Guid("16BE3651-8FE2-4159-8DD0-A803D4683DD3"); // Contact
            public static readonly Guid Case = new Guid("117D32F9-8275-4534-8411-1C66115CE9CD"); // Case
        }

        public static class Call
        {
            public static class Direction
            {
                public static readonly Guid Incomming = new Guid("1D96A65F-2131-4916-8825-2D142B1000B2"); // Входящий
            }
        }

        public static class Contact
        {
            public static class Type
            {
                public static readonly Guid Client = new Guid("00783EF6-F36B-1410-A883-16D83CAB0980"); // Клиент
                public static readonly Guid PartnerId = new Guid("EBF46417-6189-49A0-8C75-3868D87E4479"); // Партнер
                public static readonly Guid EmployeeId = new Guid("60733EFC-F36B-1410-A883-16D83CAB0980"); // Сотрудник
            }
        }

        public static class Case
        {
            public static class Status
            {
                public static readonly Guid InProgressId = new Guid("7E9F1204-F46B-1410-FB9A-0050BA5D6C38");
                public static readonly Guid ClosedId = new Guid("3E7F420C-F46B-1410-FC9A-0050BA5D6C38");
                public static readonly Guid NewId = new Guid("AE5F2F10-F46B-1410-FD9A-0050BA5D6C38");
                public static readonly Guid CanceledId = new Guid("6E5F4218-F46B-1410-FE9A-0050BA5D6C38");
                public static readonly Guid ReopenedId = new Guid("F063EBBE-FDC6-4982-8431-D8CFA52FEDCF");
                public static readonly Guid ResolvedId = new Guid("AE7F411E-F46B-1410-009B-0050BA5D6C38");
                public static readonly Guid WaitingForResponseId = new Guid("3859C6E7-CBCB-486B-BA53-77808FE6E593");
                public static readonly Guid RegistrationId = new Guid("8E7C1707-C514-4E40-B1D0-CC41C1B10F46"); //Регистрация обработки
                public static readonly Guid SwitchId = new Guid("9855757E-2C1E-4C1C-BE70-927EB538AA2B"); //Перевод на другую линию
                public static readonly Guid OrdainedId = new Guid("6C0FD2F4-B8E0-43B2-92EF-13BC7E14D815"); //Назначено
                public static readonly Guid ReturnToQueueId = new Guid("D226D32D-C8B2-408B-8CFC-BBED8283E032"); //Возвращение в очередь
            }
            public static class Origin
            {
                public static readonly Guid CallId = new Guid("5E5E202A-F46B-1410-3692-0050BA5D6C38");
                public static readonly Guid ChatId = new Guid("019259AF-EC9C-4C87-9565-3C00700C7875");
                public static readonly Guid EmailId = new Guid("7F9E1F1E-F46B-1410-3492-0050BA5D6C38");
                public static readonly Guid InternalId = new Guid("40349262-BB11-4F90-8215-931ECE6ABE22");
                public static readonly Guid ForumId = new Guid("406AC5A1-A924-4AF6-9F64-F4FEDC66BE8C");
                public static readonly Guid PersonalVisitId = new Guid("43090402-DEEF-4155-8AAF-D4B72721F2BE");
                public static readonly Guid SocialNetworksId = new Guid("673DE246-F464-4AD1-883C-8A21508BDF33");
                public static readonly Guid SiteId = new Guid("D38A1692-5A83-4253-A592-7D19FE00593C");
                public static readonly Guid PortalId = new Guid("DEBF4124-F46B-1410-3592-0050BA5D6C38");
                public static readonly Guid MissedCallId = new Guid("4142C2FA-2FFD-47FE-AD44-D085EC89319C"); //Пропущенный звонок
            }
            public static class CrocTypeClient
            {
                public static readonly Guid B2BId = new Guid("514E6DEA-A68C-4409-A406-7025D4152827");
                public static readonly Guid B2CId = new Guid("68EF1788-2411-4850-AB16-6713CB9900D2");
            }
        }

        public static class QueueItem
        {
            public static class Status
            {
                public static readonly Guid InProgressId = new Guid("16368C9C-EFC6-465D-A7AE-18F2DC3D3049");
                public static readonly Guid NotProcessedId = new Guid("2B341A1D-6FA1-4960-9C85-FEF60D1BBCC4");
                public static readonly Guid ProcessedId = new Guid("2EAB3927-B5C8-4F78-AF5D-3F472174D3EE");
            }
        }

        public static class ServiceItem
        {
            public static class Status
            {
                public static readonly Guid InactiveId = new Guid("1362011D-4DFC-4FF4-99BA-DE2AD0344C7A");
                public static readonly Guid ActiveId = new Guid("9A32E65F-7D52-49AC-AEF5-836A9A01F14E");
                public static readonly Guid UnderDevelopmentId = new Guid("32613B73-AEEB-4469-BFCC-1F168F994DBF");
                public static readonly Guid UnderTestingId = new Guid("1FE3F769-6781-4F30-8D2A-04049DE05446");
            }
            public static class TimeUnit
            {
                public static readonly Guid CalendarHoursId = new Guid("B788B4DE-5AE9-42E2-AF34-CD3AD9E6C96F");
                public static readonly Guid CalendarMinutesId = new Guid("48B4FF98-E3BF-4F59-A6CF-284E4084FB2F");
                public static readonly Guid CalendarDaysId = new Guid("36DF302E-5AB6-43A0-AEC7-45C2795D839D");
                public static readonly Guid WorkHoursId = new Guid("2A608ED7-D118-402A-99C0-2F583291ED2E");
                public static readonly Guid WorkMinutesId = new Guid("3AB432A6-CA84-4315-BA33-F343C758A8F0");
                public static readonly Guid WorkDaysId = new Guid("BDCBB819-9B26-4627-946F-D00645A2D401");
            }
        }

        public static class Operator
        {
            public static class Status
            {
                public static readonly Guid Invisible = new Guid("314CCE15-9847-4DB3-B93C-D1E6EA175B23");
                public static readonly Guid Offline = new Guid("4F2A3D78-90C0-49CB-8168-D3002050F5E9");
                public static readonly Guid NotReady = new Guid("7B48DB6E-4C9E-4AC0-AB1F-25FDE9B2E31F");
                public static readonly Guid Ready = new Guid("A2922EE1-9BC4-4524-979A-D6E804FC1F56");
            }
            public static class Reason
            {
                public static readonly Guid Dinner = new Guid("FA1E6C75-0A7E-494F-8BE1-9AF05876890A"); //Обед
                public static readonly Guid BusyAuto = new Guid("EE9BFA4C-5197-4DA1-8881-880B40E72A6B"); //Занят авто
                public static readonly Guid Lerning = new Guid("E99C0C8F-C186-4620-B071-BE28101D1902"); //Обучение
                public static readonly Guid Break = new Guid("BE0F299D-7F65-4A8F-9ECF-3B4C4BB471E3"); //Перерыв
                public static readonly Guid PostProcessing = new Guid("FC92F4A2-8ABB-4071-89C5-254C88FAC6CC"); //Постобработка
            }
        }

        public static class Queue
        {
            public static class ColumnSortDirection
            {
                public static readonly Guid NonId = new Guid("7C155038-77D2-4EA1-BCAE-82D64698119D");
                public static readonly Guid AscendingId = new Guid("91E2CCE1-6F51-4AFD-BFD9-DC786BD7E666");
                public static readonly Guid DescendingId = new Guid("8E7E394F-B0C5-4C06-8E5D-CF789128AC08");
            }

            public static class CommChannel
            {
                public static readonly Guid ChatId = new Guid("9CFBE672-E6AA-40AF-88AA-C4FB61401332");
                public static readonly Guid CallId = new Guid("689E6D65-42F4-478D-B774-8B45515C80A7");
                public static readonly Guid EmailId = new Guid("8275C550-6CE8-4937-A12F-56177C059555");
                public static readonly Guid MissedCallId = new Guid("050BADFF-2CF7-4EE7-8D83-53DCA90E6C7D");
            }

            public static class Status
            {
                public static readonly Guid InProgressId = new Guid("EC5828F6-B883-4EBC-AFE3-C80792252ADB");
                public static readonly Guid CompletedId = new Guid("8EE54ED5-4EB9-411C-810B-6AD5BFCCFA87");
                public static readonly Guid PlannedId = new Guid("34BD9093-D0FF-422C-9C15-9C0668E31BCB");
            }
        }

        public static class Mapping
        {
            public static class Type
            {
                public static readonly Guid Column = new Guid("D6049D3B-F4F4-4B57-8298-C3FE885E072D");
                public static readonly Guid Table = new Guid("0462CD88-889A-4222-92EE-F0F130EB86AB");
            }

            public static class Rule
            {
                public static readonly Guid OrderBus = new Guid("D574A3FC-7D5F-4EDB-BCDD-C679ED7116C1");
            }
        }

        public static class Activity
        {
            public static class Type
            {
                public static readonly Guid Email = new Guid("E2831DEC-CFC0-DF11-B00F-001D60E938C6");
                public static readonly Guid Call = new Guid("E1831DEC-CFC0-DF11-B00F-001D60E938C6");
            }

            public static class Category
            {
                public static readonly Guid Email = new Guid("8038a396-7825-e011-8165-00155d043204");
            }

            public static class Status
            {
                public static readonly Guid NotStarted = new Guid("384D4B84-58E6-DF11-971B-001D60E938C6");
                public static readonly Guid Cancel = new Guid("201CFBA8-58E6-DF11-971B-001D60E938C6");
                public static readonly Guid Done = new Guid("4BDBB88F-58E6-DF11-971B-001D60E938C6");
                public static readonly Guid InProgress = new Guid("394D4B84-58E6-DF11-971B-001D60E938C6");
            }

            public static class MessageType
            {
                public static readonly Guid Incoming = new Guid("7F9D1F86-F36B-1410-068C-20CF30B39373");
                public static readonly Guid Outgoing = new Guid("7F6D3F94-F36B-1410-068C-20CF30B39373");
            }
        }

        public static class OrderPossible
        {
            public static class Type
            {
                public static readonly Guid ByContent = new Guid("A9B9CB6C-7D1E-4E9D-9E13-B6D8A9391283");
                public static readonly Guid BySender = new Guid("9C9EE8F7-5C04-4FA2-A3EC-43CCA4A3FBE3");
            }
        }

        public static class ChartExecutionStatus
        {
            public static readonly Guid Closed = new Guid("2b8e623a-d3be-410b-bbc0-cf92f926852a"); //Закрыт
            public static readonly Guid CompletionWaiting = new Guid("023751f2-883d-4a68-85e3-4a43b114669c"); //Ожидает завершения
            public static readonly Guid Plan = new Guid("f72e8252-7491-4d13-8f42-af63710a0418"); //План
        }

        public static class CrocEmployeeRole
        {
            public static readonly Guid PurchasingManager = new Guid("d8fac9dc-d264-4a5e-ad47-5a436cb77b29"); // Менеджер по продажам
            public static readonly Guid ManagerSTO = new Guid("22c28cfb-b081-4566-b7f6-78da27e3804e"); // Специалист СТО
        }

        public static class CrocCommentStatus
        {
            public static readonly Guid Fixed = new Guid("47f3582b-6654-45cf-8389-bbd4e99f7f39"); //Исправлено
            public static readonly Guid FixWaiting = new Guid("3481d8dc-7ded-498d-9a40-c4c99e053e21"); //Ожидает исправления
            public static readonly Guid FixAccepted = new Guid("5819bf7c-d133-4700-9780-b35b55419c06"); // Принято исправление
        }

        public static class CrocApplicationStatus
        {
            public static readonly Guid Processing = new Guid("a2da14da-3f47-471b-b7bb-eb6224c6ed83"); //Выполняется
            public static readonly Guid Canceled = new Guid("e8ebbc0a-4e6d-4eeb-9945-6cc6810b7618"); //Заявка отклонена
            public static readonly Guid Cofirmed = new Guid("f6e761c9-ab8b-4d03-a72e-79e76bbc055e"); //Заявка принята
        }

        public static class CrocOrderStatus
        {
            public static readonly Guid Completed = new Guid("82d5b937-67cc-42c6-9739-cdfad4c7cf01"); //Выполнено
        }

        public static class CrocApplicationType
        {
            public static readonly Guid Expidy = new Guid("1a9312db-4bc2-4311-89e2-3dc64ab5b2cb"); //Экспедитор
            public static readonly Guid Transporter = new Guid("4e903341-13c6-47a9-b511-073be951bb54"); //Перевозка
            public static readonly Guid Surveer = new Guid("f92a8cd7-3eb3-4c6d-bf33-a48a363301f4"); //Сюрвейер
        }

        public static class CrocDeliveryCondition
        {
            public static readonly Guid CPT = new Guid("ba3fb45a-2a01-49bd-bb0e-2b01bc011cd0"); //CPT
            public static readonly Guid FCA = new Guid("3ce25053-e06f-4c8b-85c5-8fd89ab886e9"); //FCA
            public static readonly Guid EXW = new Guid("0b5f5318-738c-49fc-890c-f481739f4d0f"); //EXW
        }

        public static class CrocTransporType
        {
            public static readonly Guid AutoTransport = new Guid("0bb95c3f-fba1-4baa-99c9-bacdaee127e3"); //Автомобильный
            public static readonly Guid Railway = new Guid("efa3fcd2-6eba-406b-962b-3eb6b82be6cc"); //ЖД
        }

        public static class CrocDocument
        {
            public static class Status
            {
                public static readonly Guid PreparationId = new Guid("b1db7257-1c2d-4af5-b749-7035ea616238"); //Подготовка
                public static readonly Guid ActualId = new Guid("7936b7e6-5ac7-4d4a-8461-969c1cf5f873"); //Актуальный
            }
            public static class Type
            {
                public static readonly Guid GU12 = new Guid("ed86e071-6526-42d4-914a-b3c91ed6939d"); //Заявка ГУ-12
                public static readonly Guid AccreditationId = new Guid("7cda430d-acbc-40e2-ace8-6d05a660527a"); //Аккредитация
            }
            public static class CheckStatus
            {
                public static readonly Guid SendOnCheckId = new Guid("0f5395f1-43ad-48d7-8655-ef97186038b3"); //Направлено на проверку
                public static readonly Guid PassedId = new Guid("5ddd2e76-8273-46ab-9e0d-aae3cc713ccc"); //Проверка пройдена
                public static readonly Guid TemlpeteId = new Guid("ed61d89c-8155-42db-b1c6-6e77cd13a77a"); //Черновик - по умолчанию
                public static readonly Guid FaildId = new Guid("f219c2e5-7e0b-4c56-9653-6e04eeda4cbc"); //Проверка не пройдена
            }
            public static class ApplicationGU12Status
            {
                public static readonly Guid Processing = new Guid("5130636d-a9dd-4875-9817-3818dff67665"); //Выполняется
                public static readonly Guid Canceled = new Guid("0f1327a4-d0bf-4286-87f8-db98068e66e9"); //Заявка отклонена
                public static readonly Guid Cofirmed = new Guid("17807d8b-7f77-4e27-94ac-4cbe9ee6ffc6"); //Заявка принята
            }
        }

        public static class CrocChart
        {
            public static class Status
            {
                public static readonly Guid ProviderRejectedId = new Guid("3de65c2b-82d0-4071-a748-bc9194fb9b9c"); //Поставщик отклонил
                public static readonly Guid PlanId = new Guid("48ae599f-6993-4fd4-b6a4-4297c5bae0ba"); //План
                public static readonly Guid WaitingExporterApprovementId = new Guid("b3cfed4b-e043-45e1-901f-f11ce7604803"); //Ожидает подтверждения экспортером
                public static readonly Guid ProviderApprovementId = new Guid("b7363ade-e980-4b5a-b6be-0577dd3b70eb"); //Поставщик подтвердил
                public static readonly Guid ApprovedId = new Guid("c95757ca-1df7-4c0e-bb44-b59599917271"); //Утвержден
                public static readonly Guid WaitingProviderApprovementId = new Guid("e375dfa2-401d-44fb-849a-45b37782c316"); //Ожидает подтверждения поставщиком
            }
        }

        public static class RemindingConsts
        {
            public static readonly Guid RemindingSourceAuthorId = new Guid("A66D08E1-2E2D-E011-AC0A-00155D043205");
            public static readonly Guid RemindingSourceOwnerId = new Guid("A76D08E1-2E2D-E011-AC0A-00155D043205");
            public static readonly Guid NotificationTypeRemindingId = new Guid("9EE66ABE-EC9D-4667-8995-29E8765DE2D5");
            public static readonly Guid NotificationTypeNotificationId = new Guid("685E7149-C015-4A4D-B4A6-2E5625A6314C");
            public static readonly Guid NotificationTypeAnniversaryId = new Guid("5D4B76F0-953C-4F91-A8A4-B85DF935074F");
            public static readonly Guid AccountRemindSourceId = new Guid("363FE17A-E369-4487-A35F-A6745EDCEDFE");
            public static readonly Guid ActivityRemindSourceId = new Guid("FFC7B9EC-B411-40D7-9476-0ADAE213F0E7");
            public static readonly Guid ContactRemindSourceId = new Guid("ECCD3705-7126-4413-A5D0-ED876EF38838");
            public static readonly Guid OpportunityRemindSourceId = new Guid("0E8588B3-C9F0-4610-8303-52F1ADCC439E");
            public static readonly Guid OrderRemindSourceId = new Guid("ACCD72CE-F051-4F94-82D6-88E755117AE3");
        }

        public static class CrocExporterEmployeeRoles
        {
            public static readonly Guid LogisticRailId = new Guid("08334039-95de-4228-9398-d874cca3bc96"); //Логист (жд)
            public static readonly Guid STOId = new Guid("22c28cfb-b081-4566-b7f6-78da27e3804e"); //Специалист СТО
            public static readonly Guid ElevatorId = new Guid("2ef13703-63ef-4c71-8b5c-92c53dfcd689"); //Специалист по работе с элеваторами
            public static readonly Guid SurvayId = new Guid("5c779bf6-089f-4949-941b-3774904e9d90"); //Специалист по работе с сюрвейерами
            public static readonly Guid LogisticAvtoId = new Guid("5e4a7f8e-b33a-407a-81a8-8db714aa3558"); //Логист (авто)
            public static readonly Guid ManagerVEDId = new Guid("67c5d2c0-e270-4182-b9d8-fb0ffcf7ffb7"); //Менеджер ВЭД
            public static readonly Guid ManagerPurchaseId = new Guid("d8fac9dc-d264-4a5e-ad47-5a436cb77b29"); //Менеджер по закупкам
        }

        public static class SysAdminUnits
        {
            public static readonly Guid AllEmployeesId = new Guid("a29a3ba5-4b0d-de11-9a51-005056c00008");
        }

        public static class SyncTaskEvent
        {
            public static readonly Guid Run = new Guid("E5D228C4-B32C-49C4-8774-19239E85F485");
            public static readonly Guid Error = new Guid("4B1300BA-8F02-4B33-B76D-EC2EFA110FD9");
            public static readonly Guid Ended = new Guid("E625CBEA-20E6-4303-97D4-E44BF7279A81");
        }

        public static class ConstructorIntegrationLogStatus
        {
            public static readonly Guid SuccessId = new Guid("4e8f7d3c-6112-4efc-9158-350de5d9fb51"); //Успешно
            public static readonly Guid ErrorId = new Guid("ceaa2cba-929c-47b7-9a26-508fc32a03b6"); //Ошибка
            public static readonly Guid EmptyId = new Guid("7c8389ef-12c8-401e-8f24-3ef1b5d9e0c6"); //Пусто
            public static readonly Guid TimeoutId = new Guid("75b995c6-3fa6-420e-80c1-6243f4aaa1d1"); //Таймаут
            public static readonly Guid AbortedId = new Guid("faa96898-4532-45fc-81d8-352683eee4e4"); //Прервано
        }
    }
}
