using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Factories;

namespace Terrasoft.Configuration.Base
{
	[DefaultBinding(typeof(ICrocEntityHelper))]
	public class CrocEntityHelper : ICrocEntityHelper
	{
		#region Private: Fields

		private readonly UserConnection _userConnection;

		#endregion

		#region Constructor

		public CrocEntityHelper(UserConnection userConnection)
		{
			_userConnection = userConnection;
		}

		#endregion

		#region Methods: Public

		/// <summary>
		/// Получить идентификатор записи
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="schema"></param>
		/// <param name="credentials"></param>
		/// <returns></returns>
		public Guid SelectEntityId(string schema, Dictionary<string, object> credentials)
		{
			if (credentials.Count == 0) return Guid.Empty;
			Select select = new Select(_userConnection)
				.Top(1)
				.Column("Id")
				.From(schema) as Select;
			foreach (KeyValuePair<string, object> key in credentials)
			{
				var indexOf = credentials.Keys.ToList().IndexOf(key.Key);
				object value = null;
				if (key.Value is DateTime) value = ((DateTime)key.Value).ToUniversalTime();
				else value = key.Value;
				QueryColumnExpression columnExpression = (key.Value != null) ? Column.Parameter(value) : Column.Const(null);
				if (indexOf == 0)
					select.Where(key.Key).IsEqual(columnExpression);
				else
					select.And(key.Key).IsEqual(columnExpression);
			}
			return select.ExecuteScalar<Guid>();
		}

		/// <summary>
		/// Обновить сущность
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="id"></param>
		/// <param name="fieldsNameFieldsValue"></param>
		public void UpdateEntity(string entityName, Guid id,
			Dictionary<string, object> fieldsNameFieldsValue, bool useAdminRights = false, bool validate = true)
		{
			var entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(entityName);
			var entity = entitySchema.CreateEntity(_userConnection);
			entity.UseAdminRights = useAdminRights;
			if (entity.FetchFromDB(id, false))
			{
				try
				{
					foreach (var FildNameFildValue in fieldsNameFieldsValue)
					{
						if (FildNameFildValue.Value is string)
						{
							string fildNameFildValueValue = FildNameFildValue.Value.ToString();
							if (!string.IsNullOrEmpty(fildNameFildValueValue))
							{
								var textDataValueType =
									(TextDataValueType)
										entity.Schema.Columns.GetByColumnValueName(FildNameFildValue.Key).DataValueType;
								int size = textDataValueType.Size;
								if (!textDataValueType.IsMaxSize && (fildNameFildValueValue.Length > size))
								{
									fildNameFildValueValue = fildNameFildValueValue.Substring(0, size);
								}
								entity.SetColumnValue(FildNameFildValue.Key, fildNameFildValueValue);
							}
							else
							{
								entity.SetColumnValue(FildNameFildValue.Key, String.Empty);
							}
						}
						else if (FildNameFildValue.Value is DateTime)
						{
							if ((DateTime)FildNameFildValue.Value != DateTime.MinValue)
							{
								entity.SetColumnValue(FildNameFildValue.Key, FildNameFildValue.Value);
							}
						}
						else if (FildNameFildValue.Value is Guid)
						{
							if ((Guid)FildNameFildValue.Value != Guid.Empty)
							{
								entity.SetColumnValue(FildNameFildValue.Key, FildNameFildValue.Value);
							}
						}
						else if (FildNameFildValue.Value is byte[])
						{
							entity.SetBytesValue(FildNameFildValue.Key, (byte[])FildNameFildValue.Value);
						}
						else if (FildNameFildValue.Value != null)
						{
							entity.SetColumnValue(FildNameFildValue.Key, FildNameFildValue.Value);
						}
						else
						{
							if (FildNameFildValue.Value == null)
							{
								entity.SetColumnValue(FildNameFildValue.Key, null);
							}
						}
					}
					entity.Save(validate);
				}
				catch (Exception ex)
				{
					throw ex;
				}
			}
		}

		/// <summary>
		/// Добавить сущность
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="fieldValues"></param>
		/// <returns></returns>
		public Guid InsertEntity(string entityName,
			Dictionary<string, object> fieldValues, bool useAdminRights = false, bool validate = true)
		{
			Guid result = Guid.Empty;
			EntitySchema entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(entityName);
			Entity entity = entitySchema.CreateEntity(_userConnection);
			entity.UseAdminRights = useAdminRights;
			entity.SetDefColumnValues();
			try
			{
				foreach (var fieldKeyValue in fieldValues)
				{
					object value = fieldKeyValue.Value;
					string key = fieldKeyValue.Key;
					if (value is string)
					{
						string strValue = value.ToString();
						if (!string.IsNullOrEmpty(strValue))
						{
							var textDataValueType =
								(TextDataValueType)entity.Schema.Columns.GetByColumnValueName(key).DataValueType;
							int size = textDataValueType.Size;
							if (!textDataValueType.IsMaxSize && (strValue.Length > size))
							{
								strValue = strValue.Substring(0, size);
							}
							entity.SetColumnValue(key, strValue);
						}
					}
					else if (value is DateTime)
					{
						if ((DateTime)value != DateTime.MinValue)
						{
							entity.SetColumnValue(key, value);
						}
					}
					else if (value is byte[])
					{
						if ((byte[])value != new byte[0])
						{
							entity.SetBytesValue(key, (byte[])value);
						}
					}
					else if (value is Guid)
					{
						if ((Guid)value != Guid.Empty)
						{
							entity.SetColumnValue(key, value);
						}
					}
					else
					{
						if (value != null)
						{
							entity.SetColumnValue(key, value);
						}
					}
				}
				entity.Save(validate);
				result = entity.PrimaryColumnValue;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return result;
		}

		/// <summary>
		/// Получить сущность
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		public Entity ReadEntity(string entityName, Guid id)
		{
			var entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(entityName);
			var entity = entitySchema.CreateEntity(_userConnection);
			if (entity.FetchFromDB(id))
			{
				return entity;
			}
			return null;
		}

		/// <summary>
		/// Найти сущность
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		public Entity FindEntity(string entityName, string conditionName, object conditionValue, string[] columnsToFetch = null)
		{
			var entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(entityName);
			var entity = entitySchema.CreateEntity(_userConnection);
			if (columnsToFetch != null)
			{
				if (entity.FetchFromDB(conditionName, conditionValue, columnsToFetch))
				{
					return entity;
				}
			}
			else
			{
				if (entity.FetchFromDB(conditionName, conditionValue))
				{
					return entity;
				}
			}
			return null;
		}

		/// <summary>
		/// Найти сущность
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		public EntityCollection FindEntities(string entityName, Dictionary<string, object> conditions = null, int rowsCount = 10000, string[] columnsToFetch = null, bool useAdminRights = false)
		{
			EntityCollection collection = new EntityCollection(_userConnection, entityName);
			EntityCollection subCollection = new EntityCollection(_userConnection, entityName);
			int skipRowCount = 0;
			do
			{
				EntitySchemaQuery esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, entityName);
				esq.UseAdminRights = useAdminRights;
				esq.PrimaryQueryColumn.IsAlwaysSelect = true;
				esq.RowCount = rowsCount + skipRowCount;
				esq.SkipRowCount = skipRowCount;
				if (columnsToFetch != null) columnsToFetch.ForEach(x => esq.AddColumn(x));
				else esq.AddAllSchemaColumns();
				if (conditions != null)
				{
					conditions.Where(v => v.Value != null).ForEach(x => esq.Filters.Add(esq.CreateFilterWithParameters(FilterComparisonType.Equal, x.Key, x.Value)));
					conditions.Where(v => v.Value == null).ForEach(x => esq.Filters.Add(esq.CreateIsNullFilter(x.Key)));
				}
				subCollection = esq.GetEntityCollection(_userConnection);
				collection.AddRange(subCollection);
				skipRowCount += rowsCount;
			}
			while (subCollection.Count > 0);
			return collection;
		}

		public List<Guid> FindRecords(string entityName, Dictionary<string, object> conditions = null)
		{
			List<Guid> list = new List<Guid>();
			Select select = new Select(_userConnection)
				.Column("Id")
				.From(entityName) as Select;
			if (conditions != null)
			{
				foreach (var condition in conditions)
				{
					var indexOf = conditions.ToList().IndexOf(condition);
					if (indexOf == 0)
						select.Where($"{condition.Key}").IsEqual(Column.Parameter(condition.Value));
					else
						select.And($"{condition.Key}").IsEqual(Column.Parameter(condition.Value));
				}
			}
			using (DBExecutor dbExecutor = _userConnection.EnsureDBConnection())
			{
				using (IDataReader dataReader = select.ExecuteReader(dbExecutor))
				{
					while (dataReader.Read())
					{
						list.Add(dataReader.GetColumnValue<Guid>("Id"));
					}
				}
			}
			return list;
		}

		public List<Guid> FindRecords(string entityName, string filterColumm, List<Guid> records, string[] columnsToFetch = null)
		{
			List<Guid> list = new List<Guid>();
			Select select = new Select(_userConnection)
				.Column("Id")
				.From(entityName) as Select;
			foreach (Guid key in records)
			{
				var indexOf = records.ToList().IndexOf(key);
				if (indexOf == 0)
					select.Where($"{filterColumm}Id").IsEqual(Column.Parameter(key));
				else
					select.And($"{filterColumm}Id").IsEqual(Column.Parameter(key));
			}
			using (DBExecutor dbExecutor = _userConnection.EnsureDBConnection())
			{
				using (IDataReader dataReader = select.ExecuteReader(dbExecutor))
				{
					while (dataReader.Read())
					{
						list.Add(dataReader.GetColumnValue<Guid>("Id"));
					}
				}
			}
			return list;
		}

		/// <summary>
		/// Найти Id сущности
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		public Guid FindEntityId(string entityName, string conditionName, object conditionValue, string[] columnsToFetch)
		{
			Guid recordId = Guid.Empty;
			var entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(entityName);
			var entity = entitySchema.CreateEntity(_userConnection);
			if (entity.FetchFromDB(conditionName, conditionValue, columnsToFetch))
			{
				recordId = entity.GetTypedColumnValue<Guid>("Id");
			}
			return recordId;
		}

		/// <summary>
		/// Добавить запись
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="fieldValues"></param>
		/// <returns></returns>
		public Guid InsertRecord(string entityName,
			Dictionary<string, object> fieldValues)
		{
			Guid result = Guid.NewGuid();
			EntitySchema entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(entityName);
			Entity entity = entitySchema.CreateEntity(_userConnection);
			try
			{
				var insert = new Insert(_userConnection)
					.Into(entityName);
				if (!fieldValues.ContainsKey("Id")) insert.Set("Id", Column.Parameter(result));
				else result = (Guid)fieldValues.First(x => x.Key == "Id").Value;
				foreach (var fieldValue in fieldValues)
				{
					object value = fieldValue.Value;
					string key = fieldValue.Key;
					if (value is string)
					{
						string strValue = value.ToString();
						if (!string.IsNullOrEmpty(strValue))
						{
							var textDataValueType =
								(TextDataValueType)entity.Schema.Columns.GetByColumnValueName(key).DataValueType;
							int size = textDataValueType.Size;
							if (!textDataValueType.IsMaxSize && (strValue.Length > size))
							{
								strValue = strValue.Substring(0, size);
							}
							insert.Set(fieldValue.Key, Column.Parameter(strValue));
						}
					}
					else if (value is DateTime)
					{
						if ((DateTime)value != DateTime.MinValue)
						{
							insert.Set(fieldValue.Key, Column.Parameter(fieldValue.Value));
						}
					}
					else if (value is Guid)
					{
						if ((Guid)value != Guid.Empty)
						{
							insert.Set(fieldValue.Key, Column.Parameter(fieldValue.Value));
						}
					}
					else
					{
						if (value != null)
						{
							insert.Set(fieldValue.Key, Column.Parameter(fieldValue.Value));
						}
					}
				}
				insert.Execute();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return result;
		}

		/// <summary>
		/// Обновить запись
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="primaryColumnValue"></param>
		/// <param name="fieldValues"></param>
		/// <returns></returns>
		public int UpdateRecord(string entityName, Guid primaryColumnValue,
			Dictionary<string, object> fieldValues)
		{
			try
			{
				EntitySchema entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(entityName);
				Entity entity = entitySchema.CreateEntity(_userConnection);
				var update = new Update(_userConnection, entityName) as Update;
				foreach (var fieldValue in fieldValues)
				{
					object value = fieldValue.Value;
					string key = fieldValue.Key;
					if (value is string)
					{
						string strValue = value.ToString();
						if (!string.IsNullOrEmpty(strValue))
						{
							var textDataValueType =
								(TextDataValueType)entity.Schema.Columns.GetByColumnValueName(key).DataValueType;
							int size = textDataValueType.Size;
							if (!textDataValueType.IsMaxSize && (strValue.Length > size))
							{
								strValue = strValue.Substring(0, size);
							}
							update.Set(fieldValue.Key, Column.Parameter(strValue));
						}
					}
					else if (value is DateTime)
					{
						if ((DateTime)value != DateTime.MinValue)
						{
							update.Set(fieldValue.Key, Column.Parameter(fieldValue.Value));
						}
					}
					else if (value is Guid)
					{
						if ((Guid)value != Guid.Empty)
						{
							update.Set(fieldValue.Key, Column.Parameter(fieldValue.Value));
						}
					}
					else
					{
						if (value != null)
						{
							update.Set(fieldValue.Key, Column.Parameter(fieldValue.Value));
						}
						else update.Set(fieldValue.Key, Column.Const(null));
					}
				}
				update.Where("Id").IsEqual(Column.Parameter(primaryColumnValue));
				return update.Execute();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Обновить запись
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="conditions"></param>
		/// <param name="fieldValues"></param>
		/// <returns></returns>
		public int UpdateRecord(string entityName, Dictionary<string, object> conditions,
			Dictionary<string, object> fieldValues)
		{
			try
			{
				EntitySchema entitySchema = _userConnection.EntitySchemaManager.GetInstanceByName(entityName);
				Entity entity = entitySchema.CreateEntity(_userConnection);
				var update = new Update(_userConnection, entityName) as Update;
				foreach (var fieldValue in fieldValues)
				{
					object value = fieldValue.Value;
					string key = fieldValue.Key;
					if (value is string)
					{
						string strValue = value.ToString();
						if (!string.IsNullOrEmpty(strValue))
						{
							var textDataValueType =
								(TextDataValueType)entity.Schema.Columns.GetByColumnValueName(key).DataValueType;
							int size = textDataValueType.Size;
							if (!textDataValueType.IsMaxSize && (strValue.Length > size))
							{
								strValue = strValue.Substring(0, size);
							}
							update.Set(fieldValue.Key, Column.Parameter(strValue));
						}
					}
					else if (value is DateTime)
					{
						if ((DateTime)value != DateTime.MinValue)
						{
							update.Set(fieldValue.Key, Column.Parameter(fieldValue.Value));
						}
					}
					else if (value is Guid)
					{
						if ((Guid)value != Guid.Empty)
						{
							update.Set(fieldValue.Key, Column.Parameter(fieldValue.Value));
						}
					}
					else
					{
						if (value != null)
						{
							update.Set(fieldValue.Key, Column.Parameter(fieldValue.Value));
						}
					}
				}
				foreach (KeyValuePair<string, object> key in conditions)
				{
					var indexOf = conditions.Keys.ToList().IndexOf(key.Key);
					if (indexOf == 0)
						update.Where(key.Key).IsEqual(Column.Parameter(key.Value));
					else
						update.And(key.Key).IsEqual(Column.Parameter(key.Value));
				}
				return update.Execute();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Удалить запись
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName"></param>
		/// <param name="credentials"></param>
		/// <returns></returns>
		public int DeleteRecord(string entityName, Dictionary<string, object> credentials)
		{
			try
			{
				var delete = new Delete(_userConnection)
					.From(entityName) as Delete;
				foreach (KeyValuePair<string, object> key in credentials)
				{
					var indexOf = credentials.Keys.ToList().IndexOf(key.Key);
					if (indexOf == 0)
						delete.Where(key.Key).IsEqual(Column.Parameter(key.Value));
					else
						delete.And(key.Key).IsEqual(Column.Parameter(key.Value));
				}
				return delete.Execute();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void DeleteEntity(string entityName, Dictionary<string, object> credentials)
		{
			try
			{
				EntityCollection entityCollection = FindEntities(entityName, credentials);
				foreach (var entityItem in entityCollection)
				{
					entityItem.Delete();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Вставить запись если ее не было
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName">Название схемы</param>
		/// <param name="conditionValues">Поля условия поиска</param>
		/// <param name="fieldValues">Поля заполнения</param>
		public Guid InsertIfNotRecord(string entityName, Dictionary<string, object> conditionValues,
			Dictionary<string, object> fieldValues)
		{
			var resultId = SelectEntityId(entityName, conditionValues);
			if (resultId == Guid.Empty)
				resultId = InsertRecord(entityName, fieldValues);
			return resultId;
		}

		/// <summary>
		/// Вставить запись если ее не было
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName">Название схемы</param>
		/// <param name="conditionValues">Поля условия поиска</param>
		/// <param name="fieldValues">Поля заполнения</param>
		public Guid InsertIfNotEntity(string entityName, Dictionary<string, object> conditionValues,
			Dictionary<string, object> fieldValues, bool useAdminRights = false, bool validate = true)
		{
			var resultId = SelectEntityId(entityName, conditionValues);
			if (resultId == Guid.Empty)
				resultId = InsertEntity(entityName, fieldValues, useAdminRights, validate);
			return resultId;
		}

		/// <summary>
		/// Обновить запись если найдена запись
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName">Название схемы</param>
		/// <param name="conditionValues">Поля условия поиска</param>
		/// <param name="fieldValues">Поля заполнения</param>
		public Guid UpdateIfEntity(string entityName, Dictionary<string, object> conditionValues,
			Dictionary<string, object> fieldValues, bool useAdminRights = false, bool validate = true)
		{
			var resultId = SelectEntityId(entityName, conditionValues);
			if (resultId != Guid.Empty)
				UpdateEntity(entityName, resultId, fieldValues, useAdminRights, validate);
			return resultId;
		}

		/// <summary>
		/// Вставить запись если ее не было, иначе - обновить
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName">Название схемы</param>
		/// <param name="conditionValues">Поля условия поиска</param>
		/// <param name="fieldValues">Поля заполнения</param>
		public Guid InsertOrUpdateRecord(string entityName, Dictionary<string, object> conditionValues,
			Dictionary<string, object> fieldValues)
		{
			var resultId = SelectEntityId(entityName, conditionValues);
			if (resultId == Guid.Empty)
				resultId = InsertRecord(entityName, fieldValues);
			else UpdateRecord(entityName, resultId, fieldValues);
			return resultId;
		}

		/// <summary>
		/// Вставить запись если ее не было, иначе - обновить
		/// </summary>
		/// <param name="_userConnection"></param>
		/// <param name="entityName">Название схемы</param>
		/// <param name="conditionValues">Поля условия поиска</param>
		/// <param name="fieldValues">Поля заполнения</param>
		public Guid InsertOrUpdateEntity(string entityName, Dictionary<string, object> conditionValues,
			Dictionary<string, object> fieldValues, bool useAdminRights = false, bool validate = true)
		{
			var resultId = SelectEntityId(entityName, conditionValues);
			if (resultId == Guid.Empty)
				resultId = InsertEntity(entityName, fieldValues, useAdminRights, validate);
			else UpdateEntity(entityName, resultId, fieldValues, useAdminRights, validate);
			return resultId;
		}

		public bool IsValueChanged(object newValue, object oldValue)
		{
			if (newValue == null)
				return false;
			if (newValue is string)
			{
				if (!string.IsNullOrEmpty(newValue.ToString()) && (oldValue == null || newValue.ToString() != oldValue.ToString()))
				{
					return true;
				}
			}
			else if (newValue is DateTime)
			{
				if ((DateTime)newValue != DateTime.MinValue && newValue != oldValue)
				{
					return true;
				}
			}
			else if (newValue is Guid)
			{
				if ((Guid)newValue != Guid.Empty && newValue != oldValue)
				{
					return true;
				}
			}
			else if (newValue is Int32)
			{
				if ((Int32)newValue != 0 && newValue != oldValue)
				{
					return true;
				}
			}
			return false;
		}

		public Select GetAutoJoinSelect(string schemaName, Dictionary<string, string> columnsNamePath,
			Dictionary<string, object> equalFilterColumnValue)
		{
			var select = new Select(_userConnection) as Select;
			List<string> joinPath = new List<string>();
			foreach (var columnNamePath in columnsNamePath)
			{
				if (columnNamePath.Value.Contains(":"))
				{
					// example [CrocEvent:Id:CrocDocument:CrocEventId].Id
					string[] parametr = columnNamePath.Value.Split('.');
					string column = parametr[1];
					string[] path = parametr[0]
						.Replace("[", "")
						.Replace("]", "")
						.Split(':');
					if (!joinPath.Contains(parametr[0]))
					{
						joinPath.Add(parametr[0]);
					}
					select.Column(path[0], column).As(columnNamePath.Key);
				}
				else
				{
					// example CrocProjectId
					select.Column(schemaName, columnNamePath.Value).As(columnNamePath.Key);
				}
			}
			select.From(schemaName);
			foreach (string join in joinPath)
			{
				string[] path = join
					.Replace("[", "")
					.Replace("]", "")
					.Split(':');
				// example [CrocEvent:Id:CrocDocument:CrocEventId]
				string joinSchemaName = path[0];
				string joinColumnName = path[1];
				string baseEntityJoinSchemaName = path[2];
				string baseEntityJoinColumnName = path[3];
				select.LeftOuterJoin(joinSchemaName).On(joinSchemaName, joinColumnName)
					.IsEqual(baseEntityJoinSchemaName, baseEntityJoinColumnName);
			}
			bool isFirstFilter = true;
			foreach (var filter in equalFilterColumnValue)
			{
				if (isFirstFilter)
				{
					isFirstFilter = false;
					select.Where(schemaName, filter.Key).IsEqual(Column.Const(filter.Value));
				}
				else
				{
					select.And(schemaName, filter.Key).IsEqual(Column.Const(filter.Value));
				}
			}

			return select;
		}

		#endregion
	}
}