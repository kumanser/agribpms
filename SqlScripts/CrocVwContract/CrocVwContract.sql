DROP VIEW IF EXISTS public."CrocVwContract";
CREATE OR REPLACE VIEW public."CrocVwContract"
AS
	SELECT 
		c2."Id", CAST(NULL AS uuid) as "CreatedById", null as "CreatedOn", CAST(NULL AS uuid) as "ModifiedById", null as "ModifiedOn", c2."Name" as "CrocName", 0 as "ProcessListeners",
		c2."CrocINN" as "CrocINN", 
		ss."CrocQuantityPlanTon" as "CrocQuantityPlanTon",
		c2."Id" as "CrocAccountId", 
		CAST(NULL AS uuid) as "CrocContractId", 
		CAST(NULL AS uuid) as "CrocSubContractId", 
		CAST(NULL AS uuid) as "CrocParentId",
		'' as "CrocContractStatusName",
		CAST(NULL AS date) as "CrocStartDate",
		CAST(NULL AS date) as "CrocEndDate",
		'' as "CrocContractKindName",
		CAST(NULL AS uuid) as "CrocAssignedManagerId",
		CAST(NULL AS uuid) as "CrocAssignedSTOId"
	FROM "Account" c2
	LEFT JOIN (
		SELECT cc2."CrocAccountId", sum(cc1."CrocQuantityPlanTon") as "CrocQuantityPlanTon" FROM "CrocContract" cc1
		LEFT JOIN "CrocContract" cc2 ON cc1."CrocParentContractId" = cc2."Id"
		GROUP BY cc2."CrocAccountId"
		HAVING cc2."CrocAccountId" is not null
	) ss ON ss."CrocAccountId" = c2."Id"
	UNION ALL
	SELECT 
		c1."Id", CAST(NULL AS uuid) as "CreatedById", null as "CreatedOn", CAST(NULL AS uuid) as "ModifiedById", null as "ModifiedOn", c1."CrocContractNumber" as "CrocName", 0 as "ProcessListeners",
		'' as "CrocINN", 
		ss."CrocQuantityPlanTon" as "CrocQuantityPlanTon",
		c1."CrocAccountId" as "CrocAccountId", 
		c1."Id" as "CrocContractId", 
		CAST(NULL AS uuid) as "CrocSubContractId", 
		c1."CrocAccountId" as "CrocParentId",
		cs."Name" as "CrocContractStatusName",
		c1."CrocStartDate" as "CrocStartDate",
		c1."CrocEndDate" as "CrocEndDate",
		ck."Name" as "CrocContractKindName",
		c1."CrocAssignedManagerId" as "CrocAssignedManagerId",
		c1."CrocAssignedSTOId" as "CrocAssignedSTOId"
	FROM "CrocContract" c1
	LEFT JOIN "Account" a ON c1."CrocAccountId" = a."Id"
	LEFT JOIN (SELECT "CrocParentContractId", sum("CrocQuantityPlanTon") as "CrocQuantityPlanTon" FROM "CrocContract" GROUP BY "CrocParentContractId") ss ON ss."CrocParentContractId" = c1."Id"
	LEFT JOIN "CrocContractStatus" cs ON c1."CrocContractStatusId" = cs."Id"
	LEFT JOIN "CrocContractKind" ck ON c1."CrocContractKindId" = ck."Id"
	WHERE c1."CrocContractTypeId" = '0bd97518-7ae3-4703-b8bb-6275e5f44af1'
	UNION ALL
	SELECT 
		c2."Id", CAST(NULL AS uuid) as "CreatedById", null as "CreatedOn", CAST(NULL AS uuid) as "ModifiedById", null as "ModifiedOn", c2."CrocContractNumber" as "CrocName", 0 as "ProcessListeners",
		'' as "CrocINN", 
		c2."CrocQuantityPlanTon" as "CrocQuantityPlanTon",
		c2."CrocAccountId" as "CrocAccountId", 
		c2."CrocParentContractId" as "CrocContractId", 
		c2."Id" as "CrocSubContractId", 
		c2."CrocParentContractId" as "CrocParentId",
		cs."Name" as "CrocContractStatusName",
		c2."CrocStartDate" as "CrocStartDate",
		c2."CrocEndDate" as "CrocEndDate",
		ck."Name" as "CrocContractKindName",
		c2."CrocAssignedManagerId" as "CrocAssignedManagerId",
		c2."CrocAssignedSTOId" as "CrocAssignedSTOId"
	FROM "CrocContract" c2
	LEFT JOIN "CrocContractStatus" cs ON c2."CrocContractStatusId" = cs."Id"
	LEFT JOIN "CrocContractKind" ck ON c2."CrocContractKindId" = ck."Id"
	WHERE c2."CrocContractTypeId" = 'caa0e90b-d1b6-4fde-beb1-8762c82181bb'