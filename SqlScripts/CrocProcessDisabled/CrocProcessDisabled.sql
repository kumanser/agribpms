delete from "SysProcessDisabled"
where "SysSchemaId" in (SELECT "Id"
	FROM "SysSchema"
	WHERE "Name" IN ('CrocProcessSendEmailErrorsNotifications')
);
INSERT INTO "SysProcessDisabled" ("Name", "SysSchemaId")
SELECT "Name","Id"
FROM "SysSchema"
WHERE "Name" IN ('CrocProcessSendEmailErrorsNotifications');